using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory.Base;
using Microsoft.AspNet.Identity;
using System;

namespace DotFramework.Auth.ServiceFactory
{
    public interface IApplicationRoleService : IRoleStore<Role, Int32>, IAuthServiceBase
    {
    }
}
