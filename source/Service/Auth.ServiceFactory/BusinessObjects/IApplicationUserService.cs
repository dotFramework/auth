using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory.Base;
using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;

namespace DotFramework.Auth.ServiceFactory
{
    public interface IApplicationUserService : IAuthServiceBase, IUserLoginStore<User, Int32>, IUserClaimStore<User, Int32>, IUserRoleStore<User, Int32>, IUserPasswordStore<User, Int32>, IUserSecurityStampStore<User, Int32>, IQueryableUserStore<User, Int32>, IUserEmailStore<User, Int32>, IUserPhoneNumberStore<User, Int32>, IUserTwoFactorStore<User, Int32>, ICustomUserLockoutStore<User, Int32>, IUserStore<User, Int32>
    {

    }

    public interface ICustomUserLockoutStore<TUser, in TKey> : IUserLockoutStore<User, Int32> where TUser : class, IUser<TKey>
    {
        Task<Byte> GetLockoutTypeAsync(TUser user);
        Task SetLockoutTypeAsync(TUser user, Byte lockoutType);
    }
}
