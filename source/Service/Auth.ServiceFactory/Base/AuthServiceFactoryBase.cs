using DotFramework.Infra.ServiceFactory.Autofac;

namespace DotFramework.Auth.ServiceFactory.Base
{
    public abstract class AuthServiceFactoryBase<TServiceFacory> : SecureServiceFactoryBase<TServiceFacory> where TServiceFacory : class
    {
    }
}