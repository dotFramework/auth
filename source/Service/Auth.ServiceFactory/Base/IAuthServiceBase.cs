using DotFramework.Infra.Model;
using DotFramework.Infra.ServiceFactory;

namespace DotFramework.Auth.ServiceFactory.Base
{
    public interface IAuthServiceBase<TKey, TModel, TModelCollection> : ISecureServiceBase<TKey, TModel, TModelCollection>, IAuthServiceBase
        where TModel : SecureDomainModelBase
        where TModelCollection : ListBase<TKey, TModel>
    {
    }

    public interface IAuthServiceBase : ISecureServiceBase
    {
    }
}