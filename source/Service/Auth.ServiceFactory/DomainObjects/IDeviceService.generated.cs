using System;
using System.Threading.Tasks;
using System.ServiceModel;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IDeviceService : IAuthServiceBase<Int32, Device, DeviceCollection>
    {

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Device GetByKeySimple(String DeviceIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<Device> GetByKeySimpleAsync(String DeviceIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Device GetByKey(String DeviceIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<Device> GetByKeyAsync(String DeviceIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<Device> EditByKey(Device Device, String DeviceIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		Task<OperationResult<Device>> EditByKeyAsync(Device Device, String DeviceIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(String DeviceIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		Task<OperationResult> RemoveByKeyAsync(String DeviceIdentifier);
    }
}