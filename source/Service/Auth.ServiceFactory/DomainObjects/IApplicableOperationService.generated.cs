using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IApplicableOperationService : IAuthServiceBase<Int32, ApplicableOperation, ApplicableOperationCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicableOperationCollection GetByApplicationEndpointSimple(Int32 ApplicationEndpointID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicableOperationCollection GetByApplicationEndpoint(Int32 ApplicationEndpointID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByApplicationEndpoint(Int32 ApplicationEndpointID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicableOperationCollection GetByServiceOperationSimple(Int32 ServiceOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicableOperationCollection GetByServiceOperation(Int32 ServiceOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByServiceOperation(Int32 ServiceOperationID);


		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ApplicableOperation GetByKeySimple(Int32 ApplicationEndpointID, Int32 ServiceOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ApplicableOperation GetByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<ApplicableOperation> EditByKey(ApplicableOperation ApplicableOperation, Int32 ApplicationEndpointID, Int32 ServiceOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID);
    }
}