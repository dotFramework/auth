using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IRoleService : IAuthServiceBase<Int32, Role, RoleCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleCollection GetByApplicationSimple(Int32 ApplicationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleCollection GetByApplication(Int32 ApplicationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByApplication(Int32 ApplicationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleCollection GetByDesignationSimple(Int16 DesignationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleCollection GetByDesignation(Int16 DesignationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByDesignation(Int16 DesignationID);


		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Role GetByKeySimple(Int32 ApplicationID, String Name);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Role GetByKey(Int32 ApplicationID, String Name);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<Role> EditByKey(Role Role, Int32 ApplicationID, String Name);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 ApplicationID, String Name);
    }
}