using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IFavoriteViewService : IAuthServiceBase<Int32, FavoriteView, FavoriteViewCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		FavoriteViewCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		FavoriteViewCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		FavoriteViewCollection GetByUserSimple(Int32 UserID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		FavoriteViewCollection GetByUser(Int32 UserID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByUser(Int32 UserID);


		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		FavoriteView GetByKeySimple(Int32 ApplicationHierarchyID, Int32 UserID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		FavoriteView GetByKey(Int32 ApplicationHierarchyID, Int32 UserID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<FavoriteView> EditByKey(FavoriteView FavoriteView, Int32 ApplicationHierarchyID, Int32 UserID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 ApplicationHierarchyID, Int32 UserID);
    }
}