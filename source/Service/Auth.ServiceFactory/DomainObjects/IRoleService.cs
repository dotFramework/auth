using System;
using DotFramework.Auth.Model;
using System.ServiceModel;

namespace DotFramework.Auth.ServiceFactory
{
    public partial interface IRoleService
    {
        [OperationContract]
        Role GetByName(String RoleName);
    }
}