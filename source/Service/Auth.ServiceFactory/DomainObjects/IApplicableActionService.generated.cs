using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IApplicableActionService : IAuthServiceBase<Int32, ApplicableAction, ApplicableActionCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicableActionCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicableActionCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicableActionCollection GetByViewActionSimple(Int32 ViewActionID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicableActionCollection GetByViewAction(Int32 ViewActionID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByViewAction(Int32 ViewActionID);


		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ApplicableAction GetByKeySimple(Int32 ApplicationHierarchyID, Int32 ViewActionID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ApplicableAction GetByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<ApplicableAction> EditByKey(ApplicableAction ApplicableAction, Int32 ApplicationHierarchyID, Int32 ViewActionID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID);
    }
}