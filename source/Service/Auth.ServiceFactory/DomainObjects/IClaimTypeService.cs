using System;
using DotFramework.Auth.Model;
using System.ServiceModel;

namespace DotFramework.Auth.ServiceFactory
{
    public partial interface IClaimTypeService
    {
        [OperationContract]
        ClaimType GetByName(String Name);

        [OperationContract]
        ClaimType GetBySchema(String Schema);
    }
}