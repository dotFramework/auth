using System;
using System.Threading.Tasks;
using System.ServiceModel;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IClientDeviceService : IAuthServiceBase<Int32, ClientDevice, ClientDeviceCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ClientDeviceCollection GetByDeviceSimple(Int32 DeviceID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		Task<ClientDeviceCollection> GetByDeviceSimpleAsync(Int32 DeviceID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ClientDeviceCollection GetByDevice(Int32 DeviceID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		Task<ClientDeviceCollection> GetByDeviceAsync(Int32 DeviceID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByDevice(Int32 DeviceID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Task<Int32> GetCountByDeviceAsync(Int32 DeviceID);
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ClientDeviceCollection GetByClientSimple(Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		Task<ClientDeviceCollection> GetByClientSimpleAsync(Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ClientDeviceCollection GetByClient(Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		Task<ClientDeviceCollection> GetByClientAsync(Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByClient(Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Task<Int32> GetCountByClientAsync(Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ClientDevice GetByKeySimple(Int32 DeviceID, Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<ClientDevice> GetByKeySimpleAsync(Int32 DeviceID, Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ClientDevice GetByKey(Int32 DeviceID, Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<ClientDevice> GetByKeyAsync(Int32 DeviceID, Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<ClientDevice> EditByKey(ClientDevice ClientDevice, Int32 DeviceID, Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		Task<OperationResult<ClientDevice>> EditByKeyAsync(ClientDevice ClientDevice, Int32 DeviceID, Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 DeviceID, Int32 ClientID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		Task<OperationResult> RemoveByKeyAsync(Int32 DeviceID, Int32 ClientID);
    }
}