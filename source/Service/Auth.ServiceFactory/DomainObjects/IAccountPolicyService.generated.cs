using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IAccountPolicyService : IAuthServiceBase<Byte, AccountPolicy, AccountPolicyCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		AccountPolicy GetByKeySimple(String PolicyName);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<AccountPolicy> GetByKeySimpleAsync(String PolicyName);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		AccountPolicy GetByKey(String PolicyName);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<AccountPolicy> GetByKeyAsync(String PolicyName);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<AccountPolicy> EditByKey(AccountPolicy AccountPolicy, String PolicyName);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		Task<OperationResult<AccountPolicy>> EditByKeyAsync(AccountPolicy AccountPolicy, String PolicyName);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(String PolicyName);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		Task<OperationResult> RemoveByKeyAsync(String PolicyName);

    }
}