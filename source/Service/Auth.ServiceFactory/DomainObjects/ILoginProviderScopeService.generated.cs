using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface ILoginProviderScopeService : IAuthServiceBase<Int32, LoginProviderScope, LoginProviderScopeCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		LoginProviderScopeCollection GetByLoginProviderOptionsSimple(Int32 LoginProviderOptionsID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		Task<LoginProviderScopeCollection> GetByLoginProviderOptionsSimpleAsync(Int32 LoginProviderOptionsID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		LoginProviderScopeCollection GetByLoginProviderOptions(Int32 LoginProviderOptionsID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		Task<LoginProviderScopeCollection> GetByLoginProviderOptionsAsync(Int32 LoginProviderOptionsID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByLoginProviderOptions(Int32 LoginProviderOptionsID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Task<Int32> GetCountByLoginProviderOptionsAsync(Int32 LoginProviderOptionsID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		LoginProviderScope GetByKeySimple(Int32 LoginProviderOptionsID, String Scope);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<LoginProviderScope> GetByKeySimpleAsync(Int32 LoginProviderOptionsID, String Scope);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		LoginProviderScope GetByKey(Int32 LoginProviderOptionsID, String Scope);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<LoginProviderScope> GetByKeyAsync(Int32 LoginProviderOptionsID, String Scope);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<LoginProviderScope> EditByKey(LoginProviderScope LoginProviderScope, Int32 LoginProviderOptionsID, String Scope);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		Task<OperationResult<LoginProviderScope>> EditByKeyAsync(LoginProviderScope LoginProviderScope, Int32 LoginProviderOptionsID, String Scope);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 LoginProviderOptionsID, String Scope);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		Task<OperationResult> RemoveByKeyAsync(Int32 LoginProviderOptionsID, String Scope);

    }
}