using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IApplicationHierarchyService : IAuthServiceBase<Int32, ApplicationHierarchy, ApplicationHierarchyCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicationHierarchyCollection GetByApplicationSimple(Int32 ApplicationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicationHierarchyCollection GetByApplication(Int32 ApplicationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByApplication(Int32 ApplicationID);


		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ApplicationHierarchy GetByKeySimple(Int32 ApplicationID, String Code);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ApplicationHierarchy GetByKey(Int32 ApplicationID, String Code);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<ApplicationHierarchy> EditByKey(ApplicationHierarchy ApplicationHierarchy, Int32 ApplicationID, String Code);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 ApplicationID, String Code);
    }
}