using System;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
    public partial interface IRefreshTokenService
    {
        [OperationContract]
        [FaultContract(typeof(ProcessExecutionFault))]
        [OperationType("Get")]
        RefreshToken GetByToken(String Token);

        [OperationContract]
        [FaultContract(typeof(ProcessExecutionFault))]
        [OperationType("Add")]
        OperationResult<RefreshToken> RemoveAndAdd(RefreshToken RefreshToken);

        [OperationContract]
        [FaultContract(typeof(ProcessExecutionFault))]
        [OperationType("GetCollection")]
        RefreshTokenCollection GetByUserAndGroupTokenSimple(Int32 UserID, string GroupToken);

        [OperationType("Remove")]
        OperationResult RemoveByUserAndGroupToken(Int32 UserID, string GroupToken);

        [OperationType("Remove")]
        OperationResult RemoveByGroupToken(string GroupToken);

        [OperationContract]
        [FaultContract(typeof(ProcessExecutionFault))]
        [OperationType("Add")]
        OperationResult<RefreshToken> AddByUserAndGroupToken(RefreshToken model);
    }
}