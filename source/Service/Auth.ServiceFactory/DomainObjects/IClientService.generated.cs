using System;
using System.Threading.Tasks;
using System.ServiceModel;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IClientService : IAuthServiceBase<Int32, Client, ClientCollection>
    {

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Client GetByKeySimple(String ClientIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<Client> GetByKeySimpleAsync(String ClientIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Client GetByKey(String ClientIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Task<Client> GetByKeyAsync(String ClientIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<Client> EditByKey(Client Client, String ClientIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		Task<OperationResult<Client>> EditByKeyAsync(Client Client, String ClientIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(String ClientIdentifier);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		Task<OperationResult> RemoveByKeyAsync(String ClientIdentifier);
    }
}