using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IApplicationEndpointService : IAuthServiceBase<Int32, ApplicationEndpoint, ApplicationEndpointCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicationEndpointCollection GetByApplicationSimple(Int32 ApplicationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		ApplicationEndpointCollection GetByApplication(Int32 ApplicationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByApplication(Int32 ApplicationID);


		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ApplicationEndpoint GetByKeySimple(Int32 ApplicationID, String EndpointType);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		ApplicationEndpoint GetByKey(Int32 ApplicationID, String EndpointType);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<ApplicationEndpoint> EditByKey(ApplicationEndpoint ApplicationEndpoint, Int32 ApplicationID, String EndpointType);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 ApplicationID, String EndpointType);
    }
}