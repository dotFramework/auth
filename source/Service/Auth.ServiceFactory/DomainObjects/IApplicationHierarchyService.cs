using DotFramework.Auth.Model;
using System.ServiceModel;

namespace DotFramework.Auth.ServiceFactory
{
    public partial interface IApplicationHierarchyService
    {
        [OperationContract]
        ApplicationHierarchy GetByActionName(string applicationCode, string controllerName, string actionName);
    }
}