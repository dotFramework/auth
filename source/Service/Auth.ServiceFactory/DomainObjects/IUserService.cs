using System;
using DotFramework.Auth.Model;
using System.ServiceModel;

namespace DotFramework.Auth.ServiceFactory
{
    public partial interface IUserService
    {
        [OperationContract]
        User GetByUserName(String UserName);

        [OperationContract]
        User GetByEmail(String Email);
    }
}