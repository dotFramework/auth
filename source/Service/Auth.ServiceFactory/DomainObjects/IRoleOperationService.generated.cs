using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IRoleOperationService : IAuthServiceBase<Int32, RoleOperation, RoleOperationCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleOperationCollection GetByRoleSimple(Int32 RoleID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleOperationCollection GetByRole(Int32 RoleID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByRole(Int32 RoleID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleOperationCollection GetByApplicableOperationSimple(Int32 ApplicableOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleOperationCollection GetByApplicableOperation(Int32 ApplicableOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByApplicableOperation(Int32 ApplicableOperationID);


		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		RoleOperation GetByKeySimple(Int32 RoleID, Int32 ApplicableOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		RoleOperation GetByKey(Int32 RoleID, Int32 ApplicableOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<RoleOperation> EditByKey(RoleOperation RoleOperation, Int32 RoleID, Int32 ApplicableOperationID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 RoleID, Int32 ApplicableOperationID);
    }
}