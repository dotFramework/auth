using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IUserLoginService : IAuthServiceBase<Int64, UserLogin, UserLoginCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		UserLoginCollection GetByUserSimple(Int32 UserID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		UserLoginCollection GetByUser(Int32 UserID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByUser(Int32 UserID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		UserLoginCollection GetByLoginProviderOptionsSimple(Int32 LoginProviderOptionsID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		UserLoginCollection GetByLoginProviderOptions(Int32 LoginProviderOptionsID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByLoginProviderOptions(Int32 LoginProviderOptionsID);


		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		UserLogin GetByKeySimple(Int32 LoginProviderOptionsID, String ProviderKey);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		UserLogin GetByKey(Int32 LoginProviderOptionsID, String ProviderKey);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<UserLogin> EditByKey(UserLogin UserLogin, Int32 LoginProviderOptionsID, String ProviderKey);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 LoginProviderOptionsID, String ProviderKey);
    }
}