using DotFramework.Auth.Model;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Infra.ServiceFactory;
using System;
using System.ServiceModel;

namespace DotFramework.Auth.ServiceFactory
{
    public partial interface IClientService
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Client GetByClientUID(string clientUid);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		Client GetByClientUIDAndSecret(string clientUid, string clientSecret);
	}
}