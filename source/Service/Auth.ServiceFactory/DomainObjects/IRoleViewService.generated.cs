using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.ServiceFactory;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Auth.Model;
using System.ServiceModel;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.ServiceFactory
{
	[ServiceContract]
    public partial interface IRoleViewService : IAuthServiceBase<Int32, RoleView, RoleViewCollection>
    {
		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleViewCollection GetByRoleSimple(Int32 RoleID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleViewCollection GetByRole(Int32 RoleID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByRole(Int32 RoleID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleViewCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCollection")]
		RoleViewCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("GetCount")]
		Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID);


		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		RoleView GetByKeySimple(Int32 RoleID, Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Get")]
		RoleView GetByKey(Int32 RoleID, Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Edit")]
		OperationResult<RoleView> EditByKey(RoleView RoleView, Int32 RoleID, Int32 ApplicationHierarchyID);

		[OperationContract]
		[FaultContract(typeof(ProcessExecutionFault))]
		[OperationType("Remove")]
		OperationResult RemoveByKey(Int32 RoleID, Int32 ApplicationHierarchyID);
    }
}