using DotFramework.Auth.ServiceFactory.Base;

namespace DotFramework.Auth.ServiceFactory
{
    public class AuthServiceFactory : AuthServiceFactoryBase<AuthServiceFactory>
    {
		private AuthServiceFactory()
		{
			
		}
    }
}