using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class ApplicationRoleService : AuthServiceBase, IApplicationRoleService
    {
        #region IUserStore

        public Task CreateAsync(Role role)
        {
            return Task.Run(() =>
            {
                try
                {
                    AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Add(role);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task DeleteAsync(Role role)
        {
            return Task.Run(() =>
            {
                try
                {
                    AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Remove(role.RoleID);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task<Role> FindByIdAsync(int roleId)
        {
            return Task.Run<Role>(() =>
            {
                try
                {
                    return AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Get(roleId);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task<Role> FindByNameAsync(string roleName)
        {
            return Task.Run<Role>(() =>
            {
                try
                {
                    return AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().GetByName(roleName);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task UpdateAsync(Role role)
        {
            return Task.Run(() =>
            {
                try
                {
                    AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Edit(role);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public void Dispose()
        {
            
        }

        #endregion
    }
}