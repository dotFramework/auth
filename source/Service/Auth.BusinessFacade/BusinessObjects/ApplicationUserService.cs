using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class ApplicationUserService : AuthServiceBase, IApplicationUserService
    {
        private UserRules UserRules => AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>();
        private ApplicationRules ApplicationRules => AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>();
        private UserLoginRules UserLoginRules => AuthBusinessRulesFactory.Instance.GetBusinessRules<UserLoginRules>();
        private ClaimTypeRules ClaimTypeRules => AuthBusinessRulesFactory.Instance.GetBusinessRules<ClaimTypeRules>();
        private UserClaimRules UserClaimRules => AuthBusinessRulesFactory.Instance.GetBusinessRules<UserClaimRules>();
        private RoleRules RoleRules => AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>();
        private UserRoleRules UserRoleRules => AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRoleRules>();
        private PasswordHistoryRules PasswordHistoryRules => AuthBusinessRulesFactory.Instance.GetBusinessRules<PasswordHistoryRules>();
        private AccountPolicyRules AccountPolicyRules => AuthBusinessRulesFactory.Instance.GetBusinessRules<AccountPolicyRules>();

        #region IUserStore

        public Task CreateAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.State = ObjectState.Added;
                    UserRules.Save(user);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task DeleteAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    UserRules.Remove(user.UserID);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task<User> FindByIdAsync(Int32 userId)
        {
            return Task.Run<User>(() =>
            {
                try
                {
                    var user = UserRules.Get(userId);
                    return ReturnUser(user);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task<User> FindByNameAsync(String userName)
        {
            return Task.Run<User>(() =>
            {
                try
                {
                    var user = UserRules.GetByUserName(userName);
                    return ReturnUser(user);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task UpdateAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.State = ObjectState.Edited;
                    UserRules.Save(user);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        #endregion

        #region IUserLoginStore

        public Task AddLoginAsync(User user, UserLoginInfo login)
        {
            string applicationCode = login.LoginProvider.Split(':')[0];
            string provider = login.LoginProvider.Split(':')[1];

            return Task.Run(() =>
            {
                try
                {
                    var application = ApplicationRules.GetByKeySimple(applicationCode);

                    UserLoginRules.Add(new UserLogin
                    {
                        User = user,
                        LoginProviderOptions = new LoginProviderOptionsRules().GetByKey(application.ApplicationID, (byte)Enum.Parse(typeof(LoginProviderEnum), provider, true)),
                        ProviderKey = login.ProviderKey
                    });
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task<User> FindAsync(UserLoginInfo login)
        {
            string applicationCode = login.LoginProvider.Split(':')[0];
            string provider = login.LoginProvider.Split(':')[1];

            return Task.Run<User>(() =>
            {
                try
                {
                    var application = ApplicationRules.GetByKeySimple(applicationCode);
                    var loginProviderOptions = new LoginProviderOptionsRules().GetByKeySimple(application.ApplicationID, (byte)Enum.Parse(typeof(LoginProviderEnum), provider, true));

                    var userLogin = UserLoginRules.GetByKey(loginProviderOptions.LoginProviderOptionsID, login.ProviderKey);

                    if (userLogin.UserLoginID != 0)
                    {
                        return userLogin.User;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(User user)
        {
            return Task.Run<IList<UserLoginInfo>>(() =>
            {
                try
                {
                    var logins = UserLoginRules.GetByUser(user.UserID);

                    if (logins.Count != 0)
                    {
                        return logins.Select(ul => new UserLoginInfo(String.Format("{0}:{1}", ul.LoginProviderOptions.Application.Code, ul.LoginProviderOptions.LoginProvider.Code), ul.ProviderKey)).ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task RemoveLoginAsync(User user, UserLoginInfo login)
        {
            string applicationCode = login.LoginProvider.Split(':')[0];
            string provider = login.LoginProvider.Split(':')[1];

            return Task.Run(() =>
            {
                try
                {
                    var application = ApplicationRules.GetByKeySimple(applicationCode);
                    var loginProviderOptions = new LoginProviderOptionsRules().GetByKeySimple(application.ApplicationID, (byte)Enum.Parse(typeof(LoginProviderEnum), provider, true));

                    UserLoginRules.RemoveByKey(loginProviderOptions.LoginProviderOptionsID, login.ProviderKey);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        #endregion

        #region IUserClaimStore

        public Task AddClaimAsync(User user, Claim claim)
        {
            return Task.Run(() =>
            {
                try
                {
                    ClaimType claimType = ClaimTypeRules.GetBySchema(claim.Type);

                    if (claimType == null || !claimType.HasValue)
                    {
                        string claimCode = claim.Type.Substring(claim.Type.LastIndexOf('/') + 1);

                        claimType = new ClaimType { Code = claimCode, ClaimSchema = claim.Type, Name = claimCode };
                        ClaimTypeRules.Add(claimType);
                    }

                    UserClaimRules.Add(new UserClaim { User = user, ClaimType = claimType, ClaimValue = claim.Value });
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task<IList<Claim>> GetClaimsAsync(User user)
        {
            return Task.Run<IList<Claim>>(() =>
            {
                try
                {
                    UserClaimCollection userClaims = UserClaimRules.GetByUser(user.UserID);

                    if (userClaims.Count != 0)
                    {
                        return userClaims.Select(claim => new Claim(claim.ClaimType.ClaimSchema, claim.ClaimValue)).ToList();
                    }
                    else
                    {
                        return new List<Claim>();
                    }
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task RemoveClaimAsync(User user, Claim claim)
        {
            return Task.Run(() =>
            {
                try
                {
                    var userClaims = UserClaimRules.GetByUser(user.UserID).Where(c => c.ClaimType.ClaimSchema == claim.Type);

                    if (userClaims.Count() != 0)
                    {
                        UserClaimRules.Remove(userClaims.First().UserClaimID);
                    }
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        #endregion

        #region IUserRoleStore

        public Task AddToRoleAsync(User user, String roleName)
        {
            return Task.Run(() =>
            {
                try
                {
                    Role role = RoleRules.GetByName(roleName);

                    if (role == null || !role.HasValue)
                    {
                        role = new Role
                        {
                            Designation = new Designation { Code = roleName, Name = roleName, State = ObjectState.Added },
                            Code = roleName,
                            Name = roleName,
                            State = ObjectState.Added
                        };

                        RoleRules.AddWithDetail(role);
                    }

                    UserRoleRules.Add(new UserRole { User = user, Role = role });
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task<IList<String>> GetRolesAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    return UserRoleRules.GetByUser(user.UserID).Select(ur => String.Format("{0}:{1}", ur.Role.Application.Code, ur.Role.Name)).ToList() as IList<String>;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task<Boolean> IsInRoleAsync(User user, String roleName)
        {
            throw new NotImplementedException();
        }

        public Task RemoveFromRoleAsync(User user, String roleName)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IUserPasswordStore

        public Task<String> GetPasswordHashAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    var passwords = PasswordHistoryRules.GetByUserSimple(user.UserID);
                    return passwords.OrderByDescending(p => p.SetPasswordTime).First().PasswordHash;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task<Boolean> HasPasswordAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    return PasswordHistoryRules.GetCountByUser(user.UserID) != 0;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return false;
                }
            });
        }

        public Task SetPasswordHashAsync(User user, String passwordHash)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.PasswordHistoryCollection.Add(new PasswordHistory
                    {
                        PasswordHash = passwordHash,
                        SetPasswordTime = DateTime.Now,
                        State = ObjectState.Added
                    });
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        #endregion

        #region IUserSecurityStampStore

        public Task<String> GetSecurityStampAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    return String.IsNullOrEmpty(user.SecurityStamp) ? "" : user.SecurityStamp;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task SetSecurityStampAsync(User user, String stamp)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.SecurityStamp = stamp;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }

            });
        }

        #endregion

        #region IQueryableUserStore

        public IQueryable<User> Users
        {
            get
            {
                try
                {
                    return UserRules.GetAll().AsQueryable();
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            }
        }

        #endregion

        #region IUserEmailStore

        public Task<User> FindByEmailAsync(String email)
        {
            return Task.Run<User>(() =>
            {
                try
                {
                    var user = UserRules.GetByEmail(email);
                    return ReturnUser(user);

                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task<String> GetEmailAsync(User user)
        {
            return Task.Run<String>(() =>
            {
                try
                {
                    return String.IsNullOrEmpty(user.Email) ? "" : user.Email;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task<Boolean> GetEmailConfirmedAsync(User user)
        {
            return Task.Run<Boolean>(() =>
            {
                try
                {
                    return user.EmailConfirmed;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return false;
                }
            });
        }

        public Task SetEmailAsync(User user, String email)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.Email = email;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task SetEmailConfirmedAsync(User user, Boolean confirmed)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.EmailConfirmed = confirmed;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        #endregion

        #region IUserPhoneNumberStore

        public Task<String> GetPhoneNumberAsync(User user)
        {
            return Task.Run<String>(() =>
            {
                try
                {
                    return String.IsNullOrEmpty(user.PhoneNumber) ? "" : user.PhoneNumber;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return null;
                }
            });
        }

        public Task<Boolean> GetPhoneNumberConfirmedAsync(User user)
        {
            return Task.Run<Boolean>(() =>
            {
                try
                {
                    return (user.PhoneNumberConfirmed == null ? false : user.PhoneNumberConfirmed.Value);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return false;
                }
            });
        }

        public Task SetPhoneNumberAsync(User user, String phoneNumber)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.PhoneNumber = phoneNumber;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task SetPhoneNumberConfirmedAsync(User user, Boolean confirmed)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.PhoneNumberConfirmed = confirmed;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        #endregion

        #region IUserTwoFactorStore

        public Task<Boolean> GetTwoFactorEnabledAsync(User user)
        {
            return Task.Run<Boolean>(() =>
            {
                try
                {
                    return user.TwoFactorEnabled;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return false;
                }
            });
        }

        public Task SetTwoFactorEnabledAsync(User user, Boolean enabled)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.TwoFactorEnabled = enabled;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        #endregion

        #region ICustomUserLockoutStore

        public Task<Int32> GetAccessFailedCountAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    return user.AccessFailedCount;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return 0;
                }
            });
        }

        public Task<Boolean> GetLockoutEnabledAsync(User user)
        {
            return Task.Run<Boolean>(() =>
            {
                try
                {
                    var accountPolicy = AccountPolicyRules.Get(user.AccountPolicy.AccountPolicyID);
                    return accountPolicy.LockoutThreshold == 0 ? false : true;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return false;
                }
            });
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    return !user.LockoutEndDateUtc.HasValue ? default(DateTimeOffset) : new DateTimeOffset(DateTime.SpecifyKind(user.LockoutEndDateUtc.Value, DateTimeKind.Utc));
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return default(DateTimeOffset);
                }
            });
        }

        public Task<Int32> IncrementAccessFailedCountAsync(User user)
        {
            return Task.Run(() =>
            {
               try
               {
                   return user.AccessFailedCount++;
               }
               catch (Exception ex)
               {
                   if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                   {
                       throw ex;
                   }

                   return 0;
               }
           });
        }

        public Task ResetAccessFailedCountAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.AccessFailedCount = 0;
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task SetLockoutEnabledAsync(User user, Boolean enabled)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.LockoutEndDateUtc = ((lockoutEnd == DateTimeOffset.MinValue) ? null : new DateTime?(lockoutEnd.UtcDateTime));
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        public Task<byte> GetLockoutTypeAsync(User user)
        {
            return Task.Run(() =>
            {
                try
                {
                    return user.LockoutType.HasValue ? user.LockoutType.Value : default(byte);
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }

                    return (byte)0;
                }
            });
        }

        public Task SetLockoutTypeAsync(User user, byte lockoutType)
        {
            return Task.Run(() =>
            {
                try
                {
                    user.LockoutType = ((lockoutType == Byte.MinValue) ? null : new Byte?(lockoutType));
                }
                catch (Exception ex)
                {
                    if (BusinessRulesExceptionHandler.Instance.HandleException(ref ex, this))
                    {
                        throw ex;
                    }
                }
            });
        }

        #endregion

        #region Private Methods

        private User ReturnUser(User user)
        {
            if (user?.HasValue == true)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}