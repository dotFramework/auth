using DotFramework.Auth.BusinessRules;
using DotFramework.Auth.ServiceFactory.Base;
using DotFramework.Infra.BusinessFacade;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessFacade.Base
{
    public abstract class AuthServiceBase<TKey, TModel, TModelCollection, TBusinessRules, TDataAccess> : SecureServiceBase<TKey, TModel, TModelCollection, TBusinessRules, TDataAccess>, IAuthServiceBase<TKey, TModel, TModelCollection>
        where TModel : SecureDomainModelBase, new()
        where TModelCollection : ListBase<TKey, TModel>, new()
        where TBusinessRules : SecureRulesBase<TKey, TModel, TModelCollection, TDataAccess>, new()
        where TDataAccess : IDataAccessBase<TKey, TModel, TModelCollection>
    {
        public override IBusinessRulesFactory RulesFactory => AuthBusinessRulesFactory.Instance;
    }

    public abstract class AuthServiceBase : SecureServiceBase, IAuthServiceBase
    {
    }
}