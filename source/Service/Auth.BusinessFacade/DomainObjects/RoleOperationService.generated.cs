using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class RoleOperationService : AuthServiceBase<Int32, RoleOperation, RoleOperationCollection, RoleOperationRules, IRoleOperationDataAccess>, IRoleOperationService
    {		
		public virtual RoleOperationCollection GetByRoleSimple(Int32 RoleID)
        {
            return this.BusinessRules.GetByRoleSimple(RoleID);
        }	

		public virtual RoleOperationCollection GetByRole(Int32 RoleID)
        {
            return this.BusinessRules.GetByRole(RoleID);
        }
		
		public virtual Int32 GetCountByRole(Int32 RoleID)
        {
            return this.BusinessRules.GetCountByRole(RoleID);
        }

		public virtual RoleOperationCollection GetByApplicableOperationSimple(Int32 ApplicableOperationID)
        {
            return this.BusinessRules.GetByApplicableOperationSimple(ApplicableOperationID);
        }	

		public virtual RoleOperationCollection GetByApplicableOperation(Int32 ApplicableOperationID)
        {
            return this.BusinessRules.GetByApplicableOperation(ApplicableOperationID);
        }
		
		public virtual Int32 GetCountByApplicableOperation(Int32 ApplicableOperationID)
        {
            return this.BusinessRules.GetCountByApplicableOperation(ApplicableOperationID);
        }


		public virtual RoleOperation GetByKeySimple(Int32 RoleID, Int32 ApplicableOperationID)
		{
			return this.BusinessRules.GetByKeySimple(RoleID, ApplicableOperationID);
		}

		public virtual RoleOperation GetByKey(Int32 RoleID, Int32 ApplicableOperationID)
		{
			return this.BusinessRules.GetByKey(RoleID, ApplicableOperationID);
		}

		public virtual OperationResult<RoleOperation> EditByKey(RoleOperation RoleOperation, Int32 RoleID, Int32 ApplicableOperationID)
		{
			return this.BusinessRules.EditByKey(RoleOperation, RoleID, ApplicableOperationID);
		}

		public virtual OperationResult RemoveByKey(Int32 RoleID, Int32 ApplicableOperationID)
		{
			return this.BusinessRules.RemoveByKey(RoleID, ApplicableOperationID);
		}
    }
}