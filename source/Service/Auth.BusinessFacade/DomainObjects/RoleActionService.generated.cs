using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class RoleActionService : AuthServiceBase<Int32, RoleAction, RoleActionCollection, RoleActionRules, IRoleActionDataAccess>, IRoleActionService
    {		
		public virtual RoleActionCollection GetByRoleSimple(Int32 RoleID)
        {
            return this.BusinessRules.GetByRoleSimple(RoleID);
        }	

		public virtual RoleActionCollection GetByRole(Int32 RoleID)
        {
            return this.BusinessRules.GetByRole(RoleID);
        }
		
		public virtual Int32 GetCountByRole(Int32 RoleID)
        {
            return this.BusinessRules.GetCountByRole(RoleID);
        }

		public virtual RoleActionCollection GetByApplicableActionSimple(Int32 ApplicableActionID)
        {
            return this.BusinessRules.GetByApplicableActionSimple(ApplicableActionID);
        }	

		public virtual RoleActionCollection GetByApplicableAction(Int32 ApplicableActionID)
        {
            return this.BusinessRules.GetByApplicableAction(ApplicableActionID);
        }
		
		public virtual Int32 GetCountByApplicableAction(Int32 ApplicableActionID)
        {
            return this.BusinessRules.GetCountByApplicableAction(ApplicableActionID);
        }


		public virtual RoleAction GetByKeySimple(Int32 RoleID, Int32 ApplicableActionID)
		{
			return this.BusinessRules.GetByKeySimple(RoleID, ApplicableActionID);
		}

		public virtual RoleAction GetByKey(Int32 RoleID, Int32 ApplicableActionID)
		{
			return this.BusinessRules.GetByKey(RoleID, ApplicableActionID);
		}

		public virtual OperationResult<RoleAction> EditByKey(RoleAction RoleAction, Int32 RoleID, Int32 ApplicableActionID)
		{
			return this.BusinessRules.EditByKey(RoleAction, RoleID, ApplicableActionID);
		}

		public virtual OperationResult RemoveByKey(Int32 RoleID, Int32 ApplicableActionID)
		{
			return this.BusinessRules.RemoveByKey(RoleID, ApplicableActionID);
		}
    }
}