using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class RefreshTokenService : IRefreshTokenService
    {
        public RefreshToken GetByToken(String Token)
        {
            return this.BusinessRules.GetByToken(Token);
        }

        public OperationResult<RefreshToken> RemoveAndAdd(RefreshToken RefreshToken)
        {
            return this.BusinessRules.RemoveAndAdd(RefreshToken);
        }

        public RefreshTokenCollection GetByUserAndGroupTokenSimple(Int32 UserID, string GroupToken)
        {
            return this.BusinessRules.GetByUserAndGroupTokenSimple(UserID, GroupToken);
        }

        public OperationResult RemoveByUserAndGroupToken(Int32 UserID, string GroupToken)
        {
            return this.BusinessRules.RemoveByUserAndGroupToken(UserID, GroupToken);
        }

        public OperationResult RemoveByGroupToken(string GroupToken)
        {
            return this.BusinessRules.RemoveByGroupToken(GroupToken);
        }

        public OperationResult<RefreshToken> AddByUserAndGroupToken(RefreshToken model)
        {
            return this.BusinessRules.AddByUserAndGroupToken(model);
        }
    }
}