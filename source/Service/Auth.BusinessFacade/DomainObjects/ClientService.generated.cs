using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Auth.BusinessRules;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Infra.Model;
using System;
using System.Threading.Tasks;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class ClientService : AuthServiceBase<Int32, Client, ClientCollection, ClientRules, IClientDataAccess>, IClientService
    {		
		
		public virtual Client GetByKeySimple(String ClientIdentifier)
		{
			return this.BusinessRules.GetByKeySimple(ClientIdentifier);
		}

		public virtual Task<Client> GetByKeySimpleAsync(String ClientIdentifier)
		{
			return Task.Run<Client>(() =>
			{
				return this.BusinessRules.GetByKeySimple(ClientIdentifier);
			});
		}

		public virtual Client GetByKey(String ClientIdentifier)
		{
			return this.BusinessRules.GetByKey(ClientIdentifier);
		}

		public virtual Task<Client> GetByKeyAsync(String ClientIdentifier)
		{
			return Task.Run<Client>(() =>
			{
				return this.BusinessRules.GetByKey(ClientIdentifier);
			});
		}

		public virtual OperationResult<Client> EditByKey(Client Client, String ClientIdentifier)
		{
			return this.BusinessRules.EditByKey(Client, ClientIdentifier);
		}

		public virtual Task<OperationResult<Client>> EditByKeyAsync(Client Client, String ClientIdentifier)
		{
			return Task.Run<OperationResult<Client>>(() =>
			{
				return this.BusinessRules.EditByKey(Client, ClientIdentifier);
			});
		}

		public virtual OperationResult RemoveByKey(String ClientIdentifier)
		{
			return this.BusinessRules.RemoveByKey(ClientIdentifier);
		}

		public virtual Task<OperationResult> RemoveByKeyAsync(String ClientIdentifier)
		{
			return Task.Run<OperationResult>(() =>
			{
				return this.BusinessRules.RemoveByKey(ClientIdentifier);
			});
		}
    }
}