using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Auth.BusinessRules;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Infra.Model;
using System;
using System.Threading.Tasks;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class DeviceService : AuthServiceBase<Int32, Device, DeviceCollection, DeviceRules, IDeviceDataAccess>, IDeviceService
    {		
		
		public virtual Device GetByKeySimple(String DeviceIdentifier)
		{
			return this.BusinessRules.GetByKeySimple(DeviceIdentifier);
		}

		public virtual Task<Device> GetByKeySimpleAsync(String DeviceIdentifier)
		{
			return Task.Run<Device>(() =>
			{
				return this.BusinessRules.GetByKeySimple(DeviceIdentifier);
			});
		}

		public virtual Device GetByKey(String DeviceIdentifier)
		{
			return this.BusinessRules.GetByKey(DeviceIdentifier);
		}

		public virtual Task<Device> GetByKeyAsync(String DeviceIdentifier)
		{
			return Task.Run<Device>(() =>
			{
				return this.BusinessRules.GetByKey(DeviceIdentifier);
			});
		}

		public virtual OperationResult<Device> EditByKey(Device Device, String DeviceIdentifier)
		{
			return this.BusinessRules.EditByKey(Device, DeviceIdentifier);
		}

		public virtual Task<OperationResult<Device>> EditByKeyAsync(Device Device, String DeviceIdentifier)
		{
			return Task.Run<OperationResult<Device>>(() =>
			{
				return this.BusinessRules.EditByKey(Device, DeviceIdentifier);
			});
		}

		public virtual OperationResult RemoveByKey(String DeviceIdentifier)
		{
			return this.BusinessRules.RemoveByKey(DeviceIdentifier);
		}

		public virtual Task<OperationResult> RemoveByKeyAsync(String DeviceIdentifier)
		{
			return Task.Run<OperationResult>(() =>
			{
				return this.BusinessRules.RemoveByKey(DeviceIdentifier);
			});
		}
    }
}