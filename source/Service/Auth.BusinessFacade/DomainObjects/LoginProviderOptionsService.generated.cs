using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class LoginProviderOptionsService : AuthServiceBase<Int32, LoginProviderOptions, LoginProviderOptionsCollection, LoginProviderOptionsRules, ILoginProviderOptionsDataAccess>, ILoginProviderOptionsService
    {		
		public virtual LoginProviderOptionsCollection GetByApplicationSimple(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplicationSimple(ApplicationID);
        }	

		public virtual LoginProviderOptionsCollection GetByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplication(ApplicationID);
        }
		
		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetCountByApplication(ApplicationID);
        }

		public virtual LoginProviderOptionsCollection GetByLoginProviderSimple(Byte LoginProviderID)
        {
            return this.BusinessRules.GetByLoginProviderSimple(LoginProviderID);
        }	

		public virtual LoginProviderOptionsCollection GetByLoginProvider(Byte LoginProviderID)
        {
            return this.BusinessRules.GetByLoginProvider(LoginProviderID);
        }
		
		public virtual Int32 GetCountByLoginProvider(Byte LoginProviderID)
        {
            return this.BusinessRules.GetCountByLoginProvider(LoginProviderID);
        }


		public virtual LoginProviderOptions GetByKeySimple(Int32 ApplicationID, Byte LoginProviderID)
		{
			return this.BusinessRules.GetByKeySimple(ApplicationID, LoginProviderID);
		}

		public virtual LoginProviderOptions GetByKey(Int32 ApplicationID, Byte LoginProviderID)
		{
			return this.BusinessRules.GetByKey(ApplicationID, LoginProviderID);
		}

		public virtual OperationResult<LoginProviderOptions> EditByKey(LoginProviderOptions LoginProviderOptions, Int32 ApplicationID, Byte LoginProviderID)
		{
			return this.BusinessRules.EditByKey(LoginProviderOptions, ApplicationID, LoginProviderID);
		}

		public virtual OperationResult RemoveByKey(Int32 ApplicationID, Byte LoginProviderID)
		{
			return this.BusinessRules.RemoveByKey(ApplicationID, LoginProviderID);
		}
    }
}