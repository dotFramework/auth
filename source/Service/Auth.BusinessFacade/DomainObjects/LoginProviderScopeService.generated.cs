using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class LoginProviderScopeService : AuthServiceBase<Int32, LoginProviderScope, LoginProviderScopeCollection, LoginProviderScopeRules, ILoginProviderScopeDataAccess>, ILoginProviderScopeService
    {		
		public virtual LoginProviderScopeCollection GetByLoginProviderOptionsSimple(Int32 LoginProviderOptionsID)
        {
            return this.BusinessRules.GetByLoginProviderOptionsSimple(LoginProviderOptionsID);
        }	

		public virtual Task<LoginProviderScopeCollection> GetByLoginProviderOptionsSimpleAsync(Int32 LoginProviderOptionsID)
        {
			return Task.Run<LoginProviderScopeCollection>(() =>
			{
				return this.BusinessRules.GetByLoginProviderOptionsSimple(LoginProviderOptionsID);
			});
        }

		public virtual LoginProviderScopeCollection GetByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
            return this.BusinessRules.GetByLoginProviderOptions(LoginProviderOptionsID);
        }

		public virtual Task<LoginProviderScopeCollection> GetByLoginProviderOptionsAsync(Int32 LoginProviderOptionsID)
        {
			return Task.Run<LoginProviderScopeCollection>(() =>
			{
				return this.BusinessRules.GetByLoginProviderOptions(LoginProviderOptionsID);
			});
        }
		
		public virtual Int32 GetCountByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
            return this.BusinessRules.GetCountByLoginProviderOptions(LoginProviderOptionsID);
        }

		public virtual Task<Int32> GetCountByLoginProviderOptionsAsync(Int32 LoginProviderOptionsID)
        {
			return Task.Run<Int32>(() =>
			{
				return this.BusinessRules.GetCountByLoginProviderOptions(LoginProviderOptionsID);
			});
        }

		public virtual LoginProviderScope GetByKeySimple(Int32 LoginProviderOptionsID, String Scope)
		{
			return this.BusinessRules.GetByKeySimple(LoginProviderOptionsID, Scope);
		}

		public virtual Task<LoginProviderScope> GetByKeySimpleAsync(Int32 LoginProviderOptionsID, String Scope)
		{
			return Task.Run<LoginProviderScope>(() =>
			{
				return this.BusinessRules.GetByKeySimple(LoginProviderOptionsID, Scope);
			});
		}

		public virtual LoginProviderScope GetByKey(Int32 LoginProviderOptionsID, String Scope)
		{
			return this.BusinessRules.GetByKey(LoginProviderOptionsID, Scope);
		}

		public virtual Task<LoginProviderScope> GetByKeyAsync(Int32 LoginProviderOptionsID, String Scope)
		{
			return Task.Run<LoginProviderScope>(() =>
			{
				return this.BusinessRules.GetByKey(LoginProviderOptionsID, Scope);
			});
		}

		public virtual OperationResult<LoginProviderScope> EditByKey(LoginProviderScope LoginProviderScope, Int32 LoginProviderOptionsID, String Scope)
		{
			return this.BusinessRules.EditByKey(LoginProviderScope, LoginProviderOptionsID, Scope);
		}

		public virtual Task<OperationResult<LoginProviderScope>> EditByKeyAsync(LoginProviderScope LoginProviderScope, Int32 LoginProviderOptionsID, String Scope)
		{
			return Task.Run<OperationResult<LoginProviderScope>>(() =>
			{
				return this.BusinessRules.EditByKey(LoginProviderScope, LoginProviderOptionsID, Scope);
			});
		}

		public virtual OperationResult RemoveByKey(Int32 LoginProviderOptionsID, String Scope)
		{
			return this.BusinessRules.RemoveByKey(LoginProviderOptionsID, Scope);
		}

		public virtual Task<OperationResult> RemoveByKeyAsync(Int32 LoginProviderOptionsID, String Scope)
		{
			return Task.Run<OperationResult>(() =>
			{
				return this.BusinessRules.RemoveByKey(LoginProviderOptionsID, Scope);
			});
		}
    }
}