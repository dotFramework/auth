using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class RefreshTokenService : AuthServiceBase<Int64, RefreshToken, RefreshTokenCollection, RefreshTokenRules, IRefreshTokenDataAccess>, IRefreshTokenService
    {		
		public virtual RefreshTokenCollection GetByUserSimple(Int32 UserID)
        {
            return this.BusinessRules.GetByUserSimple(UserID);
        }	

		public virtual RefreshTokenCollection GetByUser(Int32 UserID)
        {
            return this.BusinessRules.GetByUser(UserID);
        }
		
		public virtual Int32 GetCountByUser(Int32 UserID)
        {
            return this.BusinessRules.GetCountByUser(UserID);
        }


		public virtual RefreshToken GetByKeySimple(Int32 UserID)
		{
			return this.BusinessRules.GetByKeySimple(UserID);
		}

		public virtual RefreshToken GetByKey(Int32 UserID)
		{
			return this.BusinessRules.GetByKey(UserID);
		}

		public virtual OperationResult<RefreshToken> EditByKey(RefreshToken RefreshToken, Int32 UserID)
		{
			return this.BusinessRules.EditByKey(RefreshToken, UserID);
		}

		public virtual OperationResult RemoveByKey(Int32 UserID)
		{
			return this.BusinessRules.RemoveByKey(UserID);
		}
    }
}