using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class UserLoginService : AuthServiceBase<Int64, UserLogin, UserLoginCollection, UserLoginRules, IUserLoginDataAccess>, IUserLoginService
    {		
		public virtual UserLoginCollection GetByUserSimple(Int32 UserID)
        {
            return this.BusinessRules.GetByUserSimple(UserID);
        }	

		public virtual UserLoginCollection GetByUser(Int32 UserID)
        {
            return this.BusinessRules.GetByUser(UserID);
        }
		
		public virtual Int32 GetCountByUser(Int32 UserID)
        {
            return this.BusinessRules.GetCountByUser(UserID);
        }

		public virtual UserLoginCollection GetByLoginProviderOptionsSimple(Int32 LoginProviderOptionsID)
        {
            return this.BusinessRules.GetByLoginProviderOptionsSimple(LoginProviderOptionsID);
        }	

		public virtual UserLoginCollection GetByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
            return this.BusinessRules.GetByLoginProviderOptions(LoginProviderOptionsID);
        }
		
		public virtual Int32 GetCountByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
            return this.BusinessRules.GetCountByLoginProviderOptions(LoginProviderOptionsID);
        }


		public virtual UserLogin GetByKeySimple(Int32 LoginProviderOptionsID, String ProviderKey)
		{
			return this.BusinessRules.GetByKeySimple(LoginProviderOptionsID, ProviderKey);
		}

		public virtual UserLogin GetByKey(Int32 LoginProviderOptionsID, String ProviderKey)
		{
			return this.BusinessRules.GetByKey(LoginProviderOptionsID, ProviderKey);
		}

		public virtual OperationResult<UserLogin> EditByKey(UserLogin UserLogin, Int32 LoginProviderOptionsID, String ProviderKey)
		{
			return this.BusinessRules.EditByKey(UserLogin, LoginProviderOptionsID, ProviderKey);
		}

		public virtual OperationResult RemoveByKey(Int32 LoginProviderOptionsID, String ProviderKey)
		{
			return this.BusinessRules.RemoveByKey(LoginProviderOptionsID, ProviderKey);
		}
    }
}