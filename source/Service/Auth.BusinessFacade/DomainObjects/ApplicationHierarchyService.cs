using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class ApplicationHierarchyService : IApplicationHierarchyService
    {
        public ApplicationHierarchy GetByActionName(string applicationCode, string controllerName, string actionName)
        {
            ApplicationHierarchy ApplicationHierarchy = new ApplicationHierarchyRules().GetByActionName(applicationCode, controllerName, actionName);
            return ApplicationHierarchy;
        }
    }
}