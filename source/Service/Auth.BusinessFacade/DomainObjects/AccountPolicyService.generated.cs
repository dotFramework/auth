using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class AccountPolicyService : AuthServiceBase<Byte, AccountPolicy, AccountPolicyCollection, AccountPolicyRules, IAccountPolicyDataAccess>, IAccountPolicyService
    {		
		public virtual AccountPolicy GetByKeySimple(String PolicyName)
		{
			return this.BusinessRules.GetByKeySimple(PolicyName);
		}

		public virtual Task<AccountPolicy> GetByKeySimpleAsync(String PolicyName)
		{
			return Task.Run<AccountPolicy>(() =>
			{
				return this.BusinessRules.GetByKeySimple(PolicyName);
			});
		}

		public virtual AccountPolicy GetByKey(String PolicyName)
		{
			return this.BusinessRules.GetByKey(PolicyName);
		}

		public virtual Task<AccountPolicy> GetByKeyAsync(String PolicyName)
		{
			return Task.Run<AccountPolicy>(() =>
			{
				return this.BusinessRules.GetByKey(PolicyName);
			});
		}

		public virtual OperationResult<AccountPolicy> EditByKey(AccountPolicy AccountPolicy, String PolicyName)
		{
			return this.BusinessRules.EditByKey(AccountPolicy, PolicyName);
		}

		public virtual Task<OperationResult<AccountPolicy>> EditByKeyAsync(AccountPolicy AccountPolicy, String PolicyName)
		{
			return Task.Run<OperationResult<AccountPolicy>>(() =>
			{
				return this.BusinessRules.EditByKey(AccountPolicy, PolicyName);
			});
		}

		public virtual OperationResult RemoveByKey(String PolicyName)
		{
			return this.BusinessRules.RemoveByKey(PolicyName);
		}

		public virtual Task<OperationResult> RemoveByKeyAsync(String PolicyName)
		{
			return Task.Run<OperationResult>(() =>
			{
				return this.BusinessRules.RemoveByKey(PolicyName);
			});
		}
    }
}