using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class ApplicationHierarchyService : AuthServiceBase<Int32, ApplicationHierarchy, ApplicationHierarchyCollection, ApplicationHierarchyRules, IApplicationHierarchyDataAccess>, IApplicationHierarchyService
    {		
		public virtual ApplicationHierarchyCollection GetByApplicationSimple(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplicationSimple(ApplicationID);
        }	

		public virtual ApplicationHierarchyCollection GetByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplication(ApplicationID);
        }
		
		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetCountByApplication(ApplicationID);
        }


		public virtual ApplicationHierarchy GetByKeySimple(Int32 ApplicationID, String Code)
		{
			return this.BusinessRules.GetByKeySimple(ApplicationID, Code);
		}

		public virtual ApplicationHierarchy GetByKey(Int32 ApplicationID, String Code)
		{
			return this.BusinessRules.GetByKey(ApplicationID, Code);
		}

		public virtual OperationResult<ApplicationHierarchy> EditByKey(ApplicationHierarchy ApplicationHierarchy, Int32 ApplicationID, String Code)
		{
			return this.BusinessRules.EditByKey(ApplicationHierarchy, ApplicationID, Code);
		}

		public virtual OperationResult RemoveByKey(Int32 ApplicationID, String Code)
		{
			return this.BusinessRules.RemoveByKey(ApplicationID, Code);
		}
    }
}