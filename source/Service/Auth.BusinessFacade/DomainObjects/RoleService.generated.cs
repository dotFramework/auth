using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class RoleService : AuthServiceBase<Int32, Role, RoleCollection, RoleRules, IRoleDataAccess>, IRoleService
    {		
		public virtual RoleCollection GetByApplicationSimple(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplicationSimple(ApplicationID);
        }	

		public virtual RoleCollection GetByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplication(ApplicationID);
        }
		
		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetCountByApplication(ApplicationID);
        }

		public virtual RoleCollection GetByDesignationSimple(Int16 DesignationID)
        {
            return this.BusinessRules.GetByDesignationSimple(DesignationID);
        }	

		public virtual RoleCollection GetByDesignation(Int16 DesignationID)
        {
            return this.BusinessRules.GetByDesignation(DesignationID);
        }
		
		public virtual Int32 GetCountByDesignation(Int16 DesignationID)
        {
            return this.BusinessRules.GetCountByDesignation(DesignationID);
        }


		public virtual Role GetByKeySimple(Int32 ApplicationID, String Name)
		{
			return this.BusinessRules.GetByKeySimple(ApplicationID, Name);
		}

		public virtual Role GetByKey(Int32 ApplicationID, String Name)
		{
			return this.BusinessRules.GetByKey(ApplicationID, Name);
		}

		public virtual OperationResult<Role> EditByKey(Role Role, Int32 ApplicationID, String Name)
		{
			return this.BusinessRules.EditByKey(Role, ApplicationID, Name);
		}

		public virtual OperationResult RemoveByKey(Int32 ApplicationID, String Name)
		{
			return this.BusinessRules.RemoveByKey(ApplicationID, Name);
		}
    }
}