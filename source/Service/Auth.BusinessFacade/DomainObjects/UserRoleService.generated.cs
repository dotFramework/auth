using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class UserRoleService : AuthServiceBase<Int64, UserRole, UserRoleCollection, UserRoleRules, IUserRoleDataAccess>, IUserRoleService
    {		
		public virtual UserRoleCollection GetByUserSimple(Int32 UserID)
        {
            return this.BusinessRules.GetByUserSimple(UserID);
        }	

		public virtual UserRoleCollection GetByUser(Int32 UserID)
        {
            return this.BusinessRules.GetByUser(UserID);
        }
		
		public virtual Int32 GetCountByUser(Int32 UserID)
        {
            return this.BusinessRules.GetCountByUser(UserID);
        }

		public virtual UserRoleCollection GetByRoleSimple(Int32 RoleID)
        {
            return this.BusinessRules.GetByRoleSimple(RoleID);
        }	

		public virtual UserRoleCollection GetByRole(Int32 RoleID)
        {
            return this.BusinessRules.GetByRole(RoleID);
        }
		
		public virtual Int32 GetCountByRole(Int32 RoleID)
        {
            return this.BusinessRules.GetCountByRole(RoleID);
        }


		public virtual UserRole GetByKeySimple(Int32 UserID, Int32 RoleID)
		{
			return this.BusinessRules.GetByKeySimple(UserID, RoleID);
		}

		public virtual UserRole GetByKey(Int32 UserID, Int32 RoleID)
		{
			return this.BusinessRules.GetByKey(UserID, RoleID);
		}

		public virtual OperationResult<UserRole> EditByKey(UserRole UserRole, Int32 UserID, Int32 RoleID)
		{
			return this.BusinessRules.EditByKey(UserRole, UserID, RoleID);
		}

		public virtual OperationResult RemoveByKey(Int32 UserID, Int32 RoleID)
		{
			return this.BusinessRules.RemoveByKey(UserID, RoleID);
		}
    }
}