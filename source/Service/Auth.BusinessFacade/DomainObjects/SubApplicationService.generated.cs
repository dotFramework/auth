using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class SubApplicationService : AuthServiceBase<Int32, SubApplication, SubApplicationCollection, SubApplicationRules, ISubApplicationDataAccess>, ISubApplicationService
    {		
		public virtual SubApplicationCollection GetByApplicationSimple(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplicationSimple(ApplicationID);
        }	

		public virtual SubApplicationCollection GetByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplication(ApplicationID);
        }
		
		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetCountByApplication(ApplicationID);
        }


		public virtual SubApplication GetByKeySimple(String Code)
		{
			return this.BusinessRules.GetByKeySimple(Code);
		}

		public virtual SubApplication GetByKey(String Code)
		{
			return this.BusinessRules.GetByKey(Code);
		}

		public virtual OperationResult<SubApplication> EditByKey(SubApplication SubApplication, String Code)
		{
			return this.BusinessRules.EditByKey(SubApplication, Code);
		}

		public virtual OperationResult RemoveByKey(String Code)
		{
			return this.BusinessRules.RemoveByKey(Code);
		}
    }
}