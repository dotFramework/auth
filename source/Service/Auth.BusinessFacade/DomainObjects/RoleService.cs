using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class RoleService : IRoleService
    {
        public Role GetByName(String RoleName)
        {
            Role Role = new RoleRules().GetByName(RoleName);
            return Role;
        }
    }
}