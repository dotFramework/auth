using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class UserSessionService : AuthServiceBase<Int64, UserSession, UserSessionCollection, UserSessionRules, IUserSessionDataAccess>, IUserSessionService
    {		
		public virtual UserSessionCollection GetByUserSimple(Int32 UserID)
        {
            return this.BusinessRules.GetByUserSimple(UserID);
        }	

		public virtual UserSessionCollection GetByUser(Int32 UserID)
        {
            return this.BusinessRules.GetByUser(UserID);
        }
		
		public virtual Int32 GetCountByUser(Int32 UserID)
        {
            return this.BusinessRules.GetCountByUser(UserID);
        }

		public virtual UserSessionCollection GetBySubApplicationSimple(Int32 SubApplicationID)
        {
            return this.BusinessRules.GetBySubApplicationSimple(SubApplicationID);
        }	

		public virtual UserSessionCollection GetBySubApplication(Int32 SubApplicationID)
        {
            return this.BusinessRules.GetBySubApplication(SubApplicationID);
        }
		
		public virtual Int32 GetCountBySubApplication(Int32 SubApplicationID)
        {
            return this.BusinessRules.GetCountBySubApplication(SubApplicationID);
        }

    }
}