using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class ApplicableActionService : AuthServiceBase<Int32, ApplicableAction, ApplicableActionCollection, ApplicableActionRules, IApplicableActionDataAccess>, IApplicableActionService
    {		
		public virtual ApplicableActionCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetByApplicationHierarchySimple(ApplicationHierarchyID);
        }	

		public virtual ApplicableActionCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetByApplicationHierarchy(ApplicationHierarchyID);
        }
		
		public virtual Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetCountByApplicationHierarchy(ApplicationHierarchyID);
        }

		public virtual ApplicableActionCollection GetByViewActionSimple(Int32 ViewActionID)
        {
            return this.BusinessRules.GetByViewActionSimple(ViewActionID);
        }	

		public virtual ApplicableActionCollection GetByViewAction(Int32 ViewActionID)
        {
            return this.BusinessRules.GetByViewAction(ViewActionID);
        }
		
		public virtual Int32 GetCountByViewAction(Int32 ViewActionID)
        {
            return this.BusinessRules.GetCountByViewAction(ViewActionID);
        }


		public virtual ApplicableAction GetByKeySimple(Int32 ApplicationHierarchyID, Int32 ViewActionID)
		{
			return this.BusinessRules.GetByKeySimple(ApplicationHierarchyID, ViewActionID);
		}

		public virtual ApplicableAction GetByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID)
		{
			return this.BusinessRules.GetByKey(ApplicationHierarchyID, ViewActionID);
		}

		public virtual OperationResult<ApplicableAction> EditByKey(ApplicableAction ApplicableAction, Int32 ApplicationHierarchyID, Int32 ViewActionID)
		{
			return this.BusinessRules.EditByKey(ApplicableAction, ApplicationHierarchyID, ViewActionID);
		}

		public virtual OperationResult RemoveByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID)
		{
			return this.BusinessRules.RemoveByKey(ApplicationHierarchyID, ViewActionID);
		}
    }
}