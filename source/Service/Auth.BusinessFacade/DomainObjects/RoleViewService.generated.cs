using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class RoleViewService : AuthServiceBase<Int32, RoleView, RoleViewCollection, RoleViewRules, IRoleViewDataAccess>, IRoleViewService
    {		
		public virtual RoleViewCollection GetByRoleSimple(Int32 RoleID)
        {
            return this.BusinessRules.GetByRoleSimple(RoleID);
        }	

		public virtual RoleViewCollection GetByRole(Int32 RoleID)
        {
            return this.BusinessRules.GetByRole(RoleID);
        }
		
		public virtual Int32 GetCountByRole(Int32 RoleID)
        {
            return this.BusinessRules.GetCountByRole(RoleID);
        }

		public virtual RoleViewCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetByApplicationHierarchySimple(ApplicationHierarchyID);
        }	

		public virtual RoleViewCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetByApplicationHierarchy(ApplicationHierarchyID);
        }
		
		public virtual Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetCountByApplicationHierarchy(ApplicationHierarchyID);
        }


		public virtual RoleView GetByKeySimple(Int32 RoleID, Int32 ApplicationHierarchyID)
		{
			return this.BusinessRules.GetByKeySimple(RoleID, ApplicationHierarchyID);
		}

		public virtual RoleView GetByKey(Int32 RoleID, Int32 ApplicationHierarchyID)
		{
			return this.BusinessRules.GetByKey(RoleID, ApplicationHierarchyID);
		}

		public virtual OperationResult<RoleView> EditByKey(RoleView RoleView, Int32 RoleID, Int32 ApplicationHierarchyID)
		{
			return this.BusinessRules.EditByKey(RoleView, RoleID, ApplicationHierarchyID);
		}

		public virtual OperationResult RemoveByKey(Int32 RoleID, Int32 ApplicationHierarchyID)
		{
			return this.BusinessRules.RemoveByKey(RoleID, ApplicationHierarchyID);
		}
    }
}