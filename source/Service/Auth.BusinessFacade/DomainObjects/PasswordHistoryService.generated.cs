using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class PasswordHistoryService : AuthServiceBase<Int64, PasswordHistory, PasswordHistoryCollection, PasswordHistoryRules, IPasswordHistoryDataAccess>, IPasswordHistoryService
    {		
		public virtual PasswordHistoryCollection GetByUserSimple(Int32 UserID)
        {
            return this.BusinessRules.GetByUserSimple(UserID);
        }	

		public virtual Task<PasswordHistoryCollection> GetByUserSimpleAsync(Int32 UserID)
        {
			return Task.Run<PasswordHistoryCollection>(() =>
			{
				return this.BusinessRules.GetByUserSimple(UserID);
			});
        }

		public virtual PasswordHistoryCollection GetByUser(Int32 UserID)
        {
            return this.BusinessRules.GetByUser(UserID);
        }

		public virtual Task<PasswordHistoryCollection> GetByUserAsync(Int32 UserID)
        {
			return Task.Run<PasswordHistoryCollection>(() =>
			{
				return this.BusinessRules.GetByUser(UserID);
			});
        }
		
		public virtual Int32 GetCountByUser(Int32 UserID)
        {
            return this.BusinessRules.GetCountByUser(UserID);
        }

		public virtual Task<Int32> GetCountByUserAsync(Int32 UserID)
        {
			return Task.Run<Int32>(() =>
			{
				return this.BusinessRules.GetCountByUser(UserID);
			});
        }

    }
}