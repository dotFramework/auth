using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class ViewVisitService : AuthServiceBase<Int64, ViewVisit, ViewVisitCollection, ViewVisitRules, IViewVisitDataAccess>, IViewVisitService
    {		
		public virtual ViewVisitCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetByApplicationHierarchySimple(ApplicationHierarchyID);
        }	

		public virtual ViewVisitCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetByApplicationHierarchy(ApplicationHierarchyID);
        }
		
		public virtual Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetCountByApplicationHierarchy(ApplicationHierarchyID);
        }

		public virtual ViewVisitCollection GetByUserSimple(Int32 UserID)
        {
            return this.BusinessRules.GetByUserSimple(UserID);
        }	

		public virtual ViewVisitCollection GetByUser(Int32 UserID)
        {
            return this.BusinessRules.GetByUser(UserID);
        }
		
		public virtual Int32 GetCountByUser(Int32 UserID)
        {
            return this.BusinessRules.GetCountByUser(UserID);
        }

    }
}