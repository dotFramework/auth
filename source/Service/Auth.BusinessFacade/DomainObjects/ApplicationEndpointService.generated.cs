using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class ApplicationEndpointService : AuthServiceBase<Int32, ApplicationEndpoint, ApplicationEndpointCollection, ApplicationEndpointRules, IApplicationEndpointDataAccess>, IApplicationEndpointService
    {		
		public virtual ApplicationEndpointCollection GetByApplicationSimple(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplicationSimple(ApplicationID);
        }	

		public virtual ApplicationEndpointCollection GetByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetByApplication(ApplicationID);
        }
		
		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
            return this.BusinessRules.GetCountByApplication(ApplicationID);
        }


		public virtual ApplicationEndpoint GetByKeySimple(Int32 ApplicationID, String EndpointType)
		{
			return this.BusinessRules.GetByKeySimple(ApplicationID, EndpointType);
		}

		public virtual ApplicationEndpoint GetByKey(Int32 ApplicationID, String EndpointType)
		{
			return this.BusinessRules.GetByKey(ApplicationID, EndpointType);
		}

		public virtual OperationResult<ApplicationEndpoint> EditByKey(ApplicationEndpoint ApplicationEndpoint, Int32 ApplicationID, String EndpointType)
		{
			return this.BusinessRules.EditByKey(ApplicationEndpoint, ApplicationID, EndpointType);
		}

		public virtual OperationResult RemoveByKey(Int32 ApplicationID, String EndpointType)
		{
			return this.BusinessRules.RemoveByKey(ApplicationID, EndpointType);
		}
    }
}