using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class FavoriteViewService : AuthServiceBase<Int32, FavoriteView, FavoriteViewCollection, FavoriteViewRules, IFavoriteViewDataAccess>, IFavoriteViewService
    {		
		public virtual FavoriteViewCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetByApplicationHierarchySimple(ApplicationHierarchyID);
        }	

		public virtual FavoriteViewCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetByApplicationHierarchy(ApplicationHierarchyID);
        }
		
		public virtual Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            return this.BusinessRules.GetCountByApplicationHierarchy(ApplicationHierarchyID);
        }

		public virtual FavoriteViewCollection GetByUserSimple(Int32 UserID)
        {
            return this.BusinessRules.GetByUserSimple(UserID);
        }	

		public virtual FavoriteViewCollection GetByUser(Int32 UserID)
        {
            return this.BusinessRules.GetByUser(UserID);
        }
		
		public virtual Int32 GetCountByUser(Int32 UserID)
        {
            return this.BusinessRules.GetCountByUser(UserID);
        }


		public virtual FavoriteView GetByKeySimple(Int32 ApplicationHierarchyID, Int32 UserID)
		{
			return this.BusinessRules.GetByKeySimple(ApplicationHierarchyID, UserID);
		}

		public virtual FavoriteView GetByKey(Int32 ApplicationHierarchyID, Int32 UserID)
		{
			return this.BusinessRules.GetByKey(ApplicationHierarchyID, UserID);
		}

		public virtual OperationResult<FavoriteView> EditByKey(FavoriteView FavoriteView, Int32 ApplicationHierarchyID, Int32 UserID)
		{
			return this.BusinessRules.EditByKey(FavoriteView, ApplicationHierarchyID, UserID);
		}

		public virtual OperationResult RemoveByKey(Int32 ApplicationHierarchyID, Int32 UserID)
		{
			return this.BusinessRules.RemoveByKey(ApplicationHierarchyID, UserID);
		}
    }
}