using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Auth.BusinessRules;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Infra.Model;
using System;
using System.Threading.Tasks;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class ClientDeviceService : AuthServiceBase<Int32, ClientDevice, ClientDeviceCollection, ClientDeviceRules, IClientDeviceDataAccess>, IClientDeviceService
    {		
		public virtual ClientDeviceCollection GetByDeviceSimple(Int32 DeviceID)
        {
            return this.BusinessRules.GetByDeviceSimple(DeviceID);
        }	

		public virtual Task<ClientDeviceCollection> GetByDeviceSimpleAsync(Int32 DeviceID)
        {
			return Task.Run<ClientDeviceCollection>(() =>
			{
				return this.BusinessRules.GetByDeviceSimple(DeviceID);
			});
        }

		public virtual ClientDeviceCollection GetByDevice(Int32 DeviceID)
        {
            return this.BusinessRules.GetByDevice(DeviceID);
        }

		public virtual Task<ClientDeviceCollection> GetByDeviceAsync(Int32 DeviceID)
        {
			return Task.Run<ClientDeviceCollection>(() =>
			{
				return this.BusinessRules.GetByDevice(DeviceID);
			});
        }

		public virtual Int32 GetCountByDevice(Int32 DeviceID)
        {
            return this.BusinessRules.GetCountByDevice(DeviceID);
        }

		public virtual Task<Int32> GetCountByDeviceAsync(Int32 DeviceID)
        {
			return Task.Run<Int32>(() =>
			{
				return this.BusinessRules.GetCountByDevice(DeviceID);
			});
        }
		public virtual ClientDeviceCollection GetByClientSimple(Int32 ClientID)
        {
            return this.BusinessRules.GetByClientSimple(ClientID);
        }	

		public virtual Task<ClientDeviceCollection> GetByClientSimpleAsync(Int32 ClientID)
        {
			return Task.Run<ClientDeviceCollection>(() =>
			{
				return this.BusinessRules.GetByClientSimple(ClientID);
			});
        }

		public virtual ClientDeviceCollection GetByClient(Int32 ClientID)
        {
            return this.BusinessRules.GetByClient(ClientID);
        }

		public virtual Task<ClientDeviceCollection> GetByClientAsync(Int32 ClientID)
        {
			return Task.Run<ClientDeviceCollection>(() =>
			{
				return this.BusinessRules.GetByClient(ClientID);
			});
        }

		public virtual Int32 GetCountByClient(Int32 ClientID)
        {
            return this.BusinessRules.GetCountByClient(ClientID);
        }

		public virtual Task<Int32> GetCountByClientAsync(Int32 ClientID)
        {
			return Task.Run<Int32>(() =>
			{
				return this.BusinessRules.GetCountByClient(ClientID);
			});
        }
		
		public virtual ClientDevice GetByKeySimple(Int32 DeviceID, Int32 ClientID)
		{
			return this.BusinessRules.GetByKeySimple(DeviceID, ClientID);
		}

		public virtual Task<ClientDevice> GetByKeySimpleAsync(Int32 DeviceID, Int32 ClientID)
		{
			return Task.Run<ClientDevice>(() =>
			{
				return this.BusinessRules.GetByKeySimple(DeviceID, ClientID);
			});
		}

		public virtual ClientDevice GetByKey(Int32 DeviceID, Int32 ClientID)
		{
			return this.BusinessRules.GetByKey(DeviceID, ClientID);
		}

		public virtual Task<ClientDevice> GetByKeyAsync(Int32 DeviceID, Int32 ClientID)
		{
			return Task.Run<ClientDevice>(() =>
			{
				return this.BusinessRules.GetByKey(DeviceID, ClientID);
			});
		}

		public virtual OperationResult<ClientDevice> EditByKey(ClientDevice ClientDevice, Int32 DeviceID, Int32 ClientID)
		{
			return this.BusinessRules.EditByKey(ClientDevice, DeviceID, ClientID);
		}

		public virtual Task<OperationResult<ClientDevice>> EditByKeyAsync(ClientDevice ClientDevice, Int32 DeviceID, Int32 ClientID)
		{
			return Task.Run<OperationResult<ClientDevice>>(() =>
			{
				return this.BusinessRules.EditByKey(ClientDevice, DeviceID, ClientID);
			});
		}

		public virtual OperationResult RemoveByKey(Int32 DeviceID, Int32 ClientID)
		{
			return this.BusinessRules.RemoveByKey(DeviceID, ClientID);
		}

		public virtual Task<OperationResult> RemoveByKeyAsync(Int32 DeviceID, Int32 ClientID)
		{
			return Task.Run<OperationResult>(() =>
			{
				return this.BusinessRules.RemoveByKey(DeviceID, ClientID);
			});
		}
    }
}