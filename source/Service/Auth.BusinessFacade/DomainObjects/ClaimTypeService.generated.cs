using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class ClaimTypeService : AuthServiceBase<Int16, ClaimType, ClaimTypeCollection, ClaimTypeRules, IClaimTypeDataAccess>, IClaimTypeService
    {		

		public virtual ClaimType GetByKeySimple(String Code)
		{
			return this.BusinessRules.GetByKeySimple(Code);
		}

		public virtual ClaimType GetByKey(String Code)
		{
			return this.BusinessRules.GetByKey(Code);
		}

		public virtual OperationResult<ClaimType> EditByKey(ClaimType ClaimType, String Code)
		{
			return this.BusinessRules.EditByKey(ClaimType, Code);
		}

		public virtual OperationResult RemoveByKey(String Code)
		{
			return this.BusinessRules.RemoveByKey(Code);
		}
    }
}