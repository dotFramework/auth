using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class ApplicableOperationService : AuthServiceBase<Int32, ApplicableOperation, ApplicableOperationCollection, ApplicableOperationRules, IApplicableOperationDataAccess>, IApplicableOperationService
    {		
		public virtual ApplicableOperationCollection GetByApplicationEndpointSimple(Int32 ApplicationEndpointID)
        {
            return this.BusinessRules.GetByApplicationEndpointSimple(ApplicationEndpointID);
        }	

		public virtual ApplicableOperationCollection GetByApplicationEndpoint(Int32 ApplicationEndpointID)
        {
            return this.BusinessRules.GetByApplicationEndpoint(ApplicationEndpointID);
        }
		
		public virtual Int32 GetCountByApplicationEndpoint(Int32 ApplicationEndpointID)
        {
            return this.BusinessRules.GetCountByApplicationEndpoint(ApplicationEndpointID);
        }

		public virtual ApplicableOperationCollection GetByServiceOperationSimple(Int32 ServiceOperationID)
        {
            return this.BusinessRules.GetByServiceOperationSimple(ServiceOperationID);
        }	

		public virtual ApplicableOperationCollection GetByServiceOperation(Int32 ServiceOperationID)
        {
            return this.BusinessRules.GetByServiceOperation(ServiceOperationID);
        }
		
		public virtual Int32 GetCountByServiceOperation(Int32 ServiceOperationID)
        {
            return this.BusinessRules.GetCountByServiceOperation(ServiceOperationID);
        }


		public virtual ApplicableOperation GetByKeySimple(Int32 ApplicationEndpointID, Int32 ServiceOperationID)
		{
			return this.BusinessRules.GetByKeySimple(ApplicationEndpointID, ServiceOperationID);
		}

		public virtual ApplicableOperation GetByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID)
		{
			return this.BusinessRules.GetByKey(ApplicationEndpointID, ServiceOperationID);
		}

		public virtual OperationResult<ApplicableOperation> EditByKey(ApplicableOperation ApplicableOperation, Int32 ApplicationEndpointID, Int32 ServiceOperationID)
		{
			return this.BusinessRules.EditByKey(ApplicableOperation, ApplicationEndpointID, ServiceOperationID);
		}

		public virtual OperationResult RemoveByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID)
		{
			return this.BusinessRules.RemoveByKey(ApplicationEndpointID, ServiceOperationID);
		}
    }
}