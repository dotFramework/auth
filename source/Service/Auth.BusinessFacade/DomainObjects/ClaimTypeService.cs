using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class ClaimTypeService : IClaimTypeService
    {
        public ClaimType GetByName(String Name)
        {
            ClaimType ClaimType = new ClaimTypeRules().GetByName(Name);
            return ClaimType;
        }

        public ClaimType GetBySchema(String Schema)
        {
            ClaimType ClaimType = new ClaimTypeRules().GetBySchema(Schema);
            return ClaimType;
        }
    }
}