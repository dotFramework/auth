using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class UserClaimService : AuthServiceBase<Int64, UserClaim, UserClaimCollection, UserClaimRules, IUserClaimDataAccess>, IUserClaimService
    {		
		public virtual UserClaimCollection GetByUserSimple(Int32 UserID)
        {
            return this.BusinessRules.GetByUserSimple(UserID);
        }	

		public virtual UserClaimCollection GetByUser(Int32 UserID)
        {
            return this.BusinessRules.GetByUser(UserID);
        }
		
		public virtual Int32 GetCountByUser(Int32 UserID)
        {
            return this.BusinessRules.GetCountByUser(UserID);
        }

		public virtual UserClaimCollection GetByClaimTypeSimple(Int16 ClaimTypeID)
        {
            return this.BusinessRules.GetByClaimTypeSimple(ClaimTypeID);
        }	

		public virtual UserClaimCollection GetByClaimType(Int16 ClaimTypeID)
        {
            return this.BusinessRules.GetByClaimType(ClaimTypeID);
        }
		
		public virtual Int32 GetCountByClaimType(Int16 ClaimTypeID)
        {
            return this.BusinessRules.GetCountByClaimType(ClaimTypeID);
        }


		public virtual UserClaim GetByKeySimple(Int32 UserID, Int16 ClaimTypeID)
		{
			return this.BusinessRules.GetByKeySimple(UserID, ClaimTypeID);
		}

		public virtual UserClaim GetByKey(Int32 UserID, Int16 ClaimTypeID)
		{
			return this.BusinessRules.GetByKey(UserID, ClaimTypeID);
		}

		public virtual OperationResult<UserClaim> EditByKey(UserClaim UserClaim, Int32 UserID, Int16 ClaimTypeID)
		{
			return this.BusinessRules.EditByKey(UserClaim, UserID, ClaimTypeID);
		}

		public virtual OperationResult RemoveByKey(Int32 UserID, Int16 ClaimTypeID)
		{
			return this.BusinessRules.RemoveByKey(UserID, ClaimTypeID);
		}
    }
}