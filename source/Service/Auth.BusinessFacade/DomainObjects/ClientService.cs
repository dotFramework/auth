using DotFramework.Auth.Model;
using System;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class ClientService
    {
        public virtual Client GetByClientUID(string clientUid)
        {
            return this.BusinessRules.GetByClientUID(clientUid);
        }

        public virtual Client GetByClientUIDAndSecret(string clientUid, string clientSecret)
        {
            return this.BusinessRules.GetByClientUIDAndSecret(clientUid, clientSecret);
        }
    }
}