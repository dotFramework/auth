using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;
using System.Threading.Tasks;
using DotFramework.Auth.BusinessFacade.Base;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.BusinessFacade
{
	public partial class UserService : AuthServiceBase<Int32, User, UserCollection, UserRules, IUserDataAccess>, IUserService
    {		
		public virtual UserCollection GetByAccountPolicySimple(Byte AccountPolicyID)
        {
            return this.BusinessRules.GetByAccountPolicySimple(AccountPolicyID);
        }	

		public virtual Task<UserCollection> GetByAccountPolicySimpleAsync(Byte AccountPolicyID)
        {
			return Task.Run<UserCollection>(() =>
			{
				return this.BusinessRules.GetByAccountPolicySimple(AccountPolicyID);
			});
        }

		public virtual UserCollection GetByAccountPolicy(Byte AccountPolicyID)
        {
            return this.BusinessRules.GetByAccountPolicy(AccountPolicyID);
        }

		public virtual Task<UserCollection> GetByAccountPolicyAsync(Byte AccountPolicyID)
        {
			return Task.Run<UserCollection>(() =>
			{
				return this.BusinessRules.GetByAccountPolicy(AccountPolicyID);
			});
        }
		
		public virtual Int32 GetCountByAccountPolicy(Byte AccountPolicyID)
        {
            return this.BusinessRules.GetCountByAccountPolicy(AccountPolicyID);
        }

		public virtual Task<Int32> GetCountByAccountPolicyAsync(Byte AccountPolicyID)
        {
			return Task.Run<Int32>(() =>
			{
				return this.BusinessRules.GetCountByAccountPolicy(AccountPolicyID);
			});
        }

		public virtual User GetByKeySimple(String UserName)
		{
			return this.BusinessRules.GetByKeySimple(UserName);
		}

		public virtual Task<User> GetByKeySimpleAsync(String UserName)
		{
			return Task.Run<User>(() =>
			{
				return this.BusinessRules.GetByKeySimple(UserName);
			});
		}

		public virtual User GetByKey(String UserName)
		{
			return this.BusinessRules.GetByKey(UserName);
		}

		public virtual Task<User> GetByKeyAsync(String UserName)
		{
			return Task.Run<User>(() =>
			{
				return this.BusinessRules.GetByKey(UserName);
			});
		}

		public virtual OperationResult<User> EditByKey(User User, String UserName)
		{
			return this.BusinessRules.EditByKey(User, UserName);
		}

		public virtual Task<OperationResult<User>> EditByKeyAsync(User User, String UserName)
		{
			return Task.Run<OperationResult<User>>(() =>
			{
				return this.BusinessRules.EditByKey(User, UserName);
			});
		}

		public virtual OperationResult RemoveByKey(String UserName)
		{
			return this.BusinessRules.RemoveByKey(UserName);
		}

		public virtual Task<OperationResult> RemoveByKeyAsync(String UserName)
		{
			return Task.Run<OperationResult>(() =>
			{
				return this.BusinessRules.RemoveByKey(UserName);
			});
		}
    }
}