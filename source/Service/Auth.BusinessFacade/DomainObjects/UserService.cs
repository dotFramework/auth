using System;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.BusinessRules;

namespace DotFramework.Auth.BusinessFacade
{
    public partial class UserService : IUserService
    {
        public User GetByUserName(String UserName)
        {
            User User = new UserRules().GetByUserName(UserName);
            return User;
        }

        public User GetByEmail(String Email)
        {
            User User = new UserRules().GetByEmail(Email);
            return User;
        }
    }
}