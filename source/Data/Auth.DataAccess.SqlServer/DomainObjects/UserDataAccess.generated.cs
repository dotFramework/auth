using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class UserDataAccess : AuthDataAccessBase<Int32, User, UserCollection>, IUserDataAccess
    {
        public override User Select(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                User User = new User();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        User = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return User;
            }
        }

        public override UserCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                UserCollection UserCollection = new UserCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override UserCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                UserCollection UserCollection = new UserCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    UserCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return UserCollection;
            }
        }

        public virtual UserCollection SelectByAccountPolicy(Byte AccountPolicyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_SelectByAccountPolicy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Value = AccountPolicyID });

                UserCollection UserCollection = new UserCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserCollection;
            }
        }

        public virtual Int32 SelectCountByAccountPolicy(Byte AccountPolicyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_SelectCountByAccountPolicy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Value = AccountPolicyID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(User User)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserName", SqlDbType = SqlDbType.VarChar, Value = User.UserName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SecurityStamp", SqlDbType = SqlDbType.NVarChar, Value = User.SecurityStamp.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@FullName", SqlDbType = SqlDbType.NVarChar, Value = User.FullName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Email", SqlDbType = SqlDbType.NVarChar, Value = User.Email.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EmailConfirmed", SqlDbType = SqlDbType.Bit, Value = User.EmailConfirmed.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PhoneNumber", SqlDbType = SqlDbType.NVarChar, Value = User.PhoneNumber.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PhoneNumberConfirmed", SqlDbType = SqlDbType.Bit, Value = User.PhoneNumberConfirmed.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@TwoFactorEnabled", SqlDbType = SqlDbType.Bit, Value = User.TwoFactorEnabled.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Value = User.AccountPolicy.AccountPolicyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AccessFailedCount", SqlDbType = SqlDbType.Int, Value = User.AccessFailedCount.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutEndDateUtc", SqlDbType = SqlDbType.DateTime, Value = User.LockoutEndDateUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutType", SqlDbType = SqlDbType.TinyInt, Value = User.LockoutType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = User.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = User.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = User.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                User.UserID = Convert.ToInt32(command.Parameters["@UserID"].Value);
                User.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Update(User User)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = User.UserID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserName", SqlDbType = SqlDbType.VarChar, Value = User.UserName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SecurityStamp", SqlDbType = SqlDbType.NVarChar, Value = User.SecurityStamp.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@FullName", SqlDbType = SqlDbType.NVarChar, Value = User.FullName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Email", SqlDbType = SqlDbType.NVarChar, Value = User.Email.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EmailConfirmed", SqlDbType = SqlDbType.Bit, Value = User.EmailConfirmed.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PhoneNumber", SqlDbType = SqlDbType.NVarChar, Value = User.PhoneNumber.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PhoneNumberConfirmed", SqlDbType = SqlDbType.Bit, Value = User.PhoneNumberConfirmed.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@TwoFactorEnabled", SqlDbType = SqlDbType.Bit, Value = User.TwoFactorEnabled.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Value = User.AccountPolicy.AccountPolicyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AccessFailedCount", SqlDbType = SqlDbType.Int, Value = User.AccessFailedCount.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutEndDateUtc", SqlDbType = SqlDbType.DateTime, Value = User.LockoutEndDateUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutType", SqlDbType = SqlDbType.TinyInt, Value = User.LockoutType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = User.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = User.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = User.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                User.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual User SelectByKey(String UserName)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserName", SqlDbType = SqlDbType.VarChar, Value = UserName });

                User User = new User();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        User = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return User;
            }
        }

        public virtual bool UpdateByKey(User User, String UserName)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = User.UserID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserName", SqlDbType = SqlDbType.VarChar, Value = User.UserName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SecurityStamp", SqlDbType = SqlDbType.NVarChar, Value = User.SecurityStamp.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@FullName", SqlDbType = SqlDbType.NVarChar, Value = User.FullName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Email", SqlDbType = SqlDbType.NVarChar, Value = User.Email.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EmailConfirmed", SqlDbType = SqlDbType.Bit, Value = User.EmailConfirmed.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PhoneNumber", SqlDbType = SqlDbType.NVarChar, Value = User.PhoneNumber.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PhoneNumberConfirmed", SqlDbType = SqlDbType.Bit, Value = User.PhoneNumberConfirmed.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@TwoFactorEnabled", SqlDbType = SqlDbType.Bit, Value = User.TwoFactorEnabled.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Value = User.AccountPolicy.AccountPolicyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AccessFailedCount", SqlDbType = SqlDbType.Int, Value = User.AccessFailedCount.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutEndDateUtc", SqlDbType = SqlDbType.DateTime, Value = User.LockoutEndDateUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutType", SqlDbType = SqlDbType.TinyInt, Value = User.LockoutType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = User.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = User.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = User.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserName_Original", SqlDbType = SqlDbType.VarChar, Value = UserName });

                command.ExecuteNonQuery();
                command.Connection.Close();

                User.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String UserName)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_User_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserName", SqlDbType = SqlDbType.VarChar, Value = UserName });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override User FillFromDataReader(DbDataReader dr)
        {
            User User = new User { HasValue = true };

            User.UserID = Convert.ToInt32(dr["UserID"]);
            User.UserName = Convert.ToString(dr["UserName"]);
            if (dr["SecurityStamp"] != DBNull.Value)
                User.SecurityStamp = Convert.ToString(dr["SecurityStamp"]);
            User.FullName = Convert.ToString(dr["FullName"]);
            User.Email = Convert.ToString(dr["Email"]);
            User.EmailConfirmed = Convert.ToBoolean(dr["EmailConfirmed"]);
            if (dr["PhoneNumber"] != DBNull.Value)
                User.PhoneNumber = Convert.ToString(dr["PhoneNumber"]);
            if (dr["PhoneNumberConfirmed"] != DBNull.Value)
                User.PhoneNumberConfirmed = Convert.ToBoolean(dr["PhoneNumberConfirmed"]);
            User.TwoFactorEnabled = Convert.ToBoolean(dr["TwoFactorEnabled"]);
            User.AccountPolicy.AccountPolicyID = Convert.ToByte(dr["AccountPolicyID"]);
            User.AccessFailedCount = Convert.ToInt32(dr["AccessFailedCount"]);
            if (dr["LockoutEndDateUtc"] != DBNull.Value)
                User.LockoutEndDateUtc = Convert.ToDateTime(dr["LockoutEndDateUtc"]);
            if (dr["LockoutType"] != DBNull.Value)
                User.LockoutType = Convert.ToByte(dr["LockoutType"]);
            User.Status = Convert.ToByte(dr["Status"]);
            User.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            User.SessionID = Convert.ToInt64(dr["SessionID"]);
            User.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return User;
        }
    }
}
