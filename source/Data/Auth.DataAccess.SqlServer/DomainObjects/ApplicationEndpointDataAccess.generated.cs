using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ApplicationEndpointDataAccess : AuthDataAccessBase<Int32, ApplicationEndpoint, ApplicationEndpointCollection>, IApplicationEndpointDataAccess
    {
        public override ApplicationEndpoint Select(Int32 ApplicationEndpointID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpointID });

                ApplicationEndpoint ApplicationEndpoint = new ApplicationEndpoint();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ApplicationEndpoint = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationEndpoint;
            }
        }

        public override ApplicationEndpointCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ApplicationEndpointCollection ApplicationEndpointCollection = new ApplicationEndpointCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicationEndpointCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationEndpointCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ApplicationEndpointCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ApplicationEndpointCollection ApplicationEndpointCollection = new ApplicationEndpointCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicationEndpointCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ApplicationEndpointCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationEndpointCollection;
            }
        }

        public virtual ApplicationEndpointCollection SelectByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_SelectByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                ApplicationEndpointCollection ApplicationEndpointCollection = new ApplicationEndpointCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicationEndpointCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationEndpointCollection;
            }
        }

        public virtual Int32 SelectCountByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_SelectCountByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(ApplicationEndpoint ApplicationEndpoint)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpoint.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EndpointType", SqlDbType = SqlDbType.NVarChar, Value = ApplicationEndpoint.EndpointType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ApplicationEndpoint.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ApplicationEndpoint.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.TinyInt, Value = ApplicationEndpoint.Type.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ApplicationEndpoint.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicationEndpoint.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicationEndpoint.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicationEndpoint.ApplicationEndpointID = Convert.ToInt32(command.Parameters["@ApplicationEndpointID"].Value);
                return true;
            }
        }

        public override bool Update(ApplicationEndpoint ApplicationEndpoint)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpoint.ApplicationEndpointID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpoint.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EndpointType", SqlDbType = SqlDbType.NVarChar, Value = ApplicationEndpoint.EndpointType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ApplicationEndpoint.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ApplicationEndpoint.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.TinyInt, Value = ApplicationEndpoint.Type.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ApplicationEndpoint.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicationEndpoint.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicationEndpoint.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ApplicationEndpoint.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicationEndpoint.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 ApplicationEndpointID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpointID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual ApplicationEndpoint SelectByKey(Int32 ApplicationID, String EndpointType)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EndpointType", SqlDbType = SqlDbType.NVarChar, Value = EndpointType });

                ApplicationEndpoint ApplicationEndpoint = new ApplicationEndpoint();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ApplicationEndpoint = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationEndpoint;
            }
        }

        public virtual bool UpdateByKey(ApplicationEndpoint ApplicationEndpoint, Int32 ApplicationID, String EndpointType)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpoint.ApplicationEndpointID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpoint.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EndpointType", SqlDbType = SqlDbType.NVarChar, Value = ApplicationEndpoint.EndpointType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ApplicationEndpoint.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ApplicationEndpoint.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.TinyInt, Value = ApplicationEndpoint.Type.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ApplicationEndpoint.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicationEndpoint.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicationEndpoint.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ApplicationEndpoint.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID_Original", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EndpointType_Original", SqlDbType = SqlDbType.NVarChar, Value = EndpointType });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicationEndpoint.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 ApplicationID, String EndpointType)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationEndpoint_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EndpointType", SqlDbType = SqlDbType.NVarChar, Value = EndpointType });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override ApplicationEndpoint FillFromDataReader(DbDataReader dr)
        {
            ApplicationEndpoint ApplicationEndpoint = new ApplicationEndpoint { HasValue = true };

            ApplicationEndpoint.ApplicationEndpointID = Convert.ToInt32(dr["ApplicationEndpointID"]);
            ApplicationEndpoint.Application.ApplicationID = Convert.ToInt32(dr["ApplicationID"]);
            ApplicationEndpoint.EndpointType = Convert.ToString(dr["EndpointType"]);
            ApplicationEndpoint.Name = Convert.ToString(dr["Name"]);
            if (dr["Description"] != DBNull.Value)
                ApplicationEndpoint.Description = Convert.ToString(dr["Description"]);
            ApplicationEndpoint.Type = Convert.ToByte(dr["Type"]);
            ApplicationEndpoint.NeedAuthorization = Convert.ToBoolean(dr["NeedAuthorization"]);
            ApplicationEndpoint.Status = Convert.ToByte(dr["Status"]);
            ApplicationEndpoint.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            ApplicationEndpoint.SessionID = Convert.ToInt64(dr["SessionID"]);
            ApplicationEndpoint.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return ApplicationEndpoint;
        }
    }
}
