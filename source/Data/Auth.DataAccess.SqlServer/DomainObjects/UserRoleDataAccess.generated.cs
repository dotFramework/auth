using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class UserRoleDataAccess : AuthDataAccessBase<Int64, UserRole, UserRoleCollection>, IUserRoleDataAccess
    {
        public override UserRole Select(Int64 UserRoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserRoleID", SqlDbType = SqlDbType.BigInt, Value = UserRoleID });

                UserRole UserRole = new UserRole();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        UserRole = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserRole;
            }
        }

        public override UserRoleCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                UserRoleCollection UserRoleCollection = new UserRoleCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserRoleCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserRoleCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override UserRoleCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                UserRoleCollection UserRoleCollection = new UserRoleCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserRoleCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    UserRoleCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return UserRoleCollection;
            }
        }

        public virtual UserRoleCollection SelectByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_SelectByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                UserRoleCollection UserRoleCollection = new UserRoleCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserRoleCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserRoleCollection;
            }
        }

        public virtual Int32 SelectCountByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_SelectCountByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual UserRoleCollection SelectByRole(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_SelectByRole]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                UserRoleCollection UserRoleCollection = new UserRoleCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserRoleCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserRoleCollection;
            }
        }

        public virtual Int32 SelectCountByRole(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_SelectCountByRole]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(UserRole UserRole)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserRoleID", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserRole.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = UserRole.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserRole.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserRole.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserRole.UserRoleID = Convert.ToInt64(command.Parameters["@UserRoleID"].Value);
                return true;
            }
        }

        public override bool Update(UserRole UserRole)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserRoleID", SqlDbType = SqlDbType.BigInt, Value = UserRole.UserRoleID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserRole.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = UserRole.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserRole.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserRole.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = UserRole.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserRole.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int64 UserRoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserRoleID", SqlDbType = SqlDbType.BigInt, Value = UserRoleID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual UserRole SelectByKey(Int32 UserID, Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                UserRole UserRole = new UserRole();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        UserRole = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserRole;
            }
        }

        public virtual bool UpdateByKey(UserRole UserRole, Int32 UserID, Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserRoleID", SqlDbType = SqlDbType.BigInt, Value = UserRole.UserRoleID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserRole.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = UserRole.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserRole.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserRole.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = UserRole.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID_Original", SqlDbType = SqlDbType.Int, Value = UserID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID_Original", SqlDbType = SqlDbType.Int, Value = RoleID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserRole.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 UserID, Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserRole_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override UserRole FillFromDataReader(DbDataReader dr)
        {
            UserRole UserRole = new UserRole { HasValue = true };

            UserRole.UserRoleID = Convert.ToInt64(dr["UserRoleID"]);
            UserRole.User.UserID = Convert.ToInt32(dr["UserID"]);
            UserRole.Role.RoleID = Convert.ToInt32(dr["RoleID"]);
            UserRole.Status = Convert.ToByte(dr["Status"]);
            UserRole.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            UserRole.SessionID = Convert.ToInt64(dr["SessionID"]);
            UserRole.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return UserRole;
        }
    }
}
