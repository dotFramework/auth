using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class RoleActionDataAccess : AuthDataAccessBase<Int32, RoleAction, RoleActionCollection>, IRoleActionDataAccess
    {
        public override RoleAction Select(Int32 RoleActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleActionID", SqlDbType = SqlDbType.Int, Value = RoleActionID });

                RoleAction RoleAction = new RoleAction();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        RoleAction = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleAction;
            }
        }

        public override RoleActionCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                RoleActionCollection RoleActionCollection = new RoleActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleActionCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override RoleActionCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                RoleActionCollection RoleActionCollection = new RoleActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    RoleActionCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleActionCollection;
            }
        }

        public virtual RoleActionCollection SelectByRole(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_SelectByRole]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                RoleActionCollection RoleActionCollection = new RoleActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleActionCollection;
            }
        }

        public virtual Int32 SelectCountByRole(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_SelectCountByRole]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual RoleActionCollection SelectByApplicableAction(Int32 ApplicableActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_SelectByApplicableAction]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = ApplicableActionID });

                RoleActionCollection RoleActionCollection = new RoleActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleActionCollection;
            }
        }

        public virtual Int32 SelectCountByApplicableAction(Int32 ApplicableActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_SelectCountByApplicableAction]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = ApplicableActionID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(RoleAction RoleAction)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleActionID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleAction.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = RoleAction.ApplicableAction.ApplicableActionID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RoleAction.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RoleAction.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RoleAction.RoleActionID = Convert.ToInt32(command.Parameters["@RoleActionID"].Value);
                return true;
            }
        }

        public override bool Update(RoleAction RoleAction)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleActionID", SqlDbType = SqlDbType.Int, Value = RoleAction.RoleActionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleAction.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = RoleAction.ApplicableAction.ApplicableActionID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RoleAction.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RoleAction.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = RoleAction.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RoleAction.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 RoleActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleActionID", SqlDbType = SqlDbType.Int, Value = RoleActionID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual RoleAction SelectByKey(Int32 RoleID, Int32 ApplicableActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = ApplicableActionID });

                RoleAction RoleAction = new RoleAction();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        RoleAction = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleAction;
            }
        }

        public virtual bool UpdateByKey(RoleAction RoleAction, Int32 RoleID, Int32 ApplicableActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleActionID", SqlDbType = SqlDbType.Int, Value = RoleAction.RoleActionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleAction.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = RoleAction.ApplicableAction.ApplicableActionID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RoleAction.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RoleAction.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = RoleAction.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID_Original", SqlDbType = SqlDbType.Int, Value = RoleID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID_Original", SqlDbType = SqlDbType.Int, Value = ApplicableActionID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RoleAction.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 RoleID, Int32 ApplicableActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleAction_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = ApplicableActionID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override RoleAction FillFromDataReader(DbDataReader dr)
        {
            RoleAction RoleAction = new RoleAction { HasValue = true };

            RoleAction.RoleActionID = Convert.ToInt32(dr["RoleActionID"]);
            RoleAction.Role.RoleID = Convert.ToInt32(dr["RoleID"]);
            RoleAction.ApplicableAction.ApplicableActionID = Convert.ToInt32(dr["ApplicableActionID"]);
            RoleAction.Status = Convert.ToByte(dr["Status"]);
            RoleAction.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            RoleAction.SessionID = Convert.ToInt64(dr["SessionID"]);
            RoleAction.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return RoleAction;
        }
    }
}
