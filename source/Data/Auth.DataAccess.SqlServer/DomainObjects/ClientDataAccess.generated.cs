using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ClientDataAccess : AuthDataAccessBase<Int32, Client, ClientCollection>, IClientDataAccess
    {
        public override Client Select(Int32 ClientID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = ClientID });

                Client Client = new Client();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Client = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Client;
            }
        }

        public override ClientCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ClientCollection ClientCollection = new ClientCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ClientCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ClientCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ClientCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ClientCollection ClientCollection = new ClientCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ClientCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ClientCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ClientCollection;
            }
        }

        public override bool Insert(Client Client)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientUID", SqlDbType = SqlDbType.VarChar, Value = Client.ClientUID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientSecret", SqlDbType = SqlDbType.VarChar, Value = Client.ClientSecret.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientIdentifier", SqlDbType = SqlDbType.NVarChar, Value = Client.ClientIdentifier.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientName", SqlDbType = SqlDbType.NVarChar, Value = Client.ClientName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SecurityStamp", SqlDbType = SqlDbType.NVarChar, Value = Client.SecurityStamp.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceRestricted", SqlDbType = SqlDbType.Bit, Value = Client.DeviceRestricted.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Client.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Client.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Client.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Client.ClientID = Convert.ToInt32(command.Parameters["@ClientID"].Value);
                Client.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Update(Client Client)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = Client.ClientID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientUID", SqlDbType = SqlDbType.VarChar, Value = Client.ClientUID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientSecret", SqlDbType = SqlDbType.VarChar, Value = Client.ClientSecret.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientIdentifier", SqlDbType = SqlDbType.NVarChar, Value = Client.ClientIdentifier.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientName", SqlDbType = SqlDbType.NVarChar, Value = Client.ClientName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SecurityStamp", SqlDbType = SqlDbType.NVarChar, Value = Client.SecurityStamp.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceRestricted", SqlDbType = SqlDbType.Bit, Value = Client.DeviceRestricted.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Client.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Client.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Client.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Client.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 ClientID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = ClientID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual Client SelectByKey(String ClientIdentifier)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientIdentifier", SqlDbType = SqlDbType.NVarChar, Value = ClientIdentifier });

                Client Client = new Client();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Client = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Client;
            }
        }

        public virtual bool UpdateByKey(Client Client, String ClientIdentifier)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = Client.ClientID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientUID", SqlDbType = SqlDbType.VarChar, Value = Client.ClientUID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientSecret", SqlDbType = SqlDbType.VarChar, Value = Client.ClientSecret.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientIdentifier", SqlDbType = SqlDbType.NVarChar, Value = Client.ClientIdentifier.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientName", SqlDbType = SqlDbType.NVarChar, Value = Client.ClientName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SecurityStamp", SqlDbType = SqlDbType.NVarChar, Value = Client.SecurityStamp.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceRestricted", SqlDbType = SqlDbType.Bit, Value = Client.DeviceRestricted.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Client.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Client.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Client.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientIdentifier_Original", SqlDbType = SqlDbType.NVarChar, Value = ClientIdentifier });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Client.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String ClientIdentifier)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Client_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientIdentifier", SqlDbType = SqlDbType.NVarChar, Value = ClientIdentifier });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override Client FillFromDataReader(DbDataReader dr)
        {
            Client Client = new Client { HasValue = true };

            Client.ClientID = Convert.ToInt32(dr["ClientID"]);
            Client.ClientUID = Convert.ToString(dr["ClientUID"]);
            Client.ClientSecret = Convert.ToString(dr["ClientSecret"]);
            Client.ClientIdentifier = Convert.ToString(dr["ClientIdentifier"]);
            Client.ClientName = Convert.ToString(dr["ClientName"]);
            Client.SecurityStamp = Convert.ToString(dr["SecurityStamp"]);
            Client.DeviceRestricted = Convert.ToBoolean(dr["DeviceRestricted"]);
            Client.Status = Convert.ToByte(dr["Status"]);
            Client.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            Client.SessionID = Convert.ToInt64(dr["SessionID"]);
            Client.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return Client;
        }
    }
}
