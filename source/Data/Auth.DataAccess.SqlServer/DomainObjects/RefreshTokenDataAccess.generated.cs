using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class RefreshTokenDataAccess : AuthDataAccessBase<Int64, RefreshToken, RefreshTokenCollection>, IRefreshTokenDataAccess
    {
        public override RefreshToken Select(Int64 RefreshTokenID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RefreshTokenID", SqlDbType = SqlDbType.BigInt, Value = RefreshTokenID });

                RefreshToken RefreshToken = new RefreshToken();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        RefreshToken = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RefreshToken;
            }
        }

        public override RefreshTokenCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                RefreshTokenCollection RefreshTokenCollection = new RefreshTokenCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RefreshTokenCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RefreshTokenCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override RefreshTokenCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                RefreshTokenCollection RefreshTokenCollection = new RefreshTokenCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RefreshTokenCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    RefreshTokenCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return RefreshTokenCollection;
            }
        }

        public virtual RefreshTokenCollection SelectByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_SelectByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                RefreshTokenCollection RefreshTokenCollection = new RefreshTokenCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RefreshTokenCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RefreshTokenCollection;
            }
        }

        public virtual Int32 SelectCountByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_SelectCountByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(RefreshToken RefreshToken)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RefreshTokenID", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = RefreshToken.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Token", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.Token.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@GroupToken", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.GroupToken.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AppIdentifier", SqlDbType = SqlDbType.VarChar, Value = RefreshToken.AppIdentifier.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IssuedUtc", SqlDbType = SqlDbType.DateTime, Value = RefreshToken.IssuedUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ExpiresUtc", SqlDbType = SqlDbType.DateTime, Value = RefreshToken.ExpiresUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProtectedTicket", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.ProtectedTicket.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RefreshToken.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RefreshToken.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RefreshToken.RefreshTokenID = Convert.ToInt64(command.Parameters["@RefreshTokenID"].Value);
                return true;
            }
        }

        public override bool Update(RefreshToken RefreshToken)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RefreshTokenID", SqlDbType = SqlDbType.BigInt, Value = RefreshToken.RefreshTokenID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = RefreshToken.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Token", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.Token.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@GroupToken", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.GroupToken.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IssuedUtc", SqlDbType = SqlDbType.DateTime, Value = RefreshToken.IssuedUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ExpiresUtc", SqlDbType = SqlDbType.DateTime, Value = RefreshToken.ExpiresUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProtectedTicket", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.ProtectedTicket.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RefreshToken.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RefreshToken.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = RefreshToken.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RefreshToken.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int64 RefreshTokenID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RefreshTokenID", SqlDbType = SqlDbType.BigInt, Value = RefreshTokenID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual RefreshToken SelectByKey(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                RefreshToken RefreshToken = new RefreshToken();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        RefreshToken = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RefreshToken;
            }
        }

        public virtual bool UpdateByKey(RefreshToken RefreshToken, Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RefreshTokenID", SqlDbType = SqlDbType.BigInt, Value = RefreshToken.RefreshTokenID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = RefreshToken.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Token", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.Token.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Token", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.Token.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@GroupToken", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.GroupToken.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IssuedUtc", SqlDbType = SqlDbType.DateTime, Value = RefreshToken.IssuedUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ExpiresUtc", SqlDbType = SqlDbType.DateTime, Value = RefreshToken.ExpiresUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProtectedTicket", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.ProtectedTicket.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RefreshToken.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RefreshToken.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = RefreshToken.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID_Original", SqlDbType = SqlDbType.Int, Value = UserID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RefreshToken.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override RefreshToken FillFromDataReader(DbDataReader dr)
        {
            RefreshToken RefreshToken = new RefreshToken { HasValue = true };

            RefreshToken.RefreshTokenID = Convert.ToInt64(dr["RefreshTokenID"]);
            RefreshToken.User.UserID = Convert.ToInt32(dr["UserID"]);
            RefreshToken.Token = Convert.ToString(dr["Token"]);
            RefreshToken.GroupToken = Convert.ToString(dr["GroupToken"]);
            if (dr["AppIdentifier"] != DBNull.Value)
                RefreshToken.AppIdentifier = Convert.ToString(dr["AppIdentifier"]);
            RefreshToken.IssuedUtc = Convert.ToDateTime(dr["IssuedUtc"]);
            RefreshToken.ExpiresUtc = Convert.ToDateTime(dr["ExpiresUtc"]);
            RefreshToken.ProtectedTicket = Convert.ToString(dr["ProtectedTicket"]);
            RefreshToken.Status = Convert.ToByte(dr["Status"]);
            RefreshToken.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            RefreshToken.SessionID = Convert.ToInt64(dr["SessionID"]);
            RefreshToken.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return RefreshToken;
        }
    }
}
