using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ApplicableActionDataAccess : AuthDataAccessBase<Int32, ApplicableAction, ApplicableActionCollection>, IApplicableActionDataAccess
    {
        public override ApplicableAction Select(Int32 ApplicableActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = ApplicableActionID });

                ApplicableAction ApplicableAction = new ApplicableAction();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ApplicableAction = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableAction;
            }
        }

        public override ApplicableActionCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ApplicableActionCollection ApplicableActionCollection = new ApplicableActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicableActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableActionCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ApplicableActionCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ApplicableActionCollection ApplicableActionCollection = new ApplicableActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicableActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ApplicableActionCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableActionCollection;
            }
        }

        public virtual ApplicableActionCollection SelectByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_SelectByApplicationHierarchy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                ApplicableActionCollection ApplicableActionCollection = new ApplicableActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicableActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableActionCollection;
            }
        }

        public virtual Int32 SelectCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_SelectCountByApplicationHierarchy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual ApplicableActionCollection SelectByViewAction(Int32 ViewActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_SelectByViewAction]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ViewActionID });

                ApplicableActionCollection ApplicableActionCollection = new ApplicableActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicableActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableActionCollection;
            }
        }

        public virtual Int32 SelectCountByViewAction(Int32 ViewActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_SelectCountByViewAction]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ViewActionID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(ApplicableAction ApplicableAction)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicableAction.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ApplicableAction.ViewAction.ViewActionID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicableAction.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicableAction.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicableAction.ApplicableActionID = Convert.ToInt32(command.Parameters["@ApplicableActionID"].Value);
                return true;
            }
        }

        public override bool Update(ApplicableAction ApplicableAction)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = ApplicableAction.ApplicableActionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicableAction.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ApplicableAction.ViewAction.ViewActionID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicableAction.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicableAction.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ApplicableAction.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicableAction.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 ApplicableActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = ApplicableActionID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual ApplicableAction SelectByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ViewActionID });

                ApplicableAction ApplicableAction = new ApplicableAction();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ApplicableAction = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableAction;
            }
        }

        public virtual bool UpdateByKey(ApplicableAction ApplicableAction, Int32 ApplicationHierarchyID, Int32 ViewActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableActionID", SqlDbType = SqlDbType.Int, Value = ApplicableAction.ApplicableActionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicableAction.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ApplicableAction.ViewAction.ViewActionID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicableAction.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicableAction.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ApplicableAction.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID_Original", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID_Original", SqlDbType = SqlDbType.Int, Value = ViewActionID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicableAction.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableAction_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ViewActionID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override ApplicableAction FillFromDataReader(DbDataReader dr)
        {
            ApplicableAction ApplicableAction = new ApplicableAction { HasValue = true };

            ApplicableAction.ApplicableActionID = Convert.ToInt32(dr["ApplicableActionID"]);
            ApplicableAction.ApplicationHierarchy.ApplicationHierarchyID = Convert.ToInt32(dr["ApplicationHierarchyID"]);
            ApplicableAction.ViewAction.ViewActionID = Convert.ToInt32(dr["ViewActionID"]);
            ApplicableAction.Status = Convert.ToByte(dr["Status"]);
            ApplicableAction.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            ApplicableAction.SessionID = Convert.ToInt64(dr["SessionID"]);
            ApplicableAction.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return ApplicableAction;
        }
    }
}
