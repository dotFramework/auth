using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ViewActionDataAccess : AuthDataAccessBase<Int32, ViewAction, ViewActionCollection>, IViewActionDataAccess
    {
        public override ViewAction Select(Int32 ViewActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ViewActionID });

                ViewAction ViewAction = new ViewAction();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ViewAction = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ViewAction;
            }
        }

        public override ViewActionCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ViewActionCollection ViewActionCollection = new ViewActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ViewActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ViewActionCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ViewActionCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ViewActionCollection ViewActionCollection = new ViewActionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ViewActionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ViewActionCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ViewActionCollection;
            }
        }

        public override bool Insert(ViewAction ViewAction)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = ViewAction.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ViewAction.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ViewAction.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ViewAction.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IsForAll", SqlDbType = SqlDbType.Bit, Value = ViewAction.IsForAll.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ViewAction.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ViewAction.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ViewAction.ViewActionID = Convert.ToInt32(command.Parameters["@ViewActionID"].Value);
                return true;
            }
        }

        public override bool Update(ViewAction ViewAction)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ViewAction.ViewActionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = ViewAction.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ViewAction.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ViewAction.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ViewAction.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IsForAll", SqlDbType = SqlDbType.Bit, Value = ViewAction.IsForAll.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ViewAction.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ViewAction.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ViewAction.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ViewAction.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 ViewActionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ViewActionID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual ViewAction SelectByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                ViewAction ViewAction = new ViewAction();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ViewAction = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ViewAction;
            }
        }

        public virtual bool UpdateByKey(ViewAction ViewAction, String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewActionID", SqlDbType = SqlDbType.Int, Value = ViewAction.ViewActionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = ViewAction.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ViewAction.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ViewAction.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ViewAction.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IsForAll", SqlDbType = SqlDbType.Bit, Value = ViewAction.IsForAll.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ViewAction.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ViewAction.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ViewAction.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code_Original", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ViewAction.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewAction_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override ViewAction FillFromDataReader(DbDataReader dr)
        {
            ViewAction ViewAction = new ViewAction { HasValue = true };

            ViewAction.ViewActionID = Convert.ToInt32(dr["ViewActionID"]);
            ViewAction.Code = Convert.ToString(dr["Code"]);
            ViewAction.Name = Convert.ToString(dr["Name"]);
            if (dr["Description"] != DBNull.Value)
                ViewAction.Description = Convert.ToString(dr["Description"]);
            ViewAction.NeedAuthorization = Convert.ToBoolean(dr["NeedAuthorization"]);
            ViewAction.IsForAll = Convert.ToBoolean(dr["IsForAll"]);
            ViewAction.Status = Convert.ToByte(dr["Status"]);
            ViewAction.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            ViewAction.SessionID = Convert.ToInt64(dr["SessionID"]);
            ViewAction.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return ViewAction;
        }
    }
}
