using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ApplicationDataAccess : AuthDataAccessBase<Int32, Application, ApplicationCollection>, IApplicationDataAccess
    {
        public override Application Select(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                Application Application = new Application();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Application = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Application;
            }
        }

        public override ApplicationCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ApplicationCollection ApplicationCollection = new ApplicationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ApplicationCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ApplicationCollection ApplicationCollection = new ApplicationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ApplicationCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationCollection;
            }
        }

        public override bool Insert(Application Application)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Application.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AppSecret", SqlDbType = SqlDbType.VarChar, Value = Application.AppSecret.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Application.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = Application.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Path", SqlDbType = SqlDbType.NVarChar, Value = Application.Path.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = Application.Client?.ClientID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SSOCallBackURI", SqlDbType = SqlDbType.VarChar, Value = Application.SSOCallBackURI.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Application.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Application.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Application.ApplicationID = Convert.ToInt32(command.Parameters["@ApplicationID"].Value);
                return true;
            }
        }

        public override bool Update(Application Application)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = Application.ApplicationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Application.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AppSecret", SqlDbType = SqlDbType.VarChar, Value = Application.AppSecret.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Application.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = Application.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Path", SqlDbType = SqlDbType.NVarChar, Value = Application.Path.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = Application.Client?.ClientID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SSOCallBackURI", SqlDbType = SqlDbType.VarChar, Value = Application.SSOCallBackURI.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Application.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Application.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Application.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Application.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual Application SelectByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                Application Application = new Application();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Application = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Application;
            }
        }

        public virtual bool UpdateByKey(Application Application, String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = Application.ApplicationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Application.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AppSecret", SqlDbType = SqlDbType.VarChar, Value = Application.AppSecret.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Application.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = Application.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Path", SqlDbType = SqlDbType.NVarChar, Value = Application.Path.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = Application.Client?.ClientID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SSOCallBackURI", SqlDbType = SqlDbType.VarChar, Value = Application.SSOCallBackURI.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Application.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Application.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Application.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code_Original", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Application.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Application_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override Application FillFromDataReader(DbDataReader dr)
        {
            Application Application = new Application { HasValue = true };

            Application.ApplicationID = Convert.ToInt32(dr["ApplicationID"]);
            Application.Code = Convert.ToString(dr["Code"]);
            Application.AppSecret = Convert.ToString(dr["AppSecret"]);
            Application.Name = Convert.ToString(dr["Name"]);
            if (dr["Description"] != DBNull.Value)
                Application.Description = Convert.ToString(dr["Description"]);
            if (dr["Path"] != DBNull.Value)
                Application.Path = Convert.ToString(dr["Path"]);
            if (dr["ClientID"] != DBNull.Value)
                Application.Client.ClientID = Convert.ToInt32(dr["ClientID"]);
            if (dr["SSOCallBackURI"] != DBNull.Value)
                Application.SSOCallBackURI = Convert.ToString(dr["SSOCallBackURI"]);
            Application.Status = Convert.ToByte(dr["Status"]);
            Application.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            Application.SessionID = Convert.ToInt64(dr["SessionID"]);
            Application.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return Application;
        }
    }
}
