using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ApplicableOperationDataAccess : AuthDataAccessBase<Int32, ApplicableOperation, ApplicableOperationCollection>, IApplicableOperationDataAccess
    {
        public override ApplicableOperation Select(Int32 ApplicableOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperationID });

                ApplicableOperation ApplicableOperation = new ApplicableOperation();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ApplicableOperation = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableOperation;
            }
        }

        public override ApplicableOperationCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ApplicableOperationCollection ApplicableOperationCollection = new ApplicableOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicableOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableOperationCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ApplicableOperationCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ApplicableOperationCollection ApplicableOperationCollection = new ApplicableOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicableOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ApplicableOperationCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableOperationCollection;
            }
        }

        public virtual ApplicableOperationCollection SelectByApplicationEndpoint(Int32 ApplicationEndpointID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_SelectByApplicationEndpoint]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpointID });

                ApplicableOperationCollection ApplicableOperationCollection = new ApplicableOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicableOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableOperationCollection;
            }
        }

        public virtual Int32 SelectCountByApplicationEndpoint(Int32 ApplicationEndpointID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_SelectCountByApplicationEndpoint]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpointID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual ApplicableOperationCollection SelectByServiceOperation(Int32 ServiceOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_SelectByServiceOperation]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ServiceOperationID });

                ApplicableOperationCollection ApplicableOperationCollection = new ApplicableOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicableOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableOperationCollection;
            }
        }

        public virtual Int32 SelectCountByServiceOperation(Int32 ServiceOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_SelectCountByServiceOperation]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ServiceOperationID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(ApplicableOperation ApplicableOperation)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicableOperation.ApplicationEndpoint.ApplicationEndpointID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperation.ServiceOperation.ServiceOperationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicableOperation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicableOperation.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicableOperation.ApplicableOperationID = Convert.ToInt32(command.Parameters["@ApplicableOperationID"].Value);
                return true;
            }
        }

        public override bool Update(ApplicableOperation ApplicableOperation)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperation.ApplicableOperationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicableOperation.ApplicationEndpoint.ApplicationEndpointID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperation.ServiceOperation.ServiceOperationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicableOperation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicableOperation.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ApplicableOperation.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicableOperation.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 ApplicableOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual ApplicableOperation SelectByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpointID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ServiceOperationID });

                ApplicableOperation ApplicableOperation = new ApplicableOperation();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ApplicableOperation = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicableOperation;
            }
        }

        public virtual bool UpdateByKey(ApplicableOperation ApplicableOperation, Int32 ApplicationEndpointID, Int32 ServiceOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperation.ApplicableOperationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicableOperation.ApplicationEndpoint.ApplicationEndpointID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperation.ServiceOperation.ServiceOperationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicableOperation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicableOperation.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ApplicableOperation.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID_Original", SqlDbType = SqlDbType.Int, Value = ApplicationEndpointID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID_Original", SqlDbType = SqlDbType.Int, Value = ServiceOperationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicableOperation.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicableOperation_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationEndpointID", SqlDbType = SqlDbType.Int, Value = ApplicationEndpointID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ServiceOperationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override ApplicableOperation FillFromDataReader(DbDataReader dr)
        {
            ApplicableOperation ApplicableOperation = new ApplicableOperation { HasValue = true };

            ApplicableOperation.ApplicableOperationID = Convert.ToInt32(dr["ApplicableOperationID"]);
            ApplicableOperation.ApplicationEndpoint.ApplicationEndpointID = Convert.ToInt32(dr["ApplicationEndpointID"]);
            ApplicableOperation.ServiceOperation.ServiceOperationID = Convert.ToInt32(dr["ServiceOperationID"]);
            ApplicableOperation.Status = Convert.ToByte(dr["Status"]);
            ApplicableOperation.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            ApplicableOperation.SessionID = Convert.ToInt64(dr["SessionID"]);
            ApplicableOperation.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return ApplicableOperation;
        }
    }
}
