using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ServiceOperationDataAccess : AuthDataAccessBase<Int32, ServiceOperation, ServiceOperationCollection>, IServiceOperationDataAccess
    {
        public override ServiceOperation Select(Int32 ServiceOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ServiceOperationID });

                ServiceOperation ServiceOperation = new ServiceOperation();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ServiceOperation = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ServiceOperation;
            }
        }

        public override ServiceOperationCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ServiceOperationCollection ServiceOperationCollection = new ServiceOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ServiceOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ServiceOperationCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ServiceOperationCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ServiceOperationCollection ServiceOperationCollection = new ServiceOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ServiceOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ServiceOperationCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ServiceOperationCollection;
            }
        }

        public override bool Insert(ServiceOperation ServiceOperation)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = ServiceOperation.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ServiceOperation.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ServiceOperation.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ServiceOperation.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IsForAll", SqlDbType = SqlDbType.Bit, Value = ServiceOperation.IsForAll.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ServiceOperation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ServiceOperation.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ServiceOperation.ServiceOperationID = Convert.ToInt32(command.Parameters["@ServiceOperationID"].Value);
                return true;
            }
        }

        public override bool Update(ServiceOperation ServiceOperation)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ServiceOperation.ServiceOperationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = ServiceOperation.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ServiceOperation.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ServiceOperation.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ServiceOperation.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IsForAll", SqlDbType = SqlDbType.Bit, Value = ServiceOperation.IsForAll.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ServiceOperation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ServiceOperation.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ServiceOperation.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ServiceOperation.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 ServiceOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ServiceOperationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual ServiceOperation SelectByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                ServiceOperation ServiceOperation = new ServiceOperation();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ServiceOperation = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ServiceOperation;
            }
        }

        public virtual bool UpdateByKey(ServiceOperation ServiceOperation, String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ServiceOperationID", SqlDbType = SqlDbType.Int, Value = ServiceOperation.ServiceOperationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = ServiceOperation.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ServiceOperation.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ServiceOperation.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ServiceOperation.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IsForAll", SqlDbType = SqlDbType.Bit, Value = ServiceOperation.IsForAll.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ServiceOperation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ServiceOperation.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ServiceOperation.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code_Original", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ServiceOperation.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ServiceOperation_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override ServiceOperation FillFromDataReader(DbDataReader dr)
        {
            ServiceOperation ServiceOperation = new ServiceOperation { HasValue = true };

            ServiceOperation.ServiceOperationID = Convert.ToInt32(dr["ServiceOperationID"]);
            ServiceOperation.Code = Convert.ToString(dr["Code"]);
            ServiceOperation.Name = Convert.ToString(dr["Name"]);
            if (dr["Description"] != DBNull.Value)
                ServiceOperation.Description = Convert.ToString(dr["Description"]);
            ServiceOperation.NeedAuthorization = Convert.ToBoolean(dr["NeedAuthorization"]);
            ServiceOperation.IsForAll = Convert.ToBoolean(dr["IsForAll"]);
            ServiceOperation.Status = Convert.ToByte(dr["Status"]);
            ServiceOperation.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            ServiceOperation.SessionID = Convert.ToInt64(dr["SessionID"]);
            ServiceOperation.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return ServiceOperation;
        }
    }
}
