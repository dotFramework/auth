using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class RoleOperationDataAccess : AuthDataAccessBase<Int32, RoleOperation, RoleOperationCollection>, IRoleOperationDataAccess
    {
        public override RoleOperation Select(Int32 RoleOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleOperationID", SqlDbType = SqlDbType.Int, Value = RoleOperationID });

                RoleOperation RoleOperation = new RoleOperation();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        RoleOperation = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleOperation;
            }
        }

        public override RoleOperationCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                RoleOperationCollection RoleOperationCollection = new RoleOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleOperationCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override RoleOperationCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                RoleOperationCollection RoleOperationCollection = new RoleOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    RoleOperationCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleOperationCollection;
            }
        }

        public virtual RoleOperationCollection SelectByRole(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_SelectByRole]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                RoleOperationCollection RoleOperationCollection = new RoleOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleOperationCollection;
            }
        }

        public virtual Int32 SelectCountByRole(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_SelectCountByRole]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual RoleOperationCollection SelectByApplicableOperation(Int32 ApplicableOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_SelectByApplicableOperation]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperationID });

                RoleOperationCollection RoleOperationCollection = new RoleOperationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleOperationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleOperationCollection;
            }
        }

        public virtual Int32 SelectCountByApplicableOperation(Int32 ApplicableOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_SelectCountByApplicableOperation]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperationID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(RoleOperation RoleOperation)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleOperationID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleOperation.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = RoleOperation.ApplicableOperation.ApplicableOperationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RoleOperation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RoleOperation.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RoleOperation.RoleOperationID = Convert.ToInt32(command.Parameters["@RoleOperationID"].Value);
                return true;
            }
        }

        public override bool Update(RoleOperation RoleOperation)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleOperationID", SqlDbType = SqlDbType.Int, Value = RoleOperation.RoleOperationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleOperation.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = RoleOperation.ApplicableOperation.ApplicableOperationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RoleOperation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RoleOperation.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = RoleOperation.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RoleOperation.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 RoleOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleOperationID", SqlDbType = SqlDbType.Int, Value = RoleOperationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual RoleOperation SelectByKey(Int32 RoleID, Int32 ApplicableOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperationID });

                RoleOperation RoleOperation = new RoleOperation();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        RoleOperation = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleOperation;
            }
        }

        public virtual bool UpdateByKey(RoleOperation RoleOperation, Int32 RoleID, Int32 ApplicableOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleOperationID", SqlDbType = SqlDbType.Int, Value = RoleOperation.RoleOperationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleOperation.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = RoleOperation.ApplicableOperation.ApplicableOperationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RoleOperation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RoleOperation.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = RoleOperation.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID_Original", SqlDbType = SqlDbType.Int, Value = RoleID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID_Original", SqlDbType = SqlDbType.Int, Value = ApplicableOperationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RoleOperation.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 RoleID, Int32 ApplicableOperationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleOperation_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicableOperationID", SqlDbType = SqlDbType.Int, Value = ApplicableOperationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override RoleOperation FillFromDataReader(DbDataReader dr)
        {
            RoleOperation RoleOperation = new RoleOperation { HasValue = true };

            RoleOperation.RoleOperationID = Convert.ToInt32(dr["RoleOperationID"]);
            RoleOperation.Role.RoleID = Convert.ToInt32(dr["RoleID"]);
            RoleOperation.ApplicableOperation.ApplicableOperationID = Convert.ToInt32(dr["ApplicableOperationID"]);
            RoleOperation.Status = Convert.ToByte(dr["Status"]);
            RoleOperation.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            RoleOperation.SessionID = Convert.ToInt64(dr["SessionID"]);
            RoleOperation.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return RoleOperation;
        }
    }
}
