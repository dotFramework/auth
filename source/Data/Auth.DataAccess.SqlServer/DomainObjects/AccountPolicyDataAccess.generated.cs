using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class AccountPolicyDataAccess : AuthDataAccessBase<Byte, AccountPolicy, AccountPolicyCollection>, IAccountPolicyDataAccess
    {
        public override AccountPolicy Select(Byte AccountPolicyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Value = AccountPolicyID });

                AccountPolicy AccountPolicy = new AccountPolicy();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        AccountPolicy = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return AccountPolicy;
            }
        }

        public override AccountPolicyCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                AccountPolicyCollection AccountPolicyCollection = new AccountPolicyCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        AccountPolicyCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return AccountPolicyCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override AccountPolicyCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                AccountPolicyCollection AccountPolicyCollection = new AccountPolicyCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        AccountPolicyCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    AccountPolicyCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return AccountPolicyCollection;
            }
        }

        public override bool Insert(AccountPolicy AccountPolicy)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PolicyName", SqlDbType = SqlDbType.NVarChar, Value = AccountPolicy.PolicyName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@MaxAccountAge", SqlDbType = SqlDbType.Int, Value = AccountPolicy.MaxAccountAge.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@MaxPasswordAge", SqlDbType = SqlDbType.Int, Value = AccountPolicy.MaxPasswordAge.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@MaxPasswordAgeReminder", SqlDbType = SqlDbType.Int, Value = AccountPolicy.MaxPasswordAgeReminder.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EnforcePasswordHistory", SqlDbType = SqlDbType.Int, Value = AccountPolicy.EnforcePasswordHistory.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AutoDeleteAccount", SqlDbType = SqlDbType.Int, Value = AccountPolicy.AutoDeleteAccount.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutThresholdMethod", SqlDbType = SqlDbType.SmallInt, Value = AccountPolicy.LockoutThresholdMethod.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutThreshold", SqlDbType = SqlDbType.Int, Value = AccountPolicy.LockoutThreshold.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockType", SqlDbType = SqlDbType.SmallInt, Value = AccountPolicy.LockType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutDuration", SqlDbType = SqlDbType.Int, Value = AccountPolicy.LockoutDuration.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequiredLength", SqlDbType = SqlDbType.Int, Value = AccountPolicy.RequiredLength.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireNonLetterOrDigit", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireNonLetterOrDigit.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireDigit", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireDigit.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireLowercase", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireLowercase.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireUppercase", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireUppercase.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = AccountPolicy.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = AccountPolicy.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = AccountPolicy.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                AccountPolicy.AccountPolicyID = Convert.ToByte(command.Parameters["@AccountPolicyID"].Value);
                AccountPolicy.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Update(AccountPolicy AccountPolicy)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Value = AccountPolicy.AccountPolicyID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PolicyName", SqlDbType = SqlDbType.NVarChar, Value = AccountPolicy.PolicyName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@MaxAccountAge", SqlDbType = SqlDbType.Int, Value = AccountPolicy.MaxAccountAge.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@MaxPasswordAge", SqlDbType = SqlDbType.Int, Value = AccountPolicy.MaxPasswordAge.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@MaxPasswordAgeReminder", SqlDbType = SqlDbType.Int, Value = AccountPolicy.MaxPasswordAgeReminder.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EnforcePasswordHistory", SqlDbType = SqlDbType.Int, Value = AccountPolicy.EnforcePasswordHistory.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AutoDeleteAccount", SqlDbType = SqlDbType.Int, Value = AccountPolicy.AutoDeleteAccount.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutThresholdMethod", SqlDbType = SqlDbType.SmallInt, Value = AccountPolicy.LockoutThresholdMethod.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutThreshold", SqlDbType = SqlDbType.Int, Value = AccountPolicy.LockoutThreshold.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockType", SqlDbType = SqlDbType.SmallInt, Value = AccountPolicy.LockType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutDuration", SqlDbType = SqlDbType.Int, Value = AccountPolicy.LockoutDuration.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequiredLength", SqlDbType = SqlDbType.Int, Value = AccountPolicy.RequiredLength.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireNonLetterOrDigit", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireNonLetterOrDigit.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireDigit", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireDigit.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireLowercase", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireLowercase.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireUppercase", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireUppercase.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = AccountPolicy.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = AccountPolicy.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = AccountPolicy.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                AccountPolicy.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Byte AccountPolicyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Value = AccountPolicyID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual AccountPolicy SelectByKey(String PolicyName)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PolicyName", SqlDbType = SqlDbType.NVarChar, Value = PolicyName });

                AccountPolicy AccountPolicy = new AccountPolicy();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        AccountPolicy = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return AccountPolicy;
            }
        }

        public virtual bool UpdateByKey(AccountPolicy AccountPolicy, String PolicyName)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@AccountPolicyID", SqlDbType = SqlDbType.TinyInt, Value = AccountPolicy.AccountPolicyID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PolicyName", SqlDbType = SqlDbType.NVarChar, Value = AccountPolicy.PolicyName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@MaxAccountAge", SqlDbType = SqlDbType.Int, Value = AccountPolicy.MaxAccountAge.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@MaxPasswordAge", SqlDbType = SqlDbType.Int, Value = AccountPolicy.MaxPasswordAge.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@MaxPasswordAgeReminder", SqlDbType = SqlDbType.Int, Value = AccountPolicy.MaxPasswordAgeReminder.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EnforcePasswordHistory", SqlDbType = SqlDbType.Int, Value = AccountPolicy.EnforcePasswordHistory.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AutoDeleteAccount", SqlDbType = SqlDbType.Int, Value = AccountPolicy.AutoDeleteAccount.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutThresholdMethod", SqlDbType = SqlDbType.SmallInt, Value = AccountPolicy.LockoutThresholdMethod.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutThreshold", SqlDbType = SqlDbType.Int, Value = AccountPolicy.LockoutThreshold.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockType", SqlDbType = SqlDbType.SmallInt, Value = AccountPolicy.LockType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LockoutDuration", SqlDbType = SqlDbType.Int, Value = AccountPolicy.LockoutDuration.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequiredLength", SqlDbType = SqlDbType.Int, Value = AccountPolicy.RequiredLength.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireNonLetterOrDigit", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireNonLetterOrDigit.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireDigit", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireDigit.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireLowercase", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireLowercase.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RequireUppercase", SqlDbType = SqlDbType.Bit, Value = AccountPolicy.RequireUppercase.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = AccountPolicy.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = AccountPolicy.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = AccountPolicy.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PolicyName_Original", SqlDbType = SqlDbType.NVarChar, Value = PolicyName });

                command.ExecuteNonQuery();
                command.Connection.Close();

                AccountPolicy.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String PolicyName)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_AccountPolicy_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PolicyName", SqlDbType = SqlDbType.NVarChar, Value = PolicyName });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override AccountPolicy FillFromDataReader(DbDataReader dr)
        {
            AccountPolicy AccountPolicy = new AccountPolicy { HasValue = true };

            AccountPolicy.AccountPolicyID = Convert.ToByte(dr["AccountPolicyID"]);
            AccountPolicy.PolicyName = Convert.ToString(dr["PolicyName"]);
            AccountPolicy.MaxAccountAge = Convert.ToInt32(dr["MaxAccountAge"]);
            AccountPolicy.MaxPasswordAge = Convert.ToInt32(dr["MaxPasswordAge"]);
            AccountPolicy.MaxPasswordAgeReminder = Convert.ToInt32(dr["MaxPasswordAgeReminder"]);
            AccountPolicy.EnforcePasswordHistory = Convert.ToInt32(dr["EnforcePasswordHistory"]);
            AccountPolicy.AutoDeleteAccount = Convert.ToInt32(dr["AutoDeleteAccount"]);
            AccountPolicy.LockoutThresholdMethod = Convert.ToInt16(dr["LockoutThresholdMethod"]);
            AccountPolicy.LockoutThreshold = Convert.ToInt32(dr["LockoutThreshold"]);
            AccountPolicy.LockType = Convert.ToInt16(dr["LockType"]);
            AccountPolicy.LockoutDuration = Convert.ToInt32(dr["LockoutDuration"]);
            AccountPolicy.RequiredLength = Convert.ToInt32(dr["RequiredLength"]);
            AccountPolicy.RequireNonLetterOrDigit = Convert.ToBoolean(dr["RequireNonLetterOrDigit"]);
            AccountPolicy.RequireDigit = Convert.ToBoolean(dr["RequireDigit"]);
            AccountPolicy.RequireLowercase = Convert.ToBoolean(dr["RequireLowercase"]);
            AccountPolicy.RequireUppercase = Convert.ToBoolean(dr["RequireUppercase"]);
            AccountPolicy.Status = Convert.ToByte(dr["Status"]);
            AccountPolicy.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            AccountPolicy.SessionID = Convert.ToInt64(dr["SessionID"]);
            AccountPolicy.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return AccountPolicy;
        }
    }
}
