using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class RoleDataAccess : AuthDataAccessBase<Int32, Role, RoleCollection>, IRoleDataAccess
    {
        public override Role Select(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                Role Role = new Role();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Role = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Role;
            }
        }

        public override RoleCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                RoleCollection RoleCollection = new RoleCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override RoleCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                RoleCollection RoleCollection = new RoleCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    RoleCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleCollection;
            }
        }

        public virtual RoleCollection SelectByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_SelectByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                RoleCollection RoleCollection = new RoleCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleCollection;
            }
        }

        public virtual Int32 SelectCountByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_SelectCountByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual RoleCollection SelectByDesignation(Int16 DesignationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_SelectByDesignation]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Value = DesignationID });

                RoleCollection RoleCollection = new RoleCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleCollection;
            }
        }

        public virtual Int32 SelectCountByDesignation(Int16 DesignationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_SelectCountByDesignation]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Value = DesignationID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(Role Role)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = Role.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Value = Role.Designation.DesignationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Role.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Role.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = Role.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Role.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Role.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Role.RoleID = Convert.ToInt32(command.Parameters["@RoleID"].Value);
                return true;
            }
        }

        public override bool Update(Role Role)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = Role.RoleID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = Role.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Value = Role.Designation.DesignationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Role.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Role.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = Role.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Role.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Role.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Role.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Role.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual Role SelectByKey(Int32 ApplicationID, String Name)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Name });

                Role Role = new Role();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Role = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Role;
            }
        }

        public virtual bool UpdateByKey(Role Role, Int32 ApplicationID, String Name)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = Role.RoleID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = Role.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Value = Role.Designation.DesignationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Role.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Role.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = Role.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Role.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Role.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Role.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID_Original", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name_Original", SqlDbType = SqlDbType.NVarChar, Value = Name });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Role.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 ApplicationID, String Name)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Role_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Name });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override Role FillFromDataReader(DbDataReader dr)
        {
            Role Role = new Role { HasValue = true };

            Role.RoleID = Convert.ToInt32(dr["RoleID"]);
            Role.Application.ApplicationID = Convert.ToInt32(dr["ApplicationID"]);
            Role.Designation.DesignationID = Convert.ToInt16(dr["DesignationID"]);
            Role.Code = Convert.ToString(dr["Code"]);
            Role.Name = Convert.ToString(dr["Name"]);
            if (dr["Description"] != DBNull.Value)
                Role.Description = Convert.ToString(dr["Description"]);
            Role.Status = Convert.ToByte(dr["Status"]);
            Role.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            Role.SessionID = Convert.ToInt64(dr["SessionID"]);
            Role.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return Role;
        }
    }
}
