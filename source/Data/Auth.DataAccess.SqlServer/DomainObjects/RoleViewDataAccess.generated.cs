using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class RoleViewDataAccess : AuthDataAccessBase<Int32, RoleView, RoleViewCollection>, IRoleViewDataAccess
    {
        public override RoleView Select(Int32 RoleViewID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleViewID", SqlDbType = SqlDbType.Int, Value = RoleViewID });

                RoleView RoleView = new RoleView();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        RoleView = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleView;
            }
        }

        public override RoleViewCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                RoleViewCollection RoleViewCollection = new RoleViewCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleViewCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleViewCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override RoleViewCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                RoleViewCollection RoleViewCollection = new RoleViewCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleViewCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    RoleViewCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleViewCollection;
            }
        }

        public virtual RoleViewCollection SelectByRole(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_SelectByRole]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                RoleViewCollection RoleViewCollection = new RoleViewCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleViewCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleViewCollection;
            }
        }

        public virtual Int32 SelectCountByRole(Int32 RoleID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_SelectCountByRole]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual RoleViewCollection SelectByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_SelectByApplicationHierarchy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                RoleViewCollection RoleViewCollection = new RoleViewCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RoleViewCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleViewCollection;
            }
        }

        public virtual Int32 SelectCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_SelectCountByApplicationHierarchy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(RoleView RoleView)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleViewID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleView.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = RoleView.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RoleView.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RoleView.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RoleView.RoleViewID = Convert.ToInt32(command.Parameters["@RoleViewID"].Value);
                return true;
            }
        }

        public override bool Update(RoleView RoleView)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleViewID", SqlDbType = SqlDbType.Int, Value = RoleView.RoleViewID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleView.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = RoleView.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RoleView.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RoleView.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = RoleView.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RoleView.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 RoleViewID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleViewID", SqlDbType = SqlDbType.Int, Value = RoleViewID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual RoleView SelectByKey(Int32 RoleID, Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                RoleView RoleView = new RoleView();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        RoleView = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RoleView;
            }
        }

        public virtual bool UpdateByKey(RoleView RoleView, Int32 RoleID, Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleViewID", SqlDbType = SqlDbType.Int, Value = RoleView.RoleViewID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleView.Role.RoleID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = RoleView.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RoleView.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RoleView.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = RoleView.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID_Original", SqlDbType = SqlDbType.Int, Value = RoleID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID_Original", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RoleView.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 RoleID, Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RoleView_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RoleID", SqlDbType = SqlDbType.Int, Value = RoleID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override RoleView FillFromDataReader(DbDataReader dr)
        {
            RoleView RoleView = new RoleView { HasValue = true };

            RoleView.RoleViewID = Convert.ToInt32(dr["RoleViewID"]);
            RoleView.Role.RoleID = Convert.ToInt32(dr["RoleID"]);
            RoleView.ApplicationHierarchy.ApplicationHierarchyID = Convert.ToInt32(dr["ApplicationHierarchyID"]);
            RoleView.Status = Convert.ToByte(dr["Status"]);
            RoleView.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            RoleView.SessionID = Convert.ToInt64(dr["SessionID"]);
            RoleView.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return RoleView;
        }
    }
}
