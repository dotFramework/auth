using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class FavoriteViewDataAccess : AuthDataAccessBase<Int32, FavoriteView, FavoriteViewCollection>, IFavoriteViewDataAccess
    {
        public override FavoriteView Select(Int32 FavoriteViewID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@FavoriteViewID", SqlDbType = SqlDbType.Int, Value = FavoriteViewID });

                FavoriteView FavoriteView = new FavoriteView();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        FavoriteView = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return FavoriteView;
            }
        }

        public override FavoriteViewCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                FavoriteViewCollection FavoriteViewCollection = new FavoriteViewCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FavoriteViewCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return FavoriteViewCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override FavoriteViewCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                FavoriteViewCollection FavoriteViewCollection = new FavoriteViewCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FavoriteViewCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    FavoriteViewCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return FavoriteViewCollection;
            }
        }

        public virtual FavoriteViewCollection SelectByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_SelectByApplicationHierarchy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                FavoriteViewCollection FavoriteViewCollection = new FavoriteViewCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FavoriteViewCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return FavoriteViewCollection;
            }
        }

        public virtual Int32 SelectCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_SelectCountByApplicationHierarchy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual FavoriteViewCollection SelectByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_SelectByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                FavoriteViewCollection FavoriteViewCollection = new FavoriteViewCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FavoriteViewCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return FavoriteViewCollection;
            }
        }

        public virtual Int32 SelectCountByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_SelectCountByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(FavoriteView FavoriteView)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@FavoriteViewID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = FavoriteView.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = FavoriteView.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = FavoriteView.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = FavoriteView.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                FavoriteView.FavoriteViewID = Convert.ToInt32(command.Parameters["@FavoriteViewID"].Value);
                return true;
            }
        }

        public override bool Update(FavoriteView FavoriteView)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@FavoriteViewID", SqlDbType = SqlDbType.Int, Value = FavoriteView.FavoriteViewID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = FavoriteView.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = FavoriteView.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = FavoriteView.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = FavoriteView.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = FavoriteView.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                FavoriteView.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 FavoriteViewID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@FavoriteViewID", SqlDbType = SqlDbType.Int, Value = FavoriteViewID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual FavoriteView SelectByKey(Int32 ApplicationHierarchyID, Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                FavoriteView FavoriteView = new FavoriteView();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        FavoriteView = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return FavoriteView;
            }
        }

        public virtual bool UpdateByKey(FavoriteView FavoriteView, Int32 ApplicationHierarchyID, Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@FavoriteViewID", SqlDbType = SqlDbType.Int, Value = FavoriteView.FavoriteViewID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = FavoriteView.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = FavoriteView.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = FavoriteView.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = FavoriteView.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = FavoriteView.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID_Original", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID_Original", SqlDbType = SqlDbType.Int, Value = UserID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                FavoriteView.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 ApplicationHierarchyID, Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_FavoriteView_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override FavoriteView FillFromDataReader(DbDataReader dr)
        {
            FavoriteView FavoriteView = new FavoriteView { HasValue = true };

            FavoriteView.FavoriteViewID = Convert.ToInt32(dr["FavoriteViewID"]);
            FavoriteView.ApplicationHierarchy.ApplicationHierarchyID = Convert.ToInt32(dr["ApplicationHierarchyID"]);
            FavoriteView.User.UserID = Convert.ToInt32(dr["UserID"]);
            FavoriteView.Status = Convert.ToByte(dr["Status"]);
            FavoriteView.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            FavoriteView.SessionID = Convert.ToInt64(dr["SessionID"]);
            FavoriteView.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return FavoriteView;
        }
    }
}
