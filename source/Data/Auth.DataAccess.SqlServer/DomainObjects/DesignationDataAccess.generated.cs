using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class DesignationDataAccess : AuthDataAccessBase<Int16, Designation, DesignationCollection>, IDesignationDataAccess
    {
        public override Designation Select(Int16 DesignationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Value = DesignationID });

                Designation Designation = new Designation();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Designation = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Designation;
            }
        }

        public override DesignationCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                DesignationCollection DesignationCollection = new DesignationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DesignationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return DesignationCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override DesignationCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                DesignationCollection DesignationCollection = new DesignationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DesignationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    DesignationCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return DesignationCollection;
            }
        }

        public override bool Insert(Designation Designation)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Designation.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Designation.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = Designation.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Designation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Designation.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Designation.DesignationID = Convert.ToInt16(command.Parameters["@DesignationID"].Value);
                return true;
            }
        }

        public override bool Update(Designation Designation)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Value = Designation.DesignationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Designation.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Designation.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = Designation.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Designation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Designation.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Designation.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Designation.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int16 DesignationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Value = DesignationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual Designation SelectByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                Designation Designation = new Designation();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Designation = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Designation;
            }
        }

        public virtual bool UpdateByKey(Designation Designation, String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DesignationID", SqlDbType = SqlDbType.SmallInt, Value = Designation.DesignationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Designation.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = Designation.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = Designation.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Designation.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Designation.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Designation.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code_Original", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Designation.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Designation_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override Designation FillFromDataReader(DbDataReader dr)
        {
            Designation Designation = new Designation { HasValue = true };

            Designation.DesignationID = Convert.ToInt16(dr["DesignationID"]);
            Designation.Code = Convert.ToString(dr["Code"]);
            Designation.Name = Convert.ToString(dr["Name"]);
            if (dr["Description"] != DBNull.Value)
                Designation.Description = Convert.ToString(dr["Description"]);
            Designation.Status = Convert.ToByte(dr["Status"]);
            Designation.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            Designation.SessionID = Convert.ToInt64(dr["SessionID"]);
            Designation.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return Designation;
        }
    }
}
