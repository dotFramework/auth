using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class LoginProviderDataAccess : AuthDataAccessBase<Byte, LoginProvider, LoginProviderCollection>, ILoginProviderDataAccess
    {
        public override LoginProvider Select(Byte LoginProviderID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderID });

                LoginProvider LoginProvider = new LoginProvider();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        LoginProvider = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProvider;
            }
        }

        public override LoginProviderCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                LoginProviderCollection LoginProviderCollection = new LoginProviderCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LoginProviderCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override LoginProviderCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                LoginProviderCollection LoginProviderCollection = new LoginProviderCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LoginProviderCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    LoginProviderCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderCollection;
            }
        }

        public override bool Insert(LoginProvider LoginProvider)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@URL", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.URL.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = LoginProvider.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = LoginProvider.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                LoginProvider.LoginProviderID = Convert.ToByte(command.Parameters["@LoginProviderID"].Value);
                return true;
            }
        }

        public override bool Update(LoginProvider LoginProvider)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProvider.LoginProviderID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@URL", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.URL.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = LoginProvider.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = LoginProvider.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = LoginProvider.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                LoginProvider.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Byte LoginProviderID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual LoginProvider SelectByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                LoginProvider LoginProvider = new LoginProvider();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        LoginProvider = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProvider;
            }
        }

        public virtual bool UpdateByKey(LoginProvider LoginProvider, String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProvider.LoginProviderID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@URL", SqlDbType = SqlDbType.NVarChar, Value = LoginProvider.URL.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = LoginProvider.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = LoginProvider.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = LoginProvider.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code_Original", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                LoginProvider.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProvider_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override LoginProvider FillFromDataReader(DbDataReader dr)
        {
            LoginProvider LoginProvider = new LoginProvider { HasValue = true };

            LoginProvider.LoginProviderID = Convert.ToByte(dr["LoginProviderID"]);
            LoginProvider.Code = Convert.ToString(dr["Code"]);
            LoginProvider.Name = Convert.ToString(dr["Name"]);
            if (dr["Description"] != DBNull.Value)
                LoginProvider.Description = Convert.ToString(dr["Description"]);
            if (dr["URL"] != DBNull.Value)
                LoginProvider.URL = Convert.ToString(dr["URL"]);
            LoginProvider.Status = Convert.ToByte(dr["Status"]);
            LoginProvider.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            LoginProvider.SessionID = Convert.ToInt64(dr["SessionID"]);
            LoginProvider.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return LoginProvider;
        }
    }
}
