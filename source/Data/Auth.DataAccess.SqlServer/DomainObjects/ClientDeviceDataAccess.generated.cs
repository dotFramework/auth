using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;
using DotFramework.Auth.DataAccessFactory;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ClientDeviceDataAccess : AuthDataAccessBase<Int32, ClientDevice, ClientDeviceCollection>, IClientDeviceDataAccess
    {
        public override ClientDevice Select(Int32 ClientDeviceID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientDeviceID", SqlDbType = SqlDbType.Int, Value = ClientDeviceID });

                ClientDevice ClientDevice = new ClientDevice();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ClientDevice = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ClientDevice;
            }
        }

        public override ClientDeviceCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ClientDeviceCollection ClientDeviceCollection = new ClientDeviceCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ClientDeviceCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ClientDeviceCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ClientDeviceCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ClientDeviceCollection ClientDeviceCollection = new ClientDeviceCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ClientDeviceCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ClientDeviceCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ClientDeviceCollection;
            }
        }

        public virtual ClientDeviceCollection SelectByDevice(Int32 DeviceID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_SelectByDevice]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = DeviceID });

                ClientDeviceCollection ClientDeviceCollection = new ClientDeviceCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ClientDeviceCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ClientDeviceCollection;
            }
        }

        public virtual Int32 SelectCountByDevice(Int32 DeviceID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_SelectCountByDevice]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = DeviceID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual ClientDeviceCollection SelectByClient(Int32 ClientID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_SelectByClient]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = ClientID });

                ClientDeviceCollection ClientDeviceCollection = new ClientDeviceCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ClientDeviceCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ClientDeviceCollection;
            }
        }

        public virtual Int32 SelectCountByClient(Int32 ClientID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_SelectCountByClient]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = ClientID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(ClientDevice ClientDevice)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientDeviceID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = ClientDevice.Device.DeviceID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = ClientDevice.Client.ClientID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ClientDevice.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ClientDevice.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ClientDevice.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ClientDevice.ClientDeviceID = Convert.ToInt32(command.Parameters["@ClientDeviceID"].Value);
                ClientDevice.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Update(ClientDevice ClientDevice)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientDeviceID", SqlDbType = SqlDbType.Int, Value = ClientDevice.ClientDeviceID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = ClientDevice.Device.DeviceID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = ClientDevice.Client.ClientID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ClientDevice.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ClientDevice.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ClientDevice.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ClientDevice.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 ClientDeviceID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientDeviceID", SqlDbType = SqlDbType.Int, Value = ClientDeviceID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual ClientDevice SelectByKey(Int32 DeviceID, Int32 ClientID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = DeviceID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = ClientID });

                ClientDevice ClientDevice = new ClientDevice();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ClientDevice = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ClientDevice;
            }
        }

        public virtual bool UpdateByKey(ClientDevice ClientDevice, Int32 DeviceID, Int32 ClientID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientDeviceID", SqlDbType = SqlDbType.Int, Value = ClientDevice.ClientDeviceID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = ClientDevice.Device.DeviceID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = ClientDevice.Client.ClientID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ClientDevice.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ClientDevice.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ClientDevice.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID_Original", SqlDbType = SqlDbType.Int, Value = DeviceID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID_Original", SqlDbType = SqlDbType.Int, Value = ClientID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ClientDevice.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 DeviceID, Int32 ClientID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ClientDevice_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = DeviceID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClientID", SqlDbType = SqlDbType.Int, Value = ClientID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override ClientDevice FillFromDataReader(DbDataReader dr)
        {
            ClientDevice ClientDevice = new ClientDevice { HasValue = true };

            ClientDevice.ClientDeviceID = Convert.ToInt32(dr["ClientDeviceID"]);
            ClientDevice.Device.DeviceID = Convert.ToInt32(dr["DeviceID"]);
            ClientDevice.Client.ClientID = Convert.ToInt32(dr["ClientID"]);
            ClientDevice.Status = Convert.ToByte(dr["Status"]);
            ClientDevice.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            ClientDevice.SessionID = Convert.ToInt64(dr["SessionID"]);
            ClientDevice.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return ClientDevice;
        }
    }
}
