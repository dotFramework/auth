using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class UserSessionDataAccess : AuthDataAccessBase<Int64, UserSession, UserSessionCollection>, IUserSessionDataAccess
    {
        public override UserSession Select(Int64 UserSessionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserSessionID", SqlDbType = SqlDbType.BigInt, Value = UserSessionID });

                UserSession UserSession = new UserSession();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        UserSession = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserSession;
            }
        }

        public override UserSessionCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                UserSessionCollection UserSessionCollection = new UserSessionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserSessionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserSessionCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override UserSessionCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                UserSessionCollection UserSessionCollection = new UserSessionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserSessionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    UserSessionCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return UserSessionCollection;
            }
        }

        public virtual UserSessionCollection SelectByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_SelectByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                UserSessionCollection UserSessionCollection = new UserSessionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserSessionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserSessionCollection;
            }
        }

        public virtual Int32 SelectCountByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_SelectCountByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual UserSessionCollection SelectBySubApplication(Int32 SubApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_SelectBySubApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@SubApplicationID", SqlDbType = SqlDbType.Int, Value = SubApplicationID });

                UserSessionCollection UserSessionCollection = new UserSessionCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserSessionCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserSessionCollection;
            }
        }

        public virtual Int32 SelectCountBySubApplication(Int32 SubApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_SelectCountBySubApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@SubApplicationID", SqlDbType = SqlDbType.Int, Value = SubApplicationID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(UserSession UserSession)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserSessionID", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserSession.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IsApplicationSession", SqlDbType = SqlDbType.Bit, Value = UserSession.IsApplicationSession.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SubApplicationID", SqlDbType = SqlDbType.Int, Value = UserSession.SubApplication.SubApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IP", SqlDbType = SqlDbType.VarChar, Value = UserSession.IP.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@BrowserDetail", SqlDbType = SqlDbType.NVarChar, Value = UserSession.BrowserDetail.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@OperatingSystemDetail", SqlDbType = SqlDbType.NVarChar, Value = UserSession.OperatingSystemDetail.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UrlReferrer", SqlDbType = SqlDbType.NVarChar, Value = UserSession.UrlReferrer.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@StartTime", SqlDbType = SqlDbType.DateTime, Value = UserSession.StartTime.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EndTime", SqlDbType = SqlDbType.DateTime, Value = UserSession.EndTime.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserSession.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserSession.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserSession.UserSessionID = Convert.ToInt64(command.Parameters["@UserSessionID"].Value);
                return true;
            }
        }

        public override bool Update(UserSession UserSession)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserSessionID", SqlDbType = SqlDbType.BigInt, Value = UserSession.UserSessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserSession.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IsApplicationSession", SqlDbType = SqlDbType.Bit, Value = UserSession.IsApplicationSession.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SubApplicationID", SqlDbType = SqlDbType.Int, Value = UserSession.SubApplication.SubApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IP", SqlDbType = SqlDbType.VarChar, Value = UserSession.IP.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@BrowserDetail", SqlDbType = SqlDbType.NVarChar, Value = UserSession.BrowserDetail.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@OperatingSystemDetail", SqlDbType = SqlDbType.NVarChar, Value = UserSession.OperatingSystemDetail.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UrlReferrer", SqlDbType = SqlDbType.NVarChar, Value = UserSession.UrlReferrer.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@StartTime", SqlDbType = SqlDbType.DateTime, Value = UserSession.StartTime.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@EndTime", SqlDbType = SqlDbType.DateTime, Value = UserSession.EndTime.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserSession.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserSession.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = UserSession.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserSession.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int64 UserSessionID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserSession_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserSessionID", SqlDbType = SqlDbType.BigInt, Value = UserSessionID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override UserSession FillFromDataReader(DbDataReader dr)
        {
            UserSession UserSession = new UserSession { HasValue = true };

            UserSession.UserSessionID = Convert.ToInt64(dr["UserSessionID"]);
            if (dr["UserID"] != DBNull.Value)
                UserSession.User.UserID = Convert.ToInt32(dr["UserID"]);
            UserSession.IsApplicationSession = Convert.ToBoolean(dr["IsApplicationSession"]);
            UserSession.SubApplication.SubApplicationID = Convert.ToInt32(dr["SubApplicationID"]);
            if (dr["IP"] != DBNull.Value)
                UserSession.IP = Convert.ToString(dr["IP"]);
            if (dr["BrowserDetail"] != DBNull.Value)
                UserSession.BrowserDetail = Convert.ToString(dr["BrowserDetail"]);
            if (dr["OperatingSystemDetail"] != DBNull.Value)
                UserSession.OperatingSystemDetail = Convert.ToString(dr["OperatingSystemDetail"]);
            if (dr["UrlReferrer"] != DBNull.Value)
                UserSession.UrlReferrer = Convert.ToString(dr["UrlReferrer"]);
            UserSession.StartTime = Convert.ToDateTime(dr["StartTime"]);
            if (dr["EndTime"] != DBNull.Value)
                UserSession.EndTime = Convert.ToDateTime(dr["EndTime"]);
            UserSession.Status = Convert.ToByte(dr["Status"]);
            UserSession.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            UserSession.SessionID = Convert.ToInt64(dr["SessionID"]);
            UserSession.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return UserSession;
        }
    }
}
