using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ApplicationHierarchyDataAccess : AuthDataAccessBase<Int32, ApplicationHierarchy, ApplicationHierarchyCollection>, IApplicationHierarchyDataAccess
    {
        public override ApplicationHierarchy Select(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                ApplicationHierarchy ApplicationHierarchy = new ApplicationHierarchy();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ApplicationHierarchy = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationHierarchy;
            }
        }

        public override ApplicationHierarchyCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ApplicationHierarchyCollection ApplicationHierarchyCollection = new ApplicationHierarchyCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicationHierarchyCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationHierarchyCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ApplicationHierarchyCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ApplicationHierarchyCollection ApplicationHierarchyCollection = new ApplicationHierarchyCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicationHierarchyCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ApplicationHierarchyCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationHierarchyCollection;
            }
        }

        public virtual ApplicationHierarchyCollection SelectByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_SelectByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                ApplicationHierarchyCollection ApplicationHierarchyCollection = new ApplicationHierarchyCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ApplicationHierarchyCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationHierarchyCollection;
            }
        }

        public virtual Int32 SelectCountByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_SelectCountByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(ApplicationHierarchy ApplicationHierarchy)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchy.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@HID", SqlDbType = SqlDbType.Udt, UdtTypeName = "hierarchyid", Value = ApplicationHierarchy.HID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.TinyInt, Value = ApplicationHierarchy.Type.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Path", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Path.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ControllerName", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.ControllerName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ActionName", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.ActionName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RouteValues", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.RouteValues.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IconType", SqlDbType = SqlDbType.TinyInt, Value = ApplicationHierarchy.IconType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Icon", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Icon.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ApplicationHierarchy.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicationHierarchy.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicationHierarchy.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicationHierarchy.ApplicationHierarchyID = Convert.ToInt32(command.Parameters["@ApplicationHierarchyID"].Value);
                return true;
            }
        }

        public override bool Update(ApplicationHierarchy ApplicationHierarchy)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchy.ApplicationHierarchyID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchy.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@HID", SqlDbType = SqlDbType.Udt, UdtTypeName = "hierarchyid", Value = ApplicationHierarchy.HID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.TinyInt, Value = ApplicationHierarchy.Type.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Path", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Path.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ControllerName", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.ControllerName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ActionName", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.ActionName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RouteValues", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.RouteValues.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IconType", SqlDbType = SqlDbType.TinyInt, Value = ApplicationHierarchy.IconType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Icon", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Icon.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ApplicationHierarchy.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicationHierarchy.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicationHierarchy.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ApplicationHierarchy.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicationHierarchy.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual ApplicationHierarchy SelectByKey(Int32 ApplicationID, String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                ApplicationHierarchy ApplicationHierarchy = new ApplicationHierarchy();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ApplicationHierarchy = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ApplicationHierarchy;
            }
        }

        public virtual bool UpdateByKey(ApplicationHierarchy ApplicationHierarchy, Int32 ApplicationID, String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchy.ApplicationHierarchyID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchy.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@HID", SqlDbType = SqlDbType.Udt, UdtTypeName = "hierarchyid", Value = ApplicationHierarchy.HID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.TinyInt, Value = ApplicationHierarchy.Type.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Path", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Path.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ControllerName", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.ControllerName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ActionName", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.ActionName.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RouteValues", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.RouteValues.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IconType", SqlDbType = SqlDbType.TinyInt, Value = ApplicationHierarchy.IconType.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Icon", SqlDbType = SqlDbType.NVarChar, Value = ApplicationHierarchy.Icon.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@NeedAuthorization", SqlDbType = SqlDbType.Bit, Value = ApplicationHierarchy.NeedAuthorization.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ApplicationHierarchy.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ApplicationHierarchy.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ApplicationHierarchy.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID_Original", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code_Original", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ApplicationHierarchy.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 ApplicationID, String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ApplicationHierarchy_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override ApplicationHierarchy FillFromDataReader(DbDataReader dr)
        {
            ApplicationHierarchy ApplicationHierarchy = new ApplicationHierarchy { HasValue = true };

            ApplicationHierarchy.ApplicationHierarchyID = Convert.ToInt32(dr["ApplicationHierarchyID"]);
            ApplicationHierarchy.Application.ApplicationID = Convert.ToInt32(dr["ApplicationID"]);
            ApplicationHierarchy.HID = Microsoft.SqlServer.Types.SqlHierarchyId.Parse(dr["HID"].ToString());
            ApplicationHierarchy.Code = Convert.ToString(dr["Code"]);
            ApplicationHierarchy.Name = Convert.ToString(dr["Name"]);
            if (dr["Description"] != DBNull.Value)
                ApplicationHierarchy.Description = Convert.ToString(dr["Description"]);
            ApplicationHierarchy.Type = Convert.ToByte(dr["Type"]);
            if (dr["Path"] != DBNull.Value)
                ApplicationHierarchy.Path = Convert.ToString(dr["Path"]);
            if (dr["ControllerName"] != DBNull.Value)
                ApplicationHierarchy.ControllerName = Convert.ToString(dr["ControllerName"]);
            if (dr["ActionName"] != DBNull.Value)
                ApplicationHierarchy.ActionName = Convert.ToString(dr["ActionName"]);
            if (dr["RouteValues"] != DBNull.Value)
                ApplicationHierarchy.RouteValues = Convert.ToString(dr["RouteValues"]);
            if (dr["IconType"] != DBNull.Value)
                ApplicationHierarchy.IconType = Convert.ToByte(dr["IconType"]);
            if (dr["Icon"] != DBNull.Value)
                ApplicationHierarchy.Icon = Convert.ToString(dr["Icon"]);
            ApplicationHierarchy.NeedAuthorization = Convert.ToBoolean(dr["NeedAuthorization"]);
            ApplicationHierarchy.Status = Convert.ToByte(dr["Status"]);
            ApplicationHierarchy.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            ApplicationHierarchy.SessionID = Convert.ToInt64(dr["SessionID"]);
            ApplicationHierarchy.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return ApplicationHierarchy;
        }
    }
}
