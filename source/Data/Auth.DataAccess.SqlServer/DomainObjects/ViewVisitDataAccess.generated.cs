using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class ViewVisitDataAccess : AuthDataAccessBase<Int64, ViewVisit, ViewVisitCollection>, IViewVisitDataAccess
    {
        public override ViewVisit Select(Int64 ViewVisitID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewVisitID", SqlDbType = SqlDbType.BigInt, Value = ViewVisitID });

                ViewVisit ViewVisit = new ViewVisit();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        ViewVisit = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ViewVisit;
            }
        }

        public override ViewVisitCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                ViewVisitCollection ViewVisitCollection = new ViewVisitCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ViewVisitCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ViewVisitCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override ViewVisitCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                ViewVisitCollection ViewVisitCollection = new ViewVisitCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ViewVisitCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    ViewVisitCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return ViewVisitCollection;
            }
        }

        public virtual ViewVisitCollection SelectByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_SelectByApplicationHierarchy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                ViewVisitCollection ViewVisitCollection = new ViewVisitCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ViewVisitCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ViewVisitCollection;
            }
        }

        public virtual Int32 SelectCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_SelectCountByApplicationHierarchy]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ApplicationHierarchyID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual ViewVisitCollection SelectByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_SelectByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                ViewVisitCollection ViewVisitCollection = new ViewVisitCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ViewVisitCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return ViewVisitCollection;
            }
        }

        public virtual Int32 SelectCountByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_SelectCountByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(ViewVisit ViewVisit)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewVisitID", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ViewVisit.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = ViewVisit.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ViewVisit.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ViewVisit.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ViewVisit.ViewVisitID = Convert.ToInt64(command.Parameters["@ViewVisitID"].Value);
                return true;
            }
        }

        public override bool Update(ViewVisit ViewVisit)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewVisitID", SqlDbType = SqlDbType.BigInt, Value = ViewVisit.ViewVisitID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationHierarchyID", SqlDbType = SqlDbType.Int, Value = ViewVisit.ApplicationHierarchy.ApplicationHierarchyID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = ViewVisit.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = ViewVisit.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = ViewVisit.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = ViewVisit.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                ViewVisit.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int64 ViewVisitID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_ViewVisit_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ViewVisitID", SqlDbType = SqlDbType.BigInt, Value = ViewVisitID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override ViewVisit FillFromDataReader(DbDataReader dr)
        {
            ViewVisit ViewVisit = new ViewVisit { HasValue = true };

            ViewVisit.ViewVisitID = Convert.ToInt64(dr["ViewVisitID"]);
            ViewVisit.ApplicationHierarchy.ApplicationHierarchyID = Convert.ToInt32(dr["ApplicationHierarchyID"]);
            ViewVisit.User.UserID = Convert.ToInt32(dr["UserID"]);
            ViewVisit.Status = Convert.ToByte(dr["Status"]);
            ViewVisit.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            ViewVisit.SessionID = Convert.ToInt64(dr["SessionID"]);
            ViewVisit.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return ViewVisit;
        }
    }
}
