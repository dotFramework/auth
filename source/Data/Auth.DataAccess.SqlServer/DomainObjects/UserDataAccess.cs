using DotFramework.Auth.Model;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class UserDataAccess
    {
		public virtual User SelectByEmail(String Email)
		{
			using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
			{
				command.CommandText = "[Auth].[usp_User_SelectByEmail]";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add(new SqlParameter { ParameterName = "@Email", SqlDbType = SqlDbType.VarChar, Value = Email });

				User User = new User();

				using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
				{
					while (dr.Read())
					{
						User = FillFromDataReader(dr);
					}

					dr.Close();
					command.Connection.Close();
				}

				return User;
			}
		}
	}
}