using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class LoginProviderScopeDataAccess : AuthDataAccessBase<Int32, LoginProviderScope, LoginProviderScopeCollection>, ILoginProviderScopeDataAccess
    {
        public override LoginProviderScope Select(Int32 LoginProviderScopeID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderScopeID", SqlDbType = SqlDbType.Int, Value = LoginProviderScopeID });

                LoginProviderScope LoginProviderScope = new LoginProviderScope();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        LoginProviderScope = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderScope;
            }
        }

        public override LoginProviderScopeCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                LoginProviderScopeCollection LoginProviderScopeCollection = new LoginProviderScopeCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LoginProviderScopeCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderScopeCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override LoginProviderScopeCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                LoginProviderScopeCollection LoginProviderScopeCollection = new LoginProviderScopeCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LoginProviderScopeCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    LoginProviderScopeCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderScopeCollection;
            }
        }

        public virtual LoginProviderScopeCollection SelectByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_SelectByLoginProviderOptions]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });

                LoginProviderScopeCollection LoginProviderScopeCollection = new LoginProviderScopeCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LoginProviderScopeCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderScopeCollection;
            }
        }

        public virtual Int32 SelectCountByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_SelectCountByLoginProviderOptions]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(LoginProviderScope LoginProviderScope)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderScopeID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderScope.LoginProviderOptions.LoginProviderOptionsID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Scope", SqlDbType = SqlDbType.VarChar, Value = LoginProviderScope.Scope.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Detail", SqlDbType = SqlDbType.VarChar, Value = LoginProviderScope.Detail.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderScope.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = LoginProviderScope.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = LoginProviderScope.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                LoginProviderScope.LoginProviderScopeID = Convert.ToInt32(command.Parameters["@LoginProviderScopeID"].Value);
                LoginProviderScope.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Update(LoginProviderScope LoginProviderScope)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderScopeID", SqlDbType = SqlDbType.Int, Value = LoginProviderScope.LoginProviderScopeID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderScope.LoginProviderOptions.LoginProviderOptionsID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Scope", SqlDbType = SqlDbType.VarChar, Value = LoginProviderScope.Scope.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Detail", SqlDbType = SqlDbType.VarChar, Value = LoginProviderScope.Detail.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderScope.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = LoginProviderScope.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = LoginProviderScope.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                LoginProviderScope.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 LoginProviderScopeID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderScopeID", SqlDbType = SqlDbType.Int, Value = LoginProviderScopeID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual LoginProviderScope SelectByKey(Int32 LoginProviderOptionsID, String Scope)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Scope", SqlDbType = SqlDbType.VarChar, Value = Scope });

                LoginProviderScope LoginProviderScope = new LoginProviderScope();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        LoginProviderScope = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderScope;
            }
        }

        public virtual bool UpdateByKey(LoginProviderScope LoginProviderScope, Int32 LoginProviderOptionsID, String Scope)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderScopeID", SqlDbType = SqlDbType.Int, Value = LoginProviderScope.LoginProviderScopeID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderScope.LoginProviderOptions.LoginProviderOptionsID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Scope", SqlDbType = SqlDbType.VarChar, Value = LoginProviderScope.Scope.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Detail", SqlDbType = SqlDbType.VarChar, Value = LoginProviderScope.Detail.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderScope.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = LoginProviderScope.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = LoginProviderScope.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID_Original", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Scope_Original", SqlDbType = SqlDbType.VarChar, Value = Scope });

                command.ExecuteNonQuery();
                command.Connection.Close();

                LoginProviderScope.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 LoginProviderOptionsID, String Scope)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderScope_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Scope", SqlDbType = SqlDbType.VarChar, Value = Scope });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override LoginProviderScope FillFromDataReader(DbDataReader dr)
        {
            LoginProviderScope LoginProviderScope = new LoginProviderScope { HasValue = true };

            LoginProviderScope.LoginProviderScopeID = Convert.ToInt32(dr["LoginProviderScopeID"]);
            LoginProviderScope.LoginProviderOptions.LoginProviderOptionsID = Convert.ToInt32(dr["LoginProviderOptionsID"]);
            LoginProviderScope.Scope = Convert.ToString(dr["Scope"]);
            LoginProviderScope.Detail = Convert.ToString(dr["Detail"]);
            LoginProviderScope.Status = Convert.ToByte(dr["Status"]);
            LoginProviderScope.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            LoginProviderScope.SessionID = Convert.ToInt64(dr["SessionID"]);
            LoginProviderScope.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return LoginProviderScope;
        }
    }
}
