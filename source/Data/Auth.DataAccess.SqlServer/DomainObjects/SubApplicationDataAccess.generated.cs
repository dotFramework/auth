using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class SubApplicationDataAccess : AuthDataAccessBase<Int32, SubApplication, SubApplicationCollection>, ISubApplicationDataAccess
    {
        public override SubApplication Select(Int32 SubApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@SubApplicationID", SqlDbType = SqlDbType.Int, Value = SubApplicationID });

                SubApplication SubApplication = new SubApplication();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        SubApplication = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return SubApplication;
            }
        }

        public override SubApplicationCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                SubApplicationCollection SubApplicationCollection = new SubApplicationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SubApplicationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return SubApplicationCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override SubApplicationCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                SubApplicationCollection SubApplicationCollection = new SubApplicationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SubApplicationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    SubApplicationCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return SubApplicationCollection;
            }
        }

        public virtual SubApplicationCollection SelectByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_SelectByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                SubApplicationCollection SubApplicationCollection = new SubApplicationCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SubApplicationCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return SubApplicationCollection;
            }
        }

        public virtual Int32 SelectCountByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_SelectCountByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(SubApplication SubApplication)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@SubApplicationID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = SubApplication.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = SubApplication.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = SubApplication.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = SubApplication.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = SubApplication.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = SubApplication.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                SubApplication.SubApplicationID = Convert.ToInt32(command.Parameters["@SubApplicationID"].Value);
                return true;
            }
        }

        public override bool Update(SubApplication SubApplication)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@SubApplicationID", SqlDbType = SqlDbType.Int, Value = SubApplication.SubApplicationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = SubApplication.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = SubApplication.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = SubApplication.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = SubApplication.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = SubApplication.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = SubApplication.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = SubApplication.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                SubApplication.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 SubApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@SubApplicationID", SqlDbType = SqlDbType.Int, Value = SubApplicationID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual SubApplication SelectByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                SubApplication SubApplication = new SubApplication();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        SubApplication = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return SubApplication;
            }
        }

        public virtual bool UpdateByKey(SubApplication SubApplication, String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@SubApplicationID", SqlDbType = SqlDbType.Int, Value = SubApplication.SubApplicationID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = SubApplication.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = SubApplication.Code.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = SubApplication.Name.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = SubApplication.Description.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = SubApplication.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = SubApplication.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = SubApplication.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Code_Original", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                SubApplication.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String Code)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_SubApplication_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@Code", SqlDbType = SqlDbType.NVarChar, Value = Code });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override SubApplication FillFromDataReader(DbDataReader dr)
        {
            SubApplication SubApplication = new SubApplication { HasValue = true };

            SubApplication.SubApplicationID = Convert.ToInt32(dr["SubApplicationID"]);
            SubApplication.Application.ApplicationID = Convert.ToInt32(dr["ApplicationID"]);
            SubApplication.Code = Convert.ToString(dr["Code"]);
            SubApplication.Name = Convert.ToString(dr["Name"]);
            if (dr["Description"] != DBNull.Value)
                SubApplication.Description = Convert.ToString(dr["Description"]);
            SubApplication.Status = Convert.ToByte(dr["Status"]);
            SubApplication.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            SubApplication.SessionID = Convert.ToInt64(dr["SessionID"]);
            SubApplication.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return SubApplication;
        }
    }
}
