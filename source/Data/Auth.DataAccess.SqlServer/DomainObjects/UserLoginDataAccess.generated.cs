using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class UserLoginDataAccess : AuthDataAccessBase<Int64, UserLogin, UserLoginCollection>, IUserLoginDataAccess
    {
        public override UserLogin Select(Int64 UserLoginID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserLoginID", SqlDbType = SqlDbType.BigInt, Value = UserLoginID });

                UserLogin UserLogin = new UserLogin();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        UserLogin = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserLogin;
            }
        }

        public override UserLoginCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                UserLoginCollection UserLoginCollection = new UserLoginCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserLoginCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserLoginCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override UserLoginCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                UserLoginCollection UserLoginCollection = new UserLoginCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserLoginCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    UserLoginCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return UserLoginCollection;
            }
        }

        public virtual UserLoginCollection SelectByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_SelectByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                UserLoginCollection UserLoginCollection = new UserLoginCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserLoginCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserLoginCollection;
            }
        }

        public virtual Int32 SelectCountByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_SelectCountByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual UserLoginCollection SelectByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_SelectByLoginProviderOptions]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });

                UserLoginCollection UserLoginCollection = new UserLoginCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserLoginCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserLoginCollection;
            }
        }

        public virtual Int32 SelectCountByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_SelectCountByLoginProviderOptions]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(UserLogin UserLogin)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserLoginID", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserLogin.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = UserLogin.LoginProviderOptions.LoginProviderOptionsID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProviderKey", SqlDbType = SqlDbType.VarChar, Value = UserLogin.ProviderKey.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserLogin.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserLogin.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserLogin.UserLoginID = Convert.ToInt64(command.Parameters["@UserLoginID"].Value);
                return true;
            }
        }

        public override bool Update(UserLogin UserLogin)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserLoginID", SqlDbType = SqlDbType.BigInt, Value = UserLogin.UserLoginID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserLogin.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = UserLogin.LoginProviderOptions.LoginProviderOptionsID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProviderKey", SqlDbType = SqlDbType.VarChar, Value = UserLogin.ProviderKey.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserLogin.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserLogin.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = UserLogin.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserLogin.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int64 UserLoginID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserLoginID", SqlDbType = SqlDbType.BigInt, Value = UserLoginID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual UserLogin SelectByKey(Int32 LoginProviderOptionsID, String ProviderKey)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProviderKey", SqlDbType = SqlDbType.VarChar, Value = ProviderKey });

                UserLogin UserLogin = new UserLogin();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        UserLogin = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserLogin;
            }
        }

        public virtual bool UpdateByKey(UserLogin UserLogin, Int32 LoginProviderOptionsID, String ProviderKey)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserLoginID", SqlDbType = SqlDbType.BigInt, Value = UserLogin.UserLoginID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserLogin.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = UserLogin.LoginProviderOptions.LoginProviderOptionsID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProviderKey", SqlDbType = SqlDbType.VarChar, Value = UserLogin.ProviderKey.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserLogin.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserLogin.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = UserLogin.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID_Original", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProviderKey_Original", SqlDbType = SqlDbType.VarChar, Value = ProviderKey });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserLogin.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 LoginProviderOptionsID, String ProviderKey)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserLogin_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProviderKey", SqlDbType = SqlDbType.VarChar, Value = ProviderKey });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override UserLogin FillFromDataReader(DbDataReader dr)
        {
            UserLogin UserLogin = new UserLogin { HasValue = true };

            UserLogin.UserLoginID = Convert.ToInt64(dr["UserLoginID"]);
            UserLogin.User.UserID = Convert.ToInt32(dr["UserID"]);
            UserLogin.LoginProviderOptions.LoginProviderOptionsID = Convert.ToInt32(dr["LoginProviderOptionsID"]);
            UserLogin.ProviderKey = Convert.ToString(dr["ProviderKey"]);
            UserLogin.Status = Convert.ToByte(dr["Status"]);
            UserLogin.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            UserLogin.SessionID = Convert.ToInt64(dr["SessionID"]);
            UserLogin.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return UserLogin;
        }
    }
}
