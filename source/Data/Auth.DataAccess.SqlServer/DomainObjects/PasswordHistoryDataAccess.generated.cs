using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class PasswordHistoryDataAccess : AuthDataAccessBase<Int64, PasswordHistory, PasswordHistoryCollection>, IPasswordHistoryDataAccess
    {
        public override PasswordHistory Select(Int64 PasswordHistoryID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_PasswordHistory_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PasswordHistoryID", SqlDbType = SqlDbType.BigInt, Value = PasswordHistoryID });

                PasswordHistory PasswordHistory = new PasswordHistory();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        PasswordHistory = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return PasswordHistory;
            }
        }

        public override PasswordHistoryCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_PasswordHistory_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                PasswordHistoryCollection PasswordHistoryCollection = new PasswordHistoryCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        PasswordHistoryCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return PasswordHistoryCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_PasswordHistory_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override PasswordHistoryCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_PasswordHistory_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                PasswordHistoryCollection PasswordHistoryCollection = new PasswordHistoryCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        PasswordHistoryCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    PasswordHistoryCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return PasswordHistoryCollection;
            }
        }

        public virtual PasswordHistoryCollection SelectByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_PasswordHistory_SelectByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                PasswordHistoryCollection PasswordHistoryCollection = new PasswordHistoryCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        PasswordHistoryCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return PasswordHistoryCollection;
            }
        }

        public virtual Int32 SelectCountByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_PasswordHistory_SelectCountByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(PasswordHistory PasswordHistory)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_PasswordHistory_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PasswordHistoryID", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = PasswordHistory.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PasswordHash", SqlDbType = SqlDbType.VarChar, Value = PasswordHistory.PasswordHash.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SetPasswordTime", SqlDbType = SqlDbType.DateTime, Value = PasswordHistory.SetPasswordTime.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = PasswordHistory.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = PasswordHistory.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = PasswordHistory.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                PasswordHistory.PasswordHistoryID = Convert.ToInt64(command.Parameters["@PasswordHistoryID"].Value);
                PasswordHistory.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Update(PasswordHistory PasswordHistory)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_PasswordHistory_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PasswordHistoryID", SqlDbType = SqlDbType.BigInt, Value = PasswordHistory.PasswordHistoryID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = PasswordHistory.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@PasswordHash", SqlDbType = SqlDbType.VarChar, Value = PasswordHistory.PasswordHash.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SetPasswordTime", SqlDbType = SqlDbType.DateTime, Value = PasswordHistory.SetPasswordTime.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = PasswordHistory.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = PasswordHistory.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = PasswordHistory.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                PasswordHistory.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int64 PasswordHistoryID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_PasswordHistory_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PasswordHistoryID", SqlDbType = SqlDbType.BigInt, Value = PasswordHistoryID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override PasswordHistory FillFromDataReader(DbDataReader dr)
        {
            PasswordHistory PasswordHistory = new PasswordHistory { HasValue = true };

            PasswordHistory.PasswordHistoryID = Convert.ToInt64(dr["PasswordHistoryID"]);
            PasswordHistory.User.UserID = Convert.ToInt32(dr["UserID"]);
            PasswordHistory.PasswordHash = Convert.ToString(dr["PasswordHash"]);
            PasswordHistory.SetPasswordTime = Convert.ToDateTime(dr["SetPasswordTime"]);
            PasswordHistory.Status = Convert.ToByte(dr["Status"]);
            PasswordHistory.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            PasswordHistory.SessionID = Convert.ToInt64(dr["SessionID"]);
            PasswordHistory.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return PasswordHistory;
        }
    }
}
