using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class DeviceDataAccess : AuthDataAccessBase<Int32, Device, DeviceCollection>, IDeviceDataAccess
    {
        public override Device Select(Int32 DeviceID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = DeviceID });

                Device Device = new Device();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Device = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Device;
            }
        }

        public override DeviceCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                DeviceCollection DeviceCollection = new DeviceCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DeviceCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return DeviceCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override DeviceCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                DeviceCollection DeviceCollection = new DeviceCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DeviceCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    DeviceCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return DeviceCollection;
            }
        }

        public override bool Insert(Device Device)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceUID", SqlDbType = SqlDbType.UniqueIdentifier, Value = Device.DeviceUID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceIdentifier", SqlDbType = SqlDbType.NVarChar, Value = Device.DeviceIdentifier.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Device.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Device.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Device.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Device.DeviceID = Convert.ToInt32(command.Parameters["@DeviceID"].Value);
                Device.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Update(Device Device)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = Device.DeviceID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceUID", SqlDbType = SqlDbType.UniqueIdentifier, Value = Device.DeviceUID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceIdentifier", SqlDbType = SqlDbType.NVarChar, Value = Device.DeviceIdentifier.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Device.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Device.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Device.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Device.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 DeviceID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = DeviceID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual Device SelectByKey(String DeviceIdentifier)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceIdentifier", SqlDbType = SqlDbType.NVarChar, Value = DeviceIdentifier });

                Device Device = new Device();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        Device = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return Device;
            }
        }

        public virtual bool UpdateByKey(Device Device, String DeviceIdentifier)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.Int, Value = Device.DeviceID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceUID", SqlDbType = SqlDbType.UniqueIdentifier, Value = Device.DeviceUID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceIdentifier", SqlDbType = SqlDbType.NVarChar, Value = Device.DeviceIdentifier.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = Device.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = Device.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = Device.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceIdentifier_Original", SqlDbType = SqlDbType.NVarChar, Value = DeviceIdentifier });

                command.ExecuteNonQuery();
                command.Connection.Close();

                Device.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(String DeviceIdentifier)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_Device_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@DeviceIdentifier", SqlDbType = SqlDbType.NVarChar, Value = DeviceIdentifier });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override Device FillFromDataReader(DbDataReader dr)
        {
            Device Device = new Device { HasValue = true };

            Device.DeviceID = Convert.ToInt32(dr["DeviceID"]);
            Device.DeviceUID = new Guid(dr["DeviceUID"].ToString());
            Device.DeviceIdentifier = Convert.ToString(dr["DeviceIdentifier"]);
            Device.Status = Convert.ToByte(dr["Status"]);
            Device.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            Device.SessionID = Convert.ToInt64(dr["SessionID"]);
            Device.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return Device;
        }
    }
}
