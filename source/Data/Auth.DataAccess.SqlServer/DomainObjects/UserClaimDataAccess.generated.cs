using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class UserClaimDataAccess : AuthDataAccessBase<Int64, UserClaim, UserClaimCollection>, IUserClaimDataAccess
    {
        public override UserClaim Select(Int64 UserClaimID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserClaimID", SqlDbType = SqlDbType.BigInt, Value = UserClaimID });

                UserClaim UserClaim = new UserClaim();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        UserClaim = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserClaim;
            }
        }

        public override UserClaimCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                UserClaimCollection UserClaimCollection = new UserClaimCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserClaimCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserClaimCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override UserClaimCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                UserClaimCollection UserClaimCollection = new UserClaimCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserClaimCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    UserClaimCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return UserClaimCollection;
            }
        }

        public virtual UserClaimCollection SelectByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_SelectByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                UserClaimCollection UserClaimCollection = new UserClaimCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserClaimCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserClaimCollection;
            }
        }

        public virtual Int32 SelectCountByUser(Int32 UserID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_SelectCountByUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual UserClaimCollection SelectByClaimType(Int16 ClaimTypeID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_SelectByClaimType]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimTypeID", SqlDbType = SqlDbType.SmallInt, Value = ClaimTypeID });

                UserClaimCollection UserClaimCollection = new UserClaimCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UserClaimCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserClaimCollection;
            }
        }

        public virtual Int32 SelectCountByClaimType(Int16 ClaimTypeID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_SelectCountByClaimType]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimTypeID", SqlDbType = SqlDbType.SmallInt, Value = ClaimTypeID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(UserClaim UserClaim)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserClaimID", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserClaim.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimTypeID", SqlDbType = SqlDbType.SmallInt, Value = UserClaim.ClaimType.ClaimTypeID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimValue", SqlDbType = SqlDbType.NVarChar, Value = UserClaim.ClaimValue.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserClaim.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserClaim.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserClaim.UserClaimID = Convert.ToInt64(command.Parameters["@UserClaimID"].Value);
                return true;
            }
        }

        public override bool Update(UserClaim UserClaim)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserClaimID", SqlDbType = SqlDbType.BigInt, Value = UserClaim.UserClaimID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserClaim.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimTypeID", SqlDbType = SqlDbType.SmallInt, Value = UserClaim.ClaimType.ClaimTypeID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimValue", SqlDbType = SqlDbType.NVarChar, Value = UserClaim.ClaimValue.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserClaim.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserClaim.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = UserClaim.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserClaim.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int64 UserClaimID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserClaimID", SqlDbType = SqlDbType.BigInt, Value = UserClaimID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual UserClaim SelectByKey(Int32 UserID, Int16 ClaimTypeID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimTypeID", SqlDbType = SqlDbType.SmallInt, Value = ClaimTypeID });

                UserClaim UserClaim = new UserClaim();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        UserClaim = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return UserClaim;
            }
        }

        public virtual bool UpdateByKey(UserClaim UserClaim, Int32 UserID, Int16 ClaimTypeID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserClaimID", SqlDbType = SqlDbType.BigInt, Value = UserClaim.UserClaimID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserClaim.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimTypeID", SqlDbType = SqlDbType.SmallInt, Value = UserClaim.ClaimType.ClaimTypeID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimValue", SqlDbType = SqlDbType.NVarChar, Value = UserClaim.ClaimValue.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = UserClaim.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = UserClaim.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = UserClaim.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID_Original", SqlDbType = SqlDbType.Int, Value = UserID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimTypeID_Original", SqlDbType = SqlDbType.SmallInt, Value = ClaimTypeID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                UserClaim.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 UserID, Int16 ClaimTypeID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_UserClaim_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ClaimTypeID", SqlDbType = SqlDbType.SmallInt, Value = ClaimTypeID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override UserClaim FillFromDataReader(DbDataReader dr)
        {
            UserClaim UserClaim = new UserClaim { HasValue = true };

            UserClaim.UserClaimID = Convert.ToInt64(dr["UserClaimID"]);
            UserClaim.User.UserID = Convert.ToInt32(dr["UserID"]);
            UserClaim.ClaimType.ClaimTypeID = Convert.ToInt16(dr["ClaimTypeID"]);
            UserClaim.ClaimValue = Convert.ToString(dr["ClaimValue"]);
            UserClaim.Status = Convert.ToByte(dr["Status"]);
            UserClaim.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            UserClaim.SessionID = Convert.ToInt64(dr["SessionID"]);
            UserClaim.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return UserClaim;
        }
    }
}
