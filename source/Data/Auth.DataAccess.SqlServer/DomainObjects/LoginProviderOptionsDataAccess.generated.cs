using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Auth.DataAccess.SqlServer.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Infra.DataAccess;
using DotFramework.Infra;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class LoginProviderOptionsDataAccess : AuthDataAccessBase<Int32, LoginProviderOptions, LoginProviderOptionsCollection>, ILoginProviderOptionsDataAccess
    {
        public override LoginProviderOptions Select(Int32 LoginProviderOptionsID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_Select]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });

                LoginProviderOptions LoginProviderOptions = new LoginProviderOptions();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        LoginProviderOptions = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderOptions;
            }
        }

        public override LoginProviderOptionsCollection SelectAll()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_SelectAll]";
                command.CommandType = CommandType.StoredProcedure;

                LoginProviderOptionsCollection LoginProviderOptionsCollection = new LoginProviderOptionsCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LoginProviderOptionsCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderOptionsCollection;
            }
        }

        public override Int32 SelectCount()
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_SelectCount]";
                command.CommandType = CommandType.StoredProcedure;

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override LoginProviderOptionsCollection SelectAllWithPaging(Int32 PageIndex, Int32 RowsInPage)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_SelectAll_Paging]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = PageIndex });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowsInPage", SqlDbType = SqlDbType.Int, Value = RowsInPage });

                LoginProviderOptionsCollection LoginProviderOptionsCollection = new LoginProviderOptionsCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LoginProviderOptionsCollection.Add(FillFromDataReader(dr));
                    }

                    dr.NextResult();
                    dr.Read();

                    LoginProviderOptionsCollection.TotalCount = Convert.ToInt32(dr["Count"]);

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderOptionsCollection;
            }
        }

        public virtual LoginProviderOptionsCollection SelectByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_SelectByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                LoginProviderOptionsCollection LoginProviderOptionsCollection = new LoginProviderOptionsCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LoginProviderOptionsCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderOptionsCollection;
            }
        }

        public virtual Int32 SelectCountByApplication(Int32 ApplicationID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_SelectCountByApplication]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public virtual LoginProviderOptionsCollection SelectByLoginProvider(Byte LoginProviderID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_SelectByLoginProvider]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderID });

                LoginProviderOptionsCollection LoginProviderOptionsCollection = new LoginProviderOptionsCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LoginProviderOptionsCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderOptionsCollection;
            }
        }

        public virtual Int32 SelectCountByLoginProvider(Byte LoginProviderID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_SelectCountByLoginProvider]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderID });

                int count = (int)command.ExecuteScalar();
                command.Connection.Close();

                return count;
            }
        }

        public override bool Insert(LoginProviderOptions LoginProviderOptions)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_Insert]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptions.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderOptions.LoginProvider.LoginProviderID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationIdentity", SqlDbType = SqlDbType.NVarChar, Value = LoginProviderOptions.ApplicationIdentity.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationSecret", SqlDbType = SqlDbType.NVarChar, Value = LoginProviderOptions.ApplicationSecret.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderOptions.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = LoginProviderOptions.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                LoginProviderOptions.LoginProviderOptionsID = Convert.ToInt32(command.Parameters["@LoginProviderOptionsID"].Value);
                return true;
            }
        }

        public override bool Update(LoginProviderOptions LoginProviderOptions)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_Update]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptions.LoginProviderOptionsID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptions.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderOptions.LoginProvider.LoginProviderID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationIdentity", SqlDbType = SqlDbType.NVarChar, Value = LoginProviderOptions.ApplicationIdentity.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationSecret", SqlDbType = SqlDbType.NVarChar, Value = LoginProviderOptions.ApplicationSecret.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderOptions.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = LoginProviderOptions.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = LoginProviderOptions.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });

                command.ExecuteNonQuery();
                command.Connection.Close();

                LoginProviderOptions.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public override bool Delete(Int32 LoginProviderOptionsID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_Delete]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptionsID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual LoginProviderOptions SelectByKey(Int32 ApplicationID, Byte LoginProviderID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_SelectByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderID });

                LoginProviderOptions LoginProviderOptions = new LoginProviderOptions();

                using (DbDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (dr.Read())
                    {
                        LoginProviderOptions = FillFromDataReader(dr);
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return LoginProviderOptions;
            }
        }

        public virtual bool UpdateByKey(LoginProviderOptions LoginProviderOptions, Int32 ApplicationID, Byte LoginProviderID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_UpdateByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderOptionsID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptions.LoginProviderOptionsID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = LoginProviderOptions.Application.ApplicationID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderOptions.LoginProvider.LoginProviderID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationIdentity", SqlDbType = SqlDbType.NVarChar, Value = LoginProviderOptions.ApplicationIdentity.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationSecret", SqlDbType = SqlDbType.NVarChar, Value = LoginProviderOptions.ApplicationSecret.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderOptions.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = LoginProviderOptions.SessionID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@RowVersion", SqlDbType = SqlDbType.BigInt, Value = LoginProviderOptions.RowVersion.GetDbValue(), Direction = ParameterDirection.InputOutput });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID_Original", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID_Original", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                LoginProviderOptions.RowVersion = Convert.ToInt64(command.Parameters["@RowVersion"].Value);
                return true;
            }
        }

        public virtual bool DeleteByKey(Int32 ApplicationID, Byte LoginProviderID)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_LoginProviderOptions_DeleteByKey]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@ApplicationID", SqlDbType = SqlDbType.Int, Value = ApplicationID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@LoginProviderID", SqlDbType = SqlDbType.TinyInt, Value = LoginProviderID });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        protected override LoginProviderOptions FillFromDataReader(DbDataReader dr)
        {
            LoginProviderOptions LoginProviderOptions = new LoginProviderOptions { HasValue = true };

            LoginProviderOptions.LoginProviderOptionsID = Convert.ToInt32(dr["LoginProviderOptionsID"]);
            LoginProviderOptions.Application.ApplicationID = Convert.ToInt32(dr["ApplicationID"]);
            LoginProviderOptions.LoginProvider.LoginProviderID = Convert.ToByte(dr["LoginProviderID"]);
            LoginProviderOptions.ApplicationIdentity = Convert.ToString(dr["ApplicationIdentity"]);
            LoginProviderOptions.ApplicationSecret = Convert.ToString(dr["ApplicationSecret"]);
            LoginProviderOptions.Status = Convert.ToByte(dr["Status"]);
            LoginProviderOptions.ModificationTime = Convert.ToDateTime(dr["ModificationTime"]);
            LoginProviderOptions.SessionID = Convert.ToInt64(dr["SessionID"]);
            LoginProviderOptions.RowVersion = dr["RowVersion"].ConvertToRowVersion();

            return LoginProviderOptions;
        }
    }
}
