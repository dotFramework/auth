using DotFramework.Auth.Model;
using DotFramework.Infra;
using DotFramework.Infra.DataAccess;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DotFramework.Auth.DataAccess.SqlServer
{
    public partial class RefreshTokenDataAccess
    {
        public virtual RefreshTokenCollection SelectByUserAndGroupToken(Int32 UserID, string GroupToken)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_SelectByUserAndGroupToken]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID });
                command.Parameters.Add(new SqlParameter { ParameterName = "@GroupToken", SqlDbType = SqlDbType.VarChar, Value = GroupToken });

                RefreshTokenCollection RefreshTokenCollection = new RefreshTokenCollection();

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        RefreshTokenCollection.Add(FillFromDataReader(dr));
                    }

                    dr.Close();
                    command.Connection.Close();
                }

                return RefreshTokenCollection;
            }
        }

        public virtual bool DeleteByUserAndGroupToken(Int32 UserID, string GroupToken)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_DeleteByUserAndGroupToken]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = UserID.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@GroupToken", SqlDbType = SqlDbType.VarChar, Value = GroupToken.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual bool DeleteByGroupToken(string GroupToken)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_DeleteByGroupToken]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@GroupToken", SqlDbType = SqlDbType.VarChar, Value = GroupToken.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                return true;
            }
        }

        public virtual bool InsertByUserAndGroupToken(RefreshToken RefreshToken)
        {
            using (DbCommand command = this.ConnectionHandler.Connection.CreateCommand())
            {
                command.CommandText = "[Auth].[usp_RefreshToken_InsertByUserAndGroupToken]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@RefreshTokenID", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = RefreshToken.User.UserID.GetDbValue(true) });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Token", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.Token.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@GroupToken", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.GroupToken.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@AppIdentifier", SqlDbType = SqlDbType.VarChar, Value = RefreshToken.AppIdentifier.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@IssuedUtc", SqlDbType = SqlDbType.DateTime, Value = RefreshToken.IssuedUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ExpiresUtc", SqlDbType = SqlDbType.DateTime, Value = RefreshToken.ExpiresUtc.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@ProtectedTicket", SqlDbType = SqlDbType.NVarChar, Value = RefreshToken.ProtectedTicket.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.TinyInt, Value = RefreshToken.Status.GetDbValue() });
                command.Parameters.Add(new SqlParameter { ParameterName = "@SessionID", SqlDbType = SqlDbType.BigInt, Value = RefreshToken.SessionID.GetDbValue() });

                command.ExecuteNonQuery();
                command.Connection.Close();

                RefreshToken.RefreshTokenID = Convert.ToInt64(command.Parameters["@RefreshTokenID"].Value);
                return true;
            }
        }
    }
}