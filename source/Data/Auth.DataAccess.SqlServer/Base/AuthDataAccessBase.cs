using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Infra.DataAccess.SqlServer;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.DataAccess.SqlServer.Base
{
    public abstract class AuthDataAccessBase<TKey, TModel, TModelCollection> : SqlServerDataAccessBase<TKey, TModel, TModelCollection>, IAuthDataAccessBase<TKey, TModel, TModelCollection>
        where TModel : DomainModelBase, new()
        where TModelCollection : ListBase<TKey, TModel>, new()
    {
    }
}