using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class Application : AuthModelBase
    {
		private Int32 _ApplicationID;
		[IsKey]
		[DataMember]
		public Int32 ApplicationID
		{
			get
			{
				return this._ApplicationID;
			}
			set
			{
				this.SetProperty(ref this._ApplicationID, value);
			}
		}

		private String _Code;
		[DataMember]
        public String Code
		{
			get
			{
				return this._Code;
			}
			set
			{
				this.SetProperty(ref this._Code, value);
			}
		}

		private String _AppSecret;
		[DataMember]
		public String AppSecret
		{
			get
			{
				return this._AppSecret;
			}
			set
			{
				this.SetProperty(ref this._AppSecret, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}

		private String _Path;
		[DataMember]
        public String Path
		{
			get
			{
				return this._Path;
			}
			set
			{
				this.SetProperty(ref this._Path, value);
			}
		}

		private Client _Client = new Client();
		[DataMember]
		public Client Client
		{
			get
			{
				return this._Client;
			}
			set
			{
				this.SetProperty(ref this._Client, value);
			}
		}

		private String _SSOCallBackURI;
		[DataMember]
		public String SSOCallBackURI
		{
			get
			{
				return this._SSOCallBackURI;
			}
			set
			{
				this.SetProperty(ref this._SSOCallBackURI, value);
			}
		}

		private ApplicationEndpointCollection _ApplicationEndpointCollection = new ApplicationEndpointCollection();
		[DataMember]
        public ApplicationEndpointCollection ApplicationEndpointCollection
        {
            get
            {
                return this._ApplicationEndpointCollection;
            }
            set
            {
				this.SetProperty(ref this._ApplicationEndpointCollection, value);
            }
        }
		
		private ApplicationHierarchyCollection _ApplicationHierarchyCollection = new ApplicationHierarchyCollection();
		[DataMember]
        public ApplicationHierarchyCollection ApplicationHierarchyCollection
        {
            get
            {
                return this._ApplicationHierarchyCollection;
            }
            set
            {
				this.SetProperty(ref this._ApplicationHierarchyCollection, value);
            }
        }
		
		private LoginProviderOptionsCollection _LoginProviderOptionsCollection = new LoginProviderOptionsCollection();
		[DataMember]
        public LoginProviderOptionsCollection LoginProviderOptionsCollection
        {
            get
            {
                return this._LoginProviderOptionsCollection;
            }
            set
            {
				this.SetProperty(ref this._LoginProviderOptionsCollection, value);
            }
        }
		
		private RoleCollection _RoleCollection = new RoleCollection();
		[DataMember]
        public RoleCollection RoleCollection
        {
            get
            {
                return this._RoleCollection;
            }
            set
            {
				this.SetProperty(ref this._RoleCollection, value);
            }
        }
		
		private SubApplicationCollection _SubApplicationCollection = new SubApplicationCollection();
		[DataMember]
        public SubApplicationCollection SubApplicationCollection
        {
            get
            {
                return this._SubApplicationCollection;
            }
            set
            {
				this.SetProperty(ref this._SubApplicationCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			Application obj = new Application();

			obj.ApplicationID = this.ApplicationID;
			obj.Code = this.Code;
			obj.AppSecret = this.AppSecret;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.Path = this.Path;

			if (this.Client != null)
			{
				obj.Client = this.Client.Clone<Client>(true);
			}

			obj.SSOCallBackURI = this.SSOCallBackURI;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.ApplicationEndpointCollection != null)
			{
				obj.ApplicationEndpointCollection = this.ApplicationEndpointCollection.Clone<ApplicationEndpointCollection>(true);
			}
			
			if (this.ApplicationHierarchyCollection != null)
			{
				obj.ApplicationHierarchyCollection = this.ApplicationHierarchyCollection.Clone<ApplicationHierarchyCollection>(true);
			}
			
			if (this.LoginProviderOptionsCollection != null)
			{
				obj.LoginProviderOptionsCollection = this.LoginProviderOptionsCollection.Clone<LoginProviderOptionsCollection>(true);
			}
			
			if (this.RoleCollection != null)
			{
				obj.RoleCollection = this.RoleCollection.Clone<RoleCollection>(true);
			}
			
			if (this.SubApplicationCollection != null)
			{
				obj.SubApplicationCollection = this.SubApplicationCollection.Clone<SubApplicationCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ApplicationCollection : ListBase<Int32, Application>
    {

    }
}