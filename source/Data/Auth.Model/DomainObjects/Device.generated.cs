using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;
using DotFramework.Infra.Model;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace DotFramework.Auth.Model
{
	[DataContract]
	[SchemaMetadata(SchemaName = "Auth")]
	public sealed partial class Device : AuthModelBase
    {
		private Int32 _DeviceID;
		[IsKey]
		[DataMember]
		public Int32 DeviceID
		{
			get
			{
				return this._DeviceID;
			}
			set
			{
				this.SetProperty(ref this._DeviceID, value);
			}
		}

		private Guid _DeviceUID;
		[DataMember]
        public Guid DeviceUID
		{
			get
			{
				return this._DeviceUID;
			}
			set
			{
				this.SetProperty(ref this._DeviceUID, value);
			}
		}

		private String _DeviceIdentifier;
		[DataMember]
        public String DeviceIdentifier
		{
			get
			{
				return this._DeviceIdentifier;
			}
			set
			{
				this.SetProperty(ref this._DeviceIdentifier, value);
			}
		}
		
		private ClientDeviceCollection _ClientDeviceCollection = new ClientDeviceCollection();
		[DataMember]
        public ClientDeviceCollection ClientDeviceCollection
        {
            get
            {
                return this._ClientDeviceCollection;
            }
            set
            {
				this.SetProperty(ref this._ClientDeviceCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			Device obj = new Device();

			obj.DeviceID = this.DeviceID;
			obj.DeviceUID = this.DeviceUID;
			obj.DeviceIdentifier = this.DeviceIdentifier;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.ClientDeviceCollection != null)
			{
				obj.ClientDeviceCollection = this.ClientDeviceCollection.Clone<ClientDeviceCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class DeviceCollection : ListBase<Int32, Device>
    {

    }
}