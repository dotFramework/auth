using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class ViewVisit : AuthModelBase
    {
		
    }

    public sealed partial class ViewVisitCollection : ListBase<Int64, ViewVisit>
    {

    }

	internal sealed class ViewVisitMetadata
    {
		[Required]
        public ApplicationHierarchy ApplicationHierarchy { get; set; }

        [Required]
        public User User { get; set; }

    }
}