using System;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class RefreshToken : AuthModelBase
    {
		
    }

    public sealed partial class RefreshTokenCollection : ListBase<Int64, RefreshToken>
    {

    }
}