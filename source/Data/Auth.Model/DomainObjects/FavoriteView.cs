using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class FavoriteView : AuthModelBase
    {
		
    }

    public sealed partial class FavoriteViewCollection : ListBase<Int32, FavoriteView>
    {

    }

	internal sealed class FavoriteViewMetadata
    {
		[Required]
        public ApplicationHierarchy ApplicationHierarchy { get; set; }

		[Required]
        public User User { get; set; }

    }
}