using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class RoleOperation : AuthModelBase
    {
		private Int32 _RoleOperationID;
		[IsKey]
		[DataMember]
		public Int32 RoleOperationID
		{
			get
			{
				return this._RoleOperationID;
			}
			set
			{
				this.SetProperty(ref this._RoleOperationID, value);
			}
		}

		private Role _Role = new Role();
		[DataMember]
        public Role Role
        {
            get
            {
                return this._Role;
            }
            set
            {
                this.SetProperty(ref this._Role, value);
            }
        }

		private ApplicableOperation _ApplicableOperation = new ApplicableOperation();
		[DataMember]
        public ApplicableOperation ApplicableOperation
        {
            get
            {
                return this._ApplicableOperation;
            }
            set
            {
                this.SetProperty(ref this._ApplicableOperation, value);
            }
        }

		protected override T DeepClone<T>()
		{
			RoleOperation obj = new RoleOperation();

			obj.RoleOperationID = this.RoleOperationID;

			if (this.Role != null)
			{
				obj.Role = this.Role.Clone<Role>(true);
			}

			if (this.ApplicableOperation != null)
			{
				obj.ApplicableOperation = this.ApplicableOperation.Clone<ApplicableOperation>(true);
			}
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class RoleOperationCollection : ListBase<Int32, RoleOperation>
    {

    }
}