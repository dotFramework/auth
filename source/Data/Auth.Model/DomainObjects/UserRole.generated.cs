using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class UserRole : AuthModelBase
    {
		private Int64 _UserRoleID;
		[IsKey]
		[DataMember]
		public Int64 UserRoleID
		{
			get
			{
				return this._UserRoleID;
			}
			set
			{
				this.SetProperty(ref this._UserRoleID, value);
			}
		}

		private User _User = new User();
		[DataMember]
        public User User
        {
            get
            {
                return this._User;
            }
            set
            {
                this.SetProperty(ref this._User, value);
            }
        }

		private Role _Role = new Role();
		[DataMember]
        public Role Role
        {
            get
            {
                return this._Role;
            }
            set
            {
                this.SetProperty(ref this._Role, value);
            }
        }

		protected override T DeepClone<T>()
		{
			UserRole obj = new UserRole();

			obj.UserRoleID = this.UserRoleID;

			if (this.User != null)
			{
				obj.User = this.User.Clone<User>(true);
			}

			if (this.Role != null)
			{
				obj.Role = this.Role.Clone<Role>(true);
			}
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class UserRoleCollection : ListBase<Int64, UserRole>
    {

    }
}