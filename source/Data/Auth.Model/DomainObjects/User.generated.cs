using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class User : AuthModelBase
    {
		private Int32 _UserID;
		[IsKey]
		[DataMember]
		public Int32 UserID
		{
			get
			{
				return this._UserID;
			}
			set
			{
				this.SetProperty(ref this._UserID, value);
			}
		}

		private String _UserName;
		[DataMember]
        public String UserName
		{
			get
			{
				return this._UserName;
			}
			set
			{
				this.SetProperty(ref this._UserName, value);
			}
		}

		private String _SecurityStamp;
		[DataMember]
        public String SecurityStamp
		{
			get
			{
				return this._SecurityStamp;
			}
			set
			{
				this.SetProperty(ref this._SecurityStamp, value);
			}
		}

		private String _FullName;
		[DataMember]
        public String FullName
		{
			get
			{
				return this._FullName;
			}
			set
			{
				this.SetProperty(ref this._FullName, value);
			}
		}

		private String _Email;
		[DataMember]
        public String Email
		{
			get
			{
				return this._Email;
			}
			set
			{
				this.SetProperty(ref this._Email, value);
			}
		}

		private Boolean _EmailConfirmed;
		[DataMember]
        public Boolean EmailConfirmed
		{
			get
			{
				return this._EmailConfirmed;
			}
			set
			{
				this.SetProperty(ref this._EmailConfirmed, value);
			}
		}

		private String _PhoneNumber;
		[DataMember]
        public String PhoneNumber
		{
			get
			{
				return this._PhoneNumber;
			}
			set
			{
				this.SetProperty(ref this._PhoneNumber, value);
			}
		}

		private Boolean? _PhoneNumberConfirmed;
		[DataMember]
        public Boolean? PhoneNumberConfirmed
		{
			get
			{
				return this._PhoneNumberConfirmed;
			}
			set
			{
				this.SetProperty(ref this._PhoneNumberConfirmed, value);
			}
		}

		private Boolean _TwoFactorEnabled;
		[DataMember]
        public Boolean TwoFactorEnabled
		{
			get
			{
				return this._TwoFactorEnabled;
			}
			set
			{
				this.SetProperty(ref this._TwoFactorEnabled, value);
			}
		}

		private AccountPolicy _AccountPolicy = new AccountPolicy();
		[DataMember]
        public AccountPolicy AccountPolicy
        {
            get
            {
                return this._AccountPolicy;
            }
            set
            {
                this.SetProperty(ref this._AccountPolicy, value);
            }
        }

		private Int32 _AccessFailedCount;
		[DataMember]
        public Int32 AccessFailedCount
		{
			get
			{
				return this._AccessFailedCount;
			}
			set
			{
				this.SetProperty(ref this._AccessFailedCount, value);
			}
		}

		private DateTime? _LockoutEndDateUtc;
		[DataMember]
        public DateTime? LockoutEndDateUtc
		{
			get
			{
				return this._LockoutEndDateUtc;
			}
			set
			{
				this.SetProperty(ref this._LockoutEndDateUtc, value);
			}
		}

		private Byte? _LockoutType;
		[DataMember]
        public Byte? LockoutType
		{
			get
			{
				return this._LockoutType;
			}
			set
			{
				this.SetProperty(ref this._LockoutType, value);
			}
		}
		
		private FavoriteViewCollection _FavoriteViewCollection = new FavoriteViewCollection();
		[DataMember]
        public FavoriteViewCollection FavoriteViewCollection
        {
            get
            {
                return this._FavoriteViewCollection;
            }
            set
            {
				this.SetProperty(ref this._FavoriteViewCollection, value);
            }
        }
		
		private PasswordHistoryCollection _PasswordHistoryCollection = new PasswordHistoryCollection();
		[DataMember]
        public PasswordHistoryCollection PasswordHistoryCollection
        {
            get
            {
                return this._PasswordHistoryCollection;
            }
            set
            {
				this.SetProperty(ref this._PasswordHistoryCollection, value);
            }
        }
		
		private RefreshTokenCollection _RefreshTokenCollection = new RefreshTokenCollection();
		[DataMember]
        public RefreshTokenCollection RefreshTokenCollection
        {
            get
            {
                return this._RefreshTokenCollection;
            }
            set
            {
				this.SetProperty(ref this._RefreshTokenCollection, value);
            }
        }
		
		private UserClaimCollection _UserClaimCollection = new UserClaimCollection();
		[DataMember]
        public UserClaimCollection UserClaimCollection
        {
            get
            {
                return this._UserClaimCollection;
            }
            set
            {
				this.SetProperty(ref this._UserClaimCollection, value);
            }
        }
		
		private UserLoginCollection _UserLoginCollection = new UserLoginCollection();
		[DataMember]
        public UserLoginCollection UserLoginCollection
        {
            get
            {
                return this._UserLoginCollection;
            }
            set
            {
				this.SetProperty(ref this._UserLoginCollection, value);
            }
        }
		
		private UserRoleCollection _UserRoleCollection = new UserRoleCollection();
		[DataMember]
        public UserRoleCollection UserRoleCollection
        {
            get
            {
                return this._UserRoleCollection;
            }
            set
            {
				this.SetProperty(ref this._UserRoleCollection, value);
            }
        }
		
		private UserSessionCollection _UserSessionCollection = new UserSessionCollection();
		[DataMember]
        public UserSessionCollection UserSessionCollection
        {
            get
            {
                return this._UserSessionCollection;
            }
            set
            {
				this.SetProperty(ref this._UserSessionCollection, value);
            }
        }
		
		private ViewVisitCollection _ViewVisitCollection = new ViewVisitCollection();
		[DataMember]
        public ViewVisitCollection ViewVisitCollection
        {
            get
            {
                return this._ViewVisitCollection;
            }
            set
            {
				this.SetProperty(ref this._ViewVisitCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			User obj = new User();

			obj.UserID = this.UserID;
			obj.UserName = this.UserName;
			obj.SecurityStamp = this.SecurityStamp;
			obj.FullName = this.FullName;
			obj.Email = this.Email;
			obj.EmailConfirmed = this.EmailConfirmed;
			obj.PhoneNumber = this.PhoneNumber;
			obj.PhoneNumberConfirmed = this.PhoneNumberConfirmed;
			obj.TwoFactorEnabled = this.TwoFactorEnabled;

			if (this.AccountPolicy != null)
			{
				obj.AccountPolicy = this.AccountPolicy.Clone<AccountPolicy>(true);
			}
			obj.AccessFailedCount = this.AccessFailedCount;
			obj.LockoutEndDateUtc = this.LockoutEndDateUtc;
			obj.LockoutType = this.LockoutType;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.FavoriteViewCollection != null)
			{
				obj.FavoriteViewCollection = this.FavoriteViewCollection.Clone<FavoriteViewCollection>(true);
			}
			
			if (this.PasswordHistoryCollection != null)
			{
				obj.PasswordHistoryCollection = this.PasswordHistoryCollection.Clone<PasswordHistoryCollection>(true);
			}
			
			if (this.RefreshTokenCollection != null)
			{
				obj.RefreshTokenCollection = this.RefreshTokenCollection.Clone<RefreshTokenCollection>(true);
			}
			
			if (this.UserClaimCollection != null)
			{
				obj.UserClaimCollection = this.UserClaimCollection.Clone<UserClaimCollection>(true);
			}
			
			if (this.UserLoginCollection != null)
			{
				obj.UserLoginCollection = this.UserLoginCollection.Clone<UserLoginCollection>(true);
			}
			
			if (this.UserRoleCollection != null)
			{
				obj.UserRoleCollection = this.UserRoleCollection.Clone<UserRoleCollection>(true);
			}
			
			if (this.UserSessionCollection != null)
			{
				obj.UserSessionCollection = this.UserSessionCollection.Clone<UserSessionCollection>(true);
			}
			
			if (this.ViewVisitCollection != null)
			{
				obj.ViewVisitCollection = this.ViewVisitCollection.Clone<ViewVisitCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class UserCollection : ListBase<Int32, User>
    {

    }
}