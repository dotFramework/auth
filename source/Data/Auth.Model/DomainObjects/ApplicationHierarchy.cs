using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class ApplicationHierarchy : AuthModelBase
    {
		
    }

    public sealed partial class ApplicationHierarchyCollection : ListBase<Int32, ApplicationHierarchy>
    {

    }

	internal sealed class ApplicationHierarchyMetadata
    {
		[Required]
        public Application Application { get; set; }

		[Required]
        public String HIDString { get; set; }

		[Required]
		public String Code { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

		[Required]
		public Byte Type { get; set; }

		public String Path { get; set; }

		public String ControllerName { get; set; }

		public String ActionName { get; set; }

		public String RouteValues { get; set; }

		public Byte? IconType { get; set; }

		public String Icon { get; set; }

		[Required]
		public Boolean NeedAuthorization { get; set; }

    }
}