using System;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class LoginProviderOptions : AuthModelBase
    {
		
    }

    public sealed partial class LoginProviderOptionsCollection : ListBase<Int32, LoginProviderOptions>
    {

    }
}