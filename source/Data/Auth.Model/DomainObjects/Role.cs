using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using Microsoft.AspNet.Identity;

namespace DotFramework.Auth.Model
{
    public sealed partial class Role : AuthModelBase, IRole<Int32>
    {
        public int Id
        {
            get { return this.RoleID; }
        }
    }

    public sealed partial class RoleCollection : ListBase<Int32, Role>
    {

    }

	internal sealed class RoleMetadata
    {
		[Required]
        public Designation Designation { get; set; }

		[Required]
		public String Code { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

    }
}