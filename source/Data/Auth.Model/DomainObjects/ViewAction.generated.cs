using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class ViewAction : AuthModelBase
    {
		private Int32 _ViewActionID;
		[IsKey]
		[DataMember]
		public Int32 ViewActionID
		{
			get
			{
				return this._ViewActionID;
			}
			set
			{
				this.SetProperty(ref this._ViewActionID, value);
			}
		}

		private String _Code;
		[DataMember]
        public String Code
		{
			get
			{
				return this._Code;
			}
			set
			{
				this.SetProperty(ref this._Code, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}

		private Boolean _NeedAuthorization;
		[DataMember]
        public Boolean NeedAuthorization
		{
			get
			{
				return this._NeedAuthorization;
			}
			set
			{
				this.SetProperty(ref this._NeedAuthorization, value);
			}
		}

		private Boolean _IsForAll;
		[DataMember]
        public Boolean IsForAll
		{
			get
			{
				return this._IsForAll;
			}
			set
			{
				this.SetProperty(ref this._IsForAll, value);
			}
		}
		
		private ApplicableActionCollection _ApplicableActionCollection = new ApplicableActionCollection();
		[DataMember]
        public ApplicableActionCollection ApplicableActionCollection
        {
            get
            {
                return this._ApplicableActionCollection;
            }
            set
            {
				this.SetProperty(ref this._ApplicableActionCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			ViewAction obj = new ViewAction();

			obj.ViewActionID = this.ViewActionID;
			obj.Code = this.Code;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.NeedAuthorization = this.NeedAuthorization;
			obj.IsForAll = this.IsForAll;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.ApplicableActionCollection != null)
			{
				obj.ApplicableActionCollection = this.ApplicableActionCollection.Clone<ApplicableActionCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ViewActionCollection : ListBase<Int32, ViewAction>
    {

    }
}