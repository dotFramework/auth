using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class RoleAction : AuthModelBase
    {
		private Int32 _RoleActionID;
		[IsKey]
		[DataMember]
		public Int32 RoleActionID
		{
			get
			{
				return this._RoleActionID;
			}
			set
			{
				this.SetProperty(ref this._RoleActionID, value);
			}
		}

		private Role _Role = new Role();
		[DataMember]
        public Role Role
        {
            get
            {
                return this._Role;
            }
            set
            {
                this.SetProperty(ref this._Role, value);
            }
        }

		private ApplicableAction _ApplicableAction = new ApplicableAction();
		[DataMember]
        public ApplicableAction ApplicableAction
        {
            get
            {
                return this._ApplicableAction;
            }
            set
            {
                this.SetProperty(ref this._ApplicableAction, value);
            }
        }

		protected override T DeepClone<T>()
		{
			RoleAction obj = new RoleAction();

			obj.RoleActionID = this.RoleActionID;

			if (this.Role != null)
			{
				obj.Role = this.Role.Clone<Role>(true);
			}

			if (this.ApplicableAction != null)
			{
				obj.ApplicableAction = this.ApplicableAction.Clone<ApplicableAction>(true);
			}
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class RoleActionCollection : ListBase<Int32, RoleAction>
    {

    }
}