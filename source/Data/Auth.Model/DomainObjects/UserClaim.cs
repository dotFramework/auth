using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class UserClaim : AuthModelBase
    {
		
    }

    public sealed partial class UserClaimCollection : ListBase<Int64, UserClaim>
    {

    }

	internal sealed class UserClaimMetadata
    {
		[Required]
        public User User { get; set; }

		[Required]
        public ClaimType ClaimType { get; set; }

		[Required]
		public String ClaimValue { get; set; }

    }
}