using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class UserClaim : AuthModelBase
    {
		private Int64 _UserClaimID;
		[IsKey]
		[DataMember]
		public Int64 UserClaimID
		{
			get
			{
				return this._UserClaimID;
			}
			set
			{
				this.SetProperty(ref this._UserClaimID, value);
			}
		}

		private User _User = new User();
		[DataMember]
        public User User
        {
            get
            {
                return this._User;
            }
            set
            {
                this.SetProperty(ref this._User, value);
            }
        }

		private ClaimType _ClaimType = new ClaimType();
		[DataMember]
        public ClaimType ClaimType
        {
            get
            {
                return this._ClaimType;
            }
            set
            {
                this.SetProperty(ref this._ClaimType, value);
            }
        }

		private String _ClaimValue;
		[DataMember]
        public String ClaimValue
		{
			get
			{
				return this._ClaimValue;
			}
			set
			{
				this.SetProperty(ref this._ClaimValue, value);
			}
		}

		protected override T DeepClone<T>()
		{
			UserClaim obj = new UserClaim();

			obj.UserClaimID = this.UserClaimID;

			if (this.User != null)
			{
				obj.User = this.User.Clone<User>(true);
			}

			if (this.ClaimType != null)
			{
				obj.ClaimType = this.ClaimType.Clone<ClaimType>(true);
			}
			obj.ClaimValue = this.ClaimValue;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class UserClaimCollection : ListBase<Int64, UserClaim>
    {

    }
}