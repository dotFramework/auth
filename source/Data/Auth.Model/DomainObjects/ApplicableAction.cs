using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class ApplicableAction : AuthModelBase
    {
		
    }

    public sealed partial class ApplicableActionCollection : ListBase<Int32, ApplicableAction>
    {

    }

	internal sealed class ApplicableActionMetadata
    {
		[Required]
        public ApplicationHierarchy ApplicationHierarchy { get; set; }

		[Required]
        public ViewAction ViewAction { get; set; }

    }
}