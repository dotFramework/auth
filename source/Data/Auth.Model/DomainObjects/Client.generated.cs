using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;
using DotFramework.Infra.Model;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace DotFramework.Auth.Model
{
	[DataContract]
	[SchemaMetadata(SchemaName = "Auth")]
	public sealed partial class Client : AuthModelBase
    {
		private Int32 _ClientID;
		[IsKey]
		[DataMember]
		public Int32 ClientID
		{
			get
			{
				return this._ClientID;
			}
			set
			{
				this.SetProperty(ref this._ClientID, value);
			}
		}

		private String _ClientUID;
		[DataMember]
        public String ClientUID
		{
			get
			{
				return this._ClientUID;
			}
			set
			{
				this.SetProperty(ref this._ClientUID, value);
			}
		}

		private String _ClientSecret;
		[DataMember]
        public String ClientSecret
		{
			get
			{
				return this._ClientSecret;
			}
			set
			{
				this.SetProperty(ref this._ClientSecret, value);
			}
		}

		private String _ClientIdentifier;
		[DataMember]
        public String ClientIdentifier
		{
			get
			{
				return this._ClientIdentifier;
			}
			set
			{
				this.SetProperty(ref this._ClientIdentifier, value);
			}
		}

		private String _ClientName;
		[DataMember]
        public String ClientName
		{
			get
			{
				return this._ClientName;
			}
			set
			{
				this.SetProperty(ref this._ClientName, value);
			}
		}

		private String _SecurityStamp;
		[DataMember]
        public String SecurityStamp
		{
			get
			{
				return this._SecurityStamp;
			}
			set
			{
				this.SetProperty(ref this._SecurityStamp, value);
			}
		}

		private Boolean _DeviceRestricted;
		[DataMember]
        public Boolean DeviceRestricted
		{
			get
			{
				return this._DeviceRestricted;
			}
			set
			{
				this.SetProperty(ref this._DeviceRestricted, value);
			}
		}
		
		private ClientDeviceCollection _ClientDeviceCollection = new ClientDeviceCollection();
		[DataMember]
        public ClientDeviceCollection ClientDeviceCollection
        {
            get
            {
                return this._ClientDeviceCollection;
            }
            set
            {
				this.SetProperty(ref this._ClientDeviceCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			Client obj = new Client();

			obj.ClientID = this.ClientID;
			obj.ClientUID = this.ClientUID;
			obj.ClientSecret = this.ClientSecret;
			obj.ClientIdentifier = this.ClientIdentifier;
			obj.ClientName = this.ClientName;
			obj.SecurityStamp = this.SecurityStamp;
			obj.DeviceRestricted = this.DeviceRestricted;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.ClientDeviceCollection != null)
			{
				obj.ClientDeviceCollection = this.ClientDeviceCollection.Clone<ClientDeviceCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ClientCollection : ListBase<Int32, Client>
    {

    }
}