using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class ViewVisit : AuthModelBase
    {
		private Int64 _ViewVisitID;
		[IsKey]
		[DataMember]
		public Int64 ViewVisitID
		{
			get
			{
				return this._ViewVisitID;
			}
			set
			{
				this.SetProperty(ref this._ViewVisitID, value);
			}
		}

		private ApplicationHierarchy _ApplicationHierarchy = new ApplicationHierarchy();
		[DataMember]
        public ApplicationHierarchy ApplicationHierarchy
        {
            get
            {
                return this._ApplicationHierarchy;
            }
            set
            {
                this.SetProperty(ref this._ApplicationHierarchy, value);
            }
        }

		private User _User = new User();
		[DataMember]
        public User User
        {
            get
            {
                return this._User;
            }
            set
            {
                this.SetProperty(ref this._User, value);
            }
        }

		protected override T DeepClone<T>()
		{
			ViewVisit obj = new ViewVisit();

			obj.ViewVisitID = this.ViewVisitID;

			if (this.ApplicationHierarchy != null)
			{
				obj.ApplicationHierarchy = this.ApplicationHierarchy.Clone<ApplicationHierarchy>(true);
			}

			if (this.User != null)
			{
				obj.User = this.User.Clone<User>(true);
			}
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ViewVisitCollection : ListBase<Int64, ViewVisit>
    {

    }
}