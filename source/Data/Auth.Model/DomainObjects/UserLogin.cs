using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class UserLogin : AuthModelBase
    {
		
    }

    public sealed partial class UserLoginCollection : ListBase<Int64, UserLogin>
    {

    }

	internal sealed class UserLoginMetadata
    {
		[Required]
        public User User { get; set; }

		[Required]
        public LoginProvider LoginProvider { get; set; }

		[Required]
		public String ProviderKey { get; set; }

    }
}