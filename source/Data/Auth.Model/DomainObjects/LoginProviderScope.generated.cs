using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class LoginProviderScope : AuthModelBase
    {
		private Int32 _LoginProviderScopeID;
		[IsKey]
		[DataMember]
		public Int32 LoginProviderScopeID
		{
			get
			{
				return this._LoginProviderScopeID;
			}
			set
			{
				this.SetProperty(ref this._LoginProviderScopeID, value);
			}
		}

		private LoginProviderOptions _LoginProviderOptions = new LoginProviderOptions();
		[DataMember]
        public LoginProviderOptions LoginProviderOptions
        {
            get
            {
                return this._LoginProviderOptions;
            }
            set
            {
                this.SetProperty(ref this._LoginProviderOptions, value);
            }
        }

		private String _Scope;
		[DataMember]
        public String Scope
		{
			get
			{
				return this._Scope;
			}
			set
			{
				this.SetProperty(ref this._Scope, value);
			}
		}

		private String _Detail;
		[DataMember]
        public String Detail
		{
			get
			{
				return this._Detail;
			}
			set
			{
				this.SetProperty(ref this._Detail, value);
			}
		}

		protected override T DeepClone<T>()
		{
			LoginProviderScope obj = new LoginProviderScope();

			obj.LoginProviderScopeID = this.LoginProviderScopeID;

			if (this.LoginProviderOptions != null)
			{
				obj.LoginProviderOptions = this.LoginProviderOptions.Clone<LoginProviderOptions>(true);
			}
			obj.Scope = this.Scope;
			obj.Detail = this.Detail;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class LoginProviderScopeCollection : ListBase<Int32, LoginProviderScope>
    {

    }
}