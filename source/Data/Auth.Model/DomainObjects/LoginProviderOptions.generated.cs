using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class LoginProviderOptions : AuthModelBase
    {
		private Int32 _LoginProviderOptionsID;
		[IsKey]
		[DataMember]
		public Int32 LoginProviderOptionsID
		{
			get
			{
				return this._LoginProviderOptionsID;
			}
			set
			{
				this.SetProperty(ref this._LoginProviderOptionsID, value);
			}
		}

		private Application _Application = new Application();
		[DataMember]
        public Application Application
        {
            get
            {
                return this._Application;
            }
            set
            {
                this.SetProperty(ref this._Application, value);
            }
        }

		private LoginProvider _LoginProvider = new LoginProvider();
		[DataMember]
        public LoginProvider LoginProvider
        {
            get
            {
                return this._LoginProvider;
            }
            set
            {
                this.SetProperty(ref this._LoginProvider, value);
            }
        }

		private String _ApplicationIdentity;
		[DataMember]
        public String ApplicationIdentity
		{
			get
			{
				return this._ApplicationIdentity;
			}
			set
			{
				this.SetProperty(ref this._ApplicationIdentity, value);
			}
		}

		private String _ApplicationSecret;
		[DataMember]
        public String ApplicationSecret
		{
			get
			{
				return this._ApplicationSecret;
			}
			set
			{
				this.SetProperty(ref this._ApplicationSecret, value);
			}
		}
		
		private LoginProviderScopeCollection _LoginProviderScopeCollection = new LoginProviderScopeCollection();
		[DataMember]
        public LoginProviderScopeCollection LoginProviderScopeCollection
        {
            get
            {
                return this._LoginProviderScopeCollection;
            }
            set
            {
				this.SetProperty(ref this._LoginProviderScopeCollection, value);
            }
        }
		
		private UserLoginCollection _UserLoginCollection = new UserLoginCollection();
		[DataMember]
        public UserLoginCollection UserLoginCollection
        {
            get
            {
                return this._UserLoginCollection;
            }
            set
            {
				this.SetProperty(ref this._UserLoginCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			LoginProviderOptions obj = new LoginProviderOptions();

			obj.LoginProviderOptionsID = this.LoginProviderOptionsID;

			if (this.Application != null)
			{
				obj.Application = this.Application.Clone<Application>(true);
			}

			if (this.LoginProvider != null)
			{
				obj.LoginProvider = this.LoginProvider.Clone<LoginProvider>(true);
			}
			obj.ApplicationIdentity = this.ApplicationIdentity;
			obj.ApplicationSecret = this.ApplicationSecret;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.LoginProviderScopeCollection != null)
			{
				obj.LoginProviderScopeCollection = this.LoginProviderScopeCollection.Clone<LoginProviderScopeCollection>(true);
			}
			
			if (this.UserLoginCollection != null)
			{
				obj.UserLoginCollection = this.UserLoginCollection.Clone<UserLoginCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class LoginProviderOptionsCollection : ListBase<Int32, LoginProviderOptions>
    {

    }
}