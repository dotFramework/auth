using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class UserLogin : AuthModelBase
    {
		private Int64 _UserLoginID;
		[IsKey]
		[DataMember]
		public Int64 UserLoginID
		{
			get
			{
				return this._UserLoginID;
			}
			set
			{
				this.SetProperty(ref this._UserLoginID, value);
			}
		}

		private User _User = new User();
		[DataMember]
        public User User
        {
            get
            {
                return this._User;
            }
            set
            {
                this.SetProperty(ref this._User, value);
            }
        }

		private LoginProviderOptions _LoginProviderOptions = new LoginProviderOptions();
		[DataMember]
        public LoginProviderOptions LoginProviderOptions
        {
            get
            {
                return this._LoginProviderOptions;
            }
            set
            {
                this.SetProperty(ref this._LoginProviderOptions, value);
            }
        }

		private String _ProviderKey;
		[DataMember]
        public String ProviderKey
		{
			get
			{
				return this._ProviderKey;
			}
			set
			{
				this.SetProperty(ref this._ProviderKey, value);
			}
		}

		protected override T DeepClone<T>()
		{
			UserLogin obj = new UserLogin();

			obj.UserLoginID = this.UserLoginID;

			if (this.User != null)
			{
				obj.User = this.User.Clone<User>(true);
			}

			if (this.LoginProviderOptions != null)
			{
				obj.LoginProviderOptions = this.LoginProviderOptions.Clone<LoginProviderOptions>(true);
			}
			obj.ProviderKey = this.ProviderKey;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class UserLoginCollection : ListBase<Int64, UserLogin>
    {

    }
}