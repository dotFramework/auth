using System;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class LoginProviderScope : AuthModelBase
    {
		
    }

    public sealed partial class LoginProviderScopeCollection : ListBase<Int32, LoginProviderScope>
    {

    }
}