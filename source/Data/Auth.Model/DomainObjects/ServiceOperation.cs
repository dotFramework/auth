using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class ServiceOperation : AuthModelBase
    {
		
    }

    public sealed partial class ServiceOperationCollection : ListBase<Int32, ServiceOperation>
    {

    }

	internal sealed class ServiceOperationMetadata
    {
		[Required]
		public String Code { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

		[Required]
		public Boolean NeedAuthorization { get; set; }

		[Required]
		public Boolean IsForAll { get; set; }

    }
}