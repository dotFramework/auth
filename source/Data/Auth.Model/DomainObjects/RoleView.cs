using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class RoleView : AuthModelBase
    {
		
    }

    public sealed partial class RoleViewCollection : ListBase<Int32, RoleView>
    {

    }

	internal sealed class RoleViewMetadata
    {
		[Required]
        public Role Role { get; set; }

		[Required]
        public ApplicationHierarchy ApplicationHierarchy { get; set; }

    }
}