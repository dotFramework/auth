using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class RefreshToken : AuthModelBase
    {
		private Int64 _RefreshTokenID;
		[IsKey]
		[DataMember]
		public Int64 RefreshTokenID
		{
			get
			{
				return this._RefreshTokenID;
			}
			set
			{
				this.SetProperty(ref this._RefreshTokenID, value);
			}
		}

		private User _User = new User();
		[DataMember]
        public User User
        {
            get
            {
                return this._User;
            }
            set
            {
                this.SetProperty(ref this._User, value);
            }
        }

		private String _Token;
		[DataMember]
        public String Token
		{
			get
			{
				return this._Token;
			}
			set
			{
				this.SetProperty(ref this._Token, value);
			}
		}

		private String _GroupToken;
		[DataMember]
		public String GroupToken
		{
			get
			{
				return this._GroupToken;
			}
			set
			{
				this.SetProperty(ref this._GroupToken, value);
			}
		}

        private String _AppIdentifier;
        [DataMember]
        public String AppIdentifier
        {
            get
            {
                return this._AppIdentifier;
            }
            set
            {
                this.SetProperty(ref this._AppIdentifier, value);
            }
        }

        private DateTime _IssuedUtc;
		[DataMember]
        public DateTime IssuedUtc
		{
			get
			{
				return this._IssuedUtc;
			}
			set
			{
				this.SetProperty(ref this._IssuedUtc, value);
			}
		}

		private DateTime _ExpiresUtc;
		[DataMember]
        public DateTime ExpiresUtc
		{
			get
			{
				return this._ExpiresUtc;
			}
			set
			{
				this.SetProperty(ref this._ExpiresUtc, value);
			}
		}

		private String _ProtectedTicket;
		[DataMember]
        public String ProtectedTicket
		{
			get
			{
				return this._ProtectedTicket;
			}
			set
			{
				this.SetProperty(ref this._ProtectedTicket, value);
			}
		}

		protected override T DeepClone<T>()
		{
			RefreshToken obj = new RefreshToken();

			obj.RefreshTokenID = this.RefreshTokenID;

			if (this.User != null)
			{
				obj.User = this.User.Clone<User>(true);
			}
			obj.Token = this.Token;
			obj.IssuedUtc = this.IssuedUtc;
			obj.ExpiresUtc = this.ExpiresUtc;
			obj.ProtectedTicket = this.ProtectedTicket;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class RefreshTokenCollection : ListBase<Int64, RefreshToken>
    {

    }
}