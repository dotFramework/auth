using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class ApplicableOperation : AuthModelBase
    {
		private Int32 _ApplicableOperationID;
		[IsKey]
		[DataMember]
		public Int32 ApplicableOperationID
		{
			get
			{
				return this._ApplicableOperationID;
			}
			set
			{
				this.SetProperty(ref this._ApplicableOperationID, value);
			}
		}

		private ApplicationEndpoint _ApplicationEndpoint = new ApplicationEndpoint();
		[DataMember]
        public ApplicationEndpoint ApplicationEndpoint
        {
            get
            {
                return this._ApplicationEndpoint;
            }
            set
            {
                this.SetProperty(ref this._ApplicationEndpoint, value);
            }
        }

		private ServiceOperation _ServiceOperation = new ServiceOperation();
		[DataMember]
        public ServiceOperation ServiceOperation
        {
            get
            {
                return this._ServiceOperation;
            }
            set
            {
                this.SetProperty(ref this._ServiceOperation, value);
            }
        }
		
		private RoleOperationCollection _RoleOperationCollection = new RoleOperationCollection();
		[DataMember]
        public RoleOperationCollection RoleOperationCollection
        {
            get
            {
                return this._RoleOperationCollection;
            }
            set
            {
				this.SetProperty(ref this._RoleOperationCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			ApplicableOperation obj = new ApplicableOperation();

			obj.ApplicableOperationID = this.ApplicableOperationID;

			if (this.ApplicationEndpoint != null)
			{
				obj.ApplicationEndpoint = this.ApplicationEndpoint.Clone<ApplicationEndpoint>(true);
			}

			if (this.ServiceOperation != null)
			{
				obj.ServiceOperation = this.ServiceOperation.Clone<ServiceOperation>(true);
			}
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.RoleOperationCollection != null)
			{
				obj.RoleOperationCollection = this.RoleOperationCollection.Clone<RoleOperationCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ApplicableOperationCollection : ListBase<Int32, ApplicableOperation>
    {

    }
}