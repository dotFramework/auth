using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using Microsoft.SqlServer.Types;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class ApplicationHierarchy : AuthModelBase
    {
		private Int32 _ApplicationHierarchyID;
		[IsKey]
		[DataMember]
		public Int32 ApplicationHierarchyID
		{
			get
			{
				return this._ApplicationHierarchyID;
			}
			set
			{
				this.SetProperty(ref this._ApplicationHierarchyID, value);
			}
		}

		private Application _Application = new Application();
		[DataMember]
        public Application Application
        {
            get
            {
                return this._Application;
            }
            set
            {
                this.SetProperty(ref this._Application, value);
            }
        }

        public SqlHierarchyId HID
		{
			get
			{
                return SqlHierarchyId.Parse(this.HIDString);
			}
			set
			{
                this.HIDString = value.ToString();
			}
		}

        private String _HIDString;
        [DataMember]
        public String HIDString
        {
            get
            {
                return this._HIDString;
            }
            set
            {
                this._HIDString = value;
            }
        }

		private String _Code;
		[DataMember]
        public String Code
		{
			get
			{
				return this._Code;
			}
			set
			{
				this.SetProperty(ref this._Code, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}

		private Byte _Type;
		[DataMember]
        public Byte Type
		{
			get
			{
				return this._Type;
			}
			set
			{
				this.SetProperty(ref this._Type, value);
			}
		}

		private String _Path;
		[DataMember]
        public String Path
		{
			get
			{
				return this._Path;
			}
			set
			{
				this.SetProperty(ref this._Path, value);
			}
		}

		private String _ControllerName;
		[DataMember]
        public String ControllerName
		{
			get
			{
				return this._ControllerName;
			}
			set
			{
				this.SetProperty(ref this._ControllerName, value);
			}
		}

		private String _ActionName;
		[DataMember]
        public String ActionName
		{
			get
			{
				return this._ActionName;
			}
			set
			{
				this.SetProperty(ref this._ActionName, value);
			}
		}

		private String _RouteValues;
		[DataMember]
        public String RouteValues
		{
			get
			{
				return this._RouteValues;
			}
			set
			{
				this.SetProperty(ref this._RouteValues, value);
			}
		}

		private Byte? _IconType;
		[DataMember]
        public Byte? IconType
		{
			get
			{
				return this._IconType;
			}
			set
			{
				this.SetProperty(ref this._IconType, value);
			}
		}

		private String _Icon;
		[DataMember]
        public String Icon
		{
			get
			{
				return this._Icon;
			}
			set
			{
				this.SetProperty(ref this._Icon, value);
			}
		}

		private Boolean _NeedAuthorization;
		[DataMember]
        public Boolean NeedAuthorization
		{
			get
			{
				return this._NeedAuthorization;
			}
			set
			{
				this.SetProperty(ref this._NeedAuthorization, value);
			}
		}
		
		private ApplicableActionCollection _ApplicableActionCollection = new ApplicableActionCollection();
		[DataMember]
        public ApplicableActionCollection ApplicableActionCollection
        {
            get
            {
                return this._ApplicableActionCollection;
            }
            set
            {
				this.SetProperty(ref this._ApplicableActionCollection, value);
            }
        }
		
		private FavoriteViewCollection _FavoriteViewCollection = new FavoriteViewCollection();
		[DataMember]
        public FavoriteViewCollection FavoriteViewCollection
        {
            get
            {
                return this._FavoriteViewCollection;
            }
            set
            {
				this.SetProperty(ref this._FavoriteViewCollection, value);
            }
        }
		
		private RoleViewCollection _RoleViewCollection = new RoleViewCollection();
		[DataMember]
        public RoleViewCollection RoleViewCollection
        {
            get
            {
                return this._RoleViewCollection;
            }
            set
            {
				this.SetProperty(ref this._RoleViewCollection, value);
            }
        }
		
		private ViewVisitCollection _ViewVisitCollection = new ViewVisitCollection();
		[DataMember]
        public ViewVisitCollection ViewVisitCollection
        {
            get
            {
                return this._ViewVisitCollection;
            }
            set
            {
				this.SetProperty(ref this._ViewVisitCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			ApplicationHierarchy obj = new ApplicationHierarchy();

			obj.ApplicationHierarchyID = this.ApplicationHierarchyID;

			if (this.Application != null)
			{
				obj.Application = this.Application.Clone<Application>(true);
			}
			obj.HID = this.HID;
			obj.Code = this.Code;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.Type = this.Type;
			obj.Path = this.Path;
			obj.ControllerName = this.ControllerName;
			obj.ActionName = this.ActionName;
			obj.RouteValues = this.RouteValues;
			obj.IconType = this.IconType;
			obj.Icon = this.Icon;
			obj.NeedAuthorization = this.NeedAuthorization;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.ApplicableActionCollection != null)
			{
				obj.ApplicableActionCollection = this.ApplicableActionCollection.Clone<ApplicableActionCollection>(true);
			}
			
			if (this.FavoriteViewCollection != null)
			{
				obj.FavoriteViewCollection = this.FavoriteViewCollection.Clone<FavoriteViewCollection>(true);
			}
			
			if (this.RoleViewCollection != null)
			{
				obj.RoleViewCollection = this.RoleViewCollection.Clone<RoleViewCollection>(true);
			}
			
			if (this.ViewVisitCollection != null)
			{
				obj.ViewVisitCollection = this.ViewVisitCollection.Clone<ViewVisitCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ApplicationHierarchyCollection : ListBase<Int32, ApplicationHierarchy>
    {

    }
}