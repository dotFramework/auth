using DotFramework.Auth.Model.Base;
using DotFramework.Infra.Model;
using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DotFramework.Auth.Model
{
    public sealed partial class User : AuthModelBase, IUser<Int32>
    {
        #region IUser

        public int Id
        {
            get
            {
                return this.UserID;
            }
        }

        #endregion

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, Int32> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);

            // Add custom user claims here

            return userIdentity;
        }
    }

    public sealed partial class UserCollection : ListBase<Int32, User>
    {

    }

    internal sealed class UserMetadata
    {
        [Required]
        public String UserName { get; set; }

        [Required]
        public String PasswordHash { get; set; }

        public String SecurityStamp { get; set; }

        [Required]
        public String FullName { get; set; }

        [Required]
        public String Email { get; set; }

        [Required]
        public Boolean EmailConfirmed { get; set; }

        public String PhoneNumber { get; set; }

        public Boolean? PhoneNumberConfirmed { get; set; }

        [Required]
        public Boolean TwoFactorEnabled { get; set; }

        public DateTime LockoutEndDateUtc { get; set; }

        public Boolean? LockoutEnabled { get; set; }

        [Required]
        public Int32 AccessFailedCount { get; set; }

    }

    public class IPPPasswordHasher : IPasswordHasher
    {
        public string HashPassword(string password)
        {
            return password;
        }

        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            if (hashedPassword == HashPassword(providedPassword))
            {
                return PasswordVerificationResult.Success;
            }
            else
            {
                return PasswordVerificationResult.Failed;
            }
        }
    }
}