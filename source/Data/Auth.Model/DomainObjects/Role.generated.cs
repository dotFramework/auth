using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class Role : AuthModelBase
    {
		private Int32 _RoleID;
		[IsKey]
		[DataMember]
		public Int32 RoleID
		{
			get
			{
				return this._RoleID;
			}
			set
			{
				this.SetProperty(ref this._RoleID, value);
			}
		}

		private Application _Application = new Application();
		[DataMember]
        public Application Application
        {
            get
            {
                return this._Application;
            }
            set
            {
                this.SetProperty(ref this._Application, value);
            }
        }

		private Designation _Designation = new Designation();
		[DataMember]
        public Designation Designation
        {
            get
            {
                return this._Designation;
            }
            set
            {
                this.SetProperty(ref this._Designation, value);
            }
        }

		private String _Code;
		[DataMember]
        public String Code
		{
			get
			{
				return this._Code;
			}
			set
			{
				this.SetProperty(ref this._Code, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}
		
		private RoleActionCollection _RoleActionCollection = new RoleActionCollection();
		[DataMember]
        public RoleActionCollection RoleActionCollection
        {
            get
            {
                return this._RoleActionCollection;
            }
            set
            {
				this.SetProperty(ref this._RoleActionCollection, value);
            }
        }
		
		private RoleOperationCollection _RoleOperationCollection = new RoleOperationCollection();
		[DataMember]
        public RoleOperationCollection RoleOperationCollection
        {
            get
            {
                return this._RoleOperationCollection;
            }
            set
            {
				this.SetProperty(ref this._RoleOperationCollection, value);
            }
        }
		
		private RoleViewCollection _RoleViewCollection = new RoleViewCollection();
		[DataMember]
        public RoleViewCollection RoleViewCollection
        {
            get
            {
                return this._RoleViewCollection;
            }
            set
            {
				this.SetProperty(ref this._RoleViewCollection, value);
            }
        }
		
		private UserRoleCollection _UserRoleCollection = new UserRoleCollection();
		[DataMember]
        public UserRoleCollection UserRoleCollection
        {
            get
            {
                return this._UserRoleCollection;
            }
            set
            {
				this.SetProperty(ref this._UserRoleCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			Role obj = new Role();

			obj.RoleID = this.RoleID;

			if (this.Application != null)
			{
				obj.Application = this.Application.Clone<Application>(true);
			}

			if (this.Designation != null)
			{
				obj.Designation = this.Designation.Clone<Designation>(true);
			}
			obj.Code = this.Code;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.RoleActionCollection != null)
			{
				obj.RoleActionCollection = this.RoleActionCollection.Clone<RoleActionCollection>(true);
			}
			
			if (this.RoleOperationCollection != null)
			{
				obj.RoleOperationCollection = this.RoleOperationCollection.Clone<RoleOperationCollection>(true);
			}
			
			if (this.RoleViewCollection != null)
			{
				obj.RoleViewCollection = this.RoleViewCollection.Clone<RoleViewCollection>(true);
			}
			
			if (this.UserRoleCollection != null)
			{
				obj.UserRoleCollection = this.UserRoleCollection.Clone<UserRoleCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class RoleCollection : ListBase<Int32, Role>
    {

    }
}