using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class Application : AuthModelBase
    {
		
    }

    public sealed partial class ApplicationCollection : ListBase<Int32, Application>
    {

    }

	internal sealed class ApplicationMetadata
    {
		[Required]
		public String Code { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

		public String Path { get; set; }

    }
}