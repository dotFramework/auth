using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class ApplicableOperation : AuthModelBase
    {
		
    }

    public sealed partial class ApplicableOperationCollection : ListBase<Int32, ApplicableOperation>
    {

    }

	internal sealed class ApplicableOperationMetadata
    {
		[Required]
        public ApplicationEndpoint ApplicationEndpoint { get; set; }

		[Required]
        public ServiceOperation ServiceOperation { get; set; }

    }
}