using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class SubApplication : AuthModelBase
    {
		private Int32 _SubApplicationID;
		[IsKey]
		[DataMember]
		public Int32 SubApplicationID
		{
			get
			{
				return this._SubApplicationID;
			}
			set
			{
				this.SetProperty(ref this._SubApplicationID, value);
			}
		}

		private Application _Application = new Application();
		[DataMember]
        public Application Application
        {
            get
            {
                return this._Application;
            }
            set
            {
                this.SetProperty(ref this._Application, value);
            }
        }

		private String _Code;
		[DataMember]
        public String Code
		{
			get
			{
				return this._Code;
			}
			set
			{
				this.SetProperty(ref this._Code, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}
		
		private UserSessionCollection _UserSessionCollection = new UserSessionCollection();
		[DataMember]
        public UserSessionCollection UserSessionCollection
        {
            get
            {
                return this._UserSessionCollection;
            }
            set
            {
				this.SetProperty(ref this._UserSessionCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			SubApplication obj = new SubApplication();

			obj.SubApplicationID = this.SubApplicationID;

			if (this.Application != null)
			{
				obj.Application = this.Application.Clone<Application>(true);
			}
			obj.Code = this.Code;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.UserSessionCollection != null)
			{
				obj.UserSessionCollection = this.UserSessionCollection.Clone<UserSessionCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class SubApplicationCollection : ListBase<Int32, SubApplication>
    {

    }
}