using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class SubApplication : AuthModelBase
    {
		
    }

    public sealed partial class SubApplicationCollection : ListBase<Int32, SubApplication>
    {

    }

	internal sealed class SubApplicationMetadata
    {
		[Required]
        public Application Application { get; set; }

		[Required]
		public String Code { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

    }
}