using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class ApplicationEndpoint : AuthModelBase
    {
		private Int32 _ApplicationEndpointID;
		[IsKey]
		[DataMember]
		public Int32 ApplicationEndpointID
		{
			get
			{
				return this._ApplicationEndpointID;
			}
			set
			{
				this.SetProperty(ref this._ApplicationEndpointID, value);
			}
		}

		private Application _Application = new Application();
		[DataMember]
        public Application Application
        {
            get
            {
                return this._Application;
            }
            set
            {
                this.SetProperty(ref this._Application, value);
            }
        }

		private String _EndpointType;
		[DataMember]
        public String EndpointType
		{
			get
			{
				return this._EndpointType;
			}
			set
			{
				this.SetProperty(ref this._EndpointType, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}

		private Byte _Type;
		[DataMember]
        public Byte Type
		{
			get
			{
				return this._Type;
			}
			set
			{
				this.SetProperty(ref this._Type, value);
			}
		}

		private Boolean _NeedAuthorization;
		[DataMember]
        public Boolean NeedAuthorization
		{
			get
			{
				return this._NeedAuthorization;
			}
			set
			{
				this.SetProperty(ref this._NeedAuthorization, value);
			}
		}
		
		private ApplicableOperationCollection _ApplicableOperationCollection = new ApplicableOperationCollection();
		[DataMember]
        public ApplicableOperationCollection ApplicableOperationCollection
        {
            get
            {
                return this._ApplicableOperationCollection;
            }
            set
            {
				this.SetProperty(ref this._ApplicableOperationCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			ApplicationEndpoint obj = new ApplicationEndpoint();

			obj.ApplicationEndpointID = this.ApplicationEndpointID;

			if (this.Application != null)
			{
				obj.Application = this.Application.Clone<Application>(true);
			}
			obj.EndpointType = this.EndpointType;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.Type = this.Type;
			obj.NeedAuthorization = this.NeedAuthorization;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.ApplicableOperationCollection != null)
			{
				obj.ApplicableOperationCollection = this.ApplicableOperationCollection.Clone<ApplicableOperationCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ApplicationEndpointCollection : ListBase<Int32, ApplicationEndpoint>
    {

    }
}