using System;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class AccountPolicy : AuthModelBase
    {
		
    }

    public sealed partial class AccountPolicyCollection : ListBase<Byte, AccountPolicy>
    {

    }
}