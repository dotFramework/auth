using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class Designation : AuthModelBase
    {
		private Int16 _DesignationID;
		[IsKey]
		[DataMember]
		public Int16 DesignationID
		{
			get
			{
				return this._DesignationID;
			}
			set
			{
				this.SetProperty(ref this._DesignationID, value);
			}
		}

		private String _Code;
		[DataMember]
        public String Code
		{
			get
			{
				return this._Code;
			}
			set
			{
				this.SetProperty(ref this._Code, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}
		
		private RoleCollection _RoleCollection = new RoleCollection();
		[DataMember]
        public RoleCollection RoleCollection
        {
            get
            {
                return this._RoleCollection;
            }
            set
            {
				this.SetProperty(ref this._RoleCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			Designation obj = new Designation();

			obj.DesignationID = this.DesignationID;
			obj.Code = this.Code;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.RoleCollection != null)
			{
				obj.RoleCollection = this.RoleCollection.Clone<RoleCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class DesignationCollection : ListBase<Int16, Designation>
    {

    }
}