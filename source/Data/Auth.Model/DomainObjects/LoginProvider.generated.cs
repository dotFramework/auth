using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class LoginProvider : AuthModelBase
    {
		private Byte _LoginProviderID;
		[IsKey]
		[DataMember]
		public Byte LoginProviderID
		{
			get
			{
				return this._LoginProviderID;
			}
			set
			{
				this.SetProperty(ref this._LoginProviderID, value);
			}
		}

		private String _Code;
		[DataMember]
        public String Code
		{
			get
			{
				return this._Code;
			}
			set
			{
				this.SetProperty(ref this._Code, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}

		private String _URL;
		[DataMember]
        public String URL
		{
			get
			{
				return this._URL;
			}
			set
			{
				this.SetProperty(ref this._URL, value);
			}
		}
		
		private LoginProviderOptionsCollection _LoginProviderOptionsCollection = new LoginProviderOptionsCollection();
		[DataMember]
        public LoginProviderOptionsCollection LoginProviderOptionsCollection
        {
            get
            {
                return this._LoginProviderOptionsCollection;
            }
            set
            {
				this.SetProperty(ref this._LoginProviderOptionsCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			LoginProvider obj = new LoginProvider();

			obj.LoginProviderID = this.LoginProviderID;
			obj.Code = this.Code;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.URL = this.URL;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.LoginProviderOptionsCollection != null)
			{
				obj.LoginProviderOptionsCollection = this.LoginProviderOptionsCollection.Clone<LoginProviderOptionsCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class LoginProviderCollection : ListBase<Byte, LoginProvider>
    {

    }
}