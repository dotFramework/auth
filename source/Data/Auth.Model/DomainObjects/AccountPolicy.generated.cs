using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
    [DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class AccountPolicy : AuthModelBase
    {
        private Byte _AccountPolicyID;
        [IsKey]
        [DataMember]
        public Byte AccountPolicyID
        {
            get
            {
                return this._AccountPolicyID;
            }
            set
            {
                this.SetProperty(ref this._AccountPolicyID, value);
            }
        }

        private String _PolicyName;
        [DataMember]
        public String PolicyName
        {
            get
            {
                return this._PolicyName;
            }
            set
            {
                this.SetProperty(ref this._PolicyName, value);
            }
        }

        private Int32 _MaxAccountAge;
        [DataMember]
        public Int32 MaxAccountAge
        {
            get
            {
                return this._MaxAccountAge;
            }
            set
            {
                this.SetProperty(ref this._MaxAccountAge, value);
            }
        }

        private Int32 _MaxPasswordAge;
        [DataMember]
        public Int32 MaxPasswordAge
        {
            get
            {
                return this._MaxPasswordAge;
            }
            set
            {
                this.SetProperty(ref this._MaxPasswordAge, value);
            }
        }

        private Int32 _MaxPasswordAgeReminder;
        [DataMember]
        public Int32 MaxPasswordAgeReminder
        {
            get
            {
                return this._MaxPasswordAgeReminder;
            }
            set
            {
                this.SetProperty(ref this._MaxPasswordAgeReminder, value);
            }
        }

        private Int32 _EnforcePasswordHistory;
        [DataMember]
        public Int32 EnforcePasswordHistory
        {
            get
            {
                return this._EnforcePasswordHistory;
            }
            set
            {
                this.SetProperty(ref this._EnforcePasswordHistory, value);
            }
        }

        private Int32 _AutoDeleteAccount;
        [DataMember]
        public Int32 AutoDeleteAccount
        {
            get
            {
                return this._AutoDeleteAccount;
            }
            set
            {
                this.SetProperty(ref this._AutoDeleteAccount, value);
            }
        }

        private Int16 _LockoutThresholdMethod;
        [DataMember]
        public Int16 LockoutThresholdMethod
        {
            get
            {
                return this._LockoutThresholdMethod;
            }
            set
            {
                this.SetProperty(ref this._LockoutThresholdMethod, value);
            }
        }

        private Int32 _LockoutThreshold;
        [DataMember]
        public Int32 LockoutThreshold
        {
            get
            {
                return this._LockoutThreshold;
            }
            set
            {
                this.SetProperty(ref this._LockoutThreshold, value);
            }
        }

        private Int16 _LockType;
        [DataMember]
        public Int16 LockType
        {
            get
            {
                return this._LockType;
            }
            set
            {
                this.SetProperty(ref this._LockType, value);
            }
        }

        private Int32 _LockoutDuration;
        [DataMember]
        public Int32 LockoutDuration
        {
            get
            {
                return this._LockoutDuration;
            }
            set
            {
                this.SetProperty(ref this._LockoutDuration, value);
            }
        }

        private Int32 _RequiredLength;
        [DataMember]
        public Int32 RequiredLength
        {
            get
            {
                return this._RequiredLength;
            }
            set
            {
                this.SetProperty(ref this._RequiredLength, value);
            }
        }

        private Boolean _RequireNonLetterOrDigit;
        [DataMember]
        public Boolean RequireNonLetterOrDigit
        {
            get
            {
                return this._RequireNonLetterOrDigit;
            }
            set
            {
                this.SetProperty(ref this._RequireNonLetterOrDigit, value);
            }
        }

        private Boolean _RequireDigit;
        [DataMember]
        public Boolean RequireDigit
        {
            get
            {
                return this._RequireDigit;
            }
            set
            {
                this.SetProperty(ref this._RequireDigit, value);
            }
        }

        private Boolean _RequireLowercase;
        [DataMember]
        public Boolean RequireLowercase
        {
            get
            {
                return this._RequireLowercase;
            }
            set
            {
                this.SetProperty(ref this._RequireLowercase, value);
            }
        }

        private Boolean _RequireUppercase;
        [DataMember]
        public Boolean RequireUppercase
        {
            get
            {
                return this._RequireUppercase;
            }
            set
            {
                this.SetProperty(ref this._RequireUppercase, value);
            }
        }

        private UserCollection _UserCollection = new UserCollection();
        [DataMember]
        public UserCollection UserCollection
        {
            get
            {
                return this._UserCollection;
            }
            set
            {
                this.SetProperty(ref this._UserCollection, value);
            }
        }

        protected override T DeepClone<T>()
        {
            AccountPolicy obj = new AccountPolicy();

            obj.AccountPolicyID = this.AccountPolicyID;
            obj.PolicyName = this.PolicyName;
            obj.MaxAccountAge = this.MaxAccountAge;
            obj.MaxPasswordAge = this.MaxPasswordAge;
            obj.MaxPasswordAgeReminder = this.MaxPasswordAgeReminder;
            obj.EnforcePasswordHistory = this.EnforcePasswordHistory;
            obj.AutoDeleteAccount = this.AutoDeleteAccount;
            obj.LockoutThresholdMethod = this.LockoutThresholdMethod;
            obj.LockoutThreshold = this.LockoutThreshold;
            obj.LockType = this.LockType;
            obj.LockoutDuration = this.LockoutDuration;
            obj.RequiredLength = this.RequiredLength;
            obj.RequireNonLetterOrDigit = this.RequireNonLetterOrDigit;
            obj.RequireDigit = this.RequireDigit;
            obj.RequireLowercase = this.RequireLowercase;
            obj.RequireUppercase = this.RequireUppercase;
            obj.Status = this.Status;
            obj.ModificationTime = this.ModificationTime;
            obj.SessionID = this.SessionID;
            obj.RowVersion = this.RowVersion;

            if (this.UserCollection != null)
            {
                obj.UserCollection = this.UserCollection.Clone<UserCollection>(true);
            }

            return obj as T;
        }
    }

    [CollectionDataContract]
    public sealed partial class AccountPolicyCollection : ListBase<Byte, AccountPolicy>
    {

    }
}