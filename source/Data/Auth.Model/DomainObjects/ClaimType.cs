using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class ClaimType : AuthModelBase
    {
		
    }

    public sealed partial class ClaimTypeCollection : ListBase<Int16, ClaimType>
    {

    }

	internal sealed class ClaimTypeMetadata
    {
		[Required]
		public String Code { get; set; }

		[Required]
		public String ClaimSchema { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

    }
}