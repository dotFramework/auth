using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class UserRole : AuthModelBase
    {
		
    }

    public sealed partial class UserRoleCollection : ListBase<Int64, UserRole>
    {

    }

	internal sealed class UserRoleMetadata
    {
		[Required]
        public User User { get; set; }

		[Required]
        public Role Role { get; set; }

    }
}