using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class UserSession : AuthModelBase
    {
		private Int64 _UserSessionID;
		[IsKey]
		[DataMember]
		public Int64 UserSessionID
		{
			get
			{
				return this._UserSessionID;
			}
			set
			{
				this.SetProperty(ref this._UserSessionID, value);
			}
		}

		private User _User = new User();
		[DataMember]
        public User User
        {
            get
            {
                return this._User;
            }
            set
            {
                this.SetProperty(ref this._User, value);
            }
        }

		private Boolean _IsApplicationSession;
		[DataMember]
        public Boolean IsApplicationSession
		{
			get
			{
				return this._IsApplicationSession;
			}
			set
			{
				this.SetProperty(ref this._IsApplicationSession, value);
			}
		}

		private SubApplication _SubApplication = new SubApplication();
		[DataMember]
        public SubApplication SubApplication
        {
            get
            {
                return this._SubApplication;
            }
            set
            {
                this.SetProperty(ref this._SubApplication, value);
            }
        }

		private String _IP;
		[DataMember]
        public String IP
		{
			get
			{
				return this._IP;
			}
			set
			{
				this.SetProperty(ref this._IP, value);
			}
		}

		private String _BrowserDetail;
		[DataMember]
        public String BrowserDetail
		{
			get
			{
				return this._BrowserDetail;
			}
			set
			{
				this.SetProperty(ref this._BrowserDetail, value);
			}
		}

		private String _OperatingSystemDetail;
		[DataMember]
        public String OperatingSystemDetail
		{
			get
			{
				return this._OperatingSystemDetail;
			}
			set
			{
				this.SetProperty(ref this._OperatingSystemDetail, value);
			}
		}

		private String _UrlReferrer;
		[DataMember]
        public String UrlReferrer
		{
			get
			{
				return this._UrlReferrer;
			}
			set
			{
				this.SetProperty(ref this._UrlReferrer, value);
			}
		}

		private DateTime _StartTime;
		[DataMember]
        public DateTime StartTime
		{
			get
			{
				return this._StartTime;
			}
			set
			{
				this.SetProperty(ref this._StartTime, value);
			}
		}

		private DateTime? _EndTime;
		[DataMember]
        public DateTime? EndTime
		{
			get
			{
				return this._EndTime;
			}
			set
			{
				this.SetProperty(ref this._EndTime, value);
			}
		}

		protected override T DeepClone<T>()
		{
			UserSession obj = new UserSession();

			obj.UserSessionID = this.UserSessionID;

			if (this.User != null)
			{
				obj.User = this.User.Clone<User>(true);
			}
			obj.IsApplicationSession = this.IsApplicationSession;

			if (this.SubApplication != null)
			{
				obj.SubApplication = this.SubApplication.Clone<SubApplication>(true);
			}
			obj.IP = this.IP;
			obj.BrowserDetail = this.BrowserDetail;
			obj.OperatingSystemDetail = this.OperatingSystemDetail;
			obj.UrlReferrer = this.UrlReferrer;
			obj.StartTime = this.StartTime;
			obj.EndTime = this.EndTime;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class UserSessionCollection : ListBase<Int64, UserSession>
    {

    }
}