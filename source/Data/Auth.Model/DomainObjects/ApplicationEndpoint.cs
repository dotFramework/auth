using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class ApplicationEndpoint : AuthModelBase
    {
		
    }

    public sealed partial class ApplicationEndpointCollection : ListBase<Int32, ApplicationEndpoint>
    {

    }

	internal sealed class ApplicationEndpointMetadata
    {
		[Required]
        public Application Application { get; set; }

		[Required]
		public String EndpointType { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

		[Required]
		public Byte Type { get; set; }

		[Required]
		public Boolean NeedAuthorization { get; set; }

    }
}