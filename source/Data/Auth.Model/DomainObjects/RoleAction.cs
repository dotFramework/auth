using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class RoleAction : AuthModelBase
    {
		
    }

    public sealed partial class RoleActionCollection : ListBase<Int32, RoleAction>
    {

    }

	internal sealed class RoleActionMetadata
    {
		[Required]
        public Role Role { get; set; }

		[Required]
        public ApplicableAction ApplicableAction { get; set; }

    }
}