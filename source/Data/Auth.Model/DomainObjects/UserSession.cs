using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class UserSession : AuthModelBase
    {
		
    }

    public sealed partial class UserSessionCollection : ListBase<Int64, UserSession>
    {

    }

	internal sealed class UserSessionMetadata
    {
        public User User { get; set; }

		[Required]
		public Boolean IsApplicationSession { get; set; }

		[Required]
        public SubApplication SubApplication { get; set; }

		public String IP { get; set; }

		public String BrowserDetail { get; set; }

		public String OperatingSystemDetail { get; set; }

		public String UrlReferrer { get; set; }

		[Required]
		public DateTime StartTime { get; set; }

		public DateTime EndTime { get; set; }

    }
}