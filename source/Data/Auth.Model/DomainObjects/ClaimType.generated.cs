using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class ClaimType : AuthModelBase
    {
		private Int16 _ClaimTypeID;
		[IsKey]
		[DataMember]
		public Int16 ClaimTypeID
		{
			get
			{
				return this._ClaimTypeID;
			}
			set
			{
				this.SetProperty(ref this._ClaimTypeID, value);
			}
		}

		private String _Code;
		[DataMember]
        public String Code
		{
			get
			{
				return this._Code;
			}
			set
			{
				this.SetProperty(ref this._Code, value);
			}
		}

		private String _ClaimSchema;
		[DataMember]
        public String ClaimSchema
		{
			get
			{
				return this._ClaimSchema;
			}
			set
			{
				this.SetProperty(ref this._ClaimSchema, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}
		
		private UserClaimCollection _UserClaimCollection = new UserClaimCollection();
		[DataMember]
        public UserClaimCollection UserClaimCollection
        {
            get
            {
                return this._UserClaimCollection;
            }
            set
            {
				this.SetProperty(ref this._UserClaimCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			ClaimType obj = new ClaimType();

			obj.ClaimTypeID = this.ClaimTypeID;
			obj.Code = this.Code;
			obj.ClaimSchema = this.ClaimSchema;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.UserClaimCollection != null)
			{
				obj.UserClaimCollection = this.UserClaimCollection.Clone<UserClaimCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ClaimTypeCollection : ListBase<Int16, ClaimType>
    {

    }
}