using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class LoginProvider : AuthModelBase
    {
		
    }

    public sealed partial class LoginProviderCollection : ListBase<Byte, LoginProvider>
    {

    }

	internal sealed class LoginProviderMetadata
    {
		[Required]
		public String Code { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

		public String URL { get; set; }

    }

    public enum LoginProviderEnum : byte
    {
        Password = 1,
        Google = 2,
        Facebook = 3,
        Microsoft = 4
    }
}