using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class FavoriteView : AuthModelBase
    {
		private Int32 _FavoriteViewID;
		[IsKey]
		[DataMember]
		public Int32 FavoriteViewID
		{
			get
			{
				return this._FavoriteViewID;
			}
			set
			{
				this.SetProperty(ref this._FavoriteViewID, value);
			}
		}

		private ApplicationHierarchy _ApplicationHierarchy = new ApplicationHierarchy();
		[DataMember]
        public ApplicationHierarchy ApplicationHierarchy
        {
            get
            {
                return this._ApplicationHierarchy;
            }
            set
            {
                this.SetProperty(ref this._ApplicationHierarchy, value);
            }
        }

		private User _User = new User();
		[DataMember]
        public User User
        {
            get
            {
                return this._User;
            }
            set
            {
                this.SetProperty(ref this._User, value);
            }
        }

		protected override T DeepClone<T>()
		{
			FavoriteView obj = new FavoriteView();

			obj.FavoriteViewID = this.FavoriteViewID;

			if (this.ApplicationHierarchy != null)
			{
				obj.ApplicationHierarchy = this.ApplicationHierarchy.Clone<ApplicationHierarchy>(true);
			}

			if (this.User != null)
			{
				obj.User = this.User.Clone<User>(true);
			}
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class FavoriteViewCollection : ListBase<Int32, FavoriteView>
    {

    }
}