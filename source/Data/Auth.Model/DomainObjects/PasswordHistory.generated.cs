using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class PasswordHistory : AuthModelBase
    {
		private Int64 _PasswordHistoryID;
		[IsKey]
		[DataMember]
		public Int64 PasswordHistoryID
		{
			get
			{
				return this._PasswordHistoryID;
			}
			set
			{
				this.SetProperty(ref this._PasswordHistoryID, value);
			}
		}

		private User _User = new User();
		[DataMember]
        public User User
        {
            get
            {
                return this._User;
            }
            set
            {
                this.SetProperty(ref this._User, value);
            }
        }

		private String _PasswordHash;
		[DataMember]
        public String PasswordHash
		{
			get
			{
				return this._PasswordHash;
			}
			set
			{
				this.SetProperty(ref this._PasswordHash, value);
			}
		}

		private DateTime _SetPasswordTime;
		[DataMember]
        public DateTime SetPasswordTime
		{
			get
			{
				return this._SetPasswordTime;
			}
			set
			{
				this.SetProperty(ref this._SetPasswordTime, value);
			}
		}

		protected override T DeepClone<T>()
		{
			PasswordHistory obj = new PasswordHistory();

			obj.PasswordHistoryID = this.PasswordHistoryID;

			if (this.User != null)
			{
				obj.User = this.User.Clone<User>(true);
			}
			obj.PasswordHash = this.PasswordHash;
			obj.SetPasswordTime = this.SetPasswordTime;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class PasswordHistoryCollection : ListBase<Int64, PasswordHistory>
    {

    }
}