using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class RoleOperation : AuthModelBase
    {
		
    }

    public sealed partial class RoleOperationCollection : ListBase<Int32, RoleOperation>
    {

    }

	internal sealed class RoleOperationMetadata
    {
		[Required]
        public Role Role { get; set; }

		[Required]
        public ApplicableOperation ApplicableOperation { get; set; }

    }
}