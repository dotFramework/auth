using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class ViewAction : AuthModelBase
    {
		
    }

    public sealed partial class ViewActionCollection : ListBase<Int32, ViewAction>
    {

    }

	internal sealed class ViewActionMetadata
    {
		[Required]
		public String Code { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

		[Required]
		public Boolean NeedAuthorization { get; set; }

		[Required]
		public Boolean IsForAll { get; set; }

    }
}