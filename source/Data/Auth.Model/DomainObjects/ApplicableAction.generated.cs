using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class ApplicableAction : AuthModelBase
    {
		private Int32 _ApplicableActionID;
		[IsKey]
		[DataMember]
		public Int32 ApplicableActionID
		{
			get
			{
				return this._ApplicableActionID;
			}
			set
			{
				this.SetProperty(ref this._ApplicableActionID, value);
			}
		}

		private ApplicationHierarchy _ApplicationHierarchy = new ApplicationHierarchy();
		[DataMember]
        public ApplicationHierarchy ApplicationHierarchy
        {
            get
            {
                return this._ApplicationHierarchy;
            }
            set
            {
                this.SetProperty(ref this._ApplicationHierarchy, value);
            }
        }

		private ViewAction _ViewAction = new ViewAction();
		[DataMember]
        public ViewAction ViewAction
        {
            get
            {
                return this._ViewAction;
            }
            set
            {
                this.SetProperty(ref this._ViewAction, value);
            }
        }
		
		private RoleActionCollection _RoleActionCollection = new RoleActionCollection();
		[DataMember]
        public RoleActionCollection RoleActionCollection
        {
            get
            {
                return this._RoleActionCollection;
            }
            set
            {
				this.SetProperty(ref this._RoleActionCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			ApplicableAction obj = new ApplicableAction();

			obj.ApplicableActionID = this.ApplicableActionID;

			if (this.ApplicationHierarchy != null)
			{
				obj.ApplicationHierarchy = this.ApplicationHierarchy.Clone<ApplicationHierarchy>(true);
			}

			if (this.ViewAction != null)
			{
				obj.ViewAction = this.ViewAction.Clone<ViewAction>(true);
			}
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.RoleActionCollection != null)
			{
				obj.RoleActionCollection = this.RoleActionCollection.Clone<RoleActionCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ApplicableActionCollection : ListBase<Int32, ApplicableAction>
    {

    }
}