using System;
using System.ComponentModel.DataAnnotations;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class Designation : AuthModelBase
    {
		
    }

    public sealed partial class DesignationCollection : ListBase<Int16, Designation>
    {

    }

	internal sealed class DesignationMetadata
    {
		[Required]
		public String Code { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

    }
}