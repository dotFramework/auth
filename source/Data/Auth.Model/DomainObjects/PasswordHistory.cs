using System;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;

namespace DotFramework.Auth.Model
{
    public sealed partial class PasswordHistory : AuthModelBase
    {
		
    }

    public sealed partial class PasswordHistoryCollection : ListBase<Int64, PasswordHistory>
    {

    }
}