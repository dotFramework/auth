using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;
using DotFramework.Infra.Model;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace DotFramework.Auth.Model
{
	[DataContract]
	[SchemaMetadata(SchemaName = "Auth")]
	public sealed partial class ClientDevice : AuthModelBase
    {
		private Int32 _ClientDeviceID;
		[IsKey]
		[DataMember]
		public Int32 ClientDeviceID
		{
			get
			{
				return this._ClientDeviceID;
			}
			set
			{
				this.SetProperty(ref this._ClientDeviceID, value);
			}
		}

		private Device _Device = new Device();
		[DataMember]
        public Device Device
        {
            get
            {
                return this._Device;
            }
            set
            {
                this.SetProperty(ref this._Device, value);
            }
        }

		private Client _Client = new Client();
		[DataMember]
        public Client Client
        {
            get
            {
                return this._Client;
            }
            set
            {
                this.SetProperty(ref this._Client, value);
            }
        }

		protected override T DeepClone<T>()
		{
			ClientDevice obj = new ClientDevice();

			obj.ClientDeviceID = this.ClientDeviceID;

			if (this.Device != null)
			{
				obj.Device = this.Device.Clone<Device>(true);
			}

			if (this.Client != null)
			{
				obj.Client = this.Client.Clone<Client>(true);
			}
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ClientDeviceCollection : ListBase<Int32, ClientDevice>
    {

    }
}