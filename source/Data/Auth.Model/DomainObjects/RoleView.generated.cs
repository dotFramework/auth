using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class RoleView : AuthModelBase
    {
		private Int32 _RoleViewID;
		[IsKey]
		[DataMember]
		public Int32 RoleViewID
		{
			get
			{
				return this._RoleViewID;
			}
			set
			{
				this.SetProperty(ref this._RoleViewID, value);
			}
		}

		private Role _Role = new Role();
		[DataMember]
        public Role Role
        {
            get
            {
                return this._Role;
            }
            set
            {
                this.SetProperty(ref this._Role, value);
            }
        }

		private ApplicationHierarchy _ApplicationHierarchy = new ApplicationHierarchy();
		[DataMember]
        public ApplicationHierarchy ApplicationHierarchy
        {
            get
            {
                return this._ApplicationHierarchy;
            }
            set
            {
                this.SetProperty(ref this._ApplicationHierarchy, value);
            }
        }

		protected override T DeepClone<T>()
		{
			RoleView obj = new RoleView();

			obj.RoleViewID = this.RoleViewID;

			if (this.Role != null)
			{
				obj.Role = this.Role.Clone<Role>(true);
			}

			if (this.ApplicationHierarchy != null)
			{
				obj.ApplicationHierarchy = this.ApplicationHierarchy.Clone<ApplicationHierarchy>(true);
			}
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class RoleViewCollection : ListBase<Int32, RoleView>
    {

    }
}