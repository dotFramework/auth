using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using DotFramework.Infra.Model;
using DotFramework.Auth.Model.Base;
using DotFramework.DynamicQuery.Metadata;

namespace DotFramework.Auth.Model
{
	[DataContract]
    [SchemaMetadata(SchemaName = "Auth")]
    public sealed partial class ServiceOperation : AuthModelBase
    {
		private Int32 _ServiceOperationID;
		[IsKey]
		[DataMember]
		public Int32 ServiceOperationID
		{
			get
			{
				return this._ServiceOperationID;
			}
			set
			{
				this.SetProperty(ref this._ServiceOperationID, value);
			}
		}

		private String _Code;
		[DataMember]
        public String Code
		{
			get
			{
				return this._Code;
			}
			set
			{
				this.SetProperty(ref this._Code, value);
			}
		}

		private String _Name;
		[DataMember]
        public String Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this.SetProperty(ref this._Name, value);
			}
		}

		private String _Description;
		[DataMember]
        public String Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.SetProperty(ref this._Description, value);
			}
		}

		private Boolean _NeedAuthorization;
		[DataMember]
        public Boolean NeedAuthorization
		{
			get
			{
				return this._NeedAuthorization;
			}
			set
			{
				this.SetProperty(ref this._NeedAuthorization, value);
			}
		}

		private Boolean _IsForAll;
		[DataMember]
        public Boolean IsForAll
		{
			get
			{
				return this._IsForAll;
			}
			set
			{
				this.SetProperty(ref this._IsForAll, value);
			}
		}
		
		private ApplicableOperationCollection _ApplicableOperationCollection = new ApplicableOperationCollection();
		[DataMember]
        public ApplicableOperationCollection ApplicableOperationCollection
        {
            get
            {
                return this._ApplicableOperationCollection;
            }
            set
            {
				this.SetProperty(ref this._ApplicableOperationCollection, value);
            }
        }

		protected override T DeepClone<T>()
		{
			ServiceOperation obj = new ServiceOperation();

			obj.ServiceOperationID = this.ServiceOperationID;
			obj.Code = this.Code;
			obj.Name = this.Name;
			obj.Description = this.Description;
			obj.NeedAuthorization = this.NeedAuthorization;
			obj.IsForAll = this.IsForAll;
			obj.Status = this.Status;
			obj.ModificationTime = this.ModificationTime;
			obj.SessionID = this.SessionID;
			obj.RowVersion = this.RowVersion;
			
			if (this.ApplicableOperationCollection != null)
			{
				obj.ApplicableOperationCollection = this.ApplicableOperationCollection.Clone<ApplicableOperationCollection>(true);
			}

			return obj as T;
		}
    }

	[CollectionDataContract]
    public sealed partial class ServiceOperationCollection : ListBase<Int32, ServiceOperation>
    {

    }
}