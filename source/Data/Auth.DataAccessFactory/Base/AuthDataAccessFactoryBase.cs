
using DotFramework.Infra.DataAccessFactory.Autofac;

namespace DotFramework.Auth.DataAccessFactory.Base
{
    public abstract class AuthDataAccessFactoryBase<TDALFacory> : DataAccessFactoryBase<TDALFacory> where TDALFacory : class
    {
    }
}