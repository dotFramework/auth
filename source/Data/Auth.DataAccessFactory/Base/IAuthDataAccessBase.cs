using DotFramework.Infra.DataAccessFactory;

namespace DotFramework.Auth.DataAccessFactory.Base
{
    public interface IAuthDataAccessBase<TKey, TModel, TModelCollection> : IDataAccessBase<TKey, TModel, TModelCollection>
    {
    }
}