using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;
using System;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IClientDeviceDataAccess : IAuthDataAccessBase<Int32, ClientDevice, ClientDeviceCollection>
    {
		ClientDeviceCollection SelectByDevice(Int32 Device);
		Int32 SelectCountByDevice(Int32 Device);
		ClientDeviceCollection SelectByClient(Int32 Client);
		Int32 SelectCountByClient(Int32 Client);
		ClientDevice SelectByKey(Int32 DeviceID, Int32 ClientID);
		bool UpdateByKey(ClientDevice ClientDevice, Int32 DeviceID, Int32 ClientID);
		bool DeleteByKey(Int32 DeviceID, Int32 ClientID);
    }
}