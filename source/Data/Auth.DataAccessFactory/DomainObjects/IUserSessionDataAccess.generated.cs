using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IUserSessionDataAccess : IAuthDataAccessBase<Int64, UserSession, UserSessionCollection>
    {
		UserSessionCollection SelectByUser(Int32 User);
		Int32 SelectCountByUser(Int32 User);
		UserSessionCollection SelectBySubApplication(Int32 SubApplication);
		Int32 SelectCountBySubApplication(Int32 SubApplication);
    }
}