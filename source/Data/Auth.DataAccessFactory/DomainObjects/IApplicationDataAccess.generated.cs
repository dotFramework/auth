using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IApplicationDataAccess : IAuthDataAccessBase<Int32, Application, ApplicationCollection>
    {
		Application SelectByKey(String Code);
		bool UpdateByKey(Application Application, String Code);
		bool DeleteByKey(String Code);
    }
}