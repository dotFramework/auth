using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IServiceOperationDataAccess : IAuthDataAccessBase<Int32, ServiceOperation, ServiceOperationCollection>
    {
		ServiceOperation SelectByKey(String Code);
		bool UpdateByKey(ServiceOperation ServiceOperation, String Code);
		bool DeleteByKey(String Code);
    }
}