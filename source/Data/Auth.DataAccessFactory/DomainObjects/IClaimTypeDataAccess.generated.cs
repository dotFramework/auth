using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IClaimTypeDataAccess : IAuthDataAccessBase<Int16, ClaimType, ClaimTypeCollection>
    {
		ClaimType SelectByKey(String Code);
		bool UpdateByKey(ClaimType ClaimType, String Code);
		bool DeleteByKey(String Code);
    }
}