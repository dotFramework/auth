using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IRefreshTokenDataAccess : IAuthDataAccessBase<Int64, RefreshToken, RefreshTokenCollection>
    {
		RefreshTokenCollection SelectByUser(Int32 User);
		Int32 SelectCountByUser(Int32 User);
		RefreshToken SelectByKey(Int32 UserID);
		bool UpdateByKey(RefreshToken RefreshToken, Int32 UserID);
		bool DeleteByKey(Int32 UserID);
    }
}