using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IPasswordHistoryDataAccess : IAuthDataAccessBase<Int64, PasswordHistory, PasswordHistoryCollection>
    {
		PasswordHistoryCollection SelectByUser(Int32 User);
		Int32 SelectCountByUser(Int32 User);
    }
}