using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IAccountPolicyDataAccess : IAuthDataAccessBase<Byte, AccountPolicy, AccountPolicyCollection>
    {
		AccountPolicy SelectByKey(String PolicyName);
		bool UpdateByKey(AccountPolicy AccountPolicy, String PolicyName);
		bool DeleteByKey(String PolicyName);
    }
}