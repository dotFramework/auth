using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IUserDataAccess : IAuthDataAccessBase<Int32, User, UserCollection>
    {
		UserCollection SelectByAccountPolicy(Byte AccountPolicy);
		Int32 SelectCountByAccountPolicy(Byte AccountPolicy);
		User SelectByKey(String UserName);
		bool UpdateByKey(User User, String UserName);
		bool DeleteByKey(String UserName);
    }
}