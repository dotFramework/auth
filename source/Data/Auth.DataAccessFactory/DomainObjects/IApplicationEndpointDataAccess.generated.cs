using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IApplicationEndpointDataAccess : IAuthDataAccessBase<Int32, ApplicationEndpoint, ApplicationEndpointCollection>
    {
		ApplicationEndpointCollection SelectByApplication(Int32 Application);
		Int32 SelectCountByApplication(Int32 Application);
		ApplicationEndpoint SelectByKey(Int32 ApplicationID, String EndpointType);
		bool UpdateByKey(ApplicationEndpoint ApplicationEndpoint, Int32 ApplicationID, String EndpointType);
		bool DeleteByKey(Int32 ApplicationID, String EndpointType);
    }
}