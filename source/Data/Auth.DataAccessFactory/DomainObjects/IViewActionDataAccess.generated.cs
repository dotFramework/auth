using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IViewActionDataAccess : IAuthDataAccessBase<Int32, ViewAction, ViewActionCollection>
    {
		ViewAction SelectByKey(String Code);
		bool UpdateByKey(ViewAction ViewAction, String Code);
		bool DeleteByKey(String Code);
    }
}