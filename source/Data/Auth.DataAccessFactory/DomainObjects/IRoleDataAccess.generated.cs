using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IRoleDataAccess : IAuthDataAccessBase<Int32, Role, RoleCollection>
    {
		RoleCollection SelectByApplication(Int32 Application);
		Int32 SelectCountByApplication(Int32 Application);
		RoleCollection SelectByDesignation(Int16 Designation);
		Int32 SelectCountByDesignation(Int16 Designation);
		Role SelectByKey(Int32 ApplicationID, String Name);
		bool UpdateByKey(Role Role, Int32 ApplicationID, String Name);
		bool DeleteByKey(Int32 ApplicationID, String Name);
    }
}