using DotFramework.Auth.Model;
using System;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IUserDataAccess
    {
        User SelectByEmail(String Email);
    }
}