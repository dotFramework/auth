using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;
using System;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IClientDataAccess : IAuthDataAccessBase<Int32, Client, ClientCollection>
    {
		Client SelectByKey(String ClientIdentifier);
		bool UpdateByKey(Client Client, String ClientIdentifier);
		bool DeleteByKey(String ClientIdentifier);
    }
}