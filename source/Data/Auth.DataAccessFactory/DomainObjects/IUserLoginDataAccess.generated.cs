using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IUserLoginDataAccess : IAuthDataAccessBase<Int64, UserLogin, UserLoginCollection>
    {
		UserLoginCollection SelectByUser(Int32 User);
		Int32 SelectCountByUser(Int32 User);
		UserLoginCollection SelectByLoginProviderOptions(Int32 LoginProviderOptions);
		Int32 SelectCountByLoginProviderOptions(Int32 LoginProviderOptions);
		UserLogin SelectByKey(Int32 LoginProviderOptionsID, String ProviderKey);
		bool UpdateByKey(UserLogin UserLogin, Int32 LoginProviderOptionsID, String ProviderKey);
		bool DeleteByKey(Int32 LoginProviderOptionsID, String ProviderKey);
    }
}