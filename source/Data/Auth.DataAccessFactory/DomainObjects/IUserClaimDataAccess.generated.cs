using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IUserClaimDataAccess : IAuthDataAccessBase<Int64, UserClaim, UserClaimCollection>
    {
		UserClaimCollection SelectByUser(Int32 User);
		Int32 SelectCountByUser(Int32 User);
		UserClaimCollection SelectByClaimType(Int16 ClaimType);
		Int32 SelectCountByClaimType(Int16 ClaimType);
		UserClaim SelectByKey(Int32 UserID, Int16 ClaimTypeID);
		bool UpdateByKey(UserClaim UserClaim, Int32 UserID, Int16 ClaimTypeID);
		bool DeleteByKey(Int32 UserID, Int16 ClaimTypeID);
    }
}