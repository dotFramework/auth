using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IApplicableActionDataAccess : IAuthDataAccessBase<Int32, ApplicableAction, ApplicableActionCollection>
    {
		ApplicableActionCollection SelectByApplicationHierarchy(Int32 ApplicationHierarchy);
		Int32 SelectCountByApplicationHierarchy(Int32 ApplicationHierarchy);
		ApplicableActionCollection SelectByViewAction(Int32 ViewAction);
		Int32 SelectCountByViewAction(Int32 ViewAction);
		ApplicableAction SelectByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID);
		bool UpdateByKey(ApplicableAction ApplicableAction, Int32 ApplicationHierarchyID, Int32 ViewActionID);
		bool DeleteByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID);
    }
}