using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IRoleActionDataAccess : IAuthDataAccessBase<Int32, RoleAction, RoleActionCollection>
    {
		RoleActionCollection SelectByRole(Int32 Role);
		Int32 SelectCountByRole(Int32 Role);
		RoleActionCollection SelectByApplicableAction(Int32 ApplicableAction);
		Int32 SelectCountByApplicableAction(Int32 ApplicableAction);
		RoleAction SelectByKey(Int32 RoleID, Int32 ApplicableActionID);
		bool UpdateByKey(RoleAction RoleAction, Int32 RoleID, Int32 ApplicableActionID);
		bool DeleteByKey(Int32 RoleID, Int32 ApplicableActionID);
    }
}