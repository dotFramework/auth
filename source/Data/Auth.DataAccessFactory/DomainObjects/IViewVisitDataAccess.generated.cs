using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IViewVisitDataAccess : IAuthDataAccessBase<Int64, ViewVisit, ViewVisitCollection>
    {
		ViewVisitCollection SelectByApplicationHierarchy(Int32 ApplicationHierarchy);
		Int32 SelectCountByApplicationHierarchy(Int32 ApplicationHierarchy);
		ViewVisitCollection SelectByUser(Int32 User);
		Int32 SelectCountByUser(Int32 User);
    }
}