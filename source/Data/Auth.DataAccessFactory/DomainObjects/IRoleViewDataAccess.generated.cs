using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IRoleViewDataAccess : IAuthDataAccessBase<Int32, RoleView, RoleViewCollection>
    {
		RoleViewCollection SelectByRole(Int32 Role);
		Int32 SelectCountByRole(Int32 Role);
		RoleViewCollection SelectByApplicationHierarchy(Int32 ApplicationHierarchy);
		Int32 SelectCountByApplicationHierarchy(Int32 ApplicationHierarchy);
		RoleView SelectByKey(Int32 RoleID, Int32 ApplicationHierarchyID);
		bool UpdateByKey(RoleView RoleView, Int32 RoleID, Int32 ApplicationHierarchyID);
		bool DeleteByKey(Int32 RoleID, Int32 ApplicationHierarchyID);
    }
}