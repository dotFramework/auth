using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface ILoginProviderDataAccess : IAuthDataAccessBase<Byte, LoginProvider, LoginProviderCollection>
    {
		LoginProvider SelectByKey(String Code);
		bool UpdateByKey(LoginProvider LoginProvider, String Code);
		bool DeleteByKey(String Code);
    }
}