using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;
using System;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IDeviceDataAccess : IAuthDataAccessBase<Int32, Device, DeviceCollection>
    {
		Device SelectByKey(String DeviceIdentifier);
		bool UpdateByKey(Device Device, String DeviceIdentifier);
		bool DeleteByKey(String DeviceIdentifier);
    }
}