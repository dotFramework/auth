using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IApplicationHierarchyDataAccess : IAuthDataAccessBase<Int32, ApplicationHierarchy, ApplicationHierarchyCollection>
    {
		ApplicationHierarchyCollection SelectByApplication(Int32 Application);
		Int32 SelectCountByApplication(Int32 Application);
		ApplicationHierarchy SelectByKey(Int32 ApplicationID, String Code);
		bool UpdateByKey(ApplicationHierarchy ApplicationHierarchy, Int32 ApplicationID, String Code);
		bool DeleteByKey(Int32 ApplicationID, String Code);
    }
}