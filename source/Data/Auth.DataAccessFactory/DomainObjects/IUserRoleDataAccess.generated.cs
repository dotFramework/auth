using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IUserRoleDataAccess : IAuthDataAccessBase<Int64, UserRole, UserRoleCollection>
    {
		UserRoleCollection SelectByUser(Int32 User);
		Int32 SelectCountByUser(Int32 User);
		UserRoleCollection SelectByRole(Int32 Role);
		Int32 SelectCountByRole(Int32 Role);
		UserRole SelectByKey(Int32 UserID, Int32 RoleID);
		bool UpdateByKey(UserRole UserRole, Int32 UserID, Int32 RoleID);
		bool DeleteByKey(Int32 UserID, Int32 RoleID);
    }
}