using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface ILoginProviderScopeDataAccess : IAuthDataAccessBase<Int32, LoginProviderScope, LoginProviderScopeCollection>
    {
		LoginProviderScopeCollection SelectByLoginProviderOptions(Int32 LoginProviderOptions);
		Int32 SelectCountByLoginProviderOptions(Int32 LoginProviderOptions);
		LoginProviderScope SelectByKey(Int32 LoginProviderOptionsID, String Scope);
		bool UpdateByKey(LoginProviderScope LoginProviderScope, Int32 LoginProviderOptionsID, String Scope);
		bool DeleteByKey(Int32 LoginProviderOptionsID, String Scope);
    }
}