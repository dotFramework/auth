using DotFramework.Auth.Model;
using System;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IRefreshTokenDataAccess
    {
        RefreshTokenCollection SelectByUserAndGroupToken(Int32 UserID, string GroupToken);
        bool DeleteByUserAndGroupToken(Int32 UserID, string GroupToken);
        bool DeleteByGroupToken(string GroupToken);
        bool InsertByUserAndGroupToken(RefreshToken model);
    }
}