using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IApplicableOperationDataAccess : IAuthDataAccessBase<Int32, ApplicableOperation, ApplicableOperationCollection>
    {
		ApplicableOperationCollection SelectByApplicationEndpoint(Int32 ApplicationEndpoint);
		Int32 SelectCountByApplicationEndpoint(Int32 ApplicationEndpoint);
		ApplicableOperationCollection SelectByServiceOperation(Int32 ServiceOperation);
		Int32 SelectCountByServiceOperation(Int32 ServiceOperation);
		ApplicableOperation SelectByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID);
		bool UpdateByKey(ApplicableOperation ApplicableOperation, Int32 ApplicationEndpointID, Int32 ServiceOperationID);
		bool DeleteByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID);
    }
}