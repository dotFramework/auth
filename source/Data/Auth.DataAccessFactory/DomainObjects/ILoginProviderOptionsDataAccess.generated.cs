using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface ILoginProviderOptionsDataAccess : IAuthDataAccessBase<Int32, LoginProviderOptions, LoginProviderOptionsCollection>
    {
		LoginProviderOptionsCollection SelectByApplication(Int32 Application);
		Int32 SelectCountByApplication(Int32 Application);
		LoginProviderOptionsCollection SelectByLoginProvider(Byte LoginProvider);
		Int32 SelectCountByLoginProvider(Byte LoginProvider);
		LoginProviderOptions SelectByKey(Int32 ApplicationID, Byte LoginProviderID);
		bool UpdateByKey(LoginProviderOptions LoginProviderOptions, Int32 ApplicationID, Byte LoginProviderID);
		bool DeleteByKey(Int32 ApplicationID, Byte LoginProviderID);
    }
}