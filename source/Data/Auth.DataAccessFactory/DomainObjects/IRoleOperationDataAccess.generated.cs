using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IRoleOperationDataAccess : IAuthDataAccessBase<Int32, RoleOperation, RoleOperationCollection>
    {
		RoleOperationCollection SelectByRole(Int32 Role);
		Int32 SelectCountByRole(Int32 Role);
		RoleOperationCollection SelectByApplicableOperation(Int32 ApplicableOperation);
		Int32 SelectCountByApplicableOperation(Int32 ApplicableOperation);
		RoleOperation SelectByKey(Int32 RoleID, Int32 ApplicableOperationID);
		bool UpdateByKey(RoleOperation RoleOperation, Int32 RoleID, Int32 ApplicableOperationID);
		bool DeleteByKey(Int32 RoleID, Int32 ApplicableOperationID);
    }
}