using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface ISubApplicationDataAccess : IAuthDataAccessBase<Int32, SubApplication, SubApplicationCollection>
    {
		SubApplicationCollection SelectByApplication(Int32 Application);
		Int32 SelectCountByApplication(Int32 Application);
		SubApplication SelectByKey(String Code);
		bool UpdateByKey(SubApplication SubApplication, String Code);
		bool DeleteByKey(String Code);
    }
}