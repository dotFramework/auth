using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Auth.DataAccessFactory.Base;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.DataAccessFactory
{
    public partial interface IFavoriteViewDataAccess : IAuthDataAccessBase<Int32, FavoriteView, FavoriteViewCollection>
    {
		FavoriteViewCollection SelectByApplicationHierarchy(Int32 ApplicationHierarchy);
		Int32 SelectCountByApplicationHierarchy(Int32 ApplicationHierarchy);
		FavoriteViewCollection SelectByUser(Int32 User);
		Int32 SelectCountByUser(Int32 User);
		FavoriteView SelectByKey(Int32 ApplicationHierarchyID, Int32 UserID);
		bool UpdateByKey(FavoriteView FavoriteView, Int32 ApplicationHierarchyID, Int32 UserID);
		bool DeleteByKey(Int32 ApplicationHierarchyID, Int32 UserID);
    }
}