using DotFramework.Auth.DataAccessFactory.Base;

namespace DotFramework.Auth.DataAccessFactory
{
    public class AuthDataAccessFactory : AuthDataAccessFactoryBase<AuthDataAccessFactory>
    {
        private AuthDataAccessFactory()
		{
			
		}
    }
}