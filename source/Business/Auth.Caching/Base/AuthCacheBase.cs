using DotFramework.Infra.Caching.Autofac;

namespace DotFramework.Auth.Caching.Base
{
    public abstract class AuthCacheBase<TCache> : SimpleCacheBase<TCache>
        where TCache : class
    {
    }
}