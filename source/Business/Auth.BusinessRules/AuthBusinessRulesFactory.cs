﻿using DotFramework.Infra.BusinessRules.Autofac;

namespace DotFramework.Auth.BusinessRules
{
    public class AuthBusinessRulesFactory : BusinessRulesFactory<AuthBusinessRulesFactory>
    {
        private AuthBusinessRulesFactory()
        {

        }
    }
}
