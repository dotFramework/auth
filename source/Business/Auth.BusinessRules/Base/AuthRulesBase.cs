using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.DataAccessFactory;
using DotFramework.Infra.Model;
using System;

namespace DotFramework.Auth.BusinessRules.Base
{
    public abstract class AuthRulesBase<TKey, TModel, TModelCollection, TDataAccess> : SecureRulesBase<TKey, TModel, TModelCollection, TDataAccess>
        where TModel : SecureDomainModelBase, new()
        where TModelCollection : ListBase<TKey, TModel>, new()
        where TDataAccess : IDataAccessBase<TKey, TModel, TModelCollection>
    {
        protected virtual short EntityID
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
}