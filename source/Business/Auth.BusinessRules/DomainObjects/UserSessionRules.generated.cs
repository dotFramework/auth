using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class UserSessionRules : AuthRulesBase<Int64, UserSession, UserSessionCollection, IUserSessionDataAccess>
    {
		#region Constructors

        public UserSessionRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IUserSessionDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IUserSessionDataAccess>();
			}
		}

		private List<Action<UserSession>> _MasterPopulateActions;
        protected override List<Action<UserSession>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<UserSession>>();

						//this._MasterPopulateActions.Add(this.PopulateUser);
						this._MasterPopulateActions.Add(this.PopulateSubApplication);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<UserSession>> _DetailPopulateActions;
        protected override List<Action<UserSession>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<UserSession>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<UserSessionCollection>();
            }
        }

        protected override UserSessionCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<UserSessionCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateUser(UserSession UserSession)
		{
			if (UserSession.User != null && UserSession.User.UserID != default(Int32))
			{
				UserSession.User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetMaster(UserSession.User.UserID);
			}
		}
		
		private void PopulateSubApplication(UserSession UserSession)
		{
			if (UserSession.SubApplication != null && UserSession.SubApplication.SubApplicationID != default(Int32))
			{
				UserSession.SubApplication = AuthBusinessRulesFactory.Instance.GetBusinessRules<SubApplicationRules>().GetMaster(UserSession.SubApplication.SubApplicationID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual UserSessionCollection GetByUserSimple(Int32 UserID)
        {
			UserSessionCollection UserSessionCollection = DataAccess.SelectByUser(UserID);
				
			if (AllowCache)
            {
				this.Cache.Append(UserSessionCollection.Clone(true));
            }
			
            return UserSessionCollection;
        }

		public virtual UserSessionCollection GetByUser(Int32 UserID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			UserSessionCollection UserSessionCollection = this.GetByUserSimple(UserID);

			if (UserSessionCollection.Count != 0 && parentModel != null)
			{
				UserSessionCollection.ForEach(m => m.User = null);
			}

			this.OnAfterGetCollection(ref UserSessionCollection);
			return UserSessionCollection;
		}

		public virtual Int32 GetCountByUser(Int32 UserID)
        {
			return DataAccess.SelectCountByUser(UserID);
        }
		
		public virtual UserSessionCollection GetBySubApplicationSimple(Int32 SubApplicationID)
        {
			UserSessionCollection UserSessionCollection = DataAccess.SelectBySubApplication(SubApplicationID);
				
			if (AllowCache)
            {
				this.Cache.Append(UserSessionCollection.Clone(true));
            }
			
            return UserSessionCollection;
        }

		public virtual UserSessionCollection GetBySubApplication(Int32 SubApplicationID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			UserSessionCollection UserSessionCollection = this.GetBySubApplicationSimple(SubApplicationID);

			if (UserSessionCollection.Count != 0 && parentModel != null)
			{
				UserSessionCollection.ForEach(m => m.SubApplication = null);
			}

			this.OnAfterGetCollection(ref UserSessionCollection);
			return UserSessionCollection;
		}

		public virtual Int32 GetCountBySubApplication(Int32 SubApplicationID)
        {
			return DataAccess.SelectCountBySubApplication(SubApplicationID);
        }
		
		public override OperationResult<UserSession> AddWithDetail(UserSession UserSession)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (UserSession.User.State == ObjectState.Added)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().AddWithDetail(UserSession.User);
				}

				if (UserSession.SubApplication.State == ObjectState.Added)
                {
					OperationResult<SubApplication> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<SubApplicationRules>().AddWithDetail(UserSession.SubApplication);
				}

                if (UserSession.State == ObjectState.Added)
                {
                    OperationResult<UserSession> result = this.Add(UserSession);
                }

                scope.Complete();
            }

            return new OperationResult<UserSession>(true, UserSession);
        }

        public override OperationResult<UserSession> EditWithDetail(UserSession UserSession)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (UserSession.User.State != ObjectState.None)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().Save(UserSession.User);
				}

				if (UserSession.SubApplication.State != ObjectState.None)
                {
					OperationResult<SubApplication> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<SubApplicationRules>().Save(UserSession.SubApplication);
				}

                if (UserSession.State == ObjectState.Edited)
                {
                    OperationResult<UserSession> result = this.Edit(UserSession);
                }

                scope.Complete();
            }

            return new OperationResult<UserSession>(true, UserSession);
        }

		public override OperationResult<UserSession> RemoveWithDetail(UserSession UserSession)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(UserSession.UserSessionID);

				if (UserSession.User.State == ObjectState.Removed)
                {
					OperationResult result_User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().RemoveWithDetail(UserSession.User);
				}

				if (UserSession.SubApplication.State == ObjectState.Removed)
                {
					OperationResult result_SubApplication = AuthBusinessRulesFactory.Instance.GetBusinessRules<SubApplicationRules>().RemoveWithDetail(UserSession.SubApplication);
				}

				scope.Complete();
			}

			return new OperationResult<UserSession>(true, UserSession);
        }

		#endregion
    }
}