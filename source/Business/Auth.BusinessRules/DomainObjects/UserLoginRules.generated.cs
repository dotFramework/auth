using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class UserLoginRules : AuthRulesBase<Int64, UserLogin, UserLoginCollection, IUserLoginDataAccess>
    {
		#region Constructors

        public UserLoginRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IUserLoginDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IUserLoginDataAccess>();
			}
		}

		private List<Action<UserLogin>> _MasterPopulateActions;
        protected override List<Action<UserLogin>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<UserLogin>>();

						//this._MasterPopulateActions.Add(this.PopulateUser);
						this._MasterPopulateActions.Add(this.PopulateLoginProviderOptions);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<UserLogin>> _DetailPopulateActions;
        protected override List<Action<UserLogin>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<UserLogin>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<UserLoginCollection>();
            }
        }

        protected override UserLoginCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<UserLoginCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateUser(UserLogin UserLogin)
		{
			if (UserLogin.User != null && UserLogin.User.UserID != default(Int32))
			{
				UserLogin.User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetMaster(UserLogin.User.UserID);
			}
		}
		
		private void PopulateLoginProviderOptions(UserLogin UserLogin)
		{
			if (UserLogin.LoginProviderOptions != null && UserLogin.LoginProviderOptions.LoginProviderOptionsID != default(Int32))
			{
				UserLogin.LoginProviderOptions = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().GetMaster(UserLogin.LoginProviderOptions.LoginProviderOptionsID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual UserLoginCollection GetByUserSimple(Int32 UserID)
        {
			UserLoginCollection UserLoginCollection = DataAccess.SelectByUser(UserID);
				
			if (AllowCache)
            {
				this.Cache.Append(UserLoginCollection.Clone(true));
            }
			
            return UserLoginCollection;
        }

		public virtual UserLoginCollection GetByUser(Int32 UserID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			UserLoginCollection UserLoginCollection = this.GetByUserSimple(UserID);

			if (UserLoginCollection.Count != 0 && parentModel != null)
			{
				UserLoginCollection.ForEach(m => m.User = null);
			}

			this.OnAfterGetCollection(ref UserLoginCollection);
			return UserLoginCollection;
		}

		public virtual Int32 GetCountByUser(Int32 UserID)
        {
			return DataAccess.SelectCountByUser(UserID);
        }
		
		public virtual UserLoginCollection GetByLoginProviderOptionsSimple(Int32 LoginProviderOptionsID)
        {
			UserLoginCollection UserLoginCollection = DataAccess.SelectByLoginProviderOptions(LoginProviderOptionsID);
				
			if (AllowCache)
            {
				this.Cache.Append(UserLoginCollection.Clone(true));
            }
			
            return UserLoginCollection;
        }

		public virtual UserLoginCollection GetByLoginProviderOptions(Int32 LoginProviderOptionsID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			UserLoginCollection UserLoginCollection = this.GetByLoginProviderOptionsSimple(LoginProviderOptionsID);

			if (UserLoginCollection.Count != 0 && parentModel != null)
			{
				UserLoginCollection.ForEach(m => m.LoginProviderOptions = null);
			}

			this.OnAfterGetCollection(ref UserLoginCollection);
			return UserLoginCollection;
		}

		public virtual Int32 GetCountByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
			return DataAccess.SelectCountByLoginProviderOptions(LoginProviderOptionsID);
        }
		
		public override OperationResult<UserLogin> AddWithDetail(UserLogin UserLogin)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (UserLogin.User.State == ObjectState.Added)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().AddWithDetail(UserLogin.User);
				}

				if (UserLogin.LoginProviderOptions.State == ObjectState.Added)
                {
					OperationResult<LoginProviderOptions> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().AddWithDetail(UserLogin.LoginProviderOptions);
				}

                if (UserLogin.State == ObjectState.Added)
                {
                    OperationResult<UserLogin> result = this.Add(UserLogin);
                }

                scope.Complete();
            }

            return new OperationResult<UserLogin>(true, UserLogin);
        }

        public override OperationResult<UserLogin> EditWithDetail(UserLogin UserLogin)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (UserLogin.User.State != ObjectState.None)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().Save(UserLogin.User);
				}

				if (UserLogin.LoginProviderOptions.State != ObjectState.None)
                {
					OperationResult<LoginProviderOptions> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().Save(UserLogin.LoginProviderOptions);
				}

                if (UserLogin.State == ObjectState.Edited)
                {
                    OperationResult<UserLogin> result = this.Edit(UserLogin);
                }

                scope.Complete();
            }

            return new OperationResult<UserLogin>(true, UserLogin);
        }

		public override OperationResult<UserLogin> RemoveWithDetail(UserLogin UserLogin)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(UserLogin.UserLoginID);

				if (UserLogin.User.State == ObjectState.Removed)
                {
					OperationResult result_User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().RemoveWithDetail(UserLogin.User);
				}

				if (UserLogin.LoginProviderOptions.State == ObjectState.Removed)
                {
					OperationResult result_LoginProviderOptions = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().RemoveWithDetail(UserLogin.LoginProviderOptions);
				}

				scope.Complete();
			}

			return new OperationResult<UserLogin>(true, UserLogin);
        }

		public UserLogin GetByKeySimple(Int32 LoginProviderOptionsID, String ProviderKey)
		{
			UserLogin UserLogin = DataAccess.SelectByKey(LoginProviderOptionsID, ProviderKey);

            if (AllowCache)
            {
                this.Cache.Add(UserLogin.Clone<UserLogin>(true));
            }

            return UserLogin;
		}

		public UserLogin GetByKey(Int32 LoginProviderOptionsID, String ProviderKey)
		{
			UserLogin UserLogin = this.GetByKeySimple(LoginProviderOptionsID, ProviderKey);
            this.OnAfterGet(ref UserLogin);

            return UserLogin;
		}

		public OperationResult<UserLogin> EditByKey(UserLogin UserLogin, Int32 LoginProviderOptionsID, String ProviderKey)
		{
			this.OnBeforeEdit(ref UserLogin);
            UserLogin.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(UserLogin, LoginProviderOptionsID, ProviderKey);

			this.OnAfterEdit(ref UserLogin);
            return new OperationResult<UserLogin>(isEdit, UserLogin);
		}

		public OperationResult RemoveByKey(Int32 LoginProviderOptionsID, String ProviderKey)
		{
			bool isRemove = DataAccess.DeleteByKey(LoginProviderOptionsID, ProviderKey);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}