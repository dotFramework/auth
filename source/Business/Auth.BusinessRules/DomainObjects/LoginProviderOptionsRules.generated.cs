using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class LoginProviderOptionsRules : AuthRulesBase<Int32, LoginProviderOptions, LoginProviderOptionsCollection, ILoginProviderOptionsDataAccess>
    {
		#region Constructors

        public LoginProviderOptionsRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override ILoginProviderOptionsDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<ILoginProviderOptionsDataAccess>();
			}
		}

		private List<Action<LoginProviderOptions>> _MasterPopulateActions;
        protected override List<Action<LoginProviderOptions>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<LoginProviderOptions>>();

						this._MasterPopulateActions.Add(this.PopulateApplication);
						this._MasterPopulateActions.Add(this.PopulateLoginProvider);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<LoginProviderOptions>> _DetailPopulateActions;
        protected override List<Action<LoginProviderOptions>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<LoginProviderOptions>>();

						//this._DetailPopulateActions.Add(this.PopulateLoginProviderScopeCollection);
						//this._DetailPopulateActions.Add(this.PopulateLoginProviderScopeCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateUserLoginCollection);
						//this._DetailPopulateActions.Add(this.PopulateUserLoginCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<LoginProviderOptionsCollection>();
            }
        }

        protected override LoginProviderOptionsCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<LoginProviderOptionsCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplication(LoginProviderOptions LoginProviderOptions)
		{
			if (LoginProviderOptions.Application != null && LoginProviderOptions.Application.ApplicationID != default(Int32))
			{
				LoginProviderOptions.Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().GetMaster(LoginProviderOptions.Application.ApplicationID);
			}
		}
		
		private void PopulateLoginProvider(LoginProviderOptions LoginProviderOptions)
		{
			if (LoginProviderOptions.LoginProvider != null && LoginProviderOptions.LoginProvider.LoginProviderID != default(Byte))
			{
				LoginProviderOptions.LoginProvider = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderRules>().GetMaster(LoginProviderOptions.LoginProvider.LoginProviderID);
			}
		}
		
		private void PopulateLoginProviderScopeCollection(LoginProviderOptions LoginProviderOptions)
		{
			LoginProviderOptions.LoginProviderScopeCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderScopeRules>().GetByLoginProviderOptions(LoginProviderOptions.LoginProviderOptionsID, LoginProviderOptions);
		}

		private void PopulateLoginProviderScopeCollectionCount(LoginProviderOptions LoginProviderOptions)
		{
			LoginProviderOptions.LoginProviderScopeCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderScopeRules>().GetCountByLoginProviderOptions(LoginProviderOptions.LoginProviderOptionsID);
		}
		
		private void PopulateUserLoginCollection(LoginProviderOptions LoginProviderOptions)
		{
			LoginProviderOptions.UserLoginCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserLoginRules>().GetByLoginProviderOptions(LoginProviderOptions.LoginProviderOptionsID, LoginProviderOptions);
		}

		private void PopulateUserLoginCollectionCount(LoginProviderOptions LoginProviderOptions)
		{
			LoginProviderOptions.UserLoginCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserLoginRules>().GetCountByLoginProviderOptions(LoginProviderOptions.LoginProviderOptionsID);
		}

		#endregion

		#region Public Methods
		
		public virtual LoginProviderOptionsCollection GetByApplicationSimple(Int32 ApplicationID)
        {
			LoginProviderOptionsCollection LoginProviderOptionsCollection = DataAccess.SelectByApplication(ApplicationID);
				
			if (AllowCache)
            {
				this.Cache.Append(LoginProviderOptionsCollection.Clone(true));
            }
			
            return LoginProviderOptionsCollection;
        }

		public virtual LoginProviderOptionsCollection GetByApplication(Int32 ApplicationID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			LoginProviderOptionsCollection LoginProviderOptionsCollection = this.GetByApplicationSimple(ApplicationID);

			if (LoginProviderOptionsCollection.Count != 0 && parentModel != null)
			{
				LoginProviderOptionsCollection.ForEach(m => m.Application = null);
			}

			this.OnAfterGetCollection(ref LoginProviderOptionsCollection);
			return LoginProviderOptionsCollection;
		}

		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
			return DataAccess.SelectCountByApplication(ApplicationID);
        }
		
		public virtual LoginProviderOptionsCollection GetByLoginProviderSimple(Byte LoginProviderID)
        {
			LoginProviderOptionsCollection LoginProviderOptionsCollection = DataAccess.SelectByLoginProvider(LoginProviderID);
				
			if (AllowCache)
            {
				this.Cache.Append(LoginProviderOptionsCollection.Clone(true));
            }
			
            return LoginProviderOptionsCollection;
        }

		public virtual LoginProviderOptionsCollection GetByLoginProvider(Byte LoginProviderID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			LoginProviderOptionsCollection LoginProviderOptionsCollection = this.GetByLoginProviderSimple(LoginProviderID);

			if (LoginProviderOptionsCollection.Count != 0 && parentModel != null)
			{
				LoginProviderOptionsCollection.ForEach(m => m.LoginProvider = null);
			}

			this.OnAfterGetCollection(ref LoginProviderOptionsCollection);
			return LoginProviderOptionsCollection;
		}

		public virtual Int32 GetCountByLoginProvider(Byte LoginProviderID)
        {
			return DataAccess.SelectCountByLoginProvider(LoginProviderID);
        }
		
		public override OperationResult<LoginProviderOptions> AddWithDetail(LoginProviderOptions LoginProviderOptions)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (LoginProviderOptions.Application.State == ObjectState.Added)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().AddWithDetail(LoginProviderOptions.Application);
				}

				if (LoginProviderOptions.LoginProvider.State == ObjectState.Added)
                {
					OperationResult<LoginProvider> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderRules>().AddWithDetail(LoginProviderOptions.LoginProvider);
				}

                if (LoginProviderOptions.State == ObjectState.Added)
                {
                    OperationResult<LoginProviderOptions> result = this.Add(LoginProviderOptions);
                }
				
				foreach (LoginProviderScope LoginProviderScope in LoginProviderOptions.LoginProviderScopeCollection.Where(obj => obj.State == ObjectState.Added))
                {
					LoginProviderScope.LoginProviderOptions = new LoginProviderOptions { LoginProviderOptionsID = LoginProviderOptions.LoginProviderOptionsID };

                    OperationResult<LoginProviderScope> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderScopeRules>().AddWithDetail(LoginProviderScope);
                }
				
				foreach (UserLogin UserLogin in LoginProviderOptions.UserLoginCollection.Where(obj => obj.State == ObjectState.Added))
                {
					UserLogin.LoginProviderOptions = new LoginProviderOptions { LoginProviderOptionsID = LoginProviderOptions.LoginProviderOptionsID };

                    OperationResult<UserLogin> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserLoginRules>().AddWithDetail(UserLogin);
                }

                scope.Complete();
            }

            return new OperationResult<LoginProviderOptions>(true, LoginProviderOptions);
        }

        public override OperationResult<LoginProviderOptions> EditWithDetail(LoginProviderOptions LoginProviderOptions)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (LoginProviderOptions.Application.State != ObjectState.None)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().Save(LoginProviderOptions.Application);
				}

				if (LoginProviderOptions.LoginProvider.State != ObjectState.None)
                {
					OperationResult<LoginProvider> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderRules>().Save(LoginProviderOptions.LoginProvider);
				}

                if (LoginProviderOptions.State == ObjectState.Edited)
                {
                    OperationResult<LoginProviderOptions> result = this.Edit(LoginProviderOptions);
                }
                    
				foreach (LoginProviderScope LoginProviderScope in LoginProviderOptions.LoginProviderScopeCollection.Where(obj => obj.State != ObjectState.None))
                {
					LoginProviderScope.LoginProviderOptions = new LoginProviderOptions { LoginProviderOptionsID = LoginProviderOptions.LoginProviderOptionsID };

                    OperationResult<LoginProviderScope> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderScopeRules>().Save(LoginProviderScope);
                }
                    
				foreach (UserLogin UserLogin in LoginProviderOptions.UserLoginCollection.Where(obj => obj.State != ObjectState.None))
                {
					UserLogin.LoginProviderOptions = new LoginProviderOptions { LoginProviderOptionsID = LoginProviderOptions.LoginProviderOptionsID };

                    OperationResult<UserLogin> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserLoginRules>().Save(UserLogin);
                }

                scope.Complete();
            }

            return new OperationResult<LoginProviderOptions>(true, LoginProviderOptions);
        }

		public override OperationResult<LoginProviderOptions> RemoveWithDetail(LoginProviderOptions LoginProviderOptions)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(LoginProviderOptions.LoginProviderOptionsID);

				if (LoginProviderOptions.Application.State == ObjectState.Removed)
                {
					OperationResult result_Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().RemoveWithDetail(LoginProviderOptions.Application);
				}

				if (LoginProviderOptions.LoginProvider.State == ObjectState.Removed)
                {
					OperationResult result_LoginProvider = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderRules>().RemoveWithDetail(LoginProviderOptions.LoginProvider);
				}

				scope.Complete();
			}

			return new OperationResult<LoginProviderOptions>(true, LoginProviderOptions);
        }

		public LoginProviderOptions GetByKeySimple(Int32 ApplicationID, Byte LoginProviderID)
		{
			LoginProviderOptions LoginProviderOptions = DataAccess.SelectByKey(ApplicationID, LoginProviderID);

            if (AllowCache)
            {
                this.Cache.Add(LoginProviderOptions.Clone<LoginProviderOptions>(true));
            }

            return LoginProviderOptions;
		}

		public LoginProviderOptions GetByKey(Int32 ApplicationID, Byte LoginProviderID)
		{
			LoginProviderOptions LoginProviderOptions = this.GetByKeySimple(ApplicationID, LoginProviderID);
            this.OnAfterGet(ref LoginProviderOptions);

            return LoginProviderOptions;
		}

		public OperationResult<LoginProviderOptions> EditByKey(LoginProviderOptions LoginProviderOptions, Int32 ApplicationID, Byte LoginProviderID)
		{
			this.OnBeforeEdit(ref LoginProviderOptions);
            LoginProviderOptions.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(LoginProviderOptions, ApplicationID, LoginProviderID);

			this.OnAfterEdit(ref LoginProviderOptions);
            return new OperationResult<LoginProviderOptions>(isEdit, LoginProviderOptions);
		}

		public OperationResult RemoveByKey(Int32 ApplicationID, Byte LoginProviderID)
		{
			bool isRemove = DataAccess.DeleteByKey(ApplicationID, LoginProviderID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}