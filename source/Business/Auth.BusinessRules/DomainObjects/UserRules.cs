using System;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class UserRules
	{
		#region Overrided Methods

        protected override void ResetPopulateActions()
        {
            //Change Default PopulateActions
            //this.MasterPopulateActions.Clear();
			//this.DetailPopulateActions.Clear();
        }

        #endregion

		#region Overrided Properties

        //public override Func<User, dynamic> OrderByKey
        //{
        //    get
        //    {
        //        return model => model.UserID;
        //    }
        //}

		//public override Func<User, dynamic> OrderByDescendingKey
        //{
        //    get
        //    {
        //        return model => model.UserID;
        //    }
        //}

        #endregion

        #region Public Methods

        public virtual User GetByUserName(String UserName)
        {
            return GetByKey(UserName);
        }

        public virtual User GetByEmail(String Email)
        {
            User User = DataAccess.SelectByEmail(Email);

            if (AllowCache)
            {
                this.Cache.Add(User.Clone<User>(true));
            }

            this.OnAfterGet(ref User);

            return User;
        }

        #endregion
    }
}