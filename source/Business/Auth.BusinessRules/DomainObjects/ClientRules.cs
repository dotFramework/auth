using DotFramework.Auth.Model;
using System;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ClientRules
    {
        #region Overrided Methods

        protected override void ResetPopulateActions()
        {
            //Change Default PopulateActions
            //this.MasterPopulateActions.Clear();
            //this.DetailPopulateActions.Clear();
        }

        #endregion

        #region Overrided Properties

        //public override Func<Client, dynamic> OrderByKey
        //{
        //    get
        //    {
        //        return model => model.ClientID;
        //    }
        //}

        //public override Func<Client, dynamic> OrderByDescendingKey
        //{
        //    get
        //    {
        //        return model => model.ClientID;
        //    }
        //}

        #endregion

        #region Public Methods

        public virtual Client GetByClientUID(string clientUid)
        {
            Client Client = GetByFilterSingleRow(c => c.ClientUID == clientUid);

            if (AllowCache)
            {
                this.Cache.Add(Client.Clone<Client>(true));
            }

            return Client;
        }

        public virtual Client GetByClientUIDAndSecret(string clientUid, string clientSecret)
        {
            Client Client = GetByFilterSingleRow(c => c.ClientUID == clientUid && c.ClientSecret == clientSecret);

            if (AllowCache)
            {
                this.Cache.Add(Client.Clone<Client>(true));
            }

            return Client;
        }

        #endregion
    }
}