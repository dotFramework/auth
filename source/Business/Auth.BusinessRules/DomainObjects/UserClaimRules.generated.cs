using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Caching;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Model;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;
using System;
using System.Collections.Generic;
using System.Transactions;

namespace DotFramework.Auth.BusinessRules
{
    public partial class UserClaimRules : AuthRulesBase<Int64, UserClaim, UserClaimCollection, IUserClaimDataAccess>
    {
		#region Constructors

        public UserClaimRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IUserClaimDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IUserClaimDataAccess>();
			}
		}

		private List<Action<UserClaim>> _MasterPopulateActions;
        protected override List<Action<UserClaim>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<UserClaim>>();

						//this._MasterPopulateActions.Add(this.PopulateUser);
						this._MasterPopulateActions.Add(this.PopulateClaimType);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<UserClaim>> _DetailPopulateActions;
        protected override List<Action<UserClaim>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<UserClaim>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<UserClaimCollection>();
            }
        }

        protected override UserClaimCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<UserClaimCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateUser(UserClaim UserClaim)
		{
			if (UserClaim.User != null && UserClaim.User.UserID != default(Int32))
			{
				UserClaim.User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetMaster(UserClaim.User.UserID);
			}
		}
		
		private void PopulateClaimType(UserClaim UserClaim)
		{
			if (UserClaim.ClaimType != null && UserClaim.ClaimType.ClaimTypeID != default(Int16))
			{
				UserClaim.ClaimType = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClaimTypeRules>().GetMaster(UserClaim.ClaimType.ClaimTypeID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual UserClaimCollection GetByUserSimple(Int32 UserID)
        {
			UserClaimCollection UserClaimCollection = DataAccess.SelectByUser(UserID);
				
			if (AllowCache)
            {
				this.Cache.Append(UserClaimCollection.Clone(true));
            }
			
            return UserClaimCollection;
        }

		public virtual UserClaimCollection GetByUser(Int32 UserID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			UserClaimCollection UserClaimCollection = this.GetByUserSimple(UserID);

			if (UserClaimCollection.Count != 0 && parentModel != null)
			{
				UserClaimCollection.ForEach(m => m.User = null);
			}

			this.OnAfterGetCollection(ref UserClaimCollection);
			return UserClaimCollection;
		}

		public virtual Int32 GetCountByUser(Int32 UserID)
        {
			return DataAccess.SelectCountByUser(UserID);
        }
		
		public virtual UserClaimCollection GetByClaimTypeSimple(Int16 ClaimTypeID)
        {
			UserClaimCollection UserClaimCollection = DataAccess.SelectByClaimType(ClaimTypeID);
				
			if (AllowCache)
            {
				this.Cache.Append(UserClaimCollection.Clone(true));
            }
			
            return UserClaimCollection;
        }

		public virtual UserClaimCollection GetByClaimType(Int16 ClaimTypeID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			UserClaimCollection UserClaimCollection = this.GetByClaimTypeSimple(ClaimTypeID);

			if (UserClaimCollection.Count != 0 && parentModel != null)
			{
				UserClaimCollection.ForEach(m => m.ClaimType = null);
			}

			this.OnAfterGetCollection(ref UserClaimCollection);
			return UserClaimCollection;
		}

		public virtual Int32 GetCountByClaimType(Int16 ClaimTypeID)
        {
			return DataAccess.SelectCountByClaimType(ClaimTypeID);
        }
		
		public override OperationResult<UserClaim> AddWithDetail(UserClaim UserClaim)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (UserClaim.User.State == ObjectState.Added)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().AddWithDetail(UserClaim.User);
				}

				if (UserClaim.ClaimType.State == ObjectState.Added)
                {
					OperationResult<ClaimType> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClaimTypeRules>().AddWithDetail(UserClaim.ClaimType);
				}

                if (UserClaim.State == ObjectState.Added)
                {
                    OperationResult<UserClaim> result = this.Add(UserClaim);
                }

                scope.Complete();
            }

            return new OperationResult<UserClaim>(true, UserClaim);
        }

        public override OperationResult<UserClaim> EditWithDetail(UserClaim UserClaim)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (UserClaim.User.State != ObjectState.None)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().Save(UserClaim.User);
				}

				if (UserClaim.ClaimType.State != ObjectState.None)
                {
					OperationResult<ClaimType> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClaimTypeRules>().Save(UserClaim.ClaimType);
				}

                if (UserClaim.State == ObjectState.Edited)
                {
                    OperationResult<UserClaim> result = this.Edit(UserClaim);
                }

                scope.Complete();
            }

            return new OperationResult<UserClaim>(true, UserClaim);
        }

		public override OperationResult<UserClaim> RemoveWithDetail(UserClaim UserClaim)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(UserClaim.UserClaimID);

				if (UserClaim.User.State == ObjectState.Removed)
                {
					OperationResult result_User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().RemoveWithDetail(UserClaim.User);
				}

				if (UserClaim.ClaimType.State == ObjectState.Removed)
                {
					OperationResult result_ClaimType = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClaimTypeRules>().RemoveWithDetail(UserClaim.ClaimType);
				}

				scope.Complete();
			}

			return new OperationResult<UserClaim>(true, UserClaim);
        }

		public UserClaim GetByKeySimple(Int32 UserID, Int16 ClaimTypeID)
		{
			UserClaim UserClaim = DataAccess.SelectByKey(UserID, ClaimTypeID);

            if (AllowCache)
            {
                this.Cache.Add(UserClaim.Clone<UserClaim>(true));
            }

            return UserClaim;
		}

		public UserClaim GetByKey(Int32 UserID, Int16 ClaimTypeID)
		{
			UserClaim UserClaim = this.GetByKeySimple(UserID, ClaimTypeID);
            this.OnAfterGet(ref UserClaim);

            return UserClaim;
		}

		public OperationResult<UserClaim> EditByKey(UserClaim UserClaim, Int32 UserID, Int16 ClaimTypeID)
		{
			this.OnBeforeEdit(ref UserClaim);
            UserClaim.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(UserClaim, UserID, ClaimTypeID);

			this.OnAfterEdit(ref UserClaim);
            return new OperationResult<UserClaim>(isEdit, UserClaim);
		}

		public OperationResult RemoveByKey(Int32 UserID, Int16 ClaimTypeID)
		{
			bool isRemove = DataAccess.DeleteByKey(UserID, ClaimTypeID);
            return new OperationResult(isRemove);
		}

        #endregion
    }
}