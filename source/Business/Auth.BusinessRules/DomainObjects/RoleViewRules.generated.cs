using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class RoleViewRules : AuthRulesBase<Int32, RoleView, RoleViewCollection, IRoleViewDataAccess>
    {
		#region Constructors

        public RoleViewRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IRoleViewDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IRoleViewDataAccess>();
			}
		}

		private List<Action<RoleView>> _MasterPopulateActions;
        protected override List<Action<RoleView>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<RoleView>>();

						this._MasterPopulateActions.Add(this.PopulateRole);
						this._MasterPopulateActions.Add(this.PopulateApplicationHierarchy);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<RoleView>> _DetailPopulateActions;
        protected override List<Action<RoleView>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<RoleView>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<RoleViewCollection>();
            }
        }

        protected override RoleViewCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<RoleViewCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateRole(RoleView RoleView)
		{
			if (RoleView.Role != null && RoleView.Role.RoleID != default(Int32))
			{
				RoleView.Role = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().GetMaster(RoleView.Role.RoleID);
			}
		}
		
		private void PopulateApplicationHierarchy(RoleView RoleView)
		{
			if (RoleView.ApplicationHierarchy != null && RoleView.ApplicationHierarchy.ApplicationHierarchyID != default(Int32))
			{
				RoleView.ApplicationHierarchy = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().GetMaster(RoleView.ApplicationHierarchy.ApplicationHierarchyID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual RoleViewCollection GetByRoleSimple(Int32 RoleID)
        {
			RoleViewCollection RoleViewCollection = DataAccess.SelectByRole(RoleID);
				
			if (AllowCache)
            {
				this.Cache.Append(RoleViewCollection.Clone(true));
            }
			
            return RoleViewCollection;
        }

		public virtual RoleViewCollection GetByRole(Int32 RoleID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			RoleViewCollection RoleViewCollection = this.GetByRoleSimple(RoleID);

			if (RoleViewCollection.Count != 0 && parentModel != null)
			{
				RoleViewCollection.ForEach(m => m.Role = null);
			}

			this.OnAfterGetCollection(ref RoleViewCollection);
			return RoleViewCollection;
		}

		public virtual Int32 GetCountByRole(Int32 RoleID)
        {
			return DataAccess.SelectCountByRole(RoleID);
        }
		
		public virtual RoleViewCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID)
        {
			RoleViewCollection RoleViewCollection = DataAccess.SelectByApplicationHierarchy(ApplicationHierarchyID);
				
			if (AllowCache)
            {
				this.Cache.Append(RoleViewCollection.Clone(true));
            }
			
            return RoleViewCollection;
        }

		public virtual RoleViewCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			RoleViewCollection RoleViewCollection = this.GetByApplicationHierarchySimple(ApplicationHierarchyID);

			if (RoleViewCollection.Count != 0 && parentModel != null)
			{
				RoleViewCollection.ForEach(m => m.ApplicationHierarchy = null);
			}

			this.OnAfterGetCollection(ref RoleViewCollection);
			return RoleViewCollection;
		}

		public virtual Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
			return DataAccess.SelectCountByApplicationHierarchy(ApplicationHierarchyID);
        }
		
		public override OperationResult<RoleView> AddWithDetail(RoleView RoleView)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (RoleView.Role.State == ObjectState.Added)
                {
					OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().AddWithDetail(RoleView.Role);
				}

				if (RoleView.ApplicationHierarchy.State == ObjectState.Added)
                {
					OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().AddWithDetail(RoleView.ApplicationHierarchy);
				}

                if (RoleView.State == ObjectState.Added)
                {
                    OperationResult<RoleView> result = this.Add(RoleView);
                }

                scope.Complete();
            }

            return new OperationResult<RoleView>(true, RoleView);
        }

        public override OperationResult<RoleView> EditWithDetail(RoleView RoleView)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (RoleView.Role.State != ObjectState.None)
                {
					OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Save(RoleView.Role);
				}

				if (RoleView.ApplicationHierarchy.State != ObjectState.None)
                {
					OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().Save(RoleView.ApplicationHierarchy);
				}

                if (RoleView.State == ObjectState.Edited)
                {
                    OperationResult<RoleView> result = this.Edit(RoleView);
                }

                scope.Complete();
            }

            return new OperationResult<RoleView>(true, RoleView);
        }

		public override OperationResult<RoleView> RemoveWithDetail(RoleView RoleView)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(RoleView.RoleViewID);

				if (RoleView.Role.State == ObjectState.Removed)
                {
					OperationResult result_Role = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().RemoveWithDetail(RoleView.Role);
				}

				if (RoleView.ApplicationHierarchy.State == ObjectState.Removed)
                {
					OperationResult result_ApplicationHierarchy = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().RemoveWithDetail(RoleView.ApplicationHierarchy);
				}

				scope.Complete();
			}

			return new OperationResult<RoleView>(true, RoleView);
        }

		public RoleView GetByKeySimple(Int32 RoleID, Int32 ApplicationHierarchyID)
		{
			RoleView RoleView = DataAccess.SelectByKey(RoleID, ApplicationHierarchyID);

            if (AllowCache)
            {
                this.Cache.Add(RoleView.Clone<RoleView>(true));
            }

            return RoleView;
		}

		public RoleView GetByKey(Int32 RoleID, Int32 ApplicationHierarchyID)
		{
			RoleView RoleView = this.GetByKeySimple(RoleID, ApplicationHierarchyID);
            this.OnAfterGet(ref RoleView);

            return RoleView;
		}

		public OperationResult<RoleView> EditByKey(RoleView RoleView, Int32 RoleID, Int32 ApplicationHierarchyID)
		{
			this.OnBeforeEdit(ref RoleView);
            RoleView.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(RoleView, RoleID, ApplicationHierarchyID);

			this.OnAfterEdit(ref RoleView);
            return new OperationResult<RoleView>(isEdit, RoleView);
		}

		public OperationResult RemoveByKey(Int32 RoleID, Int32 ApplicationHierarchyID)
		{
			bool isRemove = DataAccess.DeleteByKey(RoleID, ApplicationHierarchyID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}