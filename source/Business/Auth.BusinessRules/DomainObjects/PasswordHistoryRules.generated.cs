using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class PasswordHistoryRules : AuthRulesBase<Int64, PasswordHistory, PasswordHistoryCollection, IPasswordHistoryDataAccess>
    {
		#region Constructors

        public PasswordHistoryRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IPasswordHistoryDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IPasswordHistoryDataAccess>();
			}
		}

		private List<Action<PasswordHistory>> _MasterPopulateActions;
        protected override List<Action<PasswordHistory>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<PasswordHistory>>();

						//this._MasterPopulateActions.Add(this.PopulateUser);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<PasswordHistory>> _DetailPopulateActions;
        protected override List<Action<PasswordHistory>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<PasswordHistory>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<PasswordHistoryCollection>();
            }
        }

        protected override PasswordHistoryCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<PasswordHistoryCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateUser(PasswordHistory PasswordHistory)
		{
			if (PasswordHistory.User != null && PasswordHistory.User.UserID != default(Int32))
			{
				PasswordHistory.User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetMaster(PasswordHistory.User.UserID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual PasswordHistoryCollection GetByUserSimple(Int32 UserID)
        {
			PasswordHistoryCollection PasswordHistoryCollection = DataAccess.SelectByUser(UserID);
				
			if (AllowCache)
            {
				this.Cache.Append(PasswordHistoryCollection.Clone(true));
            }
			
            return PasswordHistoryCollection;
        }

		public virtual PasswordHistoryCollection GetByUser(Int32 UserID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			PasswordHistoryCollection PasswordHistoryCollection = this.GetByUserSimple(UserID);

			if (PasswordHistoryCollection.Count != 0 && parentModel != null)
			{
				PasswordHistoryCollection.ForEach(m => m.User = null);
			}

			this.OnAfterGetCollection(ref PasswordHistoryCollection);
			return PasswordHistoryCollection;
		}

		public virtual Int32 GetCountByUser(Int32 UserID)
        {
			return DataAccess.SelectCountByUser(UserID);
        }
		
		public override OperationResult<PasswordHistory> AddWithDetail(PasswordHistory PasswordHistory)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (PasswordHistory.User.State == ObjectState.Added)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().AddWithDetail(PasswordHistory.User);
				}

                if (PasswordHistory.State == ObjectState.Added)
                {
                    OperationResult<PasswordHistory> result = this.Add(PasswordHistory);
                }

                scope.Complete();
            }

            return new OperationResult<PasswordHistory>(true, PasswordHistory);
        }

        public override OperationResult<PasswordHistory> EditWithDetail(PasswordHistory PasswordHistory)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (PasswordHistory.User.State != ObjectState.None)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().Save(PasswordHistory.User);
				}

                if (PasswordHistory.State == ObjectState.Edited)
                {
                    OperationResult<PasswordHistory> result = this.Edit(PasswordHistory);
                }

                scope.Complete();
            }

            return new OperationResult<PasswordHistory>(true, PasswordHistory);
        }

		public override OperationResult<PasswordHistory> RemoveWithDetail(PasswordHistory PasswordHistory)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(PasswordHistory.PasswordHistoryID);

				if (PasswordHistory.User.State == ObjectState.Removed)
                {
					OperationResult result_User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().RemoveWithDetail(PasswordHistory.User);
				}

				scope.Complete();
			}

			return new OperationResult<PasswordHistory>(true, PasswordHistory);
        }

		#endregion
    }
}