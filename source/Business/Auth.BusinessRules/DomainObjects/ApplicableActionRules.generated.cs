using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ApplicableActionRules : AuthRulesBase<Int32, ApplicableAction, ApplicableActionCollection, IApplicableActionDataAccess>
    {
		#region Constructors

        public ApplicableActionRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IApplicableActionDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IApplicableActionDataAccess>();
			}
		}

		private List<Action<ApplicableAction>> _MasterPopulateActions;
        protected override List<Action<ApplicableAction>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<ApplicableAction>>();

						//this._MasterPopulateActions.Add(this.PopulateApplicationHierarchy);
						//this._MasterPopulateActions.Add(this.PopulateViewAction);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<ApplicableAction>> _DetailPopulateActions;
        protected override List<Action<ApplicableAction>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<ApplicableAction>>();

						//this._DetailPopulateActions.Add(this.PopulateRoleActionCollection);
						//this._DetailPopulateActions.Add(this.PopulateRoleActionCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ApplicableActionCollection>();
            }
        }

        protected override ApplicableActionCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ApplicableActionCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplicationHierarchy(ApplicableAction ApplicableAction)
		{
			if (ApplicableAction.ApplicationHierarchy != null && ApplicableAction.ApplicationHierarchy.ApplicationHierarchyID != default(Int32))
			{
				ApplicableAction.ApplicationHierarchy = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().GetMaster(ApplicableAction.ApplicationHierarchy.ApplicationHierarchyID);
			}
		}
		
		private void PopulateViewAction(ApplicableAction ApplicableAction)
		{
			if (ApplicableAction.ViewAction != null && ApplicableAction.ViewAction.ViewActionID != default(Int32))
			{
				ApplicableAction.ViewAction = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewActionRules>().GetMaster(ApplicableAction.ViewAction.ViewActionID);
			}
		}
		
		private void PopulateRoleActionCollection(ApplicableAction ApplicableAction)
		{
			ApplicableAction.RoleActionCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleActionRules>().GetByApplicableAction(ApplicableAction.ApplicableActionID, ApplicableAction);
		}

		private void PopulateRoleActionCollectionCount(ApplicableAction ApplicableAction)
		{
			ApplicableAction.RoleActionCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleActionRules>().GetCountByApplicableAction(ApplicableAction.ApplicableActionID);
		}

		#endregion

		#region Public Methods
		
		public virtual ApplicableActionCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID)
        {
			ApplicableActionCollection ApplicableActionCollection = DataAccess.SelectByApplicationHierarchy(ApplicationHierarchyID);
				
			if (AllowCache)
            {
				this.Cache.Append(ApplicableActionCollection.Clone(true));
            }
			
            return ApplicableActionCollection;
        }

		public virtual ApplicableActionCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ApplicableActionCollection ApplicableActionCollection = this.GetByApplicationHierarchySimple(ApplicationHierarchyID);

			if (ApplicableActionCollection.Count != 0 && parentModel != null)
			{
				ApplicableActionCollection.ForEach(m => m.ApplicationHierarchy = null);
			}

			this.OnAfterGetCollection(ref ApplicableActionCollection);
			return ApplicableActionCollection;
		}

		public virtual Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
			return DataAccess.SelectCountByApplicationHierarchy(ApplicationHierarchyID);
        }
		
		public virtual ApplicableActionCollection GetByViewActionSimple(Int32 ViewActionID)
        {
			ApplicableActionCollection ApplicableActionCollection = DataAccess.SelectByViewAction(ViewActionID);
				
			if (AllowCache)
            {
				this.Cache.Append(ApplicableActionCollection.Clone(true));
            }
			
            return ApplicableActionCollection;
        }

		public virtual ApplicableActionCollection GetByViewAction(Int32 ViewActionID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ApplicableActionCollection ApplicableActionCollection = this.GetByViewActionSimple(ViewActionID);

			if (ApplicableActionCollection.Count != 0 && parentModel != null)
			{
				ApplicableActionCollection.ForEach(m => m.ViewAction = null);
			}

			this.OnAfterGetCollection(ref ApplicableActionCollection);
			return ApplicableActionCollection;
		}

		public virtual Int32 GetCountByViewAction(Int32 ViewActionID)
        {
			return DataAccess.SelectCountByViewAction(ViewActionID);
        }
		
		public override OperationResult<ApplicableAction> AddWithDetail(ApplicableAction ApplicableAction)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ApplicableAction.ApplicationHierarchy.State == ObjectState.Added)
                {
					OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().AddWithDetail(ApplicableAction.ApplicationHierarchy);
				}

				if (ApplicableAction.ViewAction.State == ObjectState.Added)
                {
					OperationResult<ViewAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewActionRules>().AddWithDetail(ApplicableAction.ViewAction);
				}

                if (ApplicableAction.State == ObjectState.Added)
                {
                    OperationResult<ApplicableAction> result = this.Add(ApplicableAction);
                }
				
				foreach (RoleAction RoleAction in ApplicableAction.RoleActionCollection.Where(obj => obj.State == ObjectState.Added))
                {
					RoleAction.ApplicableAction = new ApplicableAction { ApplicableActionID = ApplicableAction.ApplicableActionID };

                    OperationResult<RoleAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleActionRules>().AddWithDetail(RoleAction);
                }

                scope.Complete();
            }

            return new OperationResult<ApplicableAction>(true, ApplicableAction);
        }

        public override OperationResult<ApplicableAction> EditWithDetail(ApplicableAction ApplicableAction)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ApplicableAction.ApplicationHierarchy.State != ObjectState.None)
                {
					OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().Save(ApplicableAction.ApplicationHierarchy);
				}

				if (ApplicableAction.ViewAction.State != ObjectState.None)
                {
					OperationResult<ViewAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewActionRules>().Save(ApplicableAction.ViewAction);
				}

                if (ApplicableAction.State == ObjectState.Edited)
                {
                    OperationResult<ApplicableAction> result = this.Edit(ApplicableAction);
                }
                    
				foreach (RoleAction RoleAction in ApplicableAction.RoleActionCollection.Where(obj => obj.State != ObjectState.None))
                {
					RoleAction.ApplicableAction = new ApplicableAction { ApplicableActionID = ApplicableAction.ApplicableActionID };

                    OperationResult<RoleAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleActionRules>().Save(RoleAction);
                }

                scope.Complete();
            }

            return new OperationResult<ApplicableAction>(true, ApplicableAction);
        }

		public override OperationResult<ApplicableAction> RemoveWithDetail(ApplicableAction ApplicableAction)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(ApplicableAction.ApplicableActionID);

				if (ApplicableAction.ApplicationHierarchy.State == ObjectState.Removed)
                {
					OperationResult result_ApplicationHierarchy = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().RemoveWithDetail(ApplicableAction.ApplicationHierarchy);
				}

				if (ApplicableAction.ViewAction.State == ObjectState.Removed)
                {
					OperationResult result_ViewAction = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewActionRules>().RemoveWithDetail(ApplicableAction.ViewAction);
				}

				scope.Complete();
			}

			return new OperationResult<ApplicableAction>(true, ApplicableAction);
        }

		public ApplicableAction GetByKeySimple(Int32 ApplicationHierarchyID, Int32 ViewActionID)
		{
			ApplicableAction ApplicableAction = DataAccess.SelectByKey(ApplicationHierarchyID, ViewActionID);

            if (AllowCache)
            {
                this.Cache.Add(ApplicableAction.Clone<ApplicableAction>(true));
            }

            return ApplicableAction;
		}

		public ApplicableAction GetByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID)
		{
			ApplicableAction ApplicableAction = this.GetByKeySimple(ApplicationHierarchyID, ViewActionID);
            this.OnAfterGet(ref ApplicableAction);

            return ApplicableAction;
		}

		public OperationResult<ApplicableAction> EditByKey(ApplicableAction ApplicableAction, Int32 ApplicationHierarchyID, Int32 ViewActionID)
		{
			this.OnBeforeEdit(ref ApplicableAction);
            ApplicableAction.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(ApplicableAction, ApplicationHierarchyID, ViewActionID);

			this.OnAfterEdit(ref ApplicableAction);
            return new OperationResult<ApplicableAction>(isEdit, ApplicableAction);
		}

		public OperationResult RemoveByKey(Int32 ApplicationHierarchyID, Int32 ViewActionID)
		{
			bool isRemove = DataAccess.DeleteByKey(ApplicationHierarchyID, ViewActionID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}