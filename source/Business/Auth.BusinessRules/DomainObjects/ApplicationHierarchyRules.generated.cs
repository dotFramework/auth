using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ApplicationHierarchyRules : AuthRulesBase<Int32, ApplicationHierarchy, ApplicationHierarchyCollection, IApplicationHierarchyDataAccess>
    {
		#region Constructors

        public ApplicationHierarchyRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IApplicationHierarchyDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IApplicationHierarchyDataAccess>();
			}
		}

		private List<Action<ApplicationHierarchy>> _MasterPopulateActions;
        protected override List<Action<ApplicationHierarchy>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<ApplicationHierarchy>>();

						//this._MasterPopulateActions.Add(this.PopulateApplication);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<ApplicationHierarchy>> _DetailPopulateActions;
        protected override List<Action<ApplicationHierarchy>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<ApplicationHierarchy>>();

						//this._DetailPopulateActions.Add(this.PopulateApplicableActionCollection);
						//this._DetailPopulateActions.Add(this.PopulateApplicableActionCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateFavoriteViewCollection);
						//this._DetailPopulateActions.Add(this.PopulateFavoriteViewCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateRoleViewCollection);
						//this._DetailPopulateActions.Add(this.PopulateRoleViewCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateViewVisitCollection);
						//this._DetailPopulateActions.Add(this.PopulateViewVisitCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ApplicationHierarchyCollection>();
            }
        }

        protected override ApplicationHierarchyCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ApplicationHierarchyCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplication(ApplicationHierarchy ApplicationHierarchy)
		{
			if (ApplicationHierarchy.Application != null && ApplicationHierarchy.Application.ApplicationID != default(Int32))
			{
				ApplicationHierarchy.Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().GetMaster(ApplicationHierarchy.Application.ApplicationID);
			}
		}
		
		private void PopulateApplicableActionCollection(ApplicationHierarchy ApplicationHierarchy)
		{
			ApplicationHierarchy.ApplicableActionCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().GetByApplicationHierarchy(ApplicationHierarchy.ApplicationHierarchyID, ApplicationHierarchy);
		}

		private void PopulateApplicableActionCollectionCount(ApplicationHierarchy ApplicationHierarchy)
		{
			ApplicationHierarchy.ApplicableActionCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().GetCountByApplicationHierarchy(ApplicationHierarchy.ApplicationHierarchyID);
		}
		
		private void PopulateFavoriteViewCollection(ApplicationHierarchy ApplicationHierarchy)
		{
			ApplicationHierarchy.FavoriteViewCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<FavoriteViewRules>().GetByApplicationHierarchy(ApplicationHierarchy.ApplicationHierarchyID, ApplicationHierarchy);
		}

		private void PopulateFavoriteViewCollectionCount(ApplicationHierarchy ApplicationHierarchy)
		{
			ApplicationHierarchy.FavoriteViewCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<FavoriteViewRules>().GetCountByApplicationHierarchy(ApplicationHierarchy.ApplicationHierarchyID);
		}
		
		private void PopulateRoleViewCollection(ApplicationHierarchy ApplicationHierarchy)
		{
			ApplicationHierarchy.RoleViewCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleViewRules>().GetByApplicationHierarchy(ApplicationHierarchy.ApplicationHierarchyID, ApplicationHierarchy);
		}

		private void PopulateRoleViewCollectionCount(ApplicationHierarchy ApplicationHierarchy)
		{
			ApplicationHierarchy.RoleViewCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleViewRules>().GetCountByApplicationHierarchy(ApplicationHierarchy.ApplicationHierarchyID);
		}
		
		private void PopulateViewVisitCollection(ApplicationHierarchy ApplicationHierarchy)
		{
			ApplicationHierarchy.ViewVisitCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewVisitRules>().GetByApplicationHierarchy(ApplicationHierarchy.ApplicationHierarchyID, ApplicationHierarchy);
		}

		private void PopulateViewVisitCollectionCount(ApplicationHierarchy ApplicationHierarchy)
		{
			ApplicationHierarchy.ViewVisitCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewVisitRules>().GetCountByApplicationHierarchy(ApplicationHierarchy.ApplicationHierarchyID);
		}

		#endregion

		#region Public Methods
		
		public virtual ApplicationHierarchyCollection GetByApplicationSimple(Int32 ApplicationID)
        {
			ApplicationHierarchyCollection ApplicationHierarchyCollection = DataAccess.SelectByApplication(ApplicationID);
				
			if (AllowCache)
            {
				this.Cache.Append(ApplicationHierarchyCollection.Clone(true));
            }
			
            return ApplicationHierarchyCollection;
        }

		public virtual ApplicationHierarchyCollection GetByApplication(Int32 ApplicationID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ApplicationHierarchyCollection ApplicationHierarchyCollection = this.GetByApplicationSimple(ApplicationID);

			if (ApplicationHierarchyCollection.Count != 0 && parentModel != null)
			{
				ApplicationHierarchyCollection.ForEach(m => m.Application = null);
			}

			this.OnAfterGetCollection(ref ApplicationHierarchyCollection);
			return ApplicationHierarchyCollection;
		}

		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
			return DataAccess.SelectCountByApplication(ApplicationID);
        }
		
		public override OperationResult<ApplicationHierarchy> AddWithDetail(ApplicationHierarchy ApplicationHierarchy)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ApplicationHierarchy.Application.State == ObjectState.Added)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().AddWithDetail(ApplicationHierarchy.Application);
				}

                if (ApplicationHierarchy.State == ObjectState.Added)
                {
                    OperationResult<ApplicationHierarchy> result = this.Add(ApplicationHierarchy);
                }
				
				foreach (ApplicableAction ApplicableAction in ApplicationHierarchy.ApplicableActionCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ApplicableAction.ApplicationHierarchy = new ApplicationHierarchy { ApplicationHierarchyID = ApplicationHierarchy.ApplicationHierarchyID };

                    OperationResult<ApplicableAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().AddWithDetail(ApplicableAction);
                }
				
				foreach (FavoriteView FavoriteView in ApplicationHierarchy.FavoriteViewCollection.Where(obj => obj.State == ObjectState.Added))
                {
					FavoriteView.ApplicationHierarchy = new ApplicationHierarchy { ApplicationHierarchyID = ApplicationHierarchy.ApplicationHierarchyID };

                    OperationResult<FavoriteView> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<FavoriteViewRules>().AddWithDetail(FavoriteView);
                }
				
				foreach (RoleView RoleView in ApplicationHierarchy.RoleViewCollection.Where(obj => obj.State == ObjectState.Added))
                {
					RoleView.ApplicationHierarchy = new ApplicationHierarchy { ApplicationHierarchyID = ApplicationHierarchy.ApplicationHierarchyID };

                    OperationResult<RoleView> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleViewRules>().AddWithDetail(RoleView);
                }
				
				foreach (ViewVisit ViewVisit in ApplicationHierarchy.ViewVisitCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ViewVisit.ApplicationHierarchy = new ApplicationHierarchy { ApplicationHierarchyID = ApplicationHierarchy.ApplicationHierarchyID };

                    OperationResult<ViewVisit> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewVisitRules>().AddWithDetail(ViewVisit);
                }

                scope.Complete();
            }

            return new OperationResult<ApplicationHierarchy>(true, ApplicationHierarchy);
        }

        public override OperationResult<ApplicationHierarchy> EditWithDetail(ApplicationHierarchy ApplicationHierarchy)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ApplicationHierarchy.Application.State != ObjectState.None)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().Save(ApplicationHierarchy.Application);
				}

                if (ApplicationHierarchy.State == ObjectState.Edited)
                {
                    OperationResult<ApplicationHierarchy> result = this.Edit(ApplicationHierarchy);
                }
                    
				foreach (ApplicableAction ApplicableAction in ApplicationHierarchy.ApplicableActionCollection.Where(obj => obj.State != ObjectState.None))
                {
					ApplicableAction.ApplicationHierarchy = new ApplicationHierarchy { ApplicationHierarchyID = ApplicationHierarchy.ApplicationHierarchyID };

                    OperationResult<ApplicableAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().Save(ApplicableAction);
                }
                    
				foreach (FavoriteView FavoriteView in ApplicationHierarchy.FavoriteViewCollection.Where(obj => obj.State != ObjectState.None))
                {
					FavoriteView.ApplicationHierarchy = new ApplicationHierarchy { ApplicationHierarchyID = ApplicationHierarchy.ApplicationHierarchyID };

                    OperationResult<FavoriteView> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<FavoriteViewRules>().Save(FavoriteView);
                }
                    
				foreach (RoleView RoleView in ApplicationHierarchy.RoleViewCollection.Where(obj => obj.State != ObjectState.None))
                {
					RoleView.ApplicationHierarchy = new ApplicationHierarchy { ApplicationHierarchyID = ApplicationHierarchy.ApplicationHierarchyID };

                    OperationResult<RoleView> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleViewRules>().Save(RoleView);
                }
                    
				foreach (ViewVisit ViewVisit in ApplicationHierarchy.ViewVisitCollection.Where(obj => obj.State != ObjectState.None))
                {
					ViewVisit.ApplicationHierarchy = new ApplicationHierarchy { ApplicationHierarchyID = ApplicationHierarchy.ApplicationHierarchyID };

                    OperationResult<ViewVisit> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewVisitRules>().Save(ViewVisit);
                }

                scope.Complete();
            }

            return new OperationResult<ApplicationHierarchy>(true, ApplicationHierarchy);
        }

		public override OperationResult<ApplicationHierarchy> RemoveWithDetail(ApplicationHierarchy ApplicationHierarchy)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(ApplicationHierarchy.ApplicationHierarchyID);

				if (ApplicationHierarchy.Application.State == ObjectState.Removed)
                {
					OperationResult result_Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().RemoveWithDetail(ApplicationHierarchy.Application);
				}

				scope.Complete();
			}

			return new OperationResult<ApplicationHierarchy>(true, ApplicationHierarchy);
        }

		public ApplicationHierarchy GetByKeySimple(Int32 ApplicationID, String Code)
		{
			ApplicationHierarchy ApplicationHierarchy = DataAccess.SelectByKey(ApplicationID, Code);

            if (AllowCache)
            {
                this.Cache.Add(ApplicationHierarchy.Clone<ApplicationHierarchy>(true));
            }

            return ApplicationHierarchy;
		}

		public ApplicationHierarchy GetByKey(Int32 ApplicationID, String Code)
		{
			ApplicationHierarchy ApplicationHierarchy = this.GetByKeySimple(ApplicationID, Code);
            this.OnAfterGet(ref ApplicationHierarchy);

            return ApplicationHierarchy;
		}

		public OperationResult<ApplicationHierarchy> EditByKey(ApplicationHierarchy ApplicationHierarchy, Int32 ApplicationID, String Code)
		{
			this.OnBeforeEdit(ref ApplicationHierarchy);
            ApplicationHierarchy.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(ApplicationHierarchy, ApplicationID, Code);

			this.OnAfterEdit(ref ApplicationHierarchy);
            return new OperationResult<ApplicationHierarchy>(isEdit, ApplicationHierarchy);
		}

		public OperationResult RemoveByKey(Int32 ApplicationID, String Code)
		{
			bool isRemove = DataAccess.DeleteByKey(ApplicationID, Code);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}