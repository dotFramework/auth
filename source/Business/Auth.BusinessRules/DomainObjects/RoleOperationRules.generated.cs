using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class RoleOperationRules : AuthRulesBase<Int32, RoleOperation, RoleOperationCollection, IRoleOperationDataAccess>
    {
		#region Constructors

        public RoleOperationRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IRoleOperationDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IRoleOperationDataAccess>();
			}
		}

		private List<Action<RoleOperation>> _MasterPopulateActions;
        protected override List<Action<RoleOperation>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<RoleOperation>>();

						this._MasterPopulateActions.Add(this.PopulateRole);
						this._MasterPopulateActions.Add(this.PopulateApplicableOperation);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<RoleOperation>> _DetailPopulateActions;
        protected override List<Action<RoleOperation>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<RoleOperation>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<RoleOperationCollection>();
            }
        }

        protected override RoleOperationCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<RoleOperationCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateRole(RoleOperation RoleOperation)
		{
			if (RoleOperation.Role != null && RoleOperation.Role.RoleID != default(Int32))
			{
				RoleOperation.Role = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().GetMaster(RoleOperation.Role.RoleID);
			}
		}
		
		private void PopulateApplicableOperation(RoleOperation RoleOperation)
		{
			if (RoleOperation.ApplicableOperation != null && RoleOperation.ApplicableOperation.ApplicableOperationID != default(Int32))
			{
				RoleOperation.ApplicableOperation = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().GetMaster(RoleOperation.ApplicableOperation.ApplicableOperationID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual RoleOperationCollection GetByRoleSimple(Int32 RoleID)
        {
			RoleOperationCollection RoleOperationCollection = DataAccess.SelectByRole(RoleID);
				
			if (AllowCache)
            {
				this.Cache.Append(RoleOperationCollection.Clone(true));
            }
			
            return RoleOperationCollection;
        }

		public virtual RoleOperationCollection GetByRole(Int32 RoleID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			RoleOperationCollection RoleOperationCollection = this.GetByRoleSimple(RoleID);

			if (RoleOperationCollection.Count != 0 && parentModel != null)
			{
				RoleOperationCollection.ForEach(m => m.Role = null);
			}

			this.OnAfterGetCollection(ref RoleOperationCollection);
			return RoleOperationCollection;
		}

		public virtual Int32 GetCountByRole(Int32 RoleID)
        {
			return DataAccess.SelectCountByRole(RoleID);
        }
		
		public virtual RoleOperationCollection GetByApplicableOperationSimple(Int32 ApplicableOperationID)
        {
			RoleOperationCollection RoleOperationCollection = DataAccess.SelectByApplicableOperation(ApplicableOperationID);
				
			if (AllowCache)
            {
				this.Cache.Append(RoleOperationCollection.Clone(true));
            }
			
            return RoleOperationCollection;
        }

		public virtual RoleOperationCollection GetByApplicableOperation(Int32 ApplicableOperationID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			RoleOperationCollection RoleOperationCollection = this.GetByApplicableOperationSimple(ApplicableOperationID);

			if (RoleOperationCollection.Count != 0 && parentModel != null)
			{
				RoleOperationCollection.ForEach(m => m.ApplicableOperation = null);
			}

			this.OnAfterGetCollection(ref RoleOperationCollection);
			return RoleOperationCollection;
		}

		public virtual Int32 GetCountByApplicableOperation(Int32 ApplicableOperationID)
        {
			return DataAccess.SelectCountByApplicableOperation(ApplicableOperationID);
        }
		
		public override OperationResult<RoleOperation> AddWithDetail(RoleOperation RoleOperation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (RoleOperation.Role.State == ObjectState.Added)
                {
					OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().AddWithDetail(RoleOperation.Role);
				}

				if (RoleOperation.ApplicableOperation.State == ObjectState.Added)
                {
					OperationResult<ApplicableOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().AddWithDetail(RoleOperation.ApplicableOperation);
				}

                if (RoleOperation.State == ObjectState.Added)
                {
                    OperationResult<RoleOperation> result = this.Add(RoleOperation);
                }

                scope.Complete();
            }

            return new OperationResult<RoleOperation>(true, RoleOperation);
        }

        public override OperationResult<RoleOperation> EditWithDetail(RoleOperation RoleOperation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (RoleOperation.Role.State != ObjectState.None)
                {
					OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Save(RoleOperation.Role);
				}

				if (RoleOperation.ApplicableOperation.State != ObjectState.None)
                {
					OperationResult<ApplicableOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().Save(RoleOperation.ApplicableOperation);
				}

                if (RoleOperation.State == ObjectState.Edited)
                {
                    OperationResult<RoleOperation> result = this.Edit(RoleOperation);
                }

                scope.Complete();
            }

            return new OperationResult<RoleOperation>(true, RoleOperation);
        }

		public override OperationResult<RoleOperation> RemoveWithDetail(RoleOperation RoleOperation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(RoleOperation.RoleOperationID);

				if (RoleOperation.Role.State == ObjectState.Removed)
                {
					OperationResult result_Role = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().RemoveWithDetail(RoleOperation.Role);
				}

				if (RoleOperation.ApplicableOperation.State == ObjectState.Removed)
                {
					OperationResult result_ApplicableOperation = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().RemoveWithDetail(RoleOperation.ApplicableOperation);
				}

				scope.Complete();
			}

			return new OperationResult<RoleOperation>(true, RoleOperation);
        }

		public RoleOperation GetByKeySimple(Int32 RoleID, Int32 ApplicableOperationID)
		{
			RoleOperation RoleOperation = DataAccess.SelectByKey(RoleID, ApplicableOperationID);

            if (AllowCache)
            {
                this.Cache.Add(RoleOperation.Clone<RoleOperation>(true));
            }

            return RoleOperation;
		}

		public RoleOperation GetByKey(Int32 RoleID, Int32 ApplicableOperationID)
		{
			RoleOperation RoleOperation = this.GetByKeySimple(RoleID, ApplicableOperationID);
            this.OnAfterGet(ref RoleOperation);

            return RoleOperation;
		}

		public OperationResult<RoleOperation> EditByKey(RoleOperation RoleOperation, Int32 RoleID, Int32 ApplicableOperationID)
		{
			this.OnBeforeEdit(ref RoleOperation);
            RoleOperation.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(RoleOperation, RoleID, ApplicableOperationID);

			this.OnAfterEdit(ref RoleOperation);
            return new OperationResult<RoleOperation>(isEdit, RoleOperation);
		}

		public OperationResult RemoveByKey(Int32 RoleID, Int32 ApplicableOperationID)
		{
			bool isRemove = DataAccess.DeleteByKey(RoleID, ApplicableOperationID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}