using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Caching;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Model;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ClientRules : AuthRulesBase<Int32, Client, ClientCollection, IClientDataAccess>
    {
		#region Constructors

        public ClientRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IClientDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IClientDataAccess>();
			}
		}

		private List<Action<Client>> _MasterPopulateActions;
        protected override List<Action<Client>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<Client>>();

					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<Client>> _DetailPopulateActions;
        protected override List<Action<Client>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<Client>>();

						//this._DetailPopulateActions.Add(this.PopulateClientDeviceCollection);
						//this._DetailPopulateActions.Add(this.PopulateClientDeviceCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ClientCollection>();
            }
        }

        protected override ClientCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ClientCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateClientDeviceCollection(Client Client)
		{
			Client.ClientDeviceCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientDeviceRules>().GetByClient(Client.ClientID, Client);
		}

		private void PopulateClientDeviceCollectionCount(Client Client)
		{
			Client.ClientDeviceCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientDeviceRules>().GetCountByClient(Client.ClientID);
		}

		#endregion

		#region Public Methods
		
		public override OperationResult<Client> AddWithDetail(Client Client)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (Client.State == ObjectState.Added)
                {
                    OperationResult<Client> result = this.Add(Client);
                }
				
				foreach (ClientDevice ClientDevice in Client.ClientDeviceCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ClientDevice.Client = new Client { ClientID = Client.ClientID };

                    OperationResult<ClientDevice> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientDeviceRules>().AddWithDetail(ClientDevice);
                }

                scope.Complete();
            }

            return new OperationResult<Client>(true, Client);
        }

        public override OperationResult<Client> EditWithDetail(Client Client)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (Client.State == ObjectState.Edited)
                {
                    OperationResult<Client> result = this.Edit(Client);
                }
                    
				foreach (ClientDevice ClientDevice in Client.ClientDeviceCollection.Where(obj => obj.State != ObjectState.None))
                {
					ClientDevice.Client = new Client { ClientID = Client.ClientID };

                    OperationResult<ClientDevice> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientDeviceRules>().Save(ClientDevice);
                }

                scope.Complete();
            }

            return new OperationResult<Client>(true, Client);
        }

		public override OperationResult<Client> RemoveWithDetail(Client Client)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(Client.ClientID);

				scope.Complete();
			}

			return new OperationResult<Client>(true, Client);
        }

		public virtual Client GetByKeySimple(String ClientIdentifier)
		{
			Client Client = DataAccess.SelectByKey(ClientIdentifier);
			
			if (AllowCache)
            {
                this.Cache.Add(Client.Clone<Client>(true));
            }

            return Client;
		}

		public virtual Client GetByKey(String ClientIdentifier)
		{
			Client Client = this.GetByKeySimple(ClientIdentifier);
            this.OnAfterGet(ref Client);

            return Client;
		}

		public virtual OperationResult<Client> EditByKey(Client Client, String ClientIdentifier)
		{
			this.OnBeforeEdit(ref Client);
            Client.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(Client, ClientIdentifier);

			this.OnAfterEdit(ref Client);
            return new OperationResult<Client>(isEdit, Client);
		}

		public virtual OperationResult RemoveByKey(String ClientIdentifier)
		{
			bool isRemove = DataAccess.DeleteByKey(ClientIdentifier);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}