using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class UserRules : AuthRulesBase<Int32, User, UserCollection, IUserDataAccess>
    {
		#region Constructors

        public UserRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IUserDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IUserDataAccess>();
			}
		}

		private List<Action<User>> _MasterPopulateActions;
        protected override List<Action<User>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<User>>();

						this._MasterPopulateActions.Add(this.PopulateAccountPolicy);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<User>> _DetailPopulateActions;
        protected override List<Action<User>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<User>>();

						//this._DetailPopulateActions.Add(this.PopulateFavoriteViewCollection);
						//this._DetailPopulateActions.Add(this.PopulateFavoriteViewCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulatePasswordHistoryCollection);
						//this._DetailPopulateActions.Add(this.PopulatePasswordHistoryCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateRefreshTokenCollection);
						//this._DetailPopulateActions.Add(this.PopulateRefreshTokenCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateUserClaimCollection);
						//this._DetailPopulateActions.Add(this.PopulateUserClaimCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateUserLoginCollection);
						//this._DetailPopulateActions.Add(this.PopulateUserLoginCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateUserRoleCollection);
						//this._DetailPopulateActions.Add(this.PopulateUserRoleCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateUserSessionCollection);
						//this._DetailPopulateActions.Add(this.PopulateUserSessionCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateViewVisitCollection);
						//this._DetailPopulateActions.Add(this.PopulateViewVisitCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<UserCollection>();
            }
        }

        protected override UserCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<UserCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateAccountPolicy(User User)
		{
			if (User.AccountPolicy != null && User.AccountPolicy.AccountPolicyID != default(Byte))
			{
				User.AccountPolicy = AuthBusinessRulesFactory.Instance.GetBusinessRules<AccountPolicyRules>().GetMaster(User.AccountPolicy.AccountPolicyID);
			}
		}
		
		private void PopulateFavoriteViewCollection(User User)
		{
			User.FavoriteViewCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<FavoriteViewRules>().GetByUser(User.UserID, User);
		}

		private void PopulateFavoriteViewCollectionCount(User User)
		{
			User.FavoriteViewCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<FavoriteViewRules>().GetCountByUser(User.UserID);
		}
		
		private void PopulatePasswordHistoryCollection(User User)
		{
			User.PasswordHistoryCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<PasswordHistoryRules>().GetByUser(User.UserID, User);
		}

		private void PopulatePasswordHistoryCollectionCount(User User)
		{
			User.PasswordHistoryCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<PasswordHistoryRules>().GetCountByUser(User.UserID);
		}
		
		private void PopulateRefreshTokenCollection(User User)
		{
			User.RefreshTokenCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<RefreshTokenRules>().GetByUser(User.UserID, User);
		}

		private void PopulateRefreshTokenCollectionCount(User User)
		{
			User.RefreshTokenCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<RefreshTokenRules>().GetCountByUser(User.UserID);
		}
		
		private void PopulateUserClaimCollection(User User)
		{
			User.UserClaimCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserClaimRules>().GetByUser(User.UserID, User);
		}

		private void PopulateUserClaimCollectionCount(User User)
		{
			User.UserClaimCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserClaimRules>().GetCountByUser(User.UserID);
		}
		
		private void PopulateUserLoginCollection(User User)
		{
			User.UserLoginCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserLoginRules>().GetByUser(User.UserID, User);
		}

		private void PopulateUserLoginCollectionCount(User User)
		{
			User.UserLoginCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserLoginRules>().GetCountByUser(User.UserID);
		}
		
		private void PopulateUserRoleCollection(User User)
		{
			User.UserRoleCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRoleRules>().GetByUser(User.UserID, User);
		}

		private void PopulateUserRoleCollectionCount(User User)
		{
			User.UserRoleCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRoleRules>().GetCountByUser(User.UserID);
		}
		
		private void PopulateUserSessionCollection(User User)
		{
			User.UserSessionCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserSessionRules>().GetByUser(User.UserID, User);
		}

		private void PopulateUserSessionCollectionCount(User User)
		{
			User.UserSessionCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserSessionRules>().GetCountByUser(User.UserID);
		}
		
		private void PopulateViewVisitCollection(User User)
		{
			User.ViewVisitCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewVisitRules>().GetByUser(User.UserID, User);
		}

		private void PopulateViewVisitCollectionCount(User User)
		{
			User.ViewVisitCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewVisitRules>().GetCountByUser(User.UserID);
		}

		#endregion

		#region Public Methods
		
		public virtual UserCollection GetByAccountPolicySimple(Byte AccountPolicyID)
        {
			UserCollection UserCollection = DataAccess.SelectByAccountPolicy(AccountPolicyID);
				
			if (AllowCache)
            {
				this.Cache.Append(UserCollection.Clone(true));
            }
			
            return UserCollection;
        }

		public virtual UserCollection GetByAccountPolicy(Byte AccountPolicyID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			UserCollection UserCollection = this.GetByAccountPolicySimple(AccountPolicyID);

			if (UserCollection.Count != 0 && parentModel != null)
			{
				UserCollection.ForEach(m => m.AccountPolicy = null);
			}

			this.OnAfterGetCollection(ref UserCollection);
			return UserCollection;
		}

		public virtual Int32 GetCountByAccountPolicy(Byte AccountPolicyID)
        {
			return DataAccess.SelectCountByAccountPolicy(AccountPolicyID);
        }
		
		public override OperationResult<User> AddWithDetail(User User)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (User.AccountPolicy.State == ObjectState.Added)
                {
					OperationResult<AccountPolicy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<AccountPolicyRules>().AddWithDetail(User.AccountPolicy);
				}

                if (User.State == ObjectState.Added)
                {
                    OperationResult<User> result = this.Add(User);
                }
				
				foreach (FavoriteView FavoriteView in User.FavoriteViewCollection.Where(obj => obj.State == ObjectState.Added))
                {
					FavoriteView.User = new User { UserID = User.UserID };

                    OperationResult<FavoriteView> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<FavoriteViewRules>().AddWithDetail(FavoriteView);
                }
				
				foreach (PasswordHistory PasswordHistory in User.PasswordHistoryCollection.Where(obj => obj.State == ObjectState.Added))
                {
					PasswordHistory.User = new User { UserID = User.UserID };

                    OperationResult<PasswordHistory> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<PasswordHistoryRules>().AddWithDetail(PasswordHistory);
                }
				
				foreach (RefreshToken RefreshToken in User.RefreshTokenCollection.Where(obj => obj.State == ObjectState.Added))
                {
					RefreshToken.User = new User { UserID = User.UserID };

                    OperationResult<RefreshToken> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RefreshTokenRules>().AddWithDetail(RefreshToken);
                }
				
				foreach (UserClaim UserClaim in User.UserClaimCollection.Where(obj => obj.State == ObjectState.Added))
                {
					UserClaim.User = new User { UserID = User.UserID };

                    OperationResult<UserClaim> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserClaimRules>().AddWithDetail(UserClaim);
                }
				
				foreach (UserLogin UserLogin in User.UserLoginCollection.Where(obj => obj.State == ObjectState.Added))
                {
					UserLogin.User = new User { UserID = User.UserID };

                    OperationResult<UserLogin> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserLoginRules>().AddWithDetail(UserLogin);
                }
				
				foreach (UserRole UserRole in User.UserRoleCollection.Where(obj => obj.State == ObjectState.Added))
                {
					UserRole.User = new User { UserID = User.UserID };

                    OperationResult<UserRole> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRoleRules>().AddWithDetail(UserRole);
                }
				
				foreach (UserSession UserSession in User.UserSessionCollection.Where(obj => obj.State == ObjectState.Added))
                {
					UserSession.User = new User { UserID = User.UserID };

                    OperationResult<UserSession> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserSessionRules>().AddWithDetail(UserSession);
                }
				
				foreach (ViewVisit ViewVisit in User.ViewVisitCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ViewVisit.User = new User { UserID = User.UserID };

                    OperationResult<ViewVisit> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewVisitRules>().AddWithDetail(ViewVisit);
                }

                scope.Complete();
            }

            return new OperationResult<User>(true, User);
        }

        public override OperationResult<User> EditWithDetail(User User)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (User.AccountPolicy.State != ObjectState.None)
                {
					OperationResult<AccountPolicy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<AccountPolicyRules>().Save(User.AccountPolicy);
				}

                if (User.State == ObjectState.Edited)
                {
                    OperationResult<User> result = this.Edit(User);
                }
                    
				foreach (FavoriteView FavoriteView in User.FavoriteViewCollection.Where(obj => obj.State != ObjectState.None))
                {
					FavoriteView.User = new User { UserID = User.UserID };

                    OperationResult<FavoriteView> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<FavoriteViewRules>().Save(FavoriteView);
                }
                    
				foreach (PasswordHistory PasswordHistory in User.PasswordHistoryCollection.Where(obj => obj.State != ObjectState.None))
                {
					PasswordHistory.User = new User { UserID = User.UserID };

                    OperationResult<PasswordHistory> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<PasswordHistoryRules>().Save(PasswordHistory);
                }
                    
				foreach (RefreshToken RefreshToken in User.RefreshTokenCollection.Where(obj => obj.State != ObjectState.None))
                {
					RefreshToken.User = new User { UserID = User.UserID };

                    OperationResult<RefreshToken> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RefreshTokenRules>().Save(RefreshToken);
                }
                    
				foreach (UserClaim UserClaim in User.UserClaimCollection.Where(obj => obj.State != ObjectState.None))
                {
					UserClaim.User = new User { UserID = User.UserID };

                    OperationResult<UserClaim> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserClaimRules>().Save(UserClaim);
                }
                    
				foreach (UserLogin UserLogin in User.UserLoginCollection.Where(obj => obj.State != ObjectState.None))
                {
					UserLogin.User = new User { UserID = User.UserID };

                    OperationResult<UserLogin> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserLoginRules>().Save(UserLogin);
                }
                    
				foreach (UserRole UserRole in User.UserRoleCollection.Where(obj => obj.State != ObjectState.None))
                {
					UserRole.User = new User { UserID = User.UserID };

                    OperationResult<UserRole> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRoleRules>().Save(UserRole);
                }
                    
				foreach (UserSession UserSession in User.UserSessionCollection.Where(obj => obj.State != ObjectState.None))
                {
					UserSession.User = new User { UserID = User.UserID };

                    OperationResult<UserSession> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserSessionRules>().Save(UserSession);
                }
                    
				foreach (ViewVisit ViewVisit in User.ViewVisitCollection.Where(obj => obj.State != ObjectState.None))
                {
					ViewVisit.User = new User { UserID = User.UserID };

                    OperationResult<ViewVisit> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ViewVisitRules>().Save(ViewVisit);
                }

                scope.Complete();
            }

            return new OperationResult<User>(true, User);
        }

		public override OperationResult<User> RemoveWithDetail(User User)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(User.UserID);

				if (User.AccountPolicy.State == ObjectState.Removed)
                {
					OperationResult result_AccountPolicy = AuthBusinessRulesFactory.Instance.GetBusinessRules<AccountPolicyRules>().RemoveWithDetail(User.AccountPolicy);
				}

				scope.Complete();
			}

			return new OperationResult<User>(true, User);
        }

		public virtual User GetByKeySimple(String UserName)
		{
			User User = DataAccess.SelectByKey(UserName);

            if (AllowCache)
            {
                this.Cache.Add(User.Clone<User>(true));
            }

            return User;
		}

		public virtual User GetByKey(String UserName)
		{
			User User = this.GetByKeySimple(UserName);
            this.OnAfterGet(ref User);

            return User;
		}

		public virtual OperationResult<User> EditByKey(User User, String UserName)
		{
			this.OnBeforeEdit(ref User);
            User.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(User, UserName);

			if (isEdit)
			{
				if (AllowCache)
				{
					this.Cache.Set(User.Key, User.Clone<User>(true));
				}
			}

			this.OnAfterEdit(ref User);
            return new OperationResult<User>(isEdit, User);
		}

		public virtual OperationResult RemoveByKey(String UserName)
		{
			bool isRemove = DataAccess.DeleteByKey(UserName);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}