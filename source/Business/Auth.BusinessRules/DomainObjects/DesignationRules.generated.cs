using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class DesignationRules : AuthRulesBase<Int16, Designation, DesignationCollection, IDesignationDataAccess>
    {
		#region Constructors

        public DesignationRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IDesignationDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IDesignationDataAccess>();
			}
		}

		private List<Action<Designation>> _MasterPopulateActions;
        protected override List<Action<Designation>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<Designation>>();

					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<Designation>> _DetailPopulateActions;
        protected override List<Action<Designation>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<Designation>>();

						//this._DetailPopulateActions.Add(this.PopulateRoleCollection);
						//this._DetailPopulateActions.Add(this.PopulateRoleCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<DesignationCollection>();
            }
        }

        protected override DesignationCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<DesignationCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateRoleCollection(Designation Designation)
		{
			Designation.RoleCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().GetByDesignation(Designation.DesignationID, Designation);
		}

		private void PopulateRoleCollectionCount(Designation Designation)
		{
			Designation.RoleCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().GetCountByDesignation(Designation.DesignationID);
		}

		#endregion

		#region Public Methods
		
		public override OperationResult<Designation> AddWithDetail(Designation Designation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (Designation.State == ObjectState.Added)
                {
                    OperationResult<Designation> result = this.Add(Designation);
                }
				
				foreach (Role Role in Designation.RoleCollection.Where(obj => obj.State == ObjectState.Added))
                {
					Role.Designation = new Designation { DesignationID = Designation.DesignationID };

                    OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().AddWithDetail(Role);
                }

                scope.Complete();
            }

            return new OperationResult<Designation>(true, Designation);
        }

        public override OperationResult<Designation> EditWithDetail(Designation Designation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (Designation.State == ObjectState.Edited)
                {
                    OperationResult<Designation> result = this.Edit(Designation);
                }
                    
				foreach (Role Role in Designation.RoleCollection.Where(obj => obj.State != ObjectState.None))
                {
					Role.Designation = new Designation { DesignationID = Designation.DesignationID };

                    OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Save(Role);
                }

                scope.Complete();
            }

            return new OperationResult<Designation>(true, Designation);
        }

		public override OperationResult<Designation> RemoveWithDetail(Designation Designation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(Designation.DesignationID);

				scope.Complete();
			}

			return new OperationResult<Designation>(true, Designation);
        }

		public Designation GetByKeySimple(String Code)
		{
			Designation Designation = DataAccess.SelectByKey(Code);

            if (AllowCache)
            {
                this.Cache.Add(Designation.Clone<Designation>(true));
            }

            return Designation;
		}

		public Designation GetByKey(String Code)
		{
			Designation Designation = this.GetByKeySimple(Code);
            this.OnAfterGet(ref Designation);

            return Designation;
		}

		public OperationResult<Designation> EditByKey(Designation Designation, String Code)
		{
			this.OnBeforeEdit(ref Designation);
            Designation.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(Designation, Code);

			this.OnAfterEdit(ref Designation);
            return new OperationResult<Designation>(isEdit, Designation);
		}

		public OperationResult RemoveByKey(String Code)
		{
			bool isRemove = DataAccess.DeleteByKey(Code);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}