using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class UserRoleRules : AuthRulesBase<Int64, UserRole, UserRoleCollection, IUserRoleDataAccess>
    {
		#region Constructors

        public UserRoleRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IUserRoleDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IUserRoleDataAccess>();
			}
		}

		private List<Action<UserRole>> _MasterPopulateActions;
        protected override List<Action<UserRole>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<UserRole>>();

						//this._MasterPopulateActions.Add(this.PopulateUser);
						this._MasterPopulateActions.Add(this.PopulateRole);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<UserRole>> _DetailPopulateActions;
        protected override List<Action<UserRole>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<UserRole>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<UserRoleCollection>();
            }
        }

        protected override UserRoleCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<UserRoleCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateUser(UserRole UserRole)
		{
			if (UserRole.User != null && UserRole.User.UserID != default(Int32))
			{
				UserRole.User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetMaster(UserRole.User.UserID);
			}
		}
		
		private void PopulateRole(UserRole UserRole)
		{
			if (UserRole.Role != null && UserRole.Role.RoleID != default(Int32))
			{
				UserRole.Role = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().GetMaster(UserRole.Role.RoleID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual UserRoleCollection GetByUserSimple(Int32 UserID)
        {
			UserRoleCollection UserRoleCollection = DataAccess.SelectByUser(UserID);
				
			if (AllowCache)
            {
				this.Cache.Append(UserRoleCollection.Clone(true));
            }
			
            return UserRoleCollection;
        }

		public virtual UserRoleCollection GetByUser(Int32 UserID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			UserRoleCollection UserRoleCollection = this.GetByUserSimple(UserID);

			if (UserRoleCollection.Count != 0 && parentModel != null)
			{
				UserRoleCollection.ForEach(m => m.User = null);
			}

			this.OnAfterGetCollection(ref UserRoleCollection);
			return UserRoleCollection;
		}

		public virtual Int32 GetCountByUser(Int32 UserID)
        {
			return DataAccess.SelectCountByUser(UserID);
        }
		
		public virtual UserRoleCollection GetByRoleSimple(Int32 RoleID)
        {
			UserRoleCollection UserRoleCollection = DataAccess.SelectByRole(RoleID);
				
			if (AllowCache)
            {
				this.Cache.Append(UserRoleCollection.Clone(true));
            }
			
            return UserRoleCollection;
        }

		public virtual UserRoleCollection GetByRole(Int32 RoleID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			UserRoleCollection UserRoleCollection = this.GetByRoleSimple(RoleID);

			if (UserRoleCollection.Count != 0 && parentModel != null)
			{
				UserRoleCollection.ForEach(m => m.Role = null);
			}

			this.OnAfterGetCollection(ref UserRoleCollection);
			return UserRoleCollection;
		}

		public virtual Int32 GetCountByRole(Int32 RoleID)
        {
			return DataAccess.SelectCountByRole(RoleID);
        }
		
		public override OperationResult<UserRole> AddWithDetail(UserRole UserRole)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (UserRole.User.State == ObjectState.Added)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().AddWithDetail(UserRole.User);
				}

				if (UserRole.Role.State == ObjectState.Added)
                {
					OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().AddWithDetail(UserRole.Role);
				}

                if (UserRole.State == ObjectState.Added)
                {
                    OperationResult<UserRole> result = this.Add(UserRole);
                }

                scope.Complete();
            }

            return new OperationResult<UserRole>(true, UserRole);
        }

        public override OperationResult<UserRole> EditWithDetail(UserRole UserRole)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (UserRole.User.State != ObjectState.None)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().Save(UserRole.User);
				}

				if (UserRole.Role.State != ObjectState.None)
                {
					OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Save(UserRole.Role);
				}

                if (UserRole.State == ObjectState.Edited)
                {
                    OperationResult<UserRole> result = this.Edit(UserRole);
                }

                scope.Complete();
            }

            return new OperationResult<UserRole>(true, UserRole);
        }

		public override OperationResult<UserRole> RemoveWithDetail(UserRole UserRole)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(UserRole.UserRoleID);

				if (UserRole.User.State == ObjectState.Removed)
                {
					OperationResult result_User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().RemoveWithDetail(UserRole.User);
				}

				if (UserRole.Role.State == ObjectState.Removed)
                {
					OperationResult result_Role = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().RemoveWithDetail(UserRole.Role);
				}

				scope.Complete();
			}

			return new OperationResult<UserRole>(true, UserRole);
        }

		public UserRole GetByKeySimple(Int32 UserID, Int32 RoleID)
		{
			UserRole UserRole = DataAccess.SelectByKey(UserID, RoleID);

            if (AllowCache)
            {
                this.Cache.Add(UserRole.Clone<UserRole>(true));
            }

            return UserRole;
		}

		public UserRole GetByKey(Int32 UserID, Int32 RoleID)
		{
			UserRole UserRole = this.GetByKeySimple(UserID, RoleID);
            this.OnAfterGet(ref UserRole);

            return UserRole;
		}

		public OperationResult<UserRole> EditByKey(UserRole UserRole, Int32 UserID, Int32 RoleID)
		{
			this.OnBeforeEdit(ref UserRole);
            UserRole.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(UserRole, UserID, RoleID);

			this.OnAfterEdit(ref UserRole);
            return new OperationResult<UserRole>(isEdit, UserRole);
		}

		public OperationResult RemoveByKey(Int32 UserID, Int32 RoleID)
		{
			bool isRemove = DataAccess.DeleteByKey(UserID, RoleID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}