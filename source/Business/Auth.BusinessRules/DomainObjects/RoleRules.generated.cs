using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class RoleRules : AuthRulesBase<Int32, Role, RoleCollection, IRoleDataAccess>
    {
		#region Constructors

        public RoleRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IRoleDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IRoleDataAccess>();
			}
		}

		private List<Action<Role>> _MasterPopulateActions;
        protected override List<Action<Role>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<Role>>();

						this._MasterPopulateActions.Add(this.PopulateApplication);
						this._MasterPopulateActions.Add(this.PopulateDesignation);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<Role>> _DetailPopulateActions;
        protected override List<Action<Role>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<Role>>();

						//this._DetailPopulateActions.Add(this.PopulateRoleActionCollection);
						//this._DetailPopulateActions.Add(this.PopulateRoleActionCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateRoleOperationCollection);
						//this._DetailPopulateActions.Add(this.PopulateRoleOperationCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateRoleViewCollection);
						//this._DetailPopulateActions.Add(this.PopulateRoleViewCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateUserRoleCollection);
						//this._DetailPopulateActions.Add(this.PopulateUserRoleCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<RoleCollection>();
            }
        }

        protected override RoleCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<RoleCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplication(Role Role)
		{
			if (Role.Application != null && Role.Application.ApplicationID != default(Int32))
			{
				Role.Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().GetMaster(Role.Application.ApplicationID);
			}
		}
		
		private void PopulateDesignation(Role Role)
		{
			if (Role.Designation != null && Role.Designation.DesignationID != default(Int16))
			{
				Role.Designation = AuthBusinessRulesFactory.Instance.GetBusinessRules<DesignationRules>().GetMaster(Role.Designation.DesignationID);
			}
		}
		
		private void PopulateRoleActionCollection(Role Role)
		{
			Role.RoleActionCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleActionRules>().GetByRole(Role.RoleID, Role);
		}

		private void PopulateRoleActionCollectionCount(Role Role)
		{
			Role.RoleActionCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleActionRules>().GetCountByRole(Role.RoleID);
		}
		
		private void PopulateRoleOperationCollection(Role Role)
		{
			Role.RoleOperationCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleOperationRules>().GetByRole(Role.RoleID, Role);
		}

		private void PopulateRoleOperationCollectionCount(Role Role)
		{
			Role.RoleOperationCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleOperationRules>().GetCountByRole(Role.RoleID);
		}
		
		private void PopulateRoleViewCollection(Role Role)
		{
			Role.RoleViewCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleViewRules>().GetByRole(Role.RoleID, Role);
		}

		private void PopulateRoleViewCollectionCount(Role Role)
		{
			Role.RoleViewCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleViewRules>().GetCountByRole(Role.RoleID);
		}
		
		private void PopulateUserRoleCollection(Role Role)
		{
			Role.UserRoleCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRoleRules>().GetByRole(Role.RoleID, Role);
		}

		private void PopulateUserRoleCollectionCount(Role Role)
		{
			Role.UserRoleCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRoleRules>().GetCountByRole(Role.RoleID);
		}

		#endregion

		#region Public Methods
		
		public virtual RoleCollection GetByApplicationSimple(Int32 ApplicationID)
        {
			RoleCollection RoleCollection = DataAccess.SelectByApplication(ApplicationID);
				
			if (AllowCache)
            {
				this.Cache.Append(RoleCollection.Clone(true));
            }
			
            return RoleCollection;
        }

		public virtual RoleCollection GetByApplication(Int32 ApplicationID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			RoleCollection RoleCollection = this.GetByApplicationSimple(ApplicationID);

			if (RoleCollection.Count != 0 && parentModel != null)
			{
				RoleCollection.ForEach(m => m.Application = null);
			}

			this.OnAfterGetCollection(ref RoleCollection);
			return RoleCollection;
		}

		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
			return DataAccess.SelectCountByApplication(ApplicationID);
        }
		
		public virtual RoleCollection GetByDesignationSimple(Int16 DesignationID)
        {
			RoleCollection RoleCollection = DataAccess.SelectByDesignation(DesignationID);
				
			if (AllowCache)
            {
				this.Cache.Append(RoleCollection.Clone(true));
            }
			
            return RoleCollection;
        }

		public virtual RoleCollection GetByDesignation(Int16 DesignationID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			RoleCollection RoleCollection = this.GetByDesignationSimple(DesignationID);

			if (RoleCollection.Count != 0 && parentModel != null)
			{
				RoleCollection.ForEach(m => m.Designation = null);
			}

			this.OnAfterGetCollection(ref RoleCollection);
			return RoleCollection;
		}

		public virtual Int32 GetCountByDesignation(Int16 DesignationID)
        {
			return DataAccess.SelectCountByDesignation(DesignationID);
        }
		
		public override OperationResult<Role> AddWithDetail(Role Role)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (Role.Application.State == ObjectState.Added)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().AddWithDetail(Role.Application);
				}

				if (Role.Designation.State == ObjectState.Added)
                {
					OperationResult<Designation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<DesignationRules>().AddWithDetail(Role.Designation);
				}

                if (Role.State == ObjectState.Added)
                {
                    OperationResult<Role> result = this.Add(Role);
                }
				
				foreach (RoleAction RoleAction in Role.RoleActionCollection.Where(obj => obj.State == ObjectState.Added))
                {
					RoleAction.Role = new Role { RoleID = Role.RoleID };

                    OperationResult<RoleAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleActionRules>().AddWithDetail(RoleAction);
                }
				
				foreach (RoleOperation RoleOperation in Role.RoleOperationCollection.Where(obj => obj.State == ObjectState.Added))
                {
					RoleOperation.Role = new Role { RoleID = Role.RoleID };

                    OperationResult<RoleOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleOperationRules>().AddWithDetail(RoleOperation);
                }
				
				foreach (RoleView RoleView in Role.RoleViewCollection.Where(obj => obj.State == ObjectState.Added))
                {
					RoleView.Role = new Role { RoleID = Role.RoleID };

                    OperationResult<RoleView> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleViewRules>().AddWithDetail(RoleView);
                }
				
				foreach (UserRole UserRole in Role.UserRoleCollection.Where(obj => obj.State == ObjectState.Added))
                {
					UserRole.Role = new Role { RoleID = Role.RoleID };

                    OperationResult<UserRole> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRoleRules>().AddWithDetail(UserRole);
                }

                scope.Complete();
            }

            return new OperationResult<Role>(true, Role);
        }

        public override OperationResult<Role> EditWithDetail(Role Role)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (Role.Application.State != ObjectState.None)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().Save(Role.Application);
				}

				if (Role.Designation.State != ObjectState.None)
                {
					OperationResult<Designation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<DesignationRules>().Save(Role.Designation);
				}

                if (Role.State == ObjectState.Edited)
                {
                    OperationResult<Role> result = this.Edit(Role);
                }
                    
				foreach (RoleAction RoleAction in Role.RoleActionCollection.Where(obj => obj.State != ObjectState.None))
                {
					RoleAction.Role = new Role { RoleID = Role.RoleID };

                    OperationResult<RoleAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleActionRules>().Save(RoleAction);
                }
                    
				foreach (RoleOperation RoleOperation in Role.RoleOperationCollection.Where(obj => obj.State != ObjectState.None))
                {
					RoleOperation.Role = new Role { RoleID = Role.RoleID };

                    OperationResult<RoleOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleOperationRules>().Save(RoleOperation);
                }
                    
				foreach (RoleView RoleView in Role.RoleViewCollection.Where(obj => obj.State != ObjectState.None))
                {
					RoleView.Role = new Role { RoleID = Role.RoleID };

                    OperationResult<RoleView> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleViewRules>().Save(RoleView);
                }
                    
				foreach (UserRole UserRole in Role.UserRoleCollection.Where(obj => obj.State != ObjectState.None))
                {
					UserRole.Role = new Role { RoleID = Role.RoleID };

                    OperationResult<UserRole> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRoleRules>().Save(UserRole);
                }

                scope.Complete();
            }

            return new OperationResult<Role>(true, Role);
        }

		public override OperationResult<Role> RemoveWithDetail(Role Role)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(Role.RoleID);

				if (Role.Application.State == ObjectState.Removed)
                {
					OperationResult result_Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().RemoveWithDetail(Role.Application);
				}

				if (Role.Designation.State == ObjectState.Removed)
                {
					OperationResult result_Designation = AuthBusinessRulesFactory.Instance.GetBusinessRules<DesignationRules>().RemoveWithDetail(Role.Designation);
				}

				scope.Complete();
			}

			return new OperationResult<Role>(true, Role);
        }

		public Role GetByKeySimple(Int32 ApplicationID, String Name)
		{
			Role Role = DataAccess.SelectByKey(ApplicationID, Name);

            if (AllowCache)
            {
                this.Cache.Add(Role.Clone<Role>(true));
            }

            return Role;
		}

		public Role GetByKey(Int32 ApplicationID, String Name)
		{
			Role Role = this.GetByKeySimple(ApplicationID, Name);
            this.OnAfterGet(ref Role);

            return Role;
		}

		public OperationResult<Role> EditByKey(Role Role, Int32 ApplicationID, String Name)
		{
			this.OnBeforeEdit(ref Role);
            Role.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(Role, ApplicationID, Name);

			this.OnAfterEdit(ref Role);
            return new OperationResult<Role>(isEdit, Role);
		}

		public OperationResult RemoveByKey(Int32 ApplicationID, String Name)
		{
			bool isRemove = DataAccess.DeleteByKey(ApplicationID, Name);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}