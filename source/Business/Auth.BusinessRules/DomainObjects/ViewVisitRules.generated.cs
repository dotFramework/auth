using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ViewVisitRules : AuthRulesBase<Int64, ViewVisit, ViewVisitCollection, IViewVisitDataAccess>
    {
		#region Constructors

        public ViewVisitRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IViewVisitDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IViewVisitDataAccess>();
			}
		}

		private List<Action<ViewVisit>> _MasterPopulateActions;
        protected override List<Action<ViewVisit>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<ViewVisit>>();

						this._MasterPopulateActions.Add(this.PopulateApplicationHierarchy);
						//this._MasterPopulateActions.Add(this.PopulateUser);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<ViewVisit>> _DetailPopulateActions;
        protected override List<Action<ViewVisit>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<ViewVisit>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ViewVisitCollection>();
            }
        }

        protected override ViewVisitCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ViewVisitCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplicationHierarchy(ViewVisit ViewVisit)
		{
			if (ViewVisit.ApplicationHierarchy != null && ViewVisit.ApplicationHierarchy.ApplicationHierarchyID != default(Int32))
			{
				ViewVisit.ApplicationHierarchy = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().GetMaster(ViewVisit.ApplicationHierarchy.ApplicationHierarchyID);
			}
		}
		
		private void PopulateUser(ViewVisit ViewVisit)
		{
			if (ViewVisit.User != null && ViewVisit.User.UserID != default(Int32))
			{
				ViewVisit.User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetMaster(ViewVisit.User.UserID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual ViewVisitCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID)
        {
			ViewVisitCollection ViewVisitCollection = DataAccess.SelectByApplicationHierarchy(ApplicationHierarchyID);
				
			if (AllowCache)
            {
				this.Cache.Append(ViewVisitCollection.Clone(true));
            }
			
            return ViewVisitCollection;
        }

		public virtual ViewVisitCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ViewVisitCollection ViewVisitCollection = this.GetByApplicationHierarchySimple(ApplicationHierarchyID);

			if (ViewVisitCollection.Count != 0 && parentModel != null)
			{
				ViewVisitCollection.ForEach(m => m.ApplicationHierarchy = null);
			}

			this.OnAfterGetCollection(ref ViewVisitCollection);
			return ViewVisitCollection;
		}

		public virtual Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
			return DataAccess.SelectCountByApplicationHierarchy(ApplicationHierarchyID);
        }
		
		public virtual ViewVisitCollection GetByUserSimple(Int32 UserID)
        {
			ViewVisitCollection ViewVisitCollection = DataAccess.SelectByUser(UserID);
				
			if (AllowCache)
            {
				this.Cache.Append(ViewVisitCollection.Clone(true));
            }
			
            return ViewVisitCollection;
        }

		public virtual ViewVisitCollection GetByUser(Int32 UserID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ViewVisitCollection ViewVisitCollection = this.GetByUserSimple(UserID);

			if (ViewVisitCollection.Count != 0 && parentModel != null)
			{
				ViewVisitCollection.ForEach(m => m.User = null);
			}

			this.OnAfterGetCollection(ref ViewVisitCollection);
			return ViewVisitCollection;
		}

		public virtual Int32 GetCountByUser(Int32 UserID)
        {
			return DataAccess.SelectCountByUser(UserID);
        }
		
		public override OperationResult<ViewVisit> AddWithDetail(ViewVisit ViewVisit)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ViewVisit.ApplicationHierarchy.State == ObjectState.Added)
                {
					OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().AddWithDetail(ViewVisit.ApplicationHierarchy);
				}

				if (ViewVisit.User.State == ObjectState.Added)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().AddWithDetail(ViewVisit.User);
				}

                if (ViewVisit.State == ObjectState.Added)
                {
                    OperationResult<ViewVisit> result = this.Add(ViewVisit);
                }

                scope.Complete();
            }

            return new OperationResult<ViewVisit>(true, ViewVisit);
        }

        public override OperationResult<ViewVisit> EditWithDetail(ViewVisit ViewVisit)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ViewVisit.ApplicationHierarchy.State != ObjectState.None)
                {
					OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().Save(ViewVisit.ApplicationHierarchy);
				}

				if (ViewVisit.User.State != ObjectState.None)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().Save(ViewVisit.User);
				}

                if (ViewVisit.State == ObjectState.Edited)
                {
                    OperationResult<ViewVisit> result = this.Edit(ViewVisit);
                }

                scope.Complete();
            }

            return new OperationResult<ViewVisit>(true, ViewVisit);
        }

		public override OperationResult<ViewVisit> RemoveWithDetail(ViewVisit ViewVisit)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(ViewVisit.ViewVisitID);

				if (ViewVisit.ApplicationHierarchy.State == ObjectState.Removed)
                {
					OperationResult result_ApplicationHierarchy = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().RemoveWithDetail(ViewVisit.ApplicationHierarchy);
				}

				if (ViewVisit.User.State == ObjectState.Removed)
                {
					OperationResult result_User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().RemoveWithDetail(ViewVisit.User);
				}

				scope.Complete();
			}

			return new OperationResult<ViewVisit>(true, ViewVisit);
        }

		#endregion
    }
}