using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ApplicableOperationRules : AuthRulesBase<Int32, ApplicableOperation, ApplicableOperationCollection, IApplicableOperationDataAccess>
    {
		#region Constructors

        public ApplicableOperationRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IApplicableOperationDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IApplicableOperationDataAccess>();
			}
		}

		private List<Action<ApplicableOperation>> _MasterPopulateActions;
        protected override List<Action<ApplicableOperation>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<ApplicableOperation>>();

						//this._MasterPopulateActions.Add(this.PopulateApplicationEndpoint);
						//this._MasterPopulateActions.Add(this.PopulateServiceOperation);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<ApplicableOperation>> _DetailPopulateActions;
        protected override List<Action<ApplicableOperation>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<ApplicableOperation>>();

						//this._DetailPopulateActions.Add(this.PopulateRoleOperationCollection);
						//this._DetailPopulateActions.Add(this.PopulateRoleOperationCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ApplicableOperationCollection>();
            }
        }

        protected override ApplicableOperationCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ApplicableOperationCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplicationEndpoint(ApplicableOperation ApplicableOperation)
		{
			if (ApplicableOperation.ApplicationEndpoint != null && ApplicableOperation.ApplicationEndpoint.ApplicationEndpointID != default(Int32))
			{
				ApplicableOperation.ApplicationEndpoint = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationEndpointRules>().GetMaster(ApplicableOperation.ApplicationEndpoint.ApplicationEndpointID);
			}
		}
		
		private void PopulateServiceOperation(ApplicableOperation ApplicableOperation)
		{
			if (ApplicableOperation.ServiceOperation != null && ApplicableOperation.ServiceOperation.ServiceOperationID != default(Int32))
			{
				ApplicableOperation.ServiceOperation = AuthBusinessRulesFactory.Instance.GetBusinessRules<ServiceOperationRules>().GetMaster(ApplicableOperation.ServiceOperation.ServiceOperationID);
			}
		}
		
		private void PopulateRoleOperationCollection(ApplicableOperation ApplicableOperation)
		{
			ApplicableOperation.RoleOperationCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleOperationRules>().GetByApplicableOperation(ApplicableOperation.ApplicableOperationID, ApplicableOperation);
		}

		private void PopulateRoleOperationCollectionCount(ApplicableOperation ApplicableOperation)
		{
			ApplicableOperation.RoleOperationCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleOperationRules>().GetCountByApplicableOperation(ApplicableOperation.ApplicableOperationID);
		}

		#endregion

		#region Public Methods
		
		public virtual ApplicableOperationCollection GetByApplicationEndpointSimple(Int32 ApplicationEndpointID)
        {
			ApplicableOperationCollection ApplicableOperationCollection = DataAccess.SelectByApplicationEndpoint(ApplicationEndpointID);
				
			if (AllowCache)
            {
				this.Cache.Append(ApplicableOperationCollection.Clone(true));
            }
			
            return ApplicableOperationCollection;
        }

		public virtual ApplicableOperationCollection GetByApplicationEndpoint(Int32 ApplicationEndpointID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ApplicableOperationCollection ApplicableOperationCollection = this.GetByApplicationEndpointSimple(ApplicationEndpointID);

			if (ApplicableOperationCollection.Count != 0 && parentModel != null)
			{
				ApplicableOperationCollection.ForEach(m => m.ApplicationEndpoint = null);
			}

			this.OnAfterGetCollection(ref ApplicableOperationCollection);
			return ApplicableOperationCollection;
		}

		public virtual Int32 GetCountByApplicationEndpoint(Int32 ApplicationEndpointID)
        {
			return DataAccess.SelectCountByApplicationEndpoint(ApplicationEndpointID);
        }
		
		public virtual ApplicableOperationCollection GetByServiceOperationSimple(Int32 ServiceOperationID)
        {
			ApplicableOperationCollection ApplicableOperationCollection = DataAccess.SelectByServiceOperation(ServiceOperationID);
				
			if (AllowCache)
            {
				this.Cache.Append(ApplicableOperationCollection.Clone(true));
            }
			
            return ApplicableOperationCollection;
        }

		public virtual ApplicableOperationCollection GetByServiceOperation(Int32 ServiceOperationID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ApplicableOperationCollection ApplicableOperationCollection = this.GetByServiceOperationSimple(ServiceOperationID);

			if (ApplicableOperationCollection.Count != 0 && parentModel != null)
			{
				ApplicableOperationCollection.ForEach(m => m.ServiceOperation = null);
			}

			this.OnAfterGetCollection(ref ApplicableOperationCollection);
			return ApplicableOperationCollection;
		}

		public virtual Int32 GetCountByServiceOperation(Int32 ServiceOperationID)
        {
			return DataAccess.SelectCountByServiceOperation(ServiceOperationID);
        }
		
		public override OperationResult<ApplicableOperation> AddWithDetail(ApplicableOperation ApplicableOperation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ApplicableOperation.ApplicationEndpoint.State == ObjectState.Added)
                {
					OperationResult<ApplicationEndpoint> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationEndpointRules>().AddWithDetail(ApplicableOperation.ApplicationEndpoint);
				}

				if (ApplicableOperation.ServiceOperation.State == ObjectState.Added)
                {
					OperationResult<ServiceOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ServiceOperationRules>().AddWithDetail(ApplicableOperation.ServiceOperation);
				}

                if (ApplicableOperation.State == ObjectState.Added)
                {
                    OperationResult<ApplicableOperation> result = this.Add(ApplicableOperation);
                }
				
				foreach (RoleOperation RoleOperation in ApplicableOperation.RoleOperationCollection.Where(obj => obj.State == ObjectState.Added))
                {
					RoleOperation.ApplicableOperation = new ApplicableOperation { ApplicableOperationID = ApplicableOperation.ApplicableOperationID };

                    OperationResult<RoleOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleOperationRules>().AddWithDetail(RoleOperation);
                }

                scope.Complete();
            }

            return new OperationResult<ApplicableOperation>(true, ApplicableOperation);
        }

        public override OperationResult<ApplicableOperation> EditWithDetail(ApplicableOperation ApplicableOperation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ApplicableOperation.ApplicationEndpoint.State != ObjectState.None)
                {
					OperationResult<ApplicationEndpoint> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationEndpointRules>().Save(ApplicableOperation.ApplicationEndpoint);
				}

				if (ApplicableOperation.ServiceOperation.State != ObjectState.None)
                {
					OperationResult<ServiceOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ServiceOperationRules>().Save(ApplicableOperation.ServiceOperation);
				}

                if (ApplicableOperation.State == ObjectState.Edited)
                {
                    OperationResult<ApplicableOperation> result = this.Edit(ApplicableOperation);
                }
                    
				foreach (RoleOperation RoleOperation in ApplicableOperation.RoleOperationCollection.Where(obj => obj.State != ObjectState.None))
                {
					RoleOperation.ApplicableOperation = new ApplicableOperation { ApplicableOperationID = ApplicableOperation.ApplicableOperationID };

                    OperationResult<RoleOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleOperationRules>().Save(RoleOperation);
                }

                scope.Complete();
            }

            return new OperationResult<ApplicableOperation>(true, ApplicableOperation);
        }

		public override OperationResult<ApplicableOperation> RemoveWithDetail(ApplicableOperation ApplicableOperation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(ApplicableOperation.ApplicableOperationID);

				if (ApplicableOperation.ApplicationEndpoint.State == ObjectState.Removed)
                {
					OperationResult result_ApplicationEndpoint = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationEndpointRules>().RemoveWithDetail(ApplicableOperation.ApplicationEndpoint);
				}

				if (ApplicableOperation.ServiceOperation.State == ObjectState.Removed)
                {
					OperationResult result_ServiceOperation = AuthBusinessRulesFactory.Instance.GetBusinessRules<ServiceOperationRules>().RemoveWithDetail(ApplicableOperation.ServiceOperation);
				}

				scope.Complete();
			}

			return new OperationResult<ApplicableOperation>(true, ApplicableOperation);
        }

		public ApplicableOperation GetByKeySimple(Int32 ApplicationEndpointID, Int32 ServiceOperationID)
		{
			ApplicableOperation ApplicableOperation = DataAccess.SelectByKey(ApplicationEndpointID, ServiceOperationID);

            if (AllowCache)
            {
                this.Cache.Add(ApplicableOperation.Clone<ApplicableOperation>(true));
            }

            return ApplicableOperation;
		}

		public ApplicableOperation GetByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID)
		{
			ApplicableOperation ApplicableOperation = this.GetByKeySimple(ApplicationEndpointID, ServiceOperationID);
            this.OnAfterGet(ref ApplicableOperation);

            return ApplicableOperation;
		}

		public OperationResult<ApplicableOperation> EditByKey(ApplicableOperation ApplicableOperation, Int32 ApplicationEndpointID, Int32 ServiceOperationID)
		{
			this.OnBeforeEdit(ref ApplicableOperation);
            ApplicableOperation.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(ApplicableOperation, ApplicationEndpointID, ServiceOperationID);

			this.OnAfterEdit(ref ApplicableOperation);
            return new OperationResult<ApplicableOperation>(isEdit, ApplicableOperation);
		}

		public OperationResult RemoveByKey(Int32 ApplicationEndpointID, Int32 ServiceOperationID)
		{
			bool isRemove = DataAccess.DeleteByKey(ApplicationEndpointID, ServiceOperationID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}