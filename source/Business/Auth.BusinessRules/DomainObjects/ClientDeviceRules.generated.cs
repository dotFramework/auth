using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Caching;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Model;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ClientDeviceRules : AuthRulesBase<Int32, ClientDevice, ClientDeviceCollection, IClientDeviceDataAccess>
    {
		#region Constructors

        public ClientDeviceRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IClientDeviceDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IClientDeviceDataAccess>();
			}
		}

		private List<Action<ClientDevice>> _MasterPopulateActions;
        protected override List<Action<ClientDevice>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<ClientDevice>>();

						this._MasterPopulateActions.Add(this.PopulateDevice);
						//this._MasterPopulateActions.Add(this.PopulateClient);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<ClientDevice>> _DetailPopulateActions;
        protected override List<Action<ClientDevice>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<ClientDevice>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ClientDeviceCollection>();
            }
        }

        protected override ClientDeviceCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ClientDeviceCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateDevice(ClientDevice ClientDevice)
		{
			if (ClientDevice.Device != null && ClientDevice.Device.DeviceID != default(Int32))
			{
				ClientDevice.Device = AuthBusinessRulesFactory.Instance.GetBusinessRules<DeviceRules>().GetMaster(ClientDevice.Device.DeviceID);
			}
		}
		
		private void PopulateClient(ClientDevice ClientDevice)
		{
			if (ClientDevice.Client != null && ClientDevice.Client.ClientID != default(Int32))
			{
				ClientDevice.Client = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientRules>().GetMaster(ClientDevice.Client.ClientID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual ClientDeviceCollection GetByDeviceSimple(Int32 DeviceID)
        {
			ClientDeviceCollection ClientDeviceCollection = DataAccess.SelectByDevice(DeviceID);
			
			if (AllowCache)
            {
				this.Cache.Append(ClientDeviceCollection.Clone(true));
            }
			
            return ClientDeviceCollection;
        }

		public virtual ClientDeviceCollection GetByDevice(Int32 DeviceID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ClientDeviceCollection ClientDeviceCollection = this.GetByDeviceSimple(DeviceID);

			if (ClientDeviceCollection.Count != 0 && parentModel != null)
			{
				ClientDeviceCollection.ForEach(m => m.Device = null);
			}

			this.OnAfterGetCollection(ref ClientDeviceCollection);
			return ClientDeviceCollection;
		}

		public virtual Int32 GetCountByDevice(Int32 DeviceID)
        {
			return DataAccess.SelectCountByDevice(DeviceID);
        }
		
		public virtual ClientDeviceCollection GetByClientSimple(Int32 ClientID)
        {
			ClientDeviceCollection ClientDeviceCollection = DataAccess.SelectByClient(ClientID);
			
			if (AllowCache)
            {
				this.Cache.Append(ClientDeviceCollection.Clone(true));
            }
			
            return ClientDeviceCollection;
        }

		public virtual ClientDeviceCollection GetByClient(Int32 ClientID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ClientDeviceCollection ClientDeviceCollection = this.GetByClientSimple(ClientID);

			if (ClientDeviceCollection.Count != 0 && parentModel != null)
			{
				ClientDeviceCollection.ForEach(m => m.Client = null);
			}

			this.OnAfterGetCollection(ref ClientDeviceCollection);
			return ClientDeviceCollection;
		}

		public virtual Int32 GetCountByClient(Int32 ClientID)
        {
			return DataAccess.SelectCountByClient(ClientID);
        }
		
		public override OperationResult<ClientDevice> AddWithDetail(ClientDevice ClientDevice)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ClientDevice.Device.State == ObjectState.Added)
                {
					OperationResult<Device> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<DeviceRules>().AddWithDetail(ClientDevice.Device);
				}

				if (ClientDevice.Client.State == ObjectState.Added)
                {
					OperationResult<Client> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientRules>().AddWithDetail(ClientDevice.Client);
				}

                if (ClientDevice.State == ObjectState.Added)
                {
                    OperationResult<ClientDevice> result = this.Add(ClientDevice);
                }

                scope.Complete();
            }

            return new OperationResult<ClientDevice>(true, ClientDevice);
        }

        public override OperationResult<ClientDevice> EditWithDetail(ClientDevice ClientDevice)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ClientDevice.Device.State != ObjectState.None)
                {
					OperationResult<Device> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<DeviceRules>().Save(ClientDevice.Device);
				}

				if (ClientDevice.Client.State != ObjectState.None)
                {
					OperationResult<Client> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientRules>().Save(ClientDevice.Client);
				}

                if (ClientDevice.State == ObjectState.Edited)
                {
                    OperationResult<ClientDevice> result = this.Edit(ClientDevice);
                }

                scope.Complete();
            }

            return new OperationResult<ClientDevice>(true, ClientDevice);
        }

		public override OperationResult<ClientDevice> RemoveWithDetail(ClientDevice ClientDevice)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(ClientDevice.ClientDeviceID);

				if (ClientDevice.Device.State == ObjectState.Removed)
                {
					OperationResult result_Device = AuthBusinessRulesFactory.Instance.GetBusinessRules<DeviceRules>().RemoveWithDetail(ClientDevice.Device);
				}

				if (ClientDevice.Client.State == ObjectState.Removed)
                {
					OperationResult result_Client = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientRules>().RemoveWithDetail(ClientDevice.Client);
				}

				scope.Complete();
			}

			return new OperationResult<ClientDevice>(true, ClientDevice);
        }

		public virtual ClientDevice GetByKeySimple(Int32 DeviceID, Int32 ClientID)
		{
			ClientDevice ClientDevice = DataAccess.SelectByKey(DeviceID, ClientID);
			
			if (AllowCache)
            {
                this.Cache.Add(ClientDevice.Clone<ClientDevice>(true));
            }

            return ClientDevice;
		}

		public virtual ClientDevice GetByKey(Int32 DeviceID, Int32 ClientID)
		{
			ClientDevice ClientDevice = this.GetByKeySimple(DeviceID, ClientID);
            this.OnAfterGet(ref ClientDevice);

            return ClientDevice;
		}

		public virtual OperationResult<ClientDevice> EditByKey(ClientDevice ClientDevice, Int32 DeviceID, Int32 ClientID)
		{
			this.OnBeforeEdit(ref ClientDevice);
            ClientDevice.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(ClientDevice, DeviceID, ClientID);

			this.OnAfterEdit(ref ClientDevice);
            return new OperationResult<ClientDevice>(isEdit, ClientDevice);
		}

		public virtual OperationResult RemoveByKey(Int32 DeviceID, Int32 ClientID)
		{
			bool isRemove = DataAccess.DeleteByKey(DeviceID, ClientID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}