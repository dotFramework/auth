using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ApplicationRules : AuthRulesBase<Int32, Application, ApplicationCollection, IApplicationDataAccess>
    {
		#region Constructors

        public ApplicationRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IApplicationDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IApplicationDataAccess>();
			}
		}

		private List<Action<Application>> _MasterPopulateActions;
        protected override List<Action<Application>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<Application>>();

						//this._MasterPopulateActions.Add(this.PopulateClient);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<Application>> _DetailPopulateActions;
        protected override List<Action<Application>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<Application>>();

						//this._DetailPopulateActions.Add(this.PopulateApplicationEndpointCollection);
						//this._DetailPopulateActions.Add(this.PopulateApplicationEndpointCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateApplicationHierarchyCollection);
						//this._DetailPopulateActions.Add(this.PopulateApplicationHierarchyCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateLoginProviderOptionsCollection);
						//this._DetailPopulateActions.Add(this.PopulateLoginProviderOptionsCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateRoleCollection);
						//this._DetailPopulateActions.Add(this.PopulateRoleCollectionCount);
						//this._DetailPopulateActions.Add(this.PopulateSubApplicationCollection);
						//this._DetailPopulateActions.Add(this.PopulateSubApplicationCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ApplicationCollection>();
            }
        }

        protected override ApplicationCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ApplicationCollection>();
            }
        }

		#endregion

		#endregion

		#region Private Methods

		private void PopulateClient(Application Application)
		{
			if (Application.Client != null && Application.Client.ClientID != default(Int32))
			{
				Application.Client = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientRules>().GetMaster(Application.Client.ClientID);
			}
		}

		private void PopulateApplicationEndpointCollection(Application Application)
		{
			Application.ApplicationEndpointCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationEndpointRules>().GetByApplication(Application.ApplicationID, Application);
		}

		private void PopulateApplicationEndpointCollectionCount(Application Application)
		{
			Application.ApplicationEndpointCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationEndpointRules>().GetCountByApplication(Application.ApplicationID);
		}
		
		private void PopulateApplicationHierarchyCollection(Application Application)
		{
			Application.ApplicationHierarchyCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().GetByApplication(Application.ApplicationID, Application);
		}

		private void PopulateApplicationHierarchyCollectionCount(Application Application)
		{
			Application.ApplicationHierarchyCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().GetCountByApplication(Application.ApplicationID);
		}
		
		private void PopulateLoginProviderOptionsCollection(Application Application)
		{
			Application.LoginProviderOptionsCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().GetByApplication(Application.ApplicationID, Application);
		}

		private void PopulateLoginProviderOptionsCollectionCount(Application Application)
		{
			Application.LoginProviderOptionsCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().GetCountByApplication(Application.ApplicationID);
		}
		
		private void PopulateRoleCollection(Application Application)
		{
			Application.RoleCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().GetByApplication(Application.ApplicationID, Application);
		}

		private void PopulateRoleCollectionCount(Application Application)
		{
			Application.RoleCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().GetCountByApplication(Application.ApplicationID);
		}
		
		private void PopulateSubApplicationCollection(Application Application)
		{
			Application.SubApplicationCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<SubApplicationRules>().GetByApplication(Application.ApplicationID, Application);
		}

		private void PopulateSubApplicationCollectionCount(Application Application)
		{
			Application.SubApplicationCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<SubApplicationRules>().GetCountByApplication(Application.ApplicationID);
		}

		#endregion

		#region Public Methods
		
		public override OperationResult<Application> AddWithDetail(Application Application)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (Application.State == ObjectState.Added)
                {
                    OperationResult<Application> result = this.Add(Application);
                }
				
				foreach (ApplicationEndpoint ApplicationEndpoint in Application.ApplicationEndpointCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ApplicationEndpoint.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<ApplicationEndpoint> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationEndpointRules>().AddWithDetail(ApplicationEndpoint);
                }
				
				foreach (ApplicationHierarchy ApplicationHierarchy in Application.ApplicationHierarchyCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ApplicationHierarchy.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().AddWithDetail(ApplicationHierarchy);
                }
				
				foreach (LoginProviderOptions LoginProviderOptions in Application.LoginProviderOptionsCollection.Where(obj => obj.State == ObjectState.Added))
                {
					LoginProviderOptions.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<LoginProviderOptions> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().AddWithDetail(LoginProviderOptions);
                }
				
				foreach (Role Role in Application.RoleCollection.Where(obj => obj.State == ObjectState.Added))
                {
					Role.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().AddWithDetail(Role);
                }
				
				foreach (SubApplication SubApplication in Application.SubApplicationCollection.Where(obj => obj.State == ObjectState.Added))
                {
					SubApplication.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<SubApplication> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<SubApplicationRules>().AddWithDetail(SubApplication);
                }

                scope.Complete();
            }

            return new OperationResult<Application>(true, Application);
        }

        public override OperationResult<Application> EditWithDetail(Application Application)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (Application.State == ObjectState.Edited)
                {
                    OperationResult<Application> result = this.Edit(Application);
                }
                    
				foreach (ApplicationEndpoint ApplicationEndpoint in Application.ApplicationEndpointCollection.Where(obj => obj.State != ObjectState.None))
                {
					ApplicationEndpoint.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<ApplicationEndpoint> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationEndpointRules>().Save(ApplicationEndpoint);
                }
                    
				foreach (ApplicationHierarchy ApplicationHierarchy in Application.ApplicationHierarchyCollection.Where(obj => obj.State != ObjectState.None))
                {
					ApplicationHierarchy.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().Save(ApplicationHierarchy);
                }
                    
				foreach (LoginProviderOptions LoginProviderOptions in Application.LoginProviderOptionsCollection.Where(obj => obj.State != ObjectState.None))
                {
					LoginProviderOptions.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<LoginProviderOptions> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().Save(LoginProviderOptions);
                }
                    
				foreach (Role Role in Application.RoleCollection.Where(obj => obj.State != ObjectState.None))
                {
					Role.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Save(Role);
                }
                    
				foreach (SubApplication SubApplication in Application.SubApplicationCollection.Where(obj => obj.State != ObjectState.None))
                {
					SubApplication.Application = new Application { ApplicationID = Application.ApplicationID };

                    OperationResult<SubApplication> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<SubApplicationRules>().Save(SubApplication);
                }

                scope.Complete();
            }

            return new OperationResult<Application>(true, Application);
        }

		public override OperationResult<Application> RemoveWithDetail(Application Application)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(Application.ApplicationID);

				scope.Complete();
			}

			return new OperationResult<Application>(true, Application);
        }

		public Application GetByKeySimple(String Code)
		{
			Application Application = DataAccess.SelectByKey(Code);

            if (AllowCache)
            {
                this.Cache.Add(Application.Clone<Application>(true));
            }

            return Application;
		}

		public Application GetByKey(String Code)
		{
			Application Application = this.GetByKeySimple(Code);
            this.OnAfterGet(ref Application);

            return Application;
		}

		public OperationResult<Application> EditByKey(Application Application, String Code)
		{
			this.OnBeforeEdit(ref Application);
            Application.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(Application, Code);

			this.OnAfterEdit(ref Application);
            return new OperationResult<Application>(isEdit, Application);
		}

		public OperationResult RemoveByKey(String Code)
		{
			bool isRemove = DataAccess.DeleteByKey(Code);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}