using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class SubApplicationRules : AuthRulesBase<Int32, SubApplication, SubApplicationCollection, ISubApplicationDataAccess>
    {
		#region Constructors

        public SubApplicationRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override ISubApplicationDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<ISubApplicationDataAccess>();
			}
		}

		private List<Action<SubApplication>> _MasterPopulateActions;
        protected override List<Action<SubApplication>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<SubApplication>>();

						this._MasterPopulateActions.Add(this.PopulateApplication);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<SubApplication>> _DetailPopulateActions;
        protected override List<Action<SubApplication>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<SubApplication>>();

						//this._DetailPopulateActions.Add(this.PopulateUserSessionCollection);
						//this._DetailPopulateActions.Add(this.PopulateUserSessionCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<SubApplicationCollection>();
            }
        }

        protected override SubApplicationCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<SubApplicationCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplication(SubApplication SubApplication)
		{
			if (SubApplication.Application != null && SubApplication.Application.ApplicationID != default(Int32))
			{
				SubApplication.Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().GetMaster(SubApplication.Application.ApplicationID);
			}
		}
		
		private void PopulateUserSessionCollection(SubApplication SubApplication)
		{
			SubApplication.UserSessionCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserSessionRules>().GetBySubApplication(SubApplication.SubApplicationID, SubApplication);
		}

		private void PopulateUserSessionCollectionCount(SubApplication SubApplication)
		{
			SubApplication.UserSessionCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserSessionRules>().GetCountBySubApplication(SubApplication.SubApplicationID);
		}

		#endregion

		#region Public Methods
		
		public virtual SubApplicationCollection GetByApplicationSimple(Int32 ApplicationID)
        {
			SubApplicationCollection SubApplicationCollection = DataAccess.SelectByApplication(ApplicationID);
				
			if (AllowCache)
            {
				this.Cache.Append(SubApplicationCollection.Clone(true));
            }
			
            return SubApplicationCollection;
        }

		public virtual SubApplicationCollection GetByApplication(Int32 ApplicationID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			SubApplicationCollection SubApplicationCollection = this.GetByApplicationSimple(ApplicationID);

			if (SubApplicationCollection.Count != 0 && parentModel != null)
			{
				SubApplicationCollection.ForEach(m => m.Application = null);
			}

			this.OnAfterGetCollection(ref SubApplicationCollection);
			return SubApplicationCollection;
		}

		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
			return DataAccess.SelectCountByApplication(ApplicationID);
        }
		
		public override OperationResult<SubApplication> AddWithDetail(SubApplication SubApplication)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (SubApplication.Application.State == ObjectState.Added)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().AddWithDetail(SubApplication.Application);
				}

                if (SubApplication.State == ObjectState.Added)
                {
                    OperationResult<SubApplication> result = this.Add(SubApplication);
                }
				
				foreach (UserSession UserSession in SubApplication.UserSessionCollection.Where(obj => obj.State == ObjectState.Added))
                {
					UserSession.SubApplication = new SubApplication { SubApplicationID = SubApplication.SubApplicationID };

                    OperationResult<UserSession> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserSessionRules>().AddWithDetail(UserSession);
                }

                scope.Complete();
            }

            return new OperationResult<SubApplication>(true, SubApplication);
        }

        public override OperationResult<SubApplication> EditWithDetail(SubApplication SubApplication)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (SubApplication.Application.State != ObjectState.None)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().Save(SubApplication.Application);
				}

                if (SubApplication.State == ObjectState.Edited)
                {
                    OperationResult<SubApplication> result = this.Edit(SubApplication);
                }
                    
				foreach (UserSession UserSession in SubApplication.UserSessionCollection.Where(obj => obj.State != ObjectState.None))
                {
					UserSession.SubApplication = new SubApplication { SubApplicationID = SubApplication.SubApplicationID };

                    OperationResult<UserSession> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserSessionRules>().Save(UserSession);
                }

                scope.Complete();
            }

            return new OperationResult<SubApplication>(true, SubApplication);
        }

		public override OperationResult<SubApplication> RemoveWithDetail(SubApplication SubApplication)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(SubApplication.SubApplicationID);

				if (SubApplication.Application.State == ObjectState.Removed)
                {
					OperationResult result_Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().RemoveWithDetail(SubApplication.Application);
				}

				scope.Complete();
			}

			return new OperationResult<SubApplication>(true, SubApplication);
        }

		public SubApplication GetByKeySimple(String Code)
		{
			SubApplication SubApplication = DataAccess.SelectByKey(Code);

            if (AllowCache)
            {
                this.Cache.Add(SubApplication.Clone<SubApplication>(true));
            }

            return SubApplication;
		}

		public SubApplication GetByKey(String Code)
		{
			SubApplication SubApplication = this.GetByKeySimple(Code);
            this.OnAfterGet(ref SubApplication);

            return SubApplication;
		}

		public OperationResult<SubApplication> EditByKey(SubApplication SubApplication, String Code)
		{
			this.OnBeforeEdit(ref SubApplication);
            SubApplication.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(SubApplication, Code);

			this.OnAfterEdit(ref SubApplication);
            return new OperationResult<SubApplication>(isEdit, SubApplication);
		}

		public OperationResult RemoveByKey(String Code)
		{
			bool isRemove = DataAccess.DeleteByKey(Code);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}