using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class LoginProviderRules : AuthRulesBase<Byte, LoginProvider, LoginProviderCollection, ILoginProviderDataAccess>
    {
		#region Constructors

        public LoginProviderRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override ILoginProviderDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<ILoginProviderDataAccess>();
			}
		}

		private List<Action<LoginProvider>> _MasterPopulateActions;
        protected override List<Action<LoginProvider>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<LoginProvider>>();

					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<LoginProvider>> _DetailPopulateActions;
        protected override List<Action<LoginProvider>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<LoginProvider>>();

						//this._DetailPopulateActions.Add(this.PopulateLoginProviderOptionsCollection);
						//this._DetailPopulateActions.Add(this.PopulateLoginProviderOptionsCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<LoginProviderCollection>();
            }
        }

        protected override LoginProviderCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<LoginProviderCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateLoginProviderOptionsCollection(LoginProvider LoginProvider)
		{
			LoginProvider.LoginProviderOptionsCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().GetByLoginProvider(LoginProvider.LoginProviderID, LoginProvider);
		}

		private void PopulateLoginProviderOptionsCollectionCount(LoginProvider LoginProvider)
		{
			LoginProvider.LoginProviderOptionsCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().GetCountByLoginProvider(LoginProvider.LoginProviderID);
		}

		#endregion

		#region Public Methods
		
		public override OperationResult<LoginProvider> AddWithDetail(LoginProvider LoginProvider)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (LoginProvider.State == ObjectState.Added)
                {
                    OperationResult<LoginProvider> result = this.Add(LoginProvider);
                }
				
				foreach (LoginProviderOptions LoginProviderOptions in LoginProvider.LoginProviderOptionsCollection.Where(obj => obj.State == ObjectState.Added))
                {
					LoginProviderOptions.LoginProvider = new LoginProvider { LoginProviderID = LoginProvider.LoginProviderID };

                    OperationResult<LoginProviderOptions> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().AddWithDetail(LoginProviderOptions);
                }

                scope.Complete();
            }

            return new OperationResult<LoginProvider>(true, LoginProvider);
        }

        public override OperationResult<LoginProvider> EditWithDetail(LoginProvider LoginProvider)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (LoginProvider.State == ObjectState.Edited)
                {
                    OperationResult<LoginProvider> result = this.Edit(LoginProvider);
                }
                    
				foreach (LoginProviderOptions LoginProviderOptions in LoginProvider.LoginProviderOptionsCollection.Where(obj => obj.State != ObjectState.None))
                {
					LoginProviderOptions.LoginProvider = new LoginProvider { LoginProviderID = LoginProvider.LoginProviderID };

                    OperationResult<LoginProviderOptions> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().Save(LoginProviderOptions);
                }

                scope.Complete();
            }

            return new OperationResult<LoginProvider>(true, LoginProvider);
        }

		public override OperationResult<LoginProvider> RemoveWithDetail(LoginProvider LoginProvider)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(LoginProvider.LoginProviderID);

				scope.Complete();
			}

			return new OperationResult<LoginProvider>(true, LoginProvider);
        }

		public LoginProvider GetByKeySimple(String Code)
		{
			LoginProvider LoginProvider = DataAccess.SelectByKey(Code);

            if (AllowCache)
            {
                this.Cache.Add(LoginProvider.Clone<LoginProvider>(true));
            }

            return LoginProvider;
		}

		public LoginProvider GetByKey(String Code)
		{
			LoginProvider LoginProvider = this.GetByKeySimple(Code);
            this.OnAfterGet(ref LoginProvider);

            return LoginProvider;
		}

		public OperationResult<LoginProvider> EditByKey(LoginProvider LoginProvider, String Code)
		{
			this.OnBeforeEdit(ref LoginProvider);
            LoginProvider.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(LoginProvider, Code);

			this.OnAfterEdit(ref LoginProvider);
            return new OperationResult<LoginProvider>(isEdit, LoginProvider);
		}

		public OperationResult RemoveByKey(String Code)
		{
			bool isRemove = DataAccess.DeleteByKey(Code);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}