using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class LoginProviderScopeRules : AuthRulesBase<Int32, LoginProviderScope, LoginProviderScopeCollection, ILoginProviderScopeDataAccess>
    {
		#region Constructors

        public LoginProviderScopeRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override ILoginProviderScopeDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<ILoginProviderScopeDataAccess>();
			}
		}

		private List<Action<LoginProviderScope>> _MasterPopulateActions;
        protected override List<Action<LoginProviderScope>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<LoginProviderScope>>();

						this._MasterPopulateActions.Add(this.PopulateLoginProviderOptions);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<LoginProviderScope>> _DetailPopulateActions;
        protected override List<Action<LoginProviderScope>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<LoginProviderScope>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<LoginProviderScopeCollection>();
            }
        }

        protected override LoginProviderScopeCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<LoginProviderScopeCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateLoginProviderOptions(LoginProviderScope LoginProviderScope)
		{
			if (LoginProviderScope.LoginProviderOptions != null && LoginProviderScope.LoginProviderOptions.LoginProviderOptionsID != default(Int32))
			{
				LoginProviderScope.LoginProviderOptions = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().GetMaster(LoginProviderScope.LoginProviderOptions.LoginProviderOptionsID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual LoginProviderScopeCollection GetByLoginProviderOptionsSimple(Int32 LoginProviderOptionsID)
        {
			LoginProviderScopeCollection LoginProviderScopeCollection = DataAccess.SelectByLoginProviderOptions(LoginProviderOptionsID);
				
			if (AllowCache)
            {
				this.Cache.Append(LoginProviderScopeCollection.Clone(true));
            }
			
            return LoginProviderScopeCollection;
        }

		public virtual LoginProviderScopeCollection GetByLoginProviderOptions(Int32 LoginProviderOptionsID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			LoginProviderScopeCollection LoginProviderScopeCollection = this.GetByLoginProviderOptionsSimple(LoginProviderOptionsID);

			if (LoginProviderScopeCollection.Count != 0 && parentModel != null)
			{
				LoginProviderScopeCollection.ForEach(m => m.LoginProviderOptions = null);
			}

			this.OnAfterGetCollection(ref LoginProviderScopeCollection);
			return LoginProviderScopeCollection;
		}

		public virtual Int32 GetCountByLoginProviderOptions(Int32 LoginProviderOptionsID)
        {
			return DataAccess.SelectCountByLoginProviderOptions(LoginProviderOptionsID);
        }
		
		public override OperationResult<LoginProviderScope> AddWithDetail(LoginProviderScope LoginProviderScope)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (LoginProviderScope.LoginProviderOptions.State == ObjectState.Added)
                {
					OperationResult<LoginProviderOptions> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().AddWithDetail(LoginProviderScope.LoginProviderOptions);
				}

                if (LoginProviderScope.State == ObjectState.Added)
                {
                    OperationResult<LoginProviderScope> result = this.Add(LoginProviderScope);
                }

                scope.Complete();
            }

            return new OperationResult<LoginProviderScope>(true, LoginProviderScope);
        }

        public override OperationResult<LoginProviderScope> EditWithDetail(LoginProviderScope LoginProviderScope)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (LoginProviderScope.LoginProviderOptions.State != ObjectState.None)
                {
					OperationResult<LoginProviderOptions> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().Save(LoginProviderScope.LoginProviderOptions);
				}

                if (LoginProviderScope.State == ObjectState.Edited)
                {
                    OperationResult<LoginProviderScope> result = this.Edit(LoginProviderScope);
                }

                scope.Complete();
            }

            return new OperationResult<LoginProviderScope>(true, LoginProviderScope);
        }

		public override OperationResult<LoginProviderScope> RemoveWithDetail(LoginProviderScope LoginProviderScope)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(LoginProviderScope.LoginProviderScopeID);

				if (LoginProviderScope.LoginProviderOptions.State == ObjectState.Removed)
                {
					OperationResult result_LoginProviderOptions = AuthBusinessRulesFactory.Instance.GetBusinessRules<LoginProviderOptionsRules>().RemoveWithDetail(LoginProviderScope.LoginProviderOptions);
				}

				scope.Complete();
			}

			return new OperationResult<LoginProviderScope>(true, LoginProviderScope);
        }

		public virtual LoginProviderScope GetByKeySimple(Int32 LoginProviderOptionsID, String Scope)
		{
			LoginProviderScope LoginProviderScope = DataAccess.SelectByKey(LoginProviderOptionsID, Scope);

            if (AllowCache)
            {
                this.Cache.Add(LoginProviderScope.Clone<LoginProviderScope>(true));
            }

            return LoginProviderScope;
		}

		public virtual LoginProviderScope GetByKey(Int32 LoginProviderOptionsID, String Scope)
		{
			LoginProviderScope LoginProviderScope = this.GetByKeySimple(LoginProviderOptionsID, Scope);
            this.OnAfterGet(ref LoginProviderScope);

            return LoginProviderScope;
		}

		public virtual OperationResult<LoginProviderScope> EditByKey(LoginProviderScope LoginProviderScope, Int32 LoginProviderOptionsID, String Scope)
		{
			this.OnBeforeEdit(ref LoginProviderScope);
            LoginProviderScope.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(LoginProviderScope, LoginProviderOptionsID, Scope);

			this.OnAfterEdit(ref LoginProviderScope);
            return new OperationResult<LoginProviderScope>(isEdit, LoginProviderScope);
		}

		public virtual OperationResult RemoveByKey(Int32 LoginProviderOptionsID, String Scope)
		{
			bool isRemove = DataAccess.DeleteByKey(LoginProviderOptionsID, Scope);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}