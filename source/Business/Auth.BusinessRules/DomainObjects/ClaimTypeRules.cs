using System;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ClaimTypeRules
	{
		#region Overrided Methods

        protected override void ResetPopulateActions()
        {
            //Change Default PopulateActions
            //this.MasterPopulateActions.Clear();
			//this.DetailPopulateActions.Clear();
        }

        #endregion

		#region Overrided Properties

        //public override Func<ClaimType, dynamic> OrderByKey
        //{
        //    get
        //    {
        //        return model => model.ClaimTypeID;
        //    }
        //}

		//public override Func<ClaimType, dynamic> OrderByDescendingKey
        //{
        //    get
        //    {
        //        return model => model.ClaimTypeID;
        //    }
        //}

        #endregion

        #region Public Methods

        public virtual ClaimType GetByName(String Name)
        {
            return this.GetByFilterSingleRow(c => c.Name == Name);
        }

        public virtual ClaimType GetBySchema(String Schema)
        {
            return this.GetByFilterSingleRow(c => c.ClaimSchema == Schema);
        }

        #endregion
    }
}