using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ApplicationEndpointRules : AuthRulesBase<Int32, ApplicationEndpoint, ApplicationEndpointCollection, IApplicationEndpointDataAccess>
    {
		#region Constructors

        public ApplicationEndpointRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IApplicationEndpointDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IApplicationEndpointDataAccess>();
			}
		}

		private List<Action<ApplicationEndpoint>> _MasterPopulateActions;
        protected override List<Action<ApplicationEndpoint>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<ApplicationEndpoint>>();

						//this._MasterPopulateActions.Add(this.PopulateApplication);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<ApplicationEndpoint>> _DetailPopulateActions;
        protected override List<Action<ApplicationEndpoint>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<ApplicationEndpoint>>();

						//this._DetailPopulateActions.Add(this.PopulateApplicableOperationCollection);
						//this._DetailPopulateActions.Add(this.PopulateApplicableOperationCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ApplicationEndpointCollection>();
            }
        }

        protected override ApplicationEndpointCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ApplicationEndpointCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplication(ApplicationEndpoint ApplicationEndpoint)
		{
			if (ApplicationEndpoint.Application != null && ApplicationEndpoint.Application.ApplicationID != default(Int32))
			{
				ApplicationEndpoint.Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().GetMaster(ApplicationEndpoint.Application.ApplicationID);
			}
		}
		
		private void PopulateApplicableOperationCollection(ApplicationEndpoint ApplicationEndpoint)
		{
			ApplicationEndpoint.ApplicableOperationCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().GetByApplicationEndpoint(ApplicationEndpoint.ApplicationEndpointID, ApplicationEndpoint);
		}

		private void PopulateApplicableOperationCollectionCount(ApplicationEndpoint ApplicationEndpoint)
		{
			ApplicationEndpoint.ApplicableOperationCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().GetCountByApplicationEndpoint(ApplicationEndpoint.ApplicationEndpointID);
		}

		#endregion

		#region Public Methods
		
		public virtual ApplicationEndpointCollection GetByApplicationSimple(Int32 ApplicationID)
        {
			ApplicationEndpointCollection ApplicationEndpointCollection = DataAccess.SelectByApplication(ApplicationID);
				
			if (AllowCache)
            {
				this.Cache.Append(ApplicationEndpointCollection.Clone(true));
            }
			
            return ApplicationEndpointCollection;
        }

		public virtual ApplicationEndpointCollection GetByApplication(Int32 ApplicationID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			ApplicationEndpointCollection ApplicationEndpointCollection = this.GetByApplicationSimple(ApplicationID);

			if (ApplicationEndpointCollection.Count != 0 && parentModel != null)
			{
				ApplicationEndpointCollection.ForEach(m => m.Application = null);
			}

			this.OnAfterGetCollection(ref ApplicationEndpointCollection);
			return ApplicationEndpointCollection;
		}

		public virtual Int32 GetCountByApplication(Int32 ApplicationID)
        {
			return DataAccess.SelectCountByApplication(ApplicationID);
        }
		
		public override OperationResult<ApplicationEndpoint> AddWithDetail(ApplicationEndpoint ApplicationEndpoint)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ApplicationEndpoint.Application.State == ObjectState.Added)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().AddWithDetail(ApplicationEndpoint.Application);
				}

                if (ApplicationEndpoint.State == ObjectState.Added)
                {
                    OperationResult<ApplicationEndpoint> result = this.Add(ApplicationEndpoint);
                }
				
				foreach (ApplicableOperation ApplicableOperation in ApplicationEndpoint.ApplicableOperationCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ApplicableOperation.ApplicationEndpoint = new ApplicationEndpoint { ApplicationEndpointID = ApplicationEndpoint.ApplicationEndpointID };

                    OperationResult<ApplicableOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().AddWithDetail(ApplicableOperation);
                }

                scope.Complete();
            }

            return new OperationResult<ApplicationEndpoint>(true, ApplicationEndpoint);
        }

        public override OperationResult<ApplicationEndpoint> EditWithDetail(ApplicationEndpoint ApplicationEndpoint)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (ApplicationEndpoint.Application.State != ObjectState.None)
                {
					OperationResult<Application> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().Save(ApplicationEndpoint.Application);
				}

                if (ApplicationEndpoint.State == ObjectState.Edited)
                {
                    OperationResult<ApplicationEndpoint> result = this.Edit(ApplicationEndpoint);
                }
                    
				foreach (ApplicableOperation ApplicableOperation in ApplicationEndpoint.ApplicableOperationCollection.Where(obj => obj.State != ObjectState.None))
                {
					ApplicableOperation.ApplicationEndpoint = new ApplicationEndpoint { ApplicationEndpointID = ApplicationEndpoint.ApplicationEndpointID };

                    OperationResult<ApplicableOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().Save(ApplicableOperation);
                }

                scope.Complete();
            }

            return new OperationResult<ApplicationEndpoint>(true, ApplicationEndpoint);
        }

		public override OperationResult<ApplicationEndpoint> RemoveWithDetail(ApplicationEndpoint ApplicationEndpoint)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(ApplicationEndpoint.ApplicationEndpointID);

				if (ApplicationEndpoint.Application.State == ObjectState.Removed)
                {
					OperationResult result_Application = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationRules>().RemoveWithDetail(ApplicationEndpoint.Application);
				}

				scope.Complete();
			}

			return new OperationResult<ApplicationEndpoint>(true, ApplicationEndpoint);
        }

		public ApplicationEndpoint GetByKeySimple(Int32 ApplicationID, String EndpointType)
		{
			ApplicationEndpoint ApplicationEndpoint = DataAccess.SelectByKey(ApplicationID, EndpointType);

            if (AllowCache)
            {
                this.Cache.Add(ApplicationEndpoint.Clone<ApplicationEndpoint>(true));
            }

            return ApplicationEndpoint;
		}

		public ApplicationEndpoint GetByKey(Int32 ApplicationID, String EndpointType)
		{
			ApplicationEndpoint ApplicationEndpoint = this.GetByKeySimple(ApplicationID, EndpointType);
            this.OnAfterGet(ref ApplicationEndpoint);

            return ApplicationEndpoint;
		}

		public OperationResult<ApplicationEndpoint> EditByKey(ApplicationEndpoint ApplicationEndpoint, Int32 ApplicationID, String EndpointType)
		{
			this.OnBeforeEdit(ref ApplicationEndpoint);
            ApplicationEndpoint.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(ApplicationEndpoint, ApplicationID, EndpointType);

			this.OnAfterEdit(ref ApplicationEndpoint);
            return new OperationResult<ApplicationEndpoint>(isEdit, ApplicationEndpoint);
		}

		public OperationResult RemoveByKey(Int32 ApplicationID, String EndpointType)
		{
			bool isRemove = DataAccess.DeleteByKey(ApplicationID, EndpointType);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}