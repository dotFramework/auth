using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Caching;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Model;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class DeviceRules : AuthRulesBase<Int32, Device, DeviceCollection, IDeviceDataAccess>
    {
		#region Constructors

        public DeviceRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IDeviceDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IDeviceDataAccess>();
			}
		}

		private List<Action<Device>> _MasterPopulateActions;
        protected override List<Action<Device>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<Device>>();

					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<Device>> _DetailPopulateActions;
        protected override List<Action<Device>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<Device>>();

						//this._DetailPopulateActions.Add(this.PopulateClientDeviceCollection);
						//this._DetailPopulateActions.Add(this.PopulateClientDeviceCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<DeviceCollection>();
            }
        }

        protected override DeviceCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<DeviceCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateClientDeviceCollection(Device Device)
		{
			Device.ClientDeviceCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientDeviceRules>().GetByDevice(Device.DeviceID, Device);
		}

		private void PopulateClientDeviceCollectionCount(Device Device)
		{
			Device.ClientDeviceCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientDeviceRules>().GetCountByDevice(Device.DeviceID);
		}

		#endregion

		#region Public Methods
		
		public override OperationResult<Device> AddWithDetail(Device Device)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (Device.State == ObjectState.Added)
                {
                    OperationResult<Device> result = this.Add(Device);
                }
				
				foreach (ClientDevice ClientDevice in Device.ClientDeviceCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ClientDevice.Device = new Device { DeviceID = Device.DeviceID };

                    OperationResult<ClientDevice> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientDeviceRules>().AddWithDetail(ClientDevice);
                }

                scope.Complete();
            }

            return new OperationResult<Device>(true, Device);
        }

        public override OperationResult<Device> EditWithDetail(Device Device)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (Device.State == ObjectState.Edited)
                {
                    OperationResult<Device> result = this.Edit(Device);
                }
                    
				foreach (ClientDevice ClientDevice in Device.ClientDeviceCollection.Where(obj => obj.State != ObjectState.None))
                {
					ClientDevice.Device = new Device { DeviceID = Device.DeviceID };

                    OperationResult<ClientDevice> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ClientDeviceRules>().Save(ClientDevice);
                }

                scope.Complete();
            }

            return new OperationResult<Device>(true, Device);
        }

		public override OperationResult<Device> RemoveWithDetail(Device Device)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(Device.DeviceID);

				scope.Complete();
			}

			return new OperationResult<Device>(true, Device);
        }

		public virtual Device GetByKeySimple(String DeviceIdentifier)
		{
			Device Device = DataAccess.SelectByKey(DeviceIdentifier);
			
			if (AllowCache)
            {
                this.Cache.Add(Device.Clone<Device>(true));
            }

            return Device;
		}

		public virtual Device GetByKey(String DeviceIdentifier)
		{
			Device Device = this.GetByKeySimple(DeviceIdentifier);
            this.OnAfterGet(ref Device);

            return Device;
		}

		public virtual OperationResult<Device> EditByKey(Device Device, String DeviceIdentifier)
		{
			this.OnBeforeEdit(ref Device);
            Device.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(Device, DeviceIdentifier);

			this.OnAfterEdit(ref Device);
            return new OperationResult<Device>(isEdit, Device);
		}

		public virtual OperationResult RemoveByKey(String DeviceIdentifier)
		{
			bool isRemove = DataAccess.DeleteByKey(DeviceIdentifier);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}