using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class AccountPolicyRules : AuthRulesBase<Byte, AccountPolicy, AccountPolicyCollection, IAccountPolicyDataAccess>
    {
		#region Constructors

        public AccountPolicyRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IAccountPolicyDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IAccountPolicyDataAccess>();
			}
		}

		private List<Action<AccountPolicy>> _MasterPopulateActions;
        protected override List<Action<AccountPolicy>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<AccountPolicy>>();

					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<AccountPolicy>> _DetailPopulateActions;
        protected override List<Action<AccountPolicy>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<AccountPolicy>>();

						//this._DetailPopulateActions.Add(this.PopulateUserCollection);
						//this._DetailPopulateActions.Add(this.PopulateUserCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<AccountPolicyCollection>();
            }
        }

        protected override AccountPolicyCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<AccountPolicyCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateUserCollection(AccountPolicy AccountPolicy)
		{
			AccountPolicy.UserCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetByAccountPolicy(AccountPolicy.AccountPolicyID, AccountPolicy);
		}

		private void PopulateUserCollectionCount(AccountPolicy AccountPolicy)
		{
			AccountPolicy.UserCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetCountByAccountPolicy(AccountPolicy.AccountPolicyID);
		}

		#endregion

		#region Public Methods
		
		public override OperationResult<AccountPolicy> AddWithDetail(AccountPolicy AccountPolicy)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (AccountPolicy.State == ObjectState.Added)
                {
                    OperationResult<AccountPolicy> result = this.Add(AccountPolicy);
                }
				
				foreach (User User in AccountPolicy.UserCollection.Where(obj => obj.State == ObjectState.Added))
                {
					User.AccountPolicy = new AccountPolicy { AccountPolicyID = AccountPolicy.AccountPolicyID };

                    OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().AddWithDetail(User);
                }

                scope.Complete();
            }

            return new OperationResult<AccountPolicy>(true, AccountPolicy);
        }

        public override OperationResult<AccountPolicy> EditWithDetail(AccountPolicy AccountPolicy)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (AccountPolicy.State == ObjectState.Edited)
                {
                    OperationResult<AccountPolicy> result = this.Edit(AccountPolicy);
                }
                    
				foreach (User User in AccountPolicy.UserCollection.Where(obj => obj.State != ObjectState.None))
                {
					User.AccountPolicy = new AccountPolicy { AccountPolicyID = AccountPolicy.AccountPolicyID };

                    OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().Save(User);
                }

                scope.Complete();
            }

            return new OperationResult<AccountPolicy>(true, AccountPolicy);
        }

		public override OperationResult<AccountPolicy> RemoveWithDetail(AccountPolicy AccountPolicy)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(AccountPolicy.AccountPolicyID);

				scope.Complete();
			}

			return new OperationResult<AccountPolicy>(true, AccountPolicy);
        }

		public virtual AccountPolicy GetByKeySimple(String PolicyName)
		{
			AccountPolicy AccountPolicy = DataAccess.SelectByKey(PolicyName);

            if (AllowCache)
            {
                this.Cache.Add(AccountPolicy.Clone<AccountPolicy>(true));
            }

            return AccountPolicy;
		}

		public virtual AccountPolicy GetByKey(String PolicyName)
		{
			AccountPolicy AccountPolicy = this.GetByKeySimple(PolicyName);
            this.OnAfterGet(ref AccountPolicy);

            return AccountPolicy;
		}

		public virtual OperationResult<AccountPolicy> EditByKey(AccountPolicy AccountPolicy, String PolicyName)
		{
			this.OnBeforeEdit(ref AccountPolicy);
            AccountPolicy.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(AccountPolicy, PolicyName);

			this.OnAfterEdit(ref AccountPolicy);
            return new OperationResult<AccountPolicy>(isEdit, AccountPolicy);
		}

		public virtual OperationResult RemoveByKey(String PolicyName)
		{
			bool isRemove = DataAccess.DeleteByKey(PolicyName);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}