using System;
using System.Linq;
using DotFramework.Auth.Model;
using System.Transactions;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class RefreshTokenRules
    {
        #region Overrided Methods

        protected override void ResetPopulateActions()
        {
            //Change Default PopulateActions
            //this.MasterPopulateActions.Clear();
            //this.DetailPopulateActions.Clear();
        }

        #endregion

        #region Overrided Properties

        //public override Func<RefreshToken, dynamic> OrderByKey
        //{
        //    get
        //    {
        //        return model => model.RefreshTokenID;
        //    }
        //}

        //public override Func<RefreshToken, dynamic> OrderByDescendingKey
        //{
        //    get
        //    {
        //        return model => model.RefreshTokenID;
        //    }
        //}

        #endregion

        #region Public Methods

        public virtual RefreshToken GetByToken(String Token)
        {
            return this.GetByFilterSingleRow(r=> r.Token == Token);
        }

        public virtual OperationResult<RefreshToken> RemoveAndAdd(RefreshToken RefreshToken)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                this.RemoveByUserAndGroupToken(RefreshToken.User.UserID, RefreshToken.GroupToken);
                this.Add(RefreshToken);

                scope.Complete();
            }

            return new OperationResult<RefreshToken>(true, RefreshToken);
        }

        public virtual RefreshTokenCollection GetByUserAndGroupTokenSimple(Int32 UserID, string GroupToken)
        {
            return DataAccess.SelectByUserAndGroupToken(UserID, GroupToken);
        }

        public virtual OperationResult RemoveByUserAndGroupToken(Int32 UserID, string GroupToken)
        {
            bool isRemove = DataAccess.DeleteByUserAndGroupToken(UserID, GroupToken);
            return new OperationResult(isRemove);
        }

        public virtual OperationResult RemoveByGroupToken(string GroupToken)
        {
            bool isRemove = DataAccess.DeleteByGroupToken(GroupToken);
            return new OperationResult(isRemove);
        }

        public virtual OperationResult<RefreshToken> AddByUserAndGroupToken(RefreshToken model)
        {
            bool isAdded = DataAccess.InsertByUserAndGroupToken(model);
            model.State = ObjectState.None;

            return new OperationResult<RefreshToken>(isAdded, model);
        }

        #endregion
    }
}