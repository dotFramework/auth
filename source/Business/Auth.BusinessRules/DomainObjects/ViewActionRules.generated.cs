using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ViewActionRules : AuthRulesBase<Int32, ViewAction, ViewActionCollection, IViewActionDataAccess>
    {
		#region Constructors

        public ViewActionRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IViewActionDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IViewActionDataAccess>();
			}
		}

		private List<Action<ViewAction>> _MasterPopulateActions;
        protected override List<Action<ViewAction>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<ViewAction>>();

					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<ViewAction>> _DetailPopulateActions;
        protected override List<Action<ViewAction>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<ViewAction>>();

						//this._DetailPopulateActions.Add(this.PopulateApplicableActionCollection);
						//this._DetailPopulateActions.Add(this.PopulateApplicableActionCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ViewActionCollection>();
            }
        }

        protected override ViewActionCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ViewActionCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplicableActionCollection(ViewAction ViewAction)
		{
			ViewAction.ApplicableActionCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().GetByViewAction(ViewAction.ViewActionID, ViewAction);
		}

		private void PopulateApplicableActionCollectionCount(ViewAction ViewAction)
		{
			ViewAction.ApplicableActionCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().GetCountByViewAction(ViewAction.ViewActionID);
		}

		#endregion

		#region Public Methods
		
		public override OperationResult<ViewAction> AddWithDetail(ViewAction ViewAction)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (ViewAction.State == ObjectState.Added)
                {
                    OperationResult<ViewAction> result = this.Add(ViewAction);
                }
				
				foreach (ApplicableAction ApplicableAction in ViewAction.ApplicableActionCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ApplicableAction.ViewAction = new ViewAction { ViewActionID = ViewAction.ViewActionID };

                    OperationResult<ApplicableAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().AddWithDetail(ApplicableAction);
                }

                scope.Complete();
            }

            return new OperationResult<ViewAction>(true, ViewAction);
        }

        public override OperationResult<ViewAction> EditWithDetail(ViewAction ViewAction)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (ViewAction.State == ObjectState.Edited)
                {
                    OperationResult<ViewAction> result = this.Edit(ViewAction);
                }
                    
				foreach (ApplicableAction ApplicableAction in ViewAction.ApplicableActionCollection.Where(obj => obj.State != ObjectState.None))
                {
					ApplicableAction.ViewAction = new ViewAction { ViewActionID = ViewAction.ViewActionID };

                    OperationResult<ApplicableAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().Save(ApplicableAction);
                }

                scope.Complete();
            }

            return new OperationResult<ViewAction>(true, ViewAction);
        }

		public override OperationResult<ViewAction> RemoveWithDetail(ViewAction ViewAction)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(ViewAction.ViewActionID);

				scope.Complete();
			}

			return new OperationResult<ViewAction>(true, ViewAction);
        }

		public ViewAction GetByKeySimple(String Code)
		{
			ViewAction ViewAction = DataAccess.SelectByKey(Code);

            if (AllowCache)
            {
                this.Cache.Add(ViewAction.Clone<ViewAction>(true));
            }

            return ViewAction;
		}

		public ViewAction GetByKey(String Code)
		{
			ViewAction ViewAction = this.GetByKeySimple(Code);
            this.OnAfterGet(ref ViewAction);

            return ViewAction;
		}

		public OperationResult<ViewAction> EditByKey(ViewAction ViewAction, String Code)
		{
			this.OnBeforeEdit(ref ViewAction);
            ViewAction.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(ViewAction, Code);

			this.OnAfterEdit(ref ViewAction);
            return new OperationResult<ViewAction>(isEdit, ViewAction);
		}

		public OperationResult RemoveByKey(String Code)
		{
			bool isRemove = DataAccess.DeleteByKey(Code);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}