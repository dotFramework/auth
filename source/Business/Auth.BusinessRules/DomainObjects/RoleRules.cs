using System;
using DotFramework.Auth.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class RoleRules
	{
		#region Overrided Methods

        protected override void ResetPopulateActions()
        {
            //Change Default PopulateActions
            //this.MasterPopulateActions.Clear();
			//this.DetailPopulateActions.Clear();
        }

        #endregion

		#region Overrided Properties

        //public override Func<Role, dynamic> OrderByKey
        //{
        //    get
        //    {
        //        return model => model.RoleID;
        //    }
        //}

		//public override Func<Role, dynamic> OrderByDescendingKey
        //{
        //    get
        //    {
        //        return model => model.RoleID;
        //    }
        //}

        #endregion

        #region Public Methods

        public virtual Role GetByName(String RoleName)
        {
            return GetByFilterSingleRow(r=> r.Name == RoleName);
        }

        #endregion
    }
}