using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ServiceOperationRules : AuthRulesBase<Int32, ServiceOperation, ServiceOperationCollection, IServiceOperationDataAccess>
    {
		#region Constructors

        public ServiceOperationRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IServiceOperationDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IServiceOperationDataAccess>();
			}
		}

		private List<Action<ServiceOperation>> _MasterPopulateActions;
        protected override List<Action<ServiceOperation>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<ServiceOperation>>();

					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<ServiceOperation>> _DetailPopulateActions;
        protected override List<Action<ServiceOperation>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<ServiceOperation>>();

						//this._DetailPopulateActions.Add(this.PopulateApplicableOperationCollection);
						//this._DetailPopulateActions.Add(this.PopulateApplicableOperationCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ServiceOperationCollection>();
            }
        }

        protected override ServiceOperationCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ServiceOperationCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplicableOperationCollection(ServiceOperation ServiceOperation)
		{
			ServiceOperation.ApplicableOperationCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().GetByServiceOperation(ServiceOperation.ServiceOperationID, ServiceOperation);
		}

		private void PopulateApplicableOperationCollectionCount(ServiceOperation ServiceOperation)
		{
			ServiceOperation.ApplicableOperationCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().GetCountByServiceOperation(ServiceOperation.ServiceOperationID);
		}

		#endregion

		#region Public Methods
		
		public override OperationResult<ServiceOperation> AddWithDetail(ServiceOperation ServiceOperation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (ServiceOperation.State == ObjectState.Added)
                {
                    OperationResult<ServiceOperation> result = this.Add(ServiceOperation);
                }
				
				foreach (ApplicableOperation ApplicableOperation in ServiceOperation.ApplicableOperationCollection.Where(obj => obj.State == ObjectState.Added))
                {
					ApplicableOperation.ServiceOperation = new ServiceOperation { ServiceOperationID = ServiceOperation.ServiceOperationID };

                    OperationResult<ApplicableOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().AddWithDetail(ApplicableOperation);
                }

                scope.Complete();
            }

            return new OperationResult<ServiceOperation>(true, ServiceOperation);
        }

        public override OperationResult<ServiceOperation> EditWithDetail(ServiceOperation ServiceOperation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (ServiceOperation.State == ObjectState.Edited)
                {
                    OperationResult<ServiceOperation> result = this.Edit(ServiceOperation);
                }
                    
				foreach (ApplicableOperation ApplicableOperation in ServiceOperation.ApplicableOperationCollection.Where(obj => obj.State != ObjectState.None))
                {
					ApplicableOperation.ServiceOperation = new ServiceOperation { ServiceOperationID = ServiceOperation.ServiceOperationID };

                    OperationResult<ApplicableOperation> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableOperationRules>().Save(ApplicableOperation);
                }

                scope.Complete();
            }

            return new OperationResult<ServiceOperation>(true, ServiceOperation);
        }

		public override OperationResult<ServiceOperation> RemoveWithDetail(ServiceOperation ServiceOperation)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(ServiceOperation.ServiceOperationID);

				scope.Complete();
			}

			return new OperationResult<ServiceOperation>(true, ServiceOperation);
        }

		public ServiceOperation GetByKeySimple(String Code)
		{
			ServiceOperation ServiceOperation = DataAccess.SelectByKey(Code);

            if (AllowCache)
            {
                this.Cache.Add(ServiceOperation.Clone<ServiceOperation>(true));
            }

            return ServiceOperation;
		}

		public ServiceOperation GetByKey(String Code)
		{
			ServiceOperation ServiceOperation = this.GetByKeySimple(Code);
            this.OnAfterGet(ref ServiceOperation);

            return ServiceOperation;
		}

		public OperationResult<ServiceOperation> EditByKey(ServiceOperation ServiceOperation, String Code)
		{
			this.OnBeforeEdit(ref ServiceOperation);
            ServiceOperation.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(ServiceOperation, Code);

			this.OnAfterEdit(ref ServiceOperation);
            return new OperationResult<ServiceOperation>(isEdit, ServiceOperation);
		}

		public OperationResult RemoveByKey(String Code)
		{
			bool isRemove = DataAccess.DeleteByKey(Code);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}