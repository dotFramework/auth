using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class RoleActionRules : AuthRulesBase<Int32, RoleAction, RoleActionCollection, IRoleActionDataAccess>
    {
		#region Constructors

        public RoleActionRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IRoleActionDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IRoleActionDataAccess>();
			}
		}

		private List<Action<RoleAction>> _MasterPopulateActions;
        protected override List<Action<RoleAction>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<RoleAction>>();

						this._MasterPopulateActions.Add(this.PopulateRole);
						this._MasterPopulateActions.Add(this.PopulateApplicableAction);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<RoleAction>> _DetailPopulateActions;
        protected override List<Action<RoleAction>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<RoleAction>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<RoleActionCollection>();
            }
        }

        protected override RoleActionCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<RoleActionCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateRole(RoleAction RoleAction)
		{
			if (RoleAction.Role != null && RoleAction.Role.RoleID != default(Int32))
			{
				RoleAction.Role = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().GetMaster(RoleAction.Role.RoleID);
			}
		}
		
		private void PopulateApplicableAction(RoleAction RoleAction)
		{
			if (RoleAction.ApplicableAction != null && RoleAction.ApplicableAction.ApplicableActionID != default(Int32))
			{
				RoleAction.ApplicableAction = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().GetMaster(RoleAction.ApplicableAction.ApplicableActionID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual RoleActionCollection GetByRoleSimple(Int32 RoleID)
        {
			RoleActionCollection RoleActionCollection = DataAccess.SelectByRole(RoleID);
				
			if (AllowCache)
            {
				this.Cache.Append(RoleActionCollection.Clone(true));
            }
			
            return RoleActionCollection;
        }

		public virtual RoleActionCollection GetByRole(Int32 RoleID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			RoleActionCollection RoleActionCollection = this.GetByRoleSimple(RoleID);

			if (RoleActionCollection.Count != 0 && parentModel != null)
			{
				RoleActionCollection.ForEach(m => m.Role = null);
			}

			this.OnAfterGetCollection(ref RoleActionCollection);
			return RoleActionCollection;
		}

		public virtual Int32 GetCountByRole(Int32 RoleID)
        {
			return DataAccess.SelectCountByRole(RoleID);
        }
		
		public virtual RoleActionCollection GetByApplicableActionSimple(Int32 ApplicableActionID)
        {
			RoleActionCollection RoleActionCollection = DataAccess.SelectByApplicableAction(ApplicableActionID);
				
			if (AllowCache)
            {
				this.Cache.Append(RoleActionCollection.Clone(true));
            }
			
            return RoleActionCollection;
        }

		public virtual RoleActionCollection GetByApplicableAction(Int32 ApplicableActionID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			RoleActionCollection RoleActionCollection = this.GetByApplicableActionSimple(ApplicableActionID);

			if (RoleActionCollection.Count != 0 && parentModel != null)
			{
				RoleActionCollection.ForEach(m => m.ApplicableAction = null);
			}

			this.OnAfterGetCollection(ref RoleActionCollection);
			return RoleActionCollection;
		}

		public virtual Int32 GetCountByApplicableAction(Int32 ApplicableActionID)
        {
			return DataAccess.SelectCountByApplicableAction(ApplicableActionID);
        }
		
		public override OperationResult<RoleAction> AddWithDetail(RoleAction RoleAction)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (RoleAction.Role.State == ObjectState.Added)
                {
					OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().AddWithDetail(RoleAction.Role);
				}

				if (RoleAction.ApplicableAction.State == ObjectState.Added)
                {
					OperationResult<ApplicableAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().AddWithDetail(RoleAction.ApplicableAction);
				}

                if (RoleAction.State == ObjectState.Added)
                {
                    OperationResult<RoleAction> result = this.Add(RoleAction);
                }

                scope.Complete();
            }

            return new OperationResult<RoleAction>(true, RoleAction);
        }

        public override OperationResult<RoleAction> EditWithDetail(RoleAction RoleAction)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (RoleAction.Role.State != ObjectState.None)
                {
					OperationResult<Role> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().Save(RoleAction.Role);
				}

				if (RoleAction.ApplicableAction.State != ObjectState.None)
                {
					OperationResult<ApplicableAction> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().Save(RoleAction.ApplicableAction);
				}

                if (RoleAction.State == ObjectState.Edited)
                {
                    OperationResult<RoleAction> result = this.Edit(RoleAction);
                }

                scope.Complete();
            }

            return new OperationResult<RoleAction>(true, RoleAction);
        }

		public override OperationResult<RoleAction> RemoveWithDetail(RoleAction RoleAction)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(RoleAction.RoleActionID);

				if (RoleAction.Role.State == ObjectState.Removed)
                {
					OperationResult result_Role = AuthBusinessRulesFactory.Instance.GetBusinessRules<RoleRules>().RemoveWithDetail(RoleAction.Role);
				}

				if (RoleAction.ApplicableAction.State == ObjectState.Removed)
                {
					OperationResult result_ApplicableAction = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicableActionRules>().RemoveWithDetail(RoleAction.ApplicableAction);
				}

				scope.Complete();
			}

			return new OperationResult<RoleAction>(true, RoleAction);
        }

		public RoleAction GetByKeySimple(Int32 RoleID, Int32 ApplicableActionID)
		{
			RoleAction RoleAction = DataAccess.SelectByKey(RoleID, ApplicableActionID);

            if (AllowCache)
            {
                this.Cache.Add(RoleAction.Clone<RoleAction>(true));
            }

            return RoleAction;
		}

		public RoleAction GetByKey(Int32 RoleID, Int32 ApplicableActionID)
		{
			RoleAction RoleAction = this.GetByKeySimple(RoleID, ApplicableActionID);
            this.OnAfterGet(ref RoleAction);

            return RoleAction;
		}

		public OperationResult<RoleAction> EditByKey(RoleAction RoleAction, Int32 RoleID, Int32 ApplicableActionID)
		{
			this.OnBeforeEdit(ref RoleAction);
            RoleAction.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(RoleAction, RoleID, ApplicableActionID);

			this.OnAfterEdit(ref RoleAction);
            return new OperationResult<RoleAction>(isEdit, RoleAction);
		}

		public OperationResult RemoveByKey(Int32 RoleID, Int32 ApplicableActionID)
		{
			bool isRemove = DataAccess.DeleteByKey(RoleID, ApplicableActionID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}