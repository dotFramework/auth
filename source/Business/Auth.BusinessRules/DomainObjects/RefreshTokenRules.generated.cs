using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class RefreshTokenRules : AuthRulesBase<Int64, RefreshToken, RefreshTokenCollection, IRefreshTokenDataAccess>
    {
		#region Constructors

        public RefreshTokenRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IRefreshTokenDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IRefreshTokenDataAccess>();
			}
		}

		private List<Action<RefreshToken>> _MasterPopulateActions;
        protected override List<Action<RefreshToken>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<RefreshToken>>();

						//this._MasterPopulateActions.Add(this.PopulateUser);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<RefreshToken>> _DetailPopulateActions;
        protected override List<Action<RefreshToken>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<RefreshToken>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<RefreshTokenCollection>();
            }
        }

        protected override RefreshTokenCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<RefreshTokenCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateUser(RefreshToken RefreshToken)
		{
			if (RefreshToken.User != null && RefreshToken.User.UserID != default(Int32))
			{
				RefreshToken.User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetMaster(RefreshToken.User.UserID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual RefreshTokenCollection GetByUserSimple(Int32 UserID)
        {
			RefreshTokenCollection RefreshTokenCollection = DataAccess.SelectByUser(UserID);
				
			if (AllowCache)
            {
				this.Cache.Append(RefreshTokenCollection.Clone(true));
            }
			
            return RefreshTokenCollection;
        }

		public virtual RefreshTokenCollection GetByUser(Int32 UserID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			RefreshTokenCollection RefreshTokenCollection = this.GetByUserSimple(UserID);

			if (RefreshTokenCollection.Count != 0 && parentModel != null)
			{
				RefreshTokenCollection.ForEach(m => m.User = null);
			}

			this.OnAfterGetCollection(ref RefreshTokenCollection);
			return RefreshTokenCollection;
		}

		public virtual Int32 GetCountByUser(Int32 UserID)
        {
			return DataAccess.SelectCountByUser(UserID);
        }
		
		public override OperationResult<RefreshToken> AddWithDetail(RefreshToken RefreshToken)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (RefreshToken.User.State == ObjectState.Added)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().AddWithDetail(RefreshToken.User);
				}

                if (RefreshToken.State == ObjectState.Added)
                {
                    OperationResult<RefreshToken> result = this.Add(RefreshToken);
                }

                scope.Complete();
            }

            return new OperationResult<RefreshToken>(true, RefreshToken);
        }

        public override OperationResult<RefreshToken> EditWithDetail(RefreshToken RefreshToken)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (RefreshToken.User.State != ObjectState.None)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().Save(RefreshToken.User);
				}

                if (RefreshToken.State == ObjectState.Edited)
                {
                    OperationResult<RefreshToken> result = this.Edit(RefreshToken);
                }

                scope.Complete();
            }

            return new OperationResult<RefreshToken>(true, RefreshToken);
        }

		public override OperationResult<RefreshToken> RemoveWithDetail(RefreshToken RefreshToken)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(RefreshToken.RefreshTokenID);

				if (RefreshToken.User.State == ObjectState.Removed)
                {
					OperationResult result_User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().RemoveWithDetail(RefreshToken.User);
				}

				scope.Complete();
			}

			return new OperationResult<RefreshToken>(true, RefreshToken);
        }

		public RefreshToken GetByKeySimple(Int32 UserID)
		{
			RefreshToken RefreshToken = DataAccess.SelectByKey(UserID);

            if (AllowCache)
            {
                this.Cache.Add(RefreshToken.Clone<RefreshToken>(true));
            }

            return RefreshToken;
		}

		public RefreshToken GetByKey(Int32 UserID)
		{
			RefreshToken RefreshToken = this.GetByKeySimple(UserID);
            this.OnAfterGet(ref RefreshToken);

            return RefreshToken;
		}

		public OperationResult<RefreshToken> EditByKey(RefreshToken RefreshToken, Int32 UserID)
		{
			this.OnBeforeEdit(ref RefreshToken);
            RefreshToken.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(RefreshToken, UserID);

			this.OnAfterEdit(ref RefreshToken);
            return new OperationResult<RefreshToken>(isEdit, RefreshToken);
		}

		public OperationResult RemoveByKey(Int32 UserID)
		{
			bool isRemove = DataAccess.DeleteByKey(UserID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}