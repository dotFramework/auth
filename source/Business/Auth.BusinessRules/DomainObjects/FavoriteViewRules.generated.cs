using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class FavoriteViewRules : AuthRulesBase<Int32, FavoriteView, FavoriteViewCollection, IFavoriteViewDataAccess>
    {
		#region Constructors

        public FavoriteViewRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IFavoriteViewDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IFavoriteViewDataAccess>();
			}
		}

		private List<Action<FavoriteView>> _MasterPopulateActions;
        protected override List<Action<FavoriteView>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<FavoriteView>>();

						//this._MasterPopulateActions.Add(this.PopulateApplicationHierarchy);
						//this._MasterPopulateActions.Add(this.PopulateUser);
					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<FavoriteView>> _DetailPopulateActions;
        protected override List<Action<FavoriteView>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<FavoriteView>>();

					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<FavoriteViewCollection>();
            }
        }

        protected override FavoriteViewCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<FavoriteViewCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateApplicationHierarchy(FavoriteView FavoriteView)
		{
			if (FavoriteView.ApplicationHierarchy != null && FavoriteView.ApplicationHierarchy.ApplicationHierarchyID != default(Int32))
			{
				FavoriteView.ApplicationHierarchy = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().GetMaster(FavoriteView.ApplicationHierarchy.ApplicationHierarchyID);
			}
		}
		
		private void PopulateUser(FavoriteView FavoriteView)
		{
			if (FavoriteView.User != null && FavoriteView.User.UserID != default(Int32))
			{
				FavoriteView.User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().GetMaster(FavoriteView.User.UserID);
			}
		}

		#endregion

		#region Public Methods
		
		public virtual FavoriteViewCollection GetByApplicationHierarchySimple(Int32 ApplicationHierarchyID)
        {
			FavoriteViewCollection FavoriteViewCollection = DataAccess.SelectByApplicationHierarchy(ApplicationHierarchyID);
				
			if (AllowCache)
            {
				this.Cache.Append(FavoriteViewCollection.Clone(true));
            }
			
            return FavoriteViewCollection;
        }

		public virtual FavoriteViewCollection GetByApplicationHierarchy(Int32 ApplicationHierarchyID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			FavoriteViewCollection FavoriteViewCollection = this.GetByApplicationHierarchySimple(ApplicationHierarchyID);

			if (FavoriteViewCollection.Count != 0 && parentModel != null)
			{
				FavoriteViewCollection.ForEach(m => m.ApplicationHierarchy = null);
			}

			this.OnAfterGetCollection(ref FavoriteViewCollection);
			return FavoriteViewCollection;
		}

		public virtual Int32 GetCountByApplicationHierarchy(Int32 ApplicationHierarchyID)
        {
			return DataAccess.SelectCountByApplicationHierarchy(ApplicationHierarchyID);
        }
		
		public virtual FavoriteViewCollection GetByUserSimple(Int32 UserID)
        {
			FavoriteViewCollection FavoriteViewCollection = DataAccess.SelectByUser(UserID);
				
			if (AllowCache)
            {
				this.Cache.Append(FavoriteViewCollection.Clone(true));
            }
			
            return FavoriteViewCollection;
        }

		public virtual FavoriteViewCollection GetByUser(Int32 UserID, DomainModelBase parentModel = null)
        {
			this.OnBeforeGetCollection();
			FavoriteViewCollection FavoriteViewCollection = this.GetByUserSimple(UserID);

			if (FavoriteViewCollection.Count != 0 && parentModel != null)
			{
				FavoriteViewCollection.ForEach(m => m.User = null);
			}

			this.OnAfterGetCollection(ref FavoriteViewCollection);
			return FavoriteViewCollection;
		}

		public virtual Int32 GetCountByUser(Int32 UserID)
        {
			return DataAccess.SelectCountByUser(UserID);
        }
		
		public override OperationResult<FavoriteView> AddWithDetail(FavoriteView FavoriteView)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (FavoriteView.ApplicationHierarchy.State == ObjectState.Added)
                {
					OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().AddWithDetail(FavoriteView.ApplicationHierarchy);
				}

				if (FavoriteView.User.State == ObjectState.Added)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().AddWithDetail(FavoriteView.User);
				}

                if (FavoriteView.State == ObjectState.Added)
                {
                    OperationResult<FavoriteView> result = this.Add(FavoriteView);
                }

                scope.Complete();
            }

            return new OperationResult<FavoriteView>(true, FavoriteView);
        }

        public override OperationResult<FavoriteView> EditWithDetail(FavoriteView FavoriteView)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				if (FavoriteView.ApplicationHierarchy.State != ObjectState.None)
                {
					OperationResult<ApplicationHierarchy> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().Save(FavoriteView.ApplicationHierarchy);
				}

				if (FavoriteView.User.State != ObjectState.None)
                {
					OperationResult<User> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().Save(FavoriteView.User);
				}

                if (FavoriteView.State == ObjectState.Edited)
                {
                    OperationResult<FavoriteView> result = this.Edit(FavoriteView);
                }

                scope.Complete();
            }

            return new OperationResult<FavoriteView>(true, FavoriteView);
        }

		public override OperationResult<FavoriteView> RemoveWithDetail(FavoriteView FavoriteView)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(FavoriteView.FavoriteViewID);

				if (FavoriteView.ApplicationHierarchy.State == ObjectState.Removed)
                {
					OperationResult result_ApplicationHierarchy = AuthBusinessRulesFactory.Instance.GetBusinessRules<ApplicationHierarchyRules>().RemoveWithDetail(FavoriteView.ApplicationHierarchy);
				}

				if (FavoriteView.User.State == ObjectState.Removed)
                {
					OperationResult result_User = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserRules>().RemoveWithDetail(FavoriteView.User);
				}

				scope.Complete();
			}

			return new OperationResult<FavoriteView>(true, FavoriteView);
        }

		public FavoriteView GetByKeySimple(Int32 ApplicationHierarchyID, Int32 UserID)
		{
			FavoriteView FavoriteView = DataAccess.SelectByKey(ApplicationHierarchyID, UserID);

            if (AllowCache)
            {
                this.Cache.Add(FavoriteView.Clone<FavoriteView>(true));
            }

            return FavoriteView;
		}

		public FavoriteView GetByKey(Int32 ApplicationHierarchyID, Int32 UserID)
		{
			FavoriteView FavoriteView = this.GetByKeySimple(ApplicationHierarchyID, UserID);
            this.OnAfterGet(ref FavoriteView);

            return FavoriteView;
		}

		public OperationResult<FavoriteView> EditByKey(FavoriteView FavoriteView, Int32 ApplicationHierarchyID, Int32 UserID)
		{
			this.OnBeforeEdit(ref FavoriteView);
            FavoriteView.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(FavoriteView, ApplicationHierarchyID, UserID);

			this.OnAfterEdit(ref FavoriteView);
            return new OperationResult<FavoriteView>(isEdit, FavoriteView);
		}

		public OperationResult RemoveByKey(Int32 ApplicationHierarchyID, Int32 UserID)
		{
			bool isRemove = DataAccess.DeleteByKey(ApplicationHierarchyID, UserID);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}