using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using DotFramework.Infra.BusinessRules;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Auth.BusinessRules.Base;
using DotFramework.Auth.Model;
using DotFramework.Auth.DataAccessFactory;
using DotFramework.Auth.Caching;
using DotFramework.Infra.Model;

namespace DotFramework.Auth.BusinessRules
{
    public partial class ClaimTypeRules : AuthRulesBase<Int16, ClaimType, ClaimTypeCollection, IClaimTypeDataAccess>
    {
		#region Constructors

        public ClaimTypeRules()
            : base()
        {
			
        }

		#endregion

		#region Properties

		protected override IClaimTypeDataAccess DataAccess
		{
			get
			{
				return AuthDataAccessFactory.Instance.GetDataAccess<IClaimTypeDataAccess>();
			}
		}

		private List<Action<ClaimType>> _MasterPopulateActions;
        protected override List<Action<ClaimType>> MasterPopulateActions
		{ 
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<ClaimType>>();

					}
				}

				return this._MasterPopulateActions;
			}
		}
		
		private List<Action<ClaimType>> _DetailPopulateActions;
        protected override List<Action<ClaimType>> DetailPopulateActions
		{ 
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<ClaimType>>();

						//this._DetailPopulateActions.Add(this.PopulateUserClaimCollection);
						//this._DetailPopulateActions.Add(this.PopulateUserClaimCollectionCount);
					}
				}

				return this._DetailPopulateActions;
			}
		}

		#region Cache

        protected override bool AllowCache
        {
            get
            {
                return AuthCache.Instance.IsAllowedCache<ClaimTypeCollection>();
            }
        }

        protected override ClaimTypeCollection Cache
        {
            get
            {
                return AuthCache.Instance.GetCache<ClaimTypeCollection>();
            }
        }

        #endregion

        #endregion

		#region Private Methods
		
		private void PopulateUserClaimCollection(ClaimType ClaimType)
		{
			ClaimType.UserClaimCollection = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserClaimRules>().GetByClaimType(ClaimType.ClaimTypeID, ClaimType);
		}

		private void PopulateUserClaimCollectionCount(ClaimType ClaimType)
		{
			ClaimType.UserClaimCollection.TotalCount = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserClaimRules>().GetCountByClaimType(ClaimType.ClaimTypeID);
		}

		#endregion

		#region Public Methods
		
		public override OperationResult<ClaimType> AddWithDetail(ClaimType ClaimType)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (ClaimType.State == ObjectState.Added)
                {
                    OperationResult<ClaimType> result = this.Add(ClaimType);
                }
				
				foreach (UserClaim UserClaim in ClaimType.UserClaimCollection.Where(obj => obj.State == ObjectState.Added))
                {
					UserClaim.ClaimType = new ClaimType { ClaimTypeID = ClaimType.ClaimTypeID };

                    OperationResult<UserClaim> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserClaimRules>().AddWithDetail(UserClaim);
                }

                scope.Complete();
            }

            return new OperationResult<ClaimType>(true, ClaimType);
        }

        public override OperationResult<ClaimType> EditWithDetail(ClaimType ClaimType)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                if (ClaimType.State == ObjectState.Edited)
                {
                    OperationResult<ClaimType> result = this.Edit(ClaimType);
                }
                    
				foreach (UserClaim UserClaim in ClaimType.UserClaimCollection.Where(obj => obj.State != ObjectState.None))
                {
					UserClaim.ClaimType = new ClaimType { ClaimTypeID = ClaimType.ClaimTypeID };

                    OperationResult<UserClaim> result = AuthBusinessRulesFactory.Instance.GetBusinessRules<UserClaimRules>().Save(UserClaim);
                }

                scope.Complete();
            }

            return new OperationResult<ClaimType>(true, ClaimType);
        }

		public override OperationResult<ClaimType> RemoveWithDetail(ClaimType ClaimType)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
				OperationResult result = this.Remove(ClaimType.ClaimTypeID);

				scope.Complete();
			}

			return new OperationResult<ClaimType>(true, ClaimType);
        }

		public ClaimType GetByKeySimple(String Code)
		{
			ClaimType ClaimType = DataAccess.SelectByKey(Code);

            if (AllowCache)
            {
                this.Cache.Add(ClaimType.Clone<ClaimType>(true));
            }

            return ClaimType;
		}

		public ClaimType GetByKey(String Code)
		{
			ClaimType ClaimType = this.GetByKeySimple(Code);
            this.OnAfterGet(ref ClaimType);

            return ClaimType;
		}

		public OperationResult<ClaimType> EditByKey(ClaimType ClaimType, String Code)
		{
			this.OnBeforeEdit(ref ClaimType);
            ClaimType.State = ObjectState.None;

            bool isEdit = DataAccess.UpdateByKey(ClaimType, Code);

			this.OnAfterEdit(ref ClaimType);
            return new OperationResult<ClaimType>(isEdit, ClaimType);
		}

		public OperationResult RemoveByKey(String Code)
		{
			bool isRemove = DataAccess.DeleteByKey(Code);
            return new OperationResult(isRemove);
		}

		#endregion
    }
}