using DotFramework.Auth.Authentication.Models;
using DotFramework.Infra;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Dynamic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Auth.Authentication.Facebook
{
    public class FacebookExternalAuthenticationHandler : ExternalAuthenticationHandler<CustomFacebookAuthenticationOptions>
    {
        #region Constructor

        public FacebookExternalAuthenticationHandler(IOwinContext context, CustomFacebookAuthenticationOptions options, HttpClient httpClient, string appToken) : base(context, options)
        {
            this._HttpClient = httpClient;
            this._AppToken = appToken;
        }

        #endregion

        #region Private Members

        private readonly HttpClient _HttpClient;

        #endregion

        #region Protected Member

        protected readonly string _AppToken;

        #endregion

        #region Overrided Members

        public override string Provider
        {
            get
            {
                return "Facebook";
            }
        }

        #endregion

        #region Overrided Methods

        public override async Task<ExternalAuthenticatedContext> AuthenticateAsync(string accessToken)
        {
            var parsedToken = await VerifyToken(accessToken);

            string fields = String.Join(",", Options.ScopeDetail);

            if (String.IsNullOrEmpty(fields))
            {
                fields = "id,name";
            }

            string userInfoUrl = String.Format("{0}?access_token={1}&fields={2}", Options.UserInformationEndpoint,
                                                                                  Uri.EscapeDataString(accessToken),
                                                                                  Uri.EscapeDataString(fields));

            if (Options.SendAppSecretProof)
            {
                userInfoUrl = String.Format("{0}&appsecret_proof={1}", userInfoUrl, this.GenerateAppSecretProof(accessToken, Options.AppSecret));
            }

            HttpResponseMessage httpResponseMessage1 = await this._HttpClient.GetAsync(userInfoUrl, Request.CallCancelled);

            if (httpResponseMessage1.StatusCode != HttpStatusCode.OK)
            {
                if (httpResponseMessage1.StatusCode == HttpStatusCode.BadRequest)
                {
                    string errorResponse = await httpResponseMessage1.Content.ReadAsStringAsync();
                    JObject errorObject = JObject.Parse(errorResponse);
                    throw new ApiCustomException(errorObject["error"]["message"].ToString());
                }
                else
                {
                    httpResponseMessage1.EnsureSuccessStatusCode();
                }
            }

            string response = await httpResponseMessage1.Content.ReadAsStringAsync();

            JObject jObject = JObject.Parse(response);

            FacebookExternalAuthenticatedContext context = new FacebookExternalAuthenticatedContext(accessToken, jObject, parsedToken.ExpiresAt, Options);
            return context;
        }

        public override async Task<ParsedExternalAccessToken> VerifyToken(string accessToken)
        {
            try
            {
                string tokenEndpoint = String.Format("{0}?input_token={1}&access_token={2}", CustomFacebookAuthenticationOptions.DebugTokenEndPoint,
                                                                                             Uri.EscapeDataString(accessToken),
                                                                                             Uri.EscapeDataString(_AppToken));

                HttpResponseMessage httpResponseMessage = await this._HttpClient.GetAsync(tokenEndpoint, Request.CallCancelled);

                if (httpResponseMessage.StatusCode != HttpStatusCode.OK)
                {
                    if (httpResponseMessage.StatusCode == HttpStatusCode.BadRequest)
                    {
                        string errorResponse = await httpResponseMessage.Content.ReadAsStringAsync();
                        JObject errorObject = JObject.Parse(errorResponse);
                        throw new ApiCustomException(errorObject["error"]["message"].ToString());
                    }
                    else
                    {
                        httpResponseMessage.EnsureSuccessStatusCode();
                    }
                }

                var content = await httpResponseMessage.Content.ReadAsStringAsync();
                dynamic jObj = JsonConvert.DeserializeObject<ExpandoObject>(content);

                dynamic error;

                try
                {
                    error = jObj.data.error;
                }
                catch (RuntimeBinderException)
                {
                    error = null;
                }

                if (error != null)
                {
                    throw new ApiCustomException(error.message);
                }

                ParsedExternalAccessToken parsedToken = new ParsedExternalAccessToken
                {
                    UserID = jObj.data.user_id,
                    ProviderKey = jObj.data.app_id,
                    ExpiresAt = UnixTimeStampToDateTime(jObj.data.expires_at)
                };

                if (!string.Equals(this.Options.AppId, parsedToken.ProviderKey, StringComparison.OrdinalIgnoreCase))
                {
                    throw new ApiCustomException("The token is incorrect.");
                }

                return parsedToken;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private string GenerateAppSecretProof(string accessToken, string appSecret)
        {
            using (HMACSHA256 hMACSHA256 = new HMACSHA256(Encoding.ASCII.GetBytes(appSecret)))
            {
                byte[] numArray = hMACSHA256.ComputeHash(Encoding.ASCII.GetBytes(accessToken));
                StringBuilder stringBuilder = new StringBuilder();

                for (int i = 0; i < (int)numArray.Length; i++)
                {
                    stringBuilder.Append(numArray[i].ToString("x2", CultureInfo.InvariantCulture));
                }

                return stringBuilder.ToString();
            }
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        }

        #endregion
    }
}