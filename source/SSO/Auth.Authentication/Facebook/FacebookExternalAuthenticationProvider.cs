using Microsoft.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Security;

namespace DotFramework.Auth.Authentication.Facebook
{
    public class FacebookExternalAuthenticationProvider : ExternalAuthenticationProvider<CustomFacebookAuthenticationOptions>
    {
        #region Constructor

        public FacebookExternalAuthenticationProvider(CustomFacebookAuthenticationOptions options) : base(options)
        {
            this._HttpClient = new HttpClient(ResolveHttpMessageHandler(Options))
            {
                Timeout = Options.BackchannelTimeout,
                MaxResponseContentBufferSize = 10485760
            };

            this._AppToken = GetAppToken();
        }

        #endregion

        #region Private Members

        private readonly HttpClient _HttpClient;

        #endregion

        #region Protected Member

        protected readonly string _AppToken;

        #endregion

        #region Public Members



        #endregion

        #region Overrided Methods

        public override ExternalAuthenticationHandler<CustomFacebookAuthenticationOptions> CreateHandler(IOwinContext context)
        {
            return new FacebookExternalAuthenticationHandler(context, Options, _HttpClient, _AppToken);
        }

        #endregion

        #region Private Methods

        public string GetAppToken()
        {
            try
            {
                string url = String.Format("{0}?client_id={1}&client_secret={2}&grant_type=client_credentials", Options.TokenEndpoint,
                                                                                                                Uri.EscapeDataString(Options.AppId),
                                                                                                                Uri.EscapeDataString(Options.AppSecret));

                HttpResponseMessage httpResponseMessage = this._HttpClient.GetAsync(url).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                string response = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JObject responseObject = JObject.Parse(response);

                return TryGetValue(responseObject, "access_token");
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static HttpMessageHandler ResolveHttpMessageHandler(CustomFacebookAuthenticationOptions options)
        {
            HttpMessageHandler backchannelHttpHandler = options.BackchannelHttpHandler;

            if (backchannelHttpHandler == null)
            {
                backchannelHttpHandler = new WebRequestHandler();
            }

            HttpMessageHandler httpMessageHandler = backchannelHttpHandler;

            if (options.BackchannelCertificateValidator != null)
            {
                WebRequestHandler remoteCertificateValidationCallback = httpMessageHandler as WebRequestHandler;

                if (remoteCertificateValidationCallback == null)
                {
                    throw new InvalidOperationException("ValidatorHandlerMismatch");
                }

                ICertificateValidator backchannelCertificateValidator = options.BackchannelCertificateValidator;
                remoteCertificateValidationCallback.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(backchannelCertificateValidator.Validate);
            }
            return httpMessageHandler;
        }

        private static string TryGetValue(JObject obj, string propertyName)
        {
            JToken value;
            return obj.TryGetValue(propertyName, out value) ? value.ToString() : null;
        }

        #endregion
    }
}