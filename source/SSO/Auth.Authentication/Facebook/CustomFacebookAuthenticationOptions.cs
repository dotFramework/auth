using Microsoft.Owin.Security.Facebook;
using System;
using System.Collections.Generic;

namespace DotFramework.Auth.Authentication.Facebook
{
    public class CustomFacebookAuthenticationOptions : FacebookAuthenticationOptions, ICustomAuthenticationOptions
    {
        #region Constants

        public const string DebugTokenEndPoint = "https://graph.facebook.com/debug_token";

        #endregion

        public CustomFacebookAuthenticationOptions()
        {
            ScopeDetail = new List<String>();
        }

        public IList<String> ScopeDetail { get; private set; }
    }
}