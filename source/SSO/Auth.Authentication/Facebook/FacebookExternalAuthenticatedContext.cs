using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.Owin.Security;

namespace DotFramework.Auth.Authentication.Facebook
{
    public class FacebookExternalAuthenticatedContext : ExternalAuthenticatedContext
    {
        public FacebookExternalAuthenticatedContext(string accessToken, JObject user, DateTime expiresAt, AuthenticationOptions options) 
            : base(accessToken, user, expiresAt, options)
        {
            this.UserID = TryGetValue(user, "id");
            this.UserName = TryGetValue(user, "username");
            this.Email = TryGetValue(user, "email");
            this.Name = TryGetValue(user, "name");
            this.FirstName = TryGetValue(user, "first_name");
            this.LastName = TryGetValue(user, "last_name");
            this.Gender = TryGetValue(user, "gender");
            this.Link = TryGetValue(user, "link");

            if (user["birthday"] != null)
            {
                this.BirthDate = DateTime.ParseExact(user["birthday"].ToString(), "MM/dd/yyyy", null);
            }

            if (user["picture"] != null)
            {
                this.ProfilePicture = user["picture"]["data"]["url"].ToString();
            }

            if (user["friends"] != null)
            {
                this.Friends = new List<ExternalAuthenticationFriend>();

                JArray friends = user["friends"]["data"] as JArray;

                foreach (var friend in friends)
                {
                    ExternalAuthenticationFriend externalAuthenticationFriend = new ExternalAuthenticationFriend();

                    externalAuthenticationFriend.ID = friend["id"].ToString();

                    if (friend["name"] != null)
                    {
                        externalAuthenticationFriend.Name = friend["name"].ToString();
                    }

                    if (friend["link"] != null)
                    {
                        externalAuthenticationFriend.Link = friend["link"].ToString();
                    }

                    if (friend["picture"] != null)
                    {
                        externalAuthenticationFriend.ProfilePicture = friend["picture"]["data"]["url"].ToString();
                    }

                    this.Friends.Add(externalAuthenticationFriend);
                }
            }

            UpdateIdentity();
        }

        public override string Provider
        {
            get
            {
                return "Facebook";
            }
        }
    }
}