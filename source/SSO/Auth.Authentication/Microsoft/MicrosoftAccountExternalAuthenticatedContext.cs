using System;
using Newtonsoft.Json.Linq;
using Microsoft.Owin.Security;

namespace DotFramework.Auth.Authentication.Microsoft
{
    public class MicrosoftAccountExternalAuthenticatedContext : ExternalAuthenticatedContext
    {
        public MicrosoftAccountExternalAuthenticatedContext(string accessToken, JObject user, DateTime expiresAt, AuthenticationOptions options)
            : base(accessToken, user, expiresAt, options)
        {
            this.UserID = TryGetValue(user, "id");
            this.UserName = user["emails"]["preferred"].ToString();
            this.Email = this.UserName;
            this.Name = TryGetValue(user, "name");
            this.FirstName = TryGetValue(user, "first_name");
            this.LastName = TryGetValue(user, "last_name");
            this.Gender = TryGetValue(user, "gender");
            this.Link = TryGetValue(user, "link");

            string year = TryGetValue(user, "birth_year");
            string month = TryGetValue(user, "birth_month");
            string day = TryGetValue(user, "birth_day");

            if (!String.IsNullOrEmpty(year) &&
                !String.IsNullOrEmpty(month) &&
                !String.IsNullOrEmpty(day))
            {
                try
                {
                    this.BirthDate = new DateTime(int.Parse(year), int.Parse(month), int.Parse(day));
                }
                catch
                {

                }
            }

            this.ProfilePicture = String.Format("https://apis.live.net/v5.0/{0}/picture", this.UserID);

            UpdateIdentity();
        }

        public override string Provider
        {
            get
            {
                return "Microsoft";
            }
        }
    }
}