using Microsoft.Owin.Security.MicrosoftAccount;
using System;
using System.Collections.Generic;

namespace DotFramework.Auth.Authentication.Microsoft
{
    public class CustomMicrosoftAccountAuthenticationOptions : MicrosoftAccountAuthenticationOptions, ICustomAuthenticationOptions
    {
        #region Constants

        public const string TokenEndpoint = "https://login.live.com/oauth20_token.srf";
        public const string AuthorizeEndpoint = "https://login.live.com/oauth20_authorize.srf";
        public const string GraphApiEndpoint = "https://apis.live.net/v5.0/me";

        #endregion

        public CustomMicrosoftAccountAuthenticationOptions()
        {
            ScopeDetail = new List<String>();
        }

        public IList<String> ScopeDetail { get; private set; }
    }
}