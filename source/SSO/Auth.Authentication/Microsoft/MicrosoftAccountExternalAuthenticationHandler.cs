using DotFramework.Auth.Authentication.Models;
using DotFramework.Infra;
using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace DotFramework.Auth.Authentication.Microsoft
{
    public class MicrosoftAccountExternalAuthenticationHandler : ExternalAuthenticationHandler<CustomMicrosoftAccountAuthenticationOptions>
    {
        #region Constructor

        public MicrosoftAccountExternalAuthenticationHandler(IOwinContext context, CustomMicrosoftAccountAuthenticationOptions options, HttpClient httpClient) : base(context, options)
        {
            this._HttpClient = httpClient;
        }

        #endregion

        #region Private Members



        private readonly HttpClient _HttpClient;

        #endregion

        #region Protected Member



        #endregion

        #region Overrided Members

        public override string Provider
        {
            get
            {
                return "Microsoft";
            }
        }

        #endregion

        #region Overrided Methods

        public override async Task<ExternalAuthenticatedContext> AuthenticateAsync(string accessToken)
        {
            HttpResponseMessage graphResponse = await _HttpClient.GetAsync(CustomMicrosoftAccountAuthenticationOptions.GraphApiEndpoint + "?access_token=" + Uri.EscapeDataString(accessToken), Request.CallCancelled);
            graphResponse.EnsureSuccessStatusCode();

            if (graphResponse.StatusCode != HttpStatusCode.OK)
            {
                if (graphResponse.StatusCode == HttpStatusCode.BadRequest)
                {
                    string errorResponse = await graphResponse.Content.ReadAsStringAsync();
                    JObject errorObject = JObject.Parse(errorResponse);
                    throw new ApiCustomException(errorObject["error"]["message"].ToString());
                }
                else
                {
                    graphResponse.EnsureSuccessStatusCode();
                }
            }

            string response = await graphResponse.Content.ReadAsStringAsync();
            JObject jObject = JObject.Parse(response);

            ParsedExternalAccessToken parsedToken = new ParsedExternalAccessToken
            {
                ExpiresAt = DateTime.Now.AddHours(1)
            };

            MicrosoftAccountExternalAuthenticatedContext context = new MicrosoftAccountExternalAuthenticatedContext(accessToken, jObject, parsedToken.ExpiresAt, Options);
            return context;
        }

        public override async Task<ParsedExternalAccessToken> VerifyToken(string accessToken)
        {
            try
            {
                string tokenEndpoint = String.Format("{0}?access_token={1}", CustomMicrosoftAccountAuthenticationOptions.GraphApiEndpoint,
                                                                             Uri.EscapeDataString(accessToken));

                HttpResponseMessage httpResponseMessage = await this._HttpClient.GetAsync(tokenEndpoint, Request.CallCancelled);

                if (httpResponseMessage.StatusCode != HttpStatusCode.OK)
                {
                    if (httpResponseMessage.StatusCode == HttpStatusCode.BadRequest)
                    {
                        string errorResponse = await httpResponseMessage.Content.ReadAsStringAsync();
                        JObject errorObject = JObject.Parse(errorResponse);
                        throw new ApiCustomException(errorObject["error"]["message"].ToString());
                    }
                    else
                    {
                        httpResponseMessage.EnsureSuccessStatusCode();
                    }
                }

                var content = await httpResponseMessage.Content.ReadAsStringAsync();
                dynamic jObj = JsonConvert.DeserializeObject<ExpandoObject>(content);

                ParsedExternalAccessToken parsedToken = new ParsedExternalAccessToken
                {
                    UserID = jObj.id,
                    //ProviderKey = jObj.audience,
                    //ExpiresAt = DateTime.Now.AddSeconds(jObj.expires_in)
                    ProviderKey = this.Options.ClientId,
                    ExpiresAt = DateTime.Now.AddHours(1)
                };

                if (!string.Equals(this.Options.ClientId, parsedToken.ProviderKey, StringComparison.OrdinalIgnoreCase))
                {
                    throw new ApiCustomException("Invalid External Access Token");
                }

                return parsedToken;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}