using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Net.Http;
using System.Net.Security;

namespace DotFramework.Auth.Authentication.Google
{
    public class GoogleExternalAuthenticationProvider : ExternalAuthenticationProvider<CustomGoogleAuthenticationOptions>
    {
        #region Constructor

        public GoogleExternalAuthenticationProvider(CustomGoogleAuthenticationOptions options) : base(options)
        {
            this._HttpClient = new HttpClient(ResolveHttpMessageHandler(Options))
            {
                Timeout = Options.BackchannelTimeout,
                MaxResponseContentBufferSize = 10485760
            };
        }

        #endregion

        #region Private Members

        private readonly HttpClient _HttpClient;

        #endregion

        #region Protected Member

        

        #endregion

        #region Public Members



        #endregion

        #region Overrided Methods

        public override ExternalAuthenticationHandler<CustomGoogleAuthenticationOptions> CreateHandler(IOwinContext context)
        {
            return new GoogleExternalAuthenticationHandler(context, Options, _HttpClient);
        }

        #endregion

        #region Private Methods

        private static HttpMessageHandler ResolveHttpMessageHandler(CustomGoogleAuthenticationOptions options)
        {
            HttpMessageHandler backchannelHttpHandler = options.BackchannelHttpHandler;

            if (backchannelHttpHandler == null)
            {
                backchannelHttpHandler = new WebRequestHandler();
            }

            HttpMessageHandler httpMessageHandler = backchannelHttpHandler;

            if (options.BackchannelCertificateValidator != null)
            {
                WebRequestHandler remoteCertificateValidationCallback = httpMessageHandler as WebRequestHandler;

                if (remoteCertificateValidationCallback == null)
                {
                    throw new InvalidOperationException("ValidatorHandlerMismatch");
                }

                ICertificateValidator backchannelCertificateValidator = options.BackchannelCertificateValidator;
                remoteCertificateValidationCallback.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(backchannelCertificateValidator.Validate);
            }
            return httpMessageHandler;
        }

        #endregion
    }
}