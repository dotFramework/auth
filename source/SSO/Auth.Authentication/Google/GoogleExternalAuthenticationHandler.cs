using DotFramework.Auth.Authentication.Models;
using DotFramework.Infra;
using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DotFramework.Auth.Authentication.Google
{
    public class GoogleExternalAuthenticationHandler : ExternalAuthenticationHandler<CustomGoogleAuthenticationOptions>
    {
        #region Constructor

        public GoogleExternalAuthenticationHandler(IOwinContext context, CustomGoogleAuthenticationOptions options, HttpClient httpClient) : base(context, options)
        {
            this._HttpClient = httpClient;
        }

        #endregion

        #region Private Members



        private readonly HttpClient _HttpClient;

        #endregion

        #region Protected Member



        #endregion

        #region Overrided Members

        public override string Provider
        {
            get
            {
                return "Google";
            }
        }

        #endregion

        #region Overrided Methods

        public override async Task<ExternalAuthenticatedContext> AuthenticateAsync(string accessToken)
        {
            var parsedToken = await VerifyToken(accessToken);

            // Get the Google user
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, CustomGoogleAuthenticationOptions.UserInfoEndpoint);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            HttpResponseMessage graphResponse = await _HttpClient.SendAsync(request, Request.CallCancelled);

            if (graphResponse.StatusCode != HttpStatusCode.OK)
            {
                if (graphResponse.StatusCode == HttpStatusCode.BadRequest)
                {
                    string errorResponse = await graphResponse.Content.ReadAsStringAsync();
                    JObject errorObject = JObject.Parse(errorResponse);
                    throw new ApiCustomException(errorObject["error_description"].ToString());
                }
                else
                {
                    graphResponse.EnsureSuccessStatusCode();
                }
            }

            string response = await graphResponse.Content.ReadAsStringAsync();
            JObject jObject = JObject.Parse(response);

            GoogleExternalAuthenticatedContext context = new GoogleExternalAuthenticatedContext(accessToken, jObject, parsedToken.ExpiresAt, Options);
            return context;
        }

        public override async Task<ParsedExternalAccessToken> VerifyToken(string accessToken)
        {
            try
            {
                string tokenEndpoint = String.Format("{0}?access_token={1}", CustomGoogleAuthenticationOptions.TokenInfoEndPoint,
                                                                             Uri.EscapeDataString(accessToken));

                HttpResponseMessage httpResponseMessage = await this._HttpClient.GetAsync(tokenEndpoint, Request.CallCancelled);

                if (httpResponseMessage.StatusCode != HttpStatusCode.OK)
                {
                    if (httpResponseMessage.StatusCode == HttpStatusCode.BadRequest)
                    {
                        string errorResponse = await httpResponseMessage.Content.ReadAsStringAsync();
                        JObject errorObject = JObject.Parse(errorResponse);
                        throw new ApiCustomException(errorObject["error_description"].ToString());
                    }
                    else
                    {
                        httpResponseMessage.EnsureSuccessStatusCode();
                    }
                }

                var content = await httpResponseMessage.Content.ReadAsStringAsync();
                dynamic jObj = JsonConvert.DeserializeObject<ExpandoObject>(content);

                ParsedExternalAccessToken parsedToken = new ParsedExternalAccessToken
                {
                    UserID = jObj.user_id,
                    ProviderKey = jObj.audience,
                    ExpiresAt = DateTime.Now.AddSeconds(jObj.expires_in)
                };

                if (!string.Equals(this.Options.ClientId, parsedToken.ProviderKey, StringComparison.OrdinalIgnoreCase))
                {
                    throw new ApiCustomException("Invalid External Access Token");
                }

                return parsedToken;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}