using Microsoft.Owin.Security.Google;
using System;
using System.Collections.Generic;

namespace DotFramework.Auth.Authentication.Google
{
    public class CustomGoogleAuthenticationOptions : GoogleOAuth2AuthenticationOptions, ICustomAuthenticationOptions
    {
        #region Constants

        public const string TokenEndpoint = "https://accounts.google.com/o/oauth2/token";
        public const string UserInfoEndpoint = "https://www.googleapis.com/plus/v1/people/me";
        public const string AuthorizeEndpoint = "https://accounts.google.com/o/oauth2/auth";
        public const string TokenInfoEndPoint = "https://www.googleapis.com/oauth2/v1/tokeninfo";

        #endregion

        public CustomGoogleAuthenticationOptions()
        {
            ScopeDetail = new List<String>();
        }

        public IList<String> ScopeDetail { get; private set; }
    }
}