using System;
using Newtonsoft.Json.Linq;
using Microsoft.Owin.Security;

namespace DotFramework.Auth.Authentication.Google
{
    public class GoogleExternalAuthenticatedContext : ExternalAuthenticatedContext
    {
        public GoogleExternalAuthenticatedContext(string accessToken, JObject user, DateTime expiresAt, AuthenticationOptions options)
            : base(accessToken, user, expiresAt, options)
        {
            this.UserID = TryGetValue(user, "id");
            this.UserName = (user["emails"] as JArray)[0]["value"].ToString();
            this.Email = this.UserName;
            this.Name = TryGetValue(user, "displayName");
            this.FirstName = user["name"]["givenName"].ToString();
            this.LastName = user["name"]["familyName"].ToString();
            this.Gender = TryGetValue(user, "gender");
            this.Link = TryGetValue(user, "url");

            if (user["birthday"] != null)
            {
                try
                {
                    this.BirthDate = DateTime.ParseExact(user["birthday"].ToString(), "yyyy-MM-dd", null);
                }
                catch
                {
                    
                }
            }

            if (user["image"] != null)
            {
                this.ProfilePicture = user["image"]["url"].ToString();
            }

            UpdateIdentity();
        }

        public override string Provider
        {
            get
            {
                return "Google";
            }
        }
    }
}