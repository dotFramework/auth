using System;
using System.Collections.Generic;

namespace DotFramework.Auth.Authentication
{
    public interface ICustomAuthenticationOptions
    {
        IList<String> ScopeDetail { get; }
    }
}