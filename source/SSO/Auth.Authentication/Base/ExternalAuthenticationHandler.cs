using DotFramework.Auth.Authentication.Models;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;

namespace DotFramework.Auth.Authentication
{
    public abstract class ExternalAuthenticationHandler
    {
        public ExternalAuthenticationHandler(IOwinContext context, AuthenticationOptions options)
        {
            this.BaseOptions = options;
            this.Context = context;
        }

        public abstract string Provider { get; }

        protected IOwinContext Context { get; private set; }

        protected IOwinRequest Request
        {
            get
            {
                return this.Context.Request;
            }
        }

        protected IOwinResponse Response
        {
            get
            {
                return this.Context.Response;
            }
        }

        protected AuthenticationOptions BaseOptions { get; private set; }

        public abstract Task<ExternalAuthenticatedContext> AuthenticateAsync(string accessToken);

        public abstract Task<ParsedExternalAccessToken> VerifyToken(string accessToken);
    }

    public abstract class ExternalAuthenticationHandler<TAuthenticationOptions> : ExternalAuthenticationHandler where TAuthenticationOptions : AuthenticationOptions
    {
        public ExternalAuthenticationHandler(IOwinContext context, TAuthenticationOptions options) : base(context, options)
        {
            this.Options = options;
        }

        public TAuthenticationOptions Options { get; set; }
    }
}