using System;

namespace DotFramework.Auth.Authentication.Models
{
    public class ParsedExternalAccessToken
    {
        public string UserID { get; set; }
        public string ProviderKey { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}