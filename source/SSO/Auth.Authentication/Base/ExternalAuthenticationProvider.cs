using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace DotFramework.Auth.Authentication
{
    public abstract class ExternalAuthenticationProvider
    {
        #region Constructor

        public ExternalAuthenticationProvider(AuthenticationOptions options)
        {
            this.BaseOptions = options;
        }

        #endregion

        #region Protected Members

        protected AuthenticationOptions BaseOptions { get; private set; }

        #endregion
    }

    public abstract class ExternalAuthenticationProvider<TOptions> : ExternalAuthenticationProvider where TOptions : AuthenticationOptions
    {
        #region Constructor

        public ExternalAuthenticationProvider(TOptions options) : base(options)
        {
            this.Options = options;
        }

        #endregion

        #region Public Members

        public TOptions Options { get; set; }

        #endregion

        #region Public Methods



        #endregion

        #region Abstract Methods

        public abstract ExternalAuthenticationHandler<TOptions> CreateHandler(IOwinContext context);

        #endregion
    }
}