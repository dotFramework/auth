using DotFramework.Infra.Security;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace DotFramework.Auth.Authentication
{
    public abstract class ExternalAuthenticatedContext
    {
        public ExternalAuthenticatedContext(string accessToken, JObject user, DateTime expiresAt, AuthenticationOptions options)
        {
            this.AccessToken = accessToken;
            this.User = user;
            this.ExpiresAt = expiresAt;

            this.BaseOptions = options;

            Identity = new ClaimsIdentity(this.BaseOptions.AuthenticationType, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name",
                                                                               "http://schemas.microsoft.com/ws/2008/06/identity/claims/role");
        }

        public abstract string Provider { get; }

        public string AccessToken { get; private set; }

        public JObject User { get; private set; }

        public DateTime ExpiresAt { get; private set; }

        public ClaimsIdentity Identity { get; private set; }

        public AuthenticationOptions BaseOptions { get; private set; }

        public TimeSpan? Expires
        {
            get
            {
                if (this.ExpiresAt > DateTime.Now)
                {
                    return this.ExpiresAt - DateTime.Now;
                }
                else
                {
                    return null;
                }
            }
        }

        public Double? ExpiresIn
        {
            get
            {
                if (this.ExpiresAt > DateTime.Now)
                {
                    return (this.ExpiresAt - DateTime.Now).TotalSeconds;
                }
                else
                {
                    return null;
                }
            }
        }

        public string UserID { get; protected set; }

        public string UserName { get; protected set; }

        public string Email { get; protected set; }

        public string Name { get; protected set; }

        public string FirstName { get; protected set; }

        public string LastName { get; protected set; }

        public string Gender { get; protected set; }

        public string Link { get; protected set; }

        public string ProfilePicture { get; protected set; }

        public DateTime? BirthDate { get; set; }

        public List<ExternalAuthenticationFriend> Friends { get; protected set; }

        protected string TryGetValue(JObject user, string propertyName)
        {
            JToken jToken = null;

            if (!user.TryGetValue(propertyName, out jToken))
            {
                return null;
            }

            return jToken.ToString();
        }

        protected virtual void UpdateIdentity()
        {
            if (!String.IsNullOrEmpty(UserID))
            {
                Identity.AddClaim(new Claim(ClaimTypes.NameIdentifier,
                                            UserID,
                                            ClaimValueTypes.String,
                                            BaseOptions.AuthenticationType));
            }

            if (!String.IsNullOrEmpty(UserName))
            {
                Identity.AddClaim(new Claim(ClaimTypes.Name,
                                            UserName,
                                            ClaimValueTypes.String,
                                            BaseOptions.AuthenticationType));
            }

            if (!String.IsNullOrEmpty(Email))
            {
                Identity.AddClaim(new Claim(ClaimTypes.Email,
                                            Email,
                                            ClaimValueTypes.String,
                                            BaseOptions.AuthenticationType));
            }

            if (!String.IsNullOrEmpty(Name))
            {
                Identity.AddClaim(new Claim(CustomClaimTypes.DisplayName,
                                            Name,
                                            ClaimValueTypes.String,
                                            BaseOptions.AuthenticationType));
            }

            if (!String.IsNullOrEmpty(FirstName))
            {
                Identity.AddClaim(new Claim(ClaimTypes.GivenName,
                                            FirstName,
                                            ClaimValueTypes.String,
                                            BaseOptions.AuthenticationType));
            }

            if (!String.IsNullOrEmpty(LastName))
            {
                Identity.AddClaim(new Claim(ClaimTypes.Surname,
                                            LastName,
                                            ClaimValueTypes.String,
                                            BaseOptions.AuthenticationType));
            }

            if (!String.IsNullOrEmpty(Gender))
            {
                Identity.AddClaim(new Claim(ClaimTypes.Gender,
                                            Gender,
                                            ClaimValueTypes.String,
                                            BaseOptions.AuthenticationType));
            }

            if (!String.IsNullOrEmpty(ProfilePicture))
            {
                Identity.AddClaim(new Claim(CustomClaimTypes.ProfilePicture,
                                            ProfilePicture,
                                            ClaimValueTypes.String,
                                            BaseOptions.AuthenticationType));
            }

            foreach (JProperty property in User.Children())
            {
                var claimType = String.Format("urn:{0}:{1}", Provider, property.Name);
                string claimValue = property.Value.ToString();

                if (!Identity.HasClaim(claimType, claimValue))
                {
                    Identity.AddClaim(new Claim(claimType,
                                                        claimValue,
                                                        "http://www.w3.org/2001/XMLSchema#string",
                                                        BaseOptions.AuthenticationType));
                }
            }
        }
    }

    public class ExternalAuthenticationFriend
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string ProfilePicture { get; set; }
    }
}