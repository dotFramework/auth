﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Security.Principal;

namespace DotFramework.Auth.Authentication
{
    public class ClientClaimsIdentity : ClaimsIdentity
    {
        public ClientClaimsIdentity()
        {
        }

        public ClientClaimsIdentity(IIdentity identity) : base(identity)
        {
        }

        public ClientClaimsIdentity(IEnumerable<Claim> claims) : base(claims)
        {
        }

        public ClientClaimsIdentity(string authenticationType) : base(authenticationType)
        {
        }

        public ClientClaimsIdentity(BinaryReader reader) : base(reader)
        {
        }

        public ClientClaimsIdentity(IEnumerable<Claim> claims, string authenticationType) : base(claims, authenticationType)
        {
        }

        public ClientClaimsIdentity(IIdentity identity, IEnumerable<Claim> claims) : base(identity, claims)
        {
        }

        public ClientClaimsIdentity(string authenticationType, string nameType, string roleType) : base(authenticationType, nameType, roleType)
        {
        }

        public ClientClaimsIdentity(IEnumerable<Claim> claims, string authenticationType, string nameType, string roleType) : base(claims, authenticationType, nameType, roleType)
        {
        }

        public ClientClaimsIdentity(IIdentity identity, IEnumerable<Claim> claims, string authenticationType, string nameType, string roleType) : base(identity, claims, authenticationType, nameType, roleType)
        {
        }

        protected ClientClaimsIdentity(ClaimsIdentity other) : base(other)
        {
        }

        protected ClientClaimsIdentity(SerializationInfo info) : base(info)
        {
        }

        protected ClientClaimsIdentity(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
