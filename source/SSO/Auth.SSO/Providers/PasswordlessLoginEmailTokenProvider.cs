using DotFramework.Auth.Model;
using Microsoft.AspNet.Identity;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace DotFramework.Auth.SSO.Providers
{
    public class PasswordlessLoginEmailTokenProvider : EmailTokenProvider<User, Int32>
    {
        public const string Purpose = "PasswordlessLogin";

        public PasswordlessLoginEmailTokenProvider()
        {
            Subject = "Account Activation";
            BodyFormat = "Your activation code is: {0}";
        }

        public override async Task<String> GetUserModifierAsync(string purpose, UserManager<User, int> manager, User user)
        {
            var email = await manager.GetEmailAsync(user.Id);
            return "PasswordlessLogin:" + purpose + ":" + email;
        }

        public override Task NotifyAsync(string token, UserManager<User, Int32> manager, User user)
        {
            if (manager == null)
            {
                throw new ArgumentNullException("manager");
            }

            return manager.SendEmailAsync(user.Id, Subject, String.Format(CultureInfo.CurrentCulture, BodyFormat, token));
        }
    }
}