using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.SSO.Helpers;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DotFramework.Auth.Authentication;
using DotFramework.Infra;

namespace DotFramework.Auth.SSO.Providers
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider, IAuthenticationTokenRemoveProvider
    {
        #region Create

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            await Task.Run(() =>
            {
                if (!(context.Ticket.Identity is ClientClaimsIdentity))
                {
                    CreateToken(context);
                }
            });
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            CreateToken(context);
        }

        private void CreateToken(AuthenticationTokenCreateContext context)
        {
            var token = Guid.NewGuid().ToString("n");

            var refreshToken = new RefreshToken()
            {
                Token = HashHelper.GetHash(token),
                GroupToken = context.Request.Headers.Get("GroupToken") ?? Guid.NewGuid().ToString("n"),
                AppIdentifier = context.Request.Headers.Get("app_identifier") ?? context.OwinContext.Get<String>("app_identifier"),
                User = new User { UserID = context.Ticket.Identity.GetUserId<Int32>() },
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(Startup.RefreshTokenExpireTimeSpan)
            };

            context.Ticket.Properties.IssuedUtc = refreshToken.IssuedUtc;
            context.Ticket.Properties.ExpiresUtc = refreshToken.ExpiresUtc;

            refreshToken.ProtectedTicket = context.SerializeTicket();

            var result = AuthServiceFactory.Instance.GetService<IRefreshTokenService>().AddByUserAndGroupToken(refreshToken);

            if (result.Result)
            {
                context.SetToken(token);
            }
        }

        #endregion

        #region Receive

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            await Task.Run(() =>
            {
                ReceiveToken(context);
            });
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            ReceiveToken(context);
        }

        private void ReceiveToken(AuthenticationTokenReceiveContext context)
        {
            string hashedTokenId = HashHelper.GetHash(context.Token);

            using (var service = AuthServiceFactory.Instance.GetService<IRefreshTokenService>())
            {
                var refreshToken = service.GetByToken(hashedTokenId);

                if (refreshToken != null && refreshToken.HasValue)
                {
                    context.Request.Headers.Add("GroupToken", new String[] { refreshToken.GroupToken });

                    if (!String.IsNullOrEmpty(refreshToken.AppIdentifier))
                    {
                        context.Request.Headers.Add("app_identifier", new String[] { refreshToken.AppIdentifier });
                    }

                    context.DeserializeTicket(refreshToken.ProtectedTicket);
                    var result = service.Remove(refreshToken.RefreshTokenID);

                }
            }
        }

        #endregion

        #region Remove

        public void Remove(int userID, string tokenGroup)
        {
            AuthServiceFactory.Instance.GetService<IRefreshTokenService>().RemoveByUserAndGroupToken(userID, tokenGroup);
        }

        public void Remove(string tokenGroup)
        {
            AuthServiceFactory.Instance.GetService<IRefreshTokenService>().RemoveByGroupToken(tokenGroup);
        }

        public async Task RemoveAsync(int userID, string tokenGroup)
        {
            await Task.Run(() =>
            {
                AuthServiceFactory.Instance.GetService<IRefreshTokenService>().RemoveByUserAndGroupToken(userID, tokenGroup);
            });
        }

        public async Task RemoveAsync(string tokenGroup)
        {
            await Task.Run(() =>
            {
                AuthServiceFactory.Instance.GetService<IRefreshTokenService>().RemoveByGroupToken(tokenGroup);
            });
        }

        #endregion

        #region Helpers

        private Application ValidateApplication(string app_identifier)
        {
            Application application = null;

            if (!String.IsNullOrEmpty(app_identifier))
            {
                application = AuthServiceFactory.Instance.GetService<IApplicationService>().GetByKeySimple(app_identifier);

                if (!application.HasValue)
                {
                    throw new ApiCustomException("Invalid Application Identifier");
                }
            }

            return application;
        }

        #endregion
    }

    public interface IAuthenticationTokenRemoveProvider
    {
        void Remove(int userID, string tokenGroup);
        void Remove(string tokenGroup);
        Task RemoveAsync(int userID, string tokenGroup);
        Task RemoveAsync(string tokenGroup);
    }
}