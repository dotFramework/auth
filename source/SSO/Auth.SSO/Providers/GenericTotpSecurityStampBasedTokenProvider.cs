﻿using DotFramework.Auth.Model;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using DotFramework.Auth.Authentication.Identity;
using System.Security.Cryptography;
using DotFramework.Core;

namespace DotFramework.Auth.SSO.Providers
{
    public class GenericTotpSecurityStampBasedTokenProvider
    {
        public virtual string Generate(string purpose, string stamp, string identifier)
        {
            var token = new SecurityToken(Encoding.Unicode.GetBytes(stamp));
            var modifier = GetModifier(purpose, identifier);
            return Rfc6238AuthenticationService.GenerateCode(token, modifier).ToString("D6", CultureInfo.InvariantCulture);
        }

        public virtual string GetModifier(string purpose, string identifier)
        {
            return "Totp:" + purpose + ":" + identifier;
        }

        public virtual bool ValidateEncryptedString(string original, string encrypted, string stamp, string identifier)
        {
            return encrypted == EncryptUtility.GetHash(original, this.Generate(TotpTokenPurpose.Encryption, stamp, identifier));
        }
    }

    public static class TotpTokenPurpose
    {
        public const string Encryption = "client_secret_encryption";
    }
}
