using DotFramework.Auth.Authentication.Facebook;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DotFramework.Auth.SSO.Providers
{
    public class FacebookAuthProvider : FacebookAuthenticationProvider
    {
        public override void ApplyRedirect(FacebookApplyRedirectContext context)
        {
            string client_id = context.Request.Query["client_id"];
            var provider = ExternalAuthenticationManager.Instance.GetProvider<CustomFacebookAuthenticationOptions>(client_id);

            context.Options.AppId = provider.Options.AppId;
            context.Options.AppSecret = provider.Options.AppSecret;

            context.Options.Scope.Clear();

            foreach (var sc in provider.Options.Scope)
            {
                context.Options.Scope.Add(sc);
            }

            string scope = string.Join(",", context.Options.Scope);

            AuthenticationResponseChallenge challenge = new SecurityHelper(context.OwinContext).LookupChallenge(context.Options.AuthenticationType, context.Options.AuthenticationMode);

            string baseUri =
                    context.Request.Scheme +
                    Uri.SchemeDelimiter +
                    context.Request.Host +
                    context.Request.PathBase;

            string currentUri =
                baseUri +
                context.Request.Path +
                context.Request.QueryString;

            string redirectUri =
                baseUri +
                context.Options.CallbackPath;

            AuthenticationProperties properties = challenge.Properties;
            if (string.IsNullOrEmpty(properties.RedirectUri))
            {
                properties.RedirectUri = currentUri;
            }

            string state = context.Options.StateDataFormat.Protect(properties);

            string authorizationEndpoint = context.Options.AuthorizationEndpoint +
                                                "?response_type=code" +
                                                "&client_id=" + Uri.EscapeDataString(context.Options.AppId) +
                                                "&redirect_uri=" + Uri.EscapeDataString(redirectUri) +
                                                "&scope=" + Uri.EscapeDataString(scope) +
                                                "&state=" + Uri.EscapeDataString(state);

            var redirectContext = new FacebookApplyRedirectContext
            (
                context.OwinContext,
                context.Options,
                properties,
                authorizationEndpoint
            );

            base.ApplyRedirect(redirectContext);
        }

        public override Task Authenticated(FacebookAuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            return Task.FromResult<object>(null);
        }
    }
}