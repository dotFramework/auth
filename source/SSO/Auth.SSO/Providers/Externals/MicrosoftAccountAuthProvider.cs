using Microsoft.Owin.Security.MicrosoftAccount;
using System;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using DotFramework.Auth.Authentication.Microsoft;

namespace DotFramework.Auth.SSO.Providers
{
    public class MicrosoftAccountAuthProvider : MicrosoftAccountAuthenticationProvider
    {
        public override void ApplyRedirect(MicrosoftAccountApplyRedirectContext context)
        {
            string client_id = context.Request.Query["client_id"];
            var provider = ExternalAuthenticationManager.Instance.GetProvider<CustomMicrosoftAccountAuthenticationOptions>(client_id);

            context.Options.ClientId = provider.Options.ClientId;
            context.Options.ClientSecret = provider.Options.ClientSecret;

            context.Options.Scope.Clear();

            foreach (var sc in provider.Options.Scope)
            {
                context.Options.Scope.Add(sc);
            }

            string scope = string.Join(" ", context.Options.Scope);

            if (String.IsNullOrEmpty(scope))
            {
                // LiveID requires a scope string, so if the user didn't set one we go for the least possible.
                scope = "wl.basic";
            }

            AuthenticationResponseChallenge challenge = new SecurityHelper(context.OwinContext).LookupChallenge(context.Options.AuthenticationType, context.Options.AuthenticationMode);

            string baseUri =
                    context.Request.Scheme +
                    Uri.SchemeDelimiter +
                    context.Request.Host +
                    context.Request.PathBase;

            string currentUri =
                baseUri +
                context.Request.Path +
                context.Request.QueryString;

            string redirectUri =
                baseUri +
                context.Options.CallbackPath;

            AuthenticationProperties properties = challenge.Properties;
            if (string.IsNullOrEmpty(properties.RedirectUri))
            {
                properties.RedirectUri = currentUri;
            }

            string state = context.Options.StateDataFormat.Protect(properties);

            string authorizationEndpoint =
                        CustomMicrosoftAccountAuthenticationOptions.AuthorizeEndpoint +
                        "?client_id=" + Uri.EscapeDataString(context.Options.ClientId) +
                        "&scope=" + Uri.EscapeDataString(scope) +
                        "&response_type=code" +
                        "&redirect_uri=" + Uri.EscapeDataString(redirectUri) +
                        "&state=" + Uri.EscapeDataString(state);

            var redirectContext = new MicrosoftAccountApplyRedirectContext
            (
                context.OwinContext, 
                context.Options,
                properties, 
                authorizationEndpoint
            );

            base.ApplyRedirect(redirectContext);
        }

        public override Task Authenticated(MicrosoftAccountAuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            return Task.FromResult<object>(null);
        }
    }
}