using DotFramework.Auth.Authentication.Google;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DotFramework.Auth.SSO.Providers
{
    public class GoogleAuthProvider : GoogleOAuth2AuthenticationProvider
    {
        public override void ApplyRedirect(GoogleOAuth2ApplyRedirectContext context)
        {
            string client_id = context.Request.Query["client_id"];
            var provider = ExternalAuthenticationManager.Instance.GetProvider<CustomGoogleAuthenticationOptions>(client_id);

            context.Options.ClientId = provider.Options.ClientId;
            context.Options.ClientSecret = provider.Options.ClientSecret;

            context.Options.Scope.Clear();

            foreach (var sc in provider.Options.Scope)
            {
                context.Options.Scope.Add(sc);
            }

            string scope = string.Join(" ", context.Options.Scope);

            if (String.IsNullOrEmpty(scope))
            {
                // Google OAuth 2.0 asks for non-empty scope. If user didn't set it, set default scope to 
                // "openid profile email" to get basic user information.
                scope = "openid profile email";
            }

            AuthenticationResponseChallenge challenge = new SecurityHelper(context.OwinContext).LookupChallenge(context.Options.AuthenticationType, context.Options.AuthenticationMode);

            string baseUri =
                    context.Request.Scheme +
                    Uri.SchemeDelimiter +
                    context.Request.Host +
                    context.Request.PathBase;

            string currentUri =
                baseUri +
                context.Request.Path +
                context.Request.QueryString;

            string redirectUri =
                baseUri +
                context.Options.CallbackPath;

            AuthenticationProperties properties = challenge.Properties;
            if (string.IsNullOrEmpty(properties.RedirectUri))
            {
                properties.RedirectUri = currentUri;
            }

            string state = context.Options.StateDataFormat.Protect(properties);

            var queryStrings = new Dictionary<String, String>(StringComparer.OrdinalIgnoreCase);

            queryStrings.Add("response_type", "code");
            queryStrings.Add("client_id", context.Options.ClientId);
            queryStrings.Add("redirect_uri", redirectUri);

            AddQueryString(queryStrings, properties, "scope", scope);
            AddQueryString(queryStrings, properties, "access_type", context.Options.AccessType);
            AddQueryString(queryStrings, properties, "approval_prompt");
            AddQueryString(queryStrings, properties, "login_hint");
            queryStrings.Add("state", state);

            string authorizationEndpoint = WebUtilities.AddQueryString(CustomGoogleAuthenticationOptions.AuthorizeEndpoint, queryStrings);

            var redirectContext = new GoogleOAuth2ApplyRedirectContext
            (
                context.OwinContext,
                context.Options,
                properties,
                authorizationEndpoint
            );

            base.ApplyRedirect(redirectContext);
        }

        public override Task Authenticated(GoogleOAuth2AuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            return Task.FromResult<object>(null);
        }

        #region Private Methods

        private static void AddQueryString(IDictionary<string, string> queryStrings, AuthenticationProperties properties, string name, string defaultValue = null)
        {
            string value;
            if (!properties.Dictionary.TryGetValue(name, out value))
            {
                value = defaultValue;
            }
            else
            {
                // Remove the parameter from AuthenticationProperties so it won't be serialized to state parameter
                properties.Dictionary.Remove(name);
            }

            if (value == null)
            {
                return;
            }

            queryStrings[name] = value;
        }

        #endregion
    }
}