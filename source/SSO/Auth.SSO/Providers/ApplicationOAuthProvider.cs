using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using DotFramework.Auth.Model;
using DotFramework.Auth.SSO.Managers;
using Newtonsoft.Json;
using DotFramework.Auth.Authentication;
using DotFramework.Infra.Security;
using DotFramework.Infra;
using DotFramework.Auth.ServiceFactory;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using Microsoft.Owin;
using DotFramework.Core.Configuration;
using DotFramework.Auth.SSO.Services;
using DotFramework.Infra.ExceptionHandling;

namespace DotFramework.Auth.SSO.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private readonly bool _requireConfirmedEmail;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
            _requireConfirmedEmail = AppSettingsManager.Instance.Get<Boolean>("RequireConfirmedEmail", false);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

                User user = await userManager.FindAsync(context.UserName, context.Password);

                if (user == null || !user.HasValue)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }

                if (_requireConfirmedEmail && !user.EmailConfirmed)
                {
                    context.SetError("unverified_email", "The email is not verified.");
                    return;
                }

                try
                {
                    ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, OAuthDefaults.AuthenticationType);
                    ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager, CookieAuthenticationDefaults.AuthenticationType);

                    var app_identifier = context.OwinContext.Get<String>("app_identifier");
                    Application application = ValidateApplication(app_identifier);

                    AuthenticationProperties properties = CreateProperties(user, GenerateGroupToken(context.Request), context.ClientId, oAuthIdentity, false, null, application);
                    AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                    context.Validated(ticket);
                    context.Request.Context.Authentication.SignIn(cookiesIdentity);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            catch (ApiCustomException ex)
            {
                context.SetError("invalid_grant", ex.Message);
            }
            catch (Exception ex)
            {
                context.SetError("invalid_grant", "Unhandled Exception");
            }
        }

        private Application ValidateApplication(string app_identifier)
        {
            Application application = null;

            if (!String.IsNullOrEmpty(app_identifier))
            {
                application = AuthServiceFactory.Instance.GetService<IApplicationService>().GetByKeySimple(app_identifier);

                if (!application.HasValue)
                {
                    throw new ApiCustomException("Invalid Application Identifier");
                }
            }

            return application;
        }

        public async Task GrantInternalActivation(OAuthGrantCustomExtensionContext context)
        {
            try
            {
                if (!VerifiedClinetsService.Instance.IsVerifiedClient(context.ClientId))
                {
                    context.SetError("invalid_clientID", "Unhandled Exception");
                }
                else
                {
                    var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
                    string username = context.Parameters["username"];

                    User user = await userManager.FindByNameAsync(username);

                    if (user == null || !user.HasValue)
                    {
                        context.SetError("invalid_grant", "The user name is incorrect.");
                        return;
                    }

                    if (user.EmailConfirmed == false)
                    {
                        await userManager.SetEmailConfirmedAsync(user, true);
                    }

                    try
                    {
                        ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, OAuthDefaults.AuthenticationType);
                        ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager, CookieAuthenticationDefaults.AuthenticationType);

                        AuthenticationProperties properties = CreateProperties(user, GenerateGroupToken(context.Request), context.ClientId, oAuthIdentity, false, null, null);
                        AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                        context.Validated(ticket);
                        context.Request.Context.Authentication.SignIn(cookiesIdentity);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            catch (ApiCustomException ex)
            {
                context.SetError("invalid_grant", ex.Message);
            }
            catch (Exception ex)
            {
                TraceLogManager.Instance.HandleException(ex, ExceptionHandlingPolicyConstants.LogOnlyPolicy);
                context.SetError("invalid_grant", "Unhandled Exception");
            }
        }

        private string GenerateGroupToken(IOwinRequest request)
        {
            string groupToken = Guid.NewGuid().ToString("n");
            request.Headers.Add("GroupToken", new String[] { groupToken });

            return groupToken;
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;

            string app_identifier = context.Parameters.Get("app_identifier");

            if (!String.IsNullOrEmpty(app_identifier))
            {
                context.OwinContext.Set("app_identifier", app_identifier);
            }

            context.TryGetBasicCredentials(out clientId, out clientSecret);

            if (context.ClientId == null)
            {
                var authentication_result = Startup.OAuthOptions.AccessTokenFormat.Unprotect(context.OwinContext.Request.Headers["Authorization"].Replace("Bearer ", ""));
                var clientIdentifier = authentication_result.Properties.Dictionary.TryGetValue("client_identifier");

                var client = AuthServiceFactory.Instance.GetService<IClientService>().GetByKeySimple(clientIdentifier);

                if (!client.HasValue || client.Status != 0)
                {
                    context.Validated();
                    context.SetError("invalid_clientId", "ClientId should be sent.");
                    return Task.FromResult<object>(null);
                }
                else
                {
                    context.Validated(client.ClientUID);
                    return Task.FromResult<object>(null);
                }
            }
            else
            {
                Client client = null;

                if (Startup.TokenProvider == null)
                {
                    client = AuthServiceFactory.Instance.GetService<IClientService>().GetByClientUIDAndSecret(context.ClientId, clientSecret);
                }
                else
                {
                    //ClientSecret is encrypted by TOTP

                    var temp_client = AuthServiceFactory.Instance.GetService<IClientService>().GetByClientUID(context.ClientId);

                    if (Startup.TokenProvider.ValidateEncryptedString(temp_client.ClientSecret, clientSecret, temp_client.SecurityStamp, temp_client.ClientID.ToString()))
                    {
                        client = temp_client;
                    }
                }

                if (client == null || !client.HasValue || client.Status != 0)
                {
                    context.Validated();
                    context.SetError("invalid_client_credential", "Client credential is not valid.");
                    return Task.FromResult<object>(null);
                }
                else
                {
                    if (client.DeviceRestricted)
                    {
                        string deviceIdentifier = context.Parameters["device_identifier"];

                        if (!String.IsNullOrWhiteSpace(deviceIdentifier))
                        {
                            var clientDevices = AuthServiceFactory.Instance.GetService<IClientDeviceService>().GetByClient(client.ClientID);

                            if (!clientDevices.Any(cd => cd.Device.DeviceIdentifier == deviceIdentifier))
                            {
                                context.Validated();
                                context.SetError("invalid_client_credential", "Device identifier is not valid.");
                                return Task.FromResult<object>(null);
                            }
                        }
                        else
                        {
                            context.Validated();
                            context.SetError("invalid_client_credential", "Device identifier is not valid.");
                            return Task.FromResult<object>(null);
                        }
                    }
                }

                context.Validated();
                return Task.FromResult<object>(null);
            }
        }

        public override Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {
            var client = AuthServiceFactory.Instance.GetService<IClientService>().GetByClientUID(context.ClientId);

            var oAuthIdentity = new ClientClaimsIdentity(context.Options.AuthenticationType);

            oAuthIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, client.ClientIdentifier));
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, client.ClientName));

            AuthenticationProperties properties = CreateClientProperties(client);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);

            context.Validated(ticket);

            return base.GrantClientCredentials(context);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public override async Task GrantCustomExtension(OAuthGrantCustomExtensionContext context)
        {
            switch (context.GrantType.ToLower())
            {
                case "external":
                    await GrantExternal(context);
                    break;
                case "activation":
                    await GrantActivation(context);
                    break;
                case "internal_activation":
                    await GrantInternalActivation(context);
                    break;
                default:
                    await base.GrantCustomExtension(context);
                    break;
            }
        }

        private async Task GrantExternal(OAuthGrantCustomExtensionContext context)
        {
            try
            {
                string applicationCode = context.ClientId;
                string provider = context.Parameters["provider"];
                string externalAccessToken = context.Parameters["access_token"];

                if (String.IsNullOrWhiteSpace(applicationCode))
                {
                    throw new ApiCustomException("Invalid Application Code");
                }

                if (String.IsNullOrWhiteSpace(provider))
                {
                    throw new ApiCustomException("Invalid Provider");
                }

                if (String.IsNullOrWhiteSpace(externalAccessToken))
                {
                    throw new ApiCustomException("Invalid Token");
                }

                var handler = ExternalAuthenticationManager.Instance.GetHandler(applicationCode, provider, context.OwinContext);

                var externalContext = await handler.AuthenticateAsync(externalAccessToken);

                var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

                var loginInfo = new UserLoginInfo(String.Format("{0}:{1}", applicationCode, provider), externalContext.UserID);

                User user = await userManager.FindAsync(loginInfo);

                bool firstTimeLogin = false;

                if (user == null || !user.HasValue)
                {
                    string userName = String.Empty;
                    string email = String.Empty;

                    if (!String.IsNullOrEmpty(externalContext.Email))
                    {
                        userName = email = externalContext.Email;
                    }
                    else
                    {
                        userName = email = String.Format("{0}@{1}", externalContext.UserID, handler.Provider);
                    }

                    user = await userManager.FindByNameAsync(userName);

                    if (user == null || !user.HasValue)
                    {
                        firstTimeLogin = true;

                        user = new User()
                        {
                            UserName = userName,
                            Email = email,
                            FullName = externalContext.Name
                        };

                        string password = System.Web.Security.Membership.GeneratePassword(20, 5);

                        IdentityResult createResult = await userManager.CreateAsync(user, password);

                        if (!createResult.Succeeded)
                        {
                            throw new Exception("Error in creating user.");
                        }
                    }

                    IdentityResult addLoginResult = await userManager.AddLoginAsync(user.UserID, loginInfo);

                    if (!addLoginResult.Succeeded)
                    {
                        throw new Exception("Error in creating login.");
                    }
                }

                if (context.Request.Context.Authentication.User != null)
                {
                    context.Request.Context.Authentication.SignOut();
                }

                await userManager.AddExternalClaims(user.UserID, externalContext);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager, CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = CreateProperties(user, GenerateGroupToken(context.Request), applicationCode, oAuthIdentity, firstTimeLogin, externalContext, null);
                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                context.Validated(ticket);
                context.Request.Context.Authentication.SignIn(cookiesIdentity);
            }
            catch (ApiCustomException ex)
            {
                context.SetError("invalid_grant", ex.Message);
            }
            catch (Exception)
            {
                context.SetError("invalid_grant", "Unhandled Exception");
            }
        }

        private async Task GrantActivation(OAuthGrantCustomExtensionContext context)
        {
            try
            {
                var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

                string username = context.Parameters["username"];
                string token = context.Parameters["token"];

                User user = await userManager.FindByNameAsync(username);

                if (user == null || !user.HasValue)
                {
                    user = await userManager.FindByEmailAsync(username);
                }

                if (user == null || !user.HasValue)
                {
                    context.SetError("invalid_grant", "The user name is invalid.");
                    return;
                }

                if (!userManager.VerifyTwoFactorToken(user.Id, PasswordlessLoginEmailTokenProvider.Purpose, token))
                {
                    context.SetError("invalid_grant", "Incorrect Code");
                    return;
                }

                if (user.EmailConfirmed == false)
                {
                    await userManager.SetEmailConfirmedAsync(user, true);
                }

                try
                {
                    ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, OAuthDefaults.AuthenticationType);
                    ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager, CookieAuthenticationDefaults.AuthenticationType);

                    AuthenticationProperties properties = CreateProperties(user, GenerateGroupToken(context.Request), context.ClientId, oAuthIdentity, false, null, null);
                    AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);

                    var restePasswordToken = await userManager.GeneratePasswordResetTokenAsync(user.Id);
                    properties.Dictionary.Add("resetPasswordToken", restePasswordToken);

                    context.Validated(ticket);
                    context.Request.Context.Authentication.SignIn(cookiesIdentity);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            catch (ApiCustomException ex)
            {
                context.SetError("invalid_grant", ex.Message);
            }
            catch (Exception)
            {
                context.SetError("invalid_grant", "Unhandled Exception");
            }
        }

        public static AuthenticationProperties CreateProperties(User user, string groupToken, string applicationCode, ClaimsIdentity oAuthIdentity, bool firstTimeLogin, ExternalAuthenticatedContext externalContext, Application application)
        {
            IDictionary<String, String> data = new Dictionary<String, String>
            {
                { "userName", user.UserName },
                { "email", user.Email },
                { "group_token", groupToken },
                { "firstTimeLogin", firstTimeLogin.ToString() }
            };

            string externalUserID = oAuthIdentity.TryGetValue(ClaimTypes.NameIdentifier);
            string fullName = oAuthIdentity.TryGetValue(CustomClaimTypes.DisplayName);
            string firstName = oAuthIdentity.TryGetValue(ClaimTypes.GivenName);
            string lastName = oAuthIdentity.TryGetValue(ClaimTypes.Surname);
            string gender = oAuthIdentity.TryGetValue(ClaimTypes.Gender);
            string profilePicture = oAuthIdentity.TryGetValue(CustomClaimTypes.ProfilePicture);

            if (!String.IsNullOrEmpty(externalUserID))
            {
                data.Add("externalUserID", externalUserID);
            }

            if (!String.IsNullOrEmpty(fullName))
            {
                data.Add("fullName", fullName);
            }

            if (!String.IsNullOrEmpty(firstName))
            {
                data.Add("firstName", firstName);
            }

            if (!String.IsNullOrEmpty(lastName))
            {
                data.Add("lastName", lastName);
            }

            if (!String.IsNullOrEmpty(gender))
            {
                data.Add("gender", gender);
            }

            if (!String.IsNullOrEmpty(profilePicture))
            {
                data.Add("profilePicture", profilePicture);
            }

            if (oAuthIdentity.Claims.Count(c => c.Type == ClaimTypes.Role && c.Value.Split(':')[0] == applicationCode) != 0)
            {
                var roles = oAuthIdentity.Claims.Where(c => c.Type == ClaimTypes.Role && c.Value.Split(':')[0] == applicationCode);
                data.Add("roles", JsonConvert.SerializeObject(roles.Select(x => x.Value.Split(':')[1])));
            }

            if (externalContext != null)
            {
                if (externalContext.Friends != null)
                {
                    data.Add("friends", JsonConvert.SerializeObject(externalContext.Friends.Select(f => f.ID)));
                }
            }

            if (application?.HasValue == true)
            {
                data.Add("app_identifier", application.Code);
                data.Add("callback_uri", application.SSOCallBackURI);
            }

            return new AuthenticationProperties(data);
        }

        public static AuthenticationProperties CreateClientProperties(Client client)
        {
            IDictionary<String, String> data = new Dictionary<String, String>
            {
                { "client_identifier", client.ClientIdentifier },
                { "client_name", client.ClientName }
            };

            return new AuthenticationProperties(data);
        }
    }
}