using System;

namespace DotFramework.Auth.SSO
{
    public class GetProviderException : Exception
    {
        public GetProviderException(string message) : base(message)
        {
        }
    }
}