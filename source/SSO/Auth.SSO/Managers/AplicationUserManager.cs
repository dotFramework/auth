using DotFramework.Auth.Authentication;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.SSO.Providers;
using DotFramework.Auth.SSO.Services;
using DotFramework.Core.Configuration;
using DotFramework.Infra;
using DotFramework.Infra.Security;
using DotFramework.Infra.Security.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace DotFramework.Auth.SSO.Managers
{
    public class ApplicationUserManager : UserManager<User, Int32>
    {
        private readonly IOwinContext _Context;

        #region Constuctor

        public ApplicationUserManager(IUserStore<User, Int32> store, IOwinContext context)
            : base(store)
        {
            _Context = context;
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(AuthServiceFactory.Instance.GetService<IApplicationUserService>(), context);

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<User, Int32>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = AppSettingsManager.Instance.Get<Boolean>("RequireUniqueEmail", true)
            };

            //manager.PasswordHasher = new PasswordHasher();

            var dataProtectionProvider = options.DataProtectionProvider;

            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<User, Int32>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            manager.RegisterTwoFactorProvider(PasswordlessLoginEmailTokenProvider.Purpose, new PasswordlessLoginEmailTokenProvider());

            manager.EmailService = new EmailService();

            return manager;
        }

        #endregion

        #region Overrided Methods

        public override async Task<IdentityResult> CreateAsync(User user)
        {
            Task<IdentityResult> result = base.CreateAsync(user);

            try
            {
                IdentityResult identityResult = await result;
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(ex.Message);
            }

            return result.Result;
        }

        public override async Task<IdentityResult> AddClaimAsync(int userId, Claim claim)
        {
            Task<IdentityResult> result = base.AddClaimAsync(userId, claim);

            try
            {
                IdentityResult identityResult = await result;
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(ex.Message);
            }

            return result.Result;
        }

        public override async Task<IdentityResult> AddToRoleAsync(int userId, string role)
        {
            Task<IdentityResult> result = base.AddToRoleAsync(userId, role);

            try
            {
                IdentityResult identityResult = await result;
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(ex.Message);
            }

            return result.Result;
        }

        protected override Task<IdentityResult> UpdatePassword(IUserPasswordStore<User, Int32> passwordStore, User user, string newPassword)
        {
            try
            {
                var accountPolicy = AuthServiceFactory.Instance.GetService<IAccountPolicyService>().GetSimple(user.AccountPolicy.AccountPolicyID);
                EnforcePasswordPolicy(user, accountPolicy, newPassword);

                // Configure validation logic for passwords
                PasswordValidator = new PasswordValidator
                {
                    RequiredLength = accountPolicy.RequiredLength,
                    RequireNonLetterOrDigit = accountPolicy.RequireNonLetterOrDigit,
                    RequireDigit = accountPolicy.RequireDigit,
                    RequireLowercase = accountPolicy.RequireLowercase,
                    RequireUppercase = accountPolicy.RequireUppercase
                };

                return base.UpdatePassword(passwordStore, user, newPassword);
            }
            catch (Exception ex)
            {
                return Task.Run(() =>
                {
                    return IdentityResult.Failed(ex.Message);
                });
            }
        }

        protected override async Task<Boolean> VerifyPasswordAsync(IUserPasswordStore<User, Int32> store, User user, string password)
        {
            if (await IsLockedOutAsync(user.UserID))
            {
                throw new ApiCustomException("Your account has been locked. Please use �Forgot Password� to reset your password.");
            }

            if (await base.VerifyPasswordAsync(store, user, password))
            {
                await ResetAccessFailedCountAsync(user.UserID);
                //await SetLockoutEndDateAsync(user.UserID, DateTimeOffset.MinValue);

                return true;
            }
            else
            {
                if (await GetLockoutEnabledAsync(user.UserID))
                {
                    var accountPolicy = AuthServiceFactory.Instance.GetService<IAccountPolicyService>().Get(user.AccountPolicy.AccountPolicyID);

                    if (accountPolicy.LockoutThreshold != 0)
                    {
                        MaxFailedAccessAttemptsBeforeLockout = accountPolicy.LockoutThreshold;

                        if (accountPolicy.LockoutDuration == 0)
                        {
                            DefaultAccountLockoutTimeSpan = TimeSpan.FromDays(365 * 200);
                        }
                        else
                        {
                            DefaultAccountLockoutTimeSpan = TimeSpan.FromDays(accountPolicy.LockoutDuration);
                        }

                        await AccessFailedAsync(user.UserID);
                    }
                }

                return false;
            }
        }

        public override async Task<IdentityResult> AccessFailedAsync(int userId)
        {
            var userLockoutStore = GetCustomUserLockoutStore();
            User user = await FindByIdAsync(userId);

            if (user == null || !user.HasValue)
            {
                throw new InvalidOperationException(String.Format(CultureInfo.CurrentCulture, "User '{0}' not found.", userId));
            }

            int num = await userLockoutStore.IncrementAccessFailedCountAsync(user);

            if (num >= MaxFailedAccessAttemptsBeforeLockout)
            {
                await userLockoutStore.SetLockoutEndDateAsync(user, DateTimeOffset.UtcNow.Add(DefaultAccountLockoutTimeSpan));
                await userLockoutStore.SetLockoutTypeAsync(user, (byte)LockoutType.System);

                await userLockoutStore.ResetAccessFailedCountAsync(user);
            }

            return await UpdateAsync(user);
        }

        public override Task<string> GenerateEmailConfirmationTokenAsync(int userId)
        {
            return base.GenerateEmailConfirmationTokenAsync(userId);
        }

        #endregion

        #region Public Methods

        public async Task AddExternalClaims(int userId, ExternalAuthenticatedContext externalContext)
        {
            //var claims = await this.GetClaimsAsync(userId);

            var nameIdentifierClaim = externalContext.Identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
            var nameClaim = externalContext.Identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
            var emailClaim = externalContext.Identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
            var displayNameClaim = externalContext.Identity.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.DisplayName);
            var givenNameClaim = externalContext.Identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.GivenName);
            var surnameClaim = externalContext.Identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Surname);
            var genderClaim = externalContext.Identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Gender);
            var profilePictureClaim = externalContext.Identity.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ProfilePicture);

            if (nameIdentifierClaim != null)
            {
                await RemoveClaimAsync(userId, nameIdentifierClaim);
                await AddClaimAsync(userId, nameIdentifierClaim);

                //if (claims.Count(c => c.Type == ClaimTypes.NameIdentifier) == 0)
                //{
                //    await AddClaimAsync(userId, nameIdentifierClaim);
                //}
                //else
                //{
                //    await RemoveClaimAsync(userId, nameIdentifierClaim);
                //}
            }

            if (nameClaim != null)
            {
                await RemoveClaimAsync(userId, nameClaim);
                await AddClaimAsync(userId, nameClaim);
            }

            if (emailClaim != null)
            {
                await RemoveClaimAsync(userId, emailClaim);
                await AddClaimAsync(userId, emailClaim);
            }

            if (displayNameClaim != null)
            {
                await RemoveClaimAsync(userId, displayNameClaim);
                await AddClaimAsync(userId, displayNameClaim);
            }

            if (givenNameClaim != null)
            {
                await RemoveClaimAsync(userId, givenNameClaim);
                await AddClaimAsync(userId, givenNameClaim);
            }

            if (surnameClaim != null)
            {
                await RemoveClaimAsync(userId, surnameClaim);
                await AddClaimAsync(userId, surnameClaim);
            }

            if (genderClaim != null)
            {
                await RemoveClaimAsync(userId, genderClaim);
                await AddClaimAsync(userId, genderClaim);
            }

            if (profilePictureClaim != null)
            {
                await RemoveClaimAsync(userId, profilePictureClaim);
                await AddClaimAsync(userId, profilePictureClaim);
            }
        }

        public AuthorizedRolesResponseModel GetAuthorizedRoles(GetAuthorizedRolesBindingModel model)
        {
            try
            {
                AuthorizedRolesResponseModel result = new AuthorizedRolesResponseModel();

                switch (model.ObjectType)
                {
                    case AuthObjectType.View:
                        result = GetRolesByView(model);
                        break;
                    case AuthObjectType.Action:
                        result = GetRolesByAction(model);
                        break;
                    case AuthObjectType.Service:
                        result = GetRolesByService(model);
                        break;
                    default:
                        break;
                }

                return result;
            }
            catch
            {
                throw;
            }
        }

        public UserSession GetLatestSession(int userID, string applicationCode)
        {
            //var applications = AuthServiceFactory.Instance.GetService<ISubApplicationService>().GetByFilter(a => a.Code == applicationCode);

            //if (applications.Count == 0)
            //{
            //    throw new ApiCustomException(String.Format("Application '{0}' is not registered.", applicationCode));
            //}

            //var userSessions = AuthServiceFactory.Instance.GetService<IUserSessionService>().GetByFilter(new DynamicQuery
            //{
            //    Filter = FilterExpression.Create<UserSession>(us => us.User.UserID, SqlOperators.Equal, userID.ToString()) &
            //             FilterExpression.Create<UserSession>(us => us.SubApplication.SubApplicationID, SqlOperators.Equal, applications.First().SubApplicationID.ToString()),
            //    OrderBy = new OrderByList().Add<UserSession>(us => us.StartTime, OrderByType.Descending)
            //});

            //if (userSessions.Count == 0)
            //{
            //    throw new ApiCustomException(String.Format("UserSession Not Found!"));
            //}

            //return userSessions.FirstOrDefault();

            throw new NotImplementedException();
        }

        public async Task AddClaims(int userId, ClaimsIdentity identity)
        {


            if (identity != null)
            {
                foreach (var claim in identity.Claims)
                {
                    await AddClaimAsync(userId, claim);
                }
            }
        }

        public async Task<Byte> GetLockoutTypeAsync(int userId)
        {
            var customUserLockoutStore = GetCustomUserLockoutStore();
            User user = await FindByIdAsync(userId);

            if (user == null || !user.HasValue)
            {
                throw new InvalidOperationException(String.Format(CultureInfo.CurrentCulture, "User  '{0}' not found.", userId));
            }

            return await customUserLockoutStore.GetLockoutTypeAsync(user);
        }

        public async Task<IdentityResult> SetLockoutTypeAsync(int userId, LockoutType lockoutType)
        {
            var customUserLockoutStore = this.GetCustomUserLockoutStore();
            User user = await FindByIdAsync(userId);

            if (user == null || !user.HasValue)
            {
                throw new InvalidOperationException(String.Format(CultureInfo.CurrentCulture, "User '{0}' not found.", userId));
            }

            await customUserLockoutStore.SetLockoutTypeAsync(user, (byte)lockoutType);
            return await UpdateAsync(user);
        }

        public async Task<IdentityResult> LockoutAsync(int userId, DateTimeOffset lockoutEnd, LockoutType lockoutType)
        {
            var userLockoutStore = GetCustomUserLockoutStore();
            User user = await FindByIdAsync(userId);

            if (user == null || !user.HasValue)
            {
                throw new InvalidOperationException(String.Format(CultureInfo.CurrentCulture, "User '{0}' not found.", userId));
            }

            IdentityResult result;

            if (!await userLockoutStore.GetLockoutEnabledAsync(user))
            {
                result = IdentityResult.Failed("Lockout is not enabled for this user.");
            }
            else
            {
                await userLockoutStore.SetLockoutEndDateAsync(user, lockoutEnd);
                await userLockoutStore.SetLockoutTypeAsync(user, (byte)lockoutType);

                result = await UpdateAsync(user);
            }

            return result;
        }

        public async Task SendForgetPasswordEmailAsync(User user, string resetUrl, List<KeyValuePair<string, string>> keyValuePairs)
        {
            string subject = "Password Reset Request";

            string body = Properties.Resources.ResetPasswordSimple.Replace("{FullName}", user.FullName)
                                                                      .Replace("{ResetUrl}", resetUrl);

            SetDefaultForgetPasswordEmailTokens(keyValuePairs);

            if (keyValuePairs.Any(kv => kv.Key == "SubjectToken"))
            {
                subject = keyValuePairs.Find(kv => kv.Key == "SubjectToken").Value;
            }

            foreach (var keyValue in keyValuePairs)
            {
                body = body.Replace("{" + keyValue.Key + "}", keyValue.Value);
            }

            await SendEmailAsync(user.UserID, subject, body);
        }

        private void SetDefaultForgetPasswordEmailTokens(List<KeyValuePair<string, string>> keyValuePairs)
        {
            if (keyValuePairs == null)
            {
                keyValuePairs = new List<KeyValuePair<string, string>>();
            }

            if (!keyValuePairs.Any(kv => kv.Key == "TitleToken"))
            {
                keyValuePairs.Add(new KeyValuePair<string, string>
                    (
                    "TitleToken", @"Password Reset Request Received"
                    ));
            }

            if (!keyValuePairs.Any(kv => kv.Key == "IntroductionToken"))
            {
                keyValuePairs.Add(new KeyValuePair<string, string>
                    (
                    "IntroductionToken", @"We have received a request to reset the password to your account.<br/>To reset your password, click on the button below."
                    ));
            }

            if (!keyValuePairs.Any(kv => kv.Key == "ResetPasswordToken"))
            {
                keyValuePairs.Add(new KeyValuePair<string, string>
                    (
                    "ResetPasswordToken", @"Reset Password"
                    ));
            }

            if (!keyValuePairs.Any(kv => kv.Key == "ConclusionToken"))
            {
                keyValuePairs.Add(new KeyValuePair<string, string>
                    (
                    "ConclusionToken", @"You can log into your account with your email address and new password.<br/>Not requested a new password or remember your old one? No problem, the old password is still valid."
                    ));
            }
        }

        public async Task SendEmailConfirmationAsync(User user, string confirmationUrl, Dictionary<String, String> keyValuePairs)
        {
            string subject = "Email Confirmation";
            string body = Properties.Resources.EmailConfirmationTemplate.Replace("{VerifyEmailUrl}", confirmationUrl);

            SetDefaultConfirmEmailTokens(keyValuePairs);

            foreach (var keyValue in keyValuePairs)
            {
                body = body.Replace("{" + keyValue.Key + "}", keyValue.Value);
            }

            await SendEmailAsync(user.UserID, subject, body);
        }

        private void SetDefaultConfirmEmailTokens(Dictionary<String, String> keyValuePairs)
        {
            if (keyValuePairs == null)
            {
                keyValuePairs = new Dictionary<String, String>();
            }

            if (!keyValuePairs.Any(kv => kv.Key == "ProjectTeamName"))
            {
                keyValuePairs.Add("ProjectTeamName", "Total e Integrated");
            }

            if (!keyValuePairs.Any(kv => kv.Key == "ImageProject"))
            {
                keyValuePairs.Add("ImageProject", "https://cdn.totaleintegrated.com/assets/logos/TEI.png");
            }
        }

        public async Task SetEmailConfirmedAsync(User user, bool confirmed)
        {
            await AuthServiceFactory.Instance.GetService<IApplicationUserService>().SetEmailConfirmedAsync(user, confirmed);
            await UpdateAsync(user);
        }

        public void RemoveValueFromContext(IOwinContext context, string key)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            // Check if the key exists before attempting to remove it
            if (context.Environment.ContainsKey(key))
            {
                context.Environment.Remove(key);
            }
        }

        #endregion

        #region Private Methods

        private void EnforcePasswordPolicy(User user, AccountPolicy accountPolicy, String passwordHash)
        {
            try
            {
                var passwords = AuthServiceFactory.Instance.GetService<IPasswordHistoryService>().GetByUserSimple(user.UserID);

                if (passwords.Count != 0)
                {
                    if (accountPolicy.EnforcePasswordHistory != 0)
                    {
                        if (passwords.OrderByDescending(p => p.SetPasswordTime).Select(p => p.PasswordHash).Take(accountPolicy.EnforcePasswordHistory).Where(p => PasswordHasher.VerifyHashedPassword(p, passwordHash) != PasswordVerificationResult.Failed).Any())
                        {
                            throw new Exception("New password cannot be same as your previous passwords.");
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private AuthorizedRolesResponseModel GetRolesByView(GetAuthorizedRolesBindingModel model)
        {
            AuthorizedRolesResponseModel result = new AuthorizedRolesResponseModel();

            try
            {
                if (String.IsNullOrEmpty(model.ControllerName) || String.IsNullOrEmpty(model.ActionName))
                {
                    result.Error = "ControllerName and ActionName cannot be empty.";
                    return result;
                }

                ApplicationHierarchy applicationHierarchy = AuthServiceFactory.Instance.GetService<IApplicationHierarchyService>().GetByActionName(model.ApplicationCode, model.ControllerName, model.ActionName);

                if (applicationHierarchy == null || !applicationHierarchy.HasValue)
                {
                    result.Error = "Invalid View!";
                    return result;
                }

                if (!applicationHierarchy.NeedAuthorization)
                {
                    result.NeedAuthorization = false;
                    return result;
                }
                else
                {
                    result.NeedAuthorization = true;
                    RoleViewCollection roleViews = AuthServiceFactory.Instance.GetService<IRoleViewService>().GetByApplicationHierarchy(applicationHierarchy.ApplicationHierarchyID);

                    if (roleViews.Count != 0)
                    {
                        result.Roles = roleViews.Select(rv => String.Format("{0}:{1}", rv.Role.Application.Code, rv.Role.Name)).Distinct().ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = ex.Message;
            }

            return result;
        }

        private AuthorizedRolesResponseModel GetRolesByAction(GetAuthorizedRolesBindingModel model)
        {
            return new AuthorizedRolesResponseModel { NeedAuthorization = false };
        }

        private AuthorizedRolesResponseModel GetRolesByService(GetAuthorizedRolesBindingModel model)
        {
            return new AuthorizedRolesResponseModel { NeedAuthorization = false };
        }

        private ICustomUserLockoutStore<User, Int32> GetCustomUserLockoutStore()
        {
            ICustomUserLockoutStore<User, Int32> customUserLockoutStore = Store as ICustomUserLockoutStore<User, Int32>;

            if (customUserLockoutStore == null)
            {
                throw new NotSupportedException("Store does not implement ICustomUserLockoutStore<TUser, TKey>.");
            }

            return customUserLockoutStore;
        }

        #endregion

        public override async Task<User> FindByIdAsync(int userId)
        {
            var user = GetUserFromContextById(userId);

            if (user == null || !user.HasValue)
            {
                user = await base.FindByIdAsync(userId);

                if (_Context != null)
                {
                    _Context.Set("CurrentUser", user);
                }

                return user;
            }
            else
            {
                return user;
            }
        }

        public override async Task<User> FindByNameAsync(string userName)
        {
            var user = GetUserFromContextByName(userName);

            if (user == null || !user.HasValue)
            {
                user = await base.FindByNameAsync(userName);

                if (_Context != null)
                {
                    _Context.Set("CurrentUser", user);
                }

                return user;
            }
            else
            {
                return user;
            }
        }

        public override async Task<User> FindByEmailAsync(string email)
        {
            var user = GetUserFromContextByEmail(email);

            if (user == null || !user.HasValue)
            {
                user = await base.FindByEmailAsync(email);

                if (_Context != null)
                {
                    _Context.Set("CurrentUser", user);
                }

                return user;
            }
            else
            {
                return user;
            }
        }

        private User GetUserFromContextById(int userId)
        {
            var user = _Context.Get<User>("CurrentUser");

            if (user != null)
            {
                if (user.UserID == userId)
                {
                    return user;
                }
            }

            return null;
        }

        private User GetUserFromContextByName(string userName)
        {
            var user = _Context.Get<User>("CurrentUser");

            if (user != null)
            {
                if (user.UserName == userName)
                {
                    return user;
                }
            }

            return null;
        }

        private User GetUserFromContextByEmail(string email)
        {
            var user = _Context.Get<User>("CurrentUser");

            if (user != null)
            {
                if (user.Email == email)
                {
                    return user;
                }
            }

            return null;
        }
    }

    public enum LockoutType : byte
    {
        System = 1,
        Admin = 2
    }
}