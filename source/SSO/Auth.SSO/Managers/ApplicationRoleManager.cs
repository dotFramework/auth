using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;

namespace DotFramework.Auth.SSO.Managers
{
    public class ApplicationRoleManager : RoleManager<Role, Int32>
    {
        public ApplicationRoleManager(IRoleStore<Role, Int32> store)
            : base(store)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var manager = new ApplicationRoleManager(AuthServiceFactory.Instance.GetService<IApplicationRoleService>());
            return manager;
        }
    }
}