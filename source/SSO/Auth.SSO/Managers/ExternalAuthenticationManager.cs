using DotFramework.Auth.Authentication;
using DotFramework.Auth.Authentication.Facebook;
using DotFramework.Auth.Authentication.Google;
using DotFramework.Auth.Authentication.Microsoft;
using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Core;
using DotFramework.Infra;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.MicrosoftAccount;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DotFramework.Auth.SSO
{
    public class ExternalAuthenticationManager : SingletonProvider<ExternalAuthenticationManager>
    {
        #region Constructor

        private ExternalAuthenticationManager()
        {
            _ApplicationExternalAuthenticationProviders = new Dictionary<String, Dictionary<Type, ExternalAuthenticationProvider>>();
        }

        #endregion

        #region Private Members

        private readonly Dictionary<String, Dictionary<Type, ExternalAuthenticationProvider>> _ApplicationExternalAuthenticationProviders;

        #endregion

        #region Public Methods

        public Dictionary<Type, ExternalAuthenticationProvider> this[string applicationCode]
        {
            get
            {
                if (!_ApplicationExternalAuthenticationProviders.ContainsKey(applicationCode))
                {
                    lock (_ApplicationExternalAuthenticationProviders)
                    {
                        var applications = AuthServiceFactory.Instance.GetService<IApplicationService>().GetByFilter(a => a.Code == applicationCode);

                        if (applications.Count == 0)
                        {
                            throw new ApiCustomException(String.Format("Application '{0}' is not registered.", applicationCode));
                        }

                        var application = applications.FirstOrDefault();

                        var loginProviderOptionsCollection = AuthServiceFactory.Instance.GetService<ILoginProviderOptionsService>().GetByApplication(application.ApplicationID);

                        Dictionary<Type, ExternalAuthenticationProvider> optionsDictionary = new Dictionary<Type, ExternalAuthenticationProvider>();

                        foreach (var loginProviderOptions in loginProviderOptionsCollection)
                        {
                            try
                            {
                                switch (loginProviderOptions.LoginProvider.Code)
                                {
                                    case "Facebook":
                                        optionsDictionary.Add(typeof(CustomFacebookAuthenticationOptions), CreateFacebookProvider(loginProviderOptions));
                                        break;
                                    case "Google":
                                        optionsDictionary.Add(typeof(CustomGoogleAuthenticationOptions), CreateGoogleProvider(loginProviderOptions));
                                        break;
                                    case "Microsoft":
                                        optionsDictionary.Add(typeof(CustomMicrosoftAccountAuthenticationOptions), CreateMicrosoftAccountProvider(loginProviderOptions));
                                        break;
                                    default:
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                //To Be Logged
                            }
                        }

                        _ApplicationExternalAuthenticationProviders.Add(applicationCode, optionsDictionary);
                    }
                }

                return _ApplicationExternalAuthenticationProviders[applicationCode];
            }
        }

        public ExternalAuthenticationProvider<TOptions> GetProvider<TOptions>(string applicationCode) where TOptions : AuthenticationOptions
        {
            var optionsDictionary = this[applicationCode];

            if (optionsDictionary.ContainsKey(typeof(TOptions)))
            {
                return optionsDictionary[typeof(TOptions)] as ExternalAuthenticationProvider<TOptions>;
            }
            else
            {
                throw new GetProviderException(String.Format("Provider '{0}' is not registered for Application '{1}'.", typeof(TOptions).Name, applicationCode));
            }
        }

        public ExternalAuthenticationProvider GetProvider(string applicationCode, string provider)
        {
            try
            {
                if (provider.ToLower() == "facebook")
                {
                    return this.GetProvider<CustomFacebookAuthenticationOptions>(applicationCode);
                }
                else if (provider.ToLower() == "google")
                {
                    return this.GetProvider<CustomGoogleAuthenticationOptions>(applicationCode);
                }
                else if (provider.ToLower() == "microsoft")
                {
                    return this.GetProvider<CustomMicrosoftAccountAuthenticationOptions>(applicationCode);
                }
                else
                {
                    throw new ApiCustomException(String.Format("Provider '{0}' is not supported.", provider));
                }
            }
            catch (GetProviderException)
            {
                throw new ApiCustomException(String.Format("Provider '{0}' is not registered for Application '{1}'.", provider, applicationCode));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ExternalAuthenticationHandler<TOptions> GetHandler<TOptions>(string applicationCode, IOwinContext context) where TOptions : AuthenticationOptions
        {
            return this.GetProvider<TOptions>(applicationCode).CreateHandler(context);
        }

        public ExternalAuthenticationHandler GetHandler(string applicationCode, string provider, IOwinContext context)
        {
            if (provider.ToLower() == "facebook")
            {
                return this.GetProvider<CustomFacebookAuthenticationOptions>(applicationCode).CreateHandler(context);
            }
            else if (provider.ToLower() == "google")
            {
                return this.GetProvider<CustomGoogleAuthenticationOptions>(applicationCode).CreateHandler(context);
            }
            else if (provider.ToLower() == "microsoft")
            {
                return this.GetProvider<CustomMicrosoftAccountAuthenticationOptions>(applicationCode).CreateHandler(context);
            }
            else
            {
                throw new ApiCustomException(String.Format("Provider '{0}' is not supported.", provider));
            }
        }

        #endregion

        #region Private Methods

        private FacebookExternalAuthenticationProvider CreateFacebookProvider(LoginProviderOptions loginProviderOptions)
        {
            var options = new CustomFacebookAuthenticationOptions
            {
                AppId = loginProviderOptions.ApplicationIdentity,
                AppSecret = loginProviderOptions.ApplicationSecret,
                Provider = new FacebookAuthenticationProvider()
            };

            var loginProviderScopes = AuthServiceFactory.Instance.GetService<ILoginProviderScopeService>().GetByLoginProviderOptions(loginProviderOptions.LoginProviderOptionsID);

            foreach (var scope in loginProviderScopes)
            {
                options.Scope.Add(scope.Scope);
                options.ScopeDetail.Add(scope.Detail);
            }

            return new FacebookExternalAuthenticationProvider(options);
        }

        private GoogleExternalAuthenticationProvider CreateGoogleProvider(LoginProviderOptions loginProviderOptions)
        {
            var options = new CustomGoogleAuthenticationOptions
            {
                ClientId = loginProviderOptions.ApplicationIdentity,
                ClientSecret = loginProviderOptions.ApplicationSecret,
                Provider = new GoogleOAuth2AuthenticationProvider()
            };

            var loginProviderScopes = AuthServiceFactory.Instance.GetService<ILoginProviderScopeService>().GetByLoginProviderOptions(loginProviderOptions.LoginProviderOptionsID);

            foreach (var scope in loginProviderScopes)
            {
                options.Scope.Add(scope.Scope);
                options.ScopeDetail.Add(scope.Detail);
            }

            return new GoogleExternalAuthenticationProvider(options);
        }

        private MicrosoftAccountExternalAuthenticationProvider CreateMicrosoftAccountProvider(LoginProviderOptions loginProviderOptions)
        {
            var options = new CustomMicrosoftAccountAuthenticationOptions
            {
                ClientId = loginProviderOptions.ApplicationIdentity,
                ClientSecret = loginProviderOptions.ApplicationSecret,
                Provider = new MicrosoftAccountAuthenticationProvider()
            };

            var loginProviderScopes = AuthServiceFactory.Instance.GetService<ILoginProviderScopeService>().GetByLoginProviderOptions(loginProviderOptions.LoginProviderOptionsID);

            foreach (var scope in loginProviderScopes)
            {
                options.Scope.Add(scope.Scope);
                options.ScopeDetail.Add(scope.Detail);
            }

            return new MicrosoftAccountExternalAuthenticationProvider(options);
        }

        #endregion
    }
}