var Auth = Object.create(null);

Auth.SetAuthorizationHeader = function (xhr)
{
    if ($.cookie('SSOToken') === null || $.cookie('SSOToken') === "" || $.cookie('SSOToken') === "null" || $.cookie('SSOToken') === undefined)
    {
        //xhr.abort();
    }
    else
    {
        xhr.setRequestHeader("Authorization", String.Format("Bearer {0}", $.cookie("SSOToken")));
    }

    if ($.cookie('UserSessionID'))
    {
        xhr.setRequestHeader("UserSessionID", $.cookie("UserSessionID"));
    }
}