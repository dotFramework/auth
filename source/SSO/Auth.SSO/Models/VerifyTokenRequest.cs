﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Auth.SSO.Models
{
    public class VerifyTokenRequestModel
    {
        public string AppSecretProof { get; set; }
    }
}
