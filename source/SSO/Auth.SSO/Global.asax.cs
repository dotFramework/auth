using DotFramework.Infra.Web;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;

namespace DotFramework.Auth.SSO
{
    public class WebApiApplication : SecureGlobalServiceBase
    {
        protected override bool NeedApplicationSession
        {
            get
            {
                return false;
            }
        }

        protected override bool NeedAuthenticationService
        {
            get
            {
                return false;
            }
        }

        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            this.ApplicationStart();
        }

        protected void Application_BeginRequest()
        {
            //FirstRequestInitialization.Initialize(() => BundleConfig.RegisterBundles(BundleTable.Bundles));
        }
    }
}
