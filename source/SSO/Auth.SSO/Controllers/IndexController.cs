﻿using System.Web.Mvc;

namespace DotFramework.Auth.SSO.Controllers
{
    public class IndexController : Controller
    {
        public ActionResult KeepAlive()
        {
            return View();
        }
    }
}
