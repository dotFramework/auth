﻿using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.SSO.Helpers;
using DotFramework.Infra;
using DotFramework.Infra.Security.Model;
using Microsoft.Ajax.Utilities;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace DotFramework.Auth.SSO.Controllers
{
    [Authorize]
    [RoutePrefix("Auth/Secure")]
    public class SecureAuthController : ApiController
    {
        [Route("CreateClient")]
        public IHttpActionResult CreateClient(CreateClientRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var client = AuthServiceFactory.Instance.GetService<IClientService>().GetByKey(request.ClientIdentifier);

                if (client?.HasValue == true)
                {
                    throw new ApiCustomException("duplicate_client_identifier");
                }

                string clientUid = GenerateGuidWitNoDashes();
                string clientIdentifier = request.ClientIdentifier;
                string clientSecret = HashHelper.GetRandomAlphanumericString(32);

                var client_new = new Client
                {
                    ClientUID = clientUid,
                    ClientSecret = clientSecret,
                    ClientIdentifier = clientIdentifier,
                    ClientName = request.ClientName,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    DeviceRestricted = false,
                };

                if (request.DeviceRestricted)
                {
                    client_new.DeviceRestricted = true;
                    client_new.Status = 255;
                }

                var result = AuthServiceFactory.Instance.GetService<IClientService>().Add(client_new);

                var response = new CreateClientResponse
                {
                    ClientIdentifier = clientIdentifier,
                    ClientID = clientUid,
                    ClientSecret = clientSecret
                };

                if (request.DeviceRestricted && !String.IsNullOrWhiteSpace(request.PreviousClientIdentifier))
                {
                    var previous_client = AuthServiceFactory.Instance.GetService<IClientService>().GetByKey(request.PreviousClientIdentifier);

                    if (previous_client?.HasValue == true)
                    {
                        previous_client.SecurityStamp = Guid.NewGuid().ToString().ToUpper();
                        previous_client.Status = 254;

                        AuthServiceFactory.Instance.GetService<IClientService>().Edit(previous_client);
                    }
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private string GenerateGuidWitNoDashes()
        {
            return Guid.NewGuid().ToString().ToLower().Replace("-", "");
        }

        [HttpPost]
        [Route("GetClient")]
        public IHttpActionResult GetClient(GetClientRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                if (String.IsNullOrWhiteSpace(request.ClientUID) && String.IsNullOrWhiteSpace(request.ClientIdentifier))
                {
                    throw new ApiCustomException("Either ClientUID or ClientIdentifier should be passed as input parameter.");
                }

                if (!String.IsNullOrWhiteSpace(request.ClientUID) && !String.IsNullOrWhiteSpace(request.ClientIdentifier))
                {
                    throw new ApiCustomException("ClientUID and ClientIdentifier cannot be passed together as input parameter.");
                }

                Client client;

                if (!String.IsNullOrWhiteSpace(request.ClientUID))
                {
                    client = AuthServiceFactory.Instance.GetService<IClientService>().GetByClientUID(request.ClientUID);
                }
                else
                {
                    client = AuthServiceFactory.Instance.GetService<IClientService>().GetByKey(request.ClientIdentifier);
                }

                if (client.HasValue && client.Status == 0)
                {
                    return Ok(client);
                }
                else
                {
                    throw new ApiCustomException("invalid_client");
                }
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
