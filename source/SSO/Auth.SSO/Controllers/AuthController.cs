using DotFramework.Auth.Model;
using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.SSO.Helpers;
using DotFramework.Auth.SSO.Managers;
using DotFramework.Auth.SSO.Models;
using DotFramework.Auth.SSO.Providers;
using DotFramework.Auth.SSO.Results;
using DotFramework.Auth.SSO.ViewModels;
using DotFramework.Core;
using DotFramework.Core.Configuration;
using DotFramework.Core.Serialization;
using DotFramework.Infra;
using DotFramework.Infra.Model;
using DotFramework.Infra.Security;
using DotFramework.Infra.Security.Model;
using DotFramework.Infra.Web.API;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace DotFramework.Auth.SSO.Controllers
{
    [Authorize]
    [RoutePrefix("Auth")]
    public class AuthController : ApiController
    {
        #region Variables

        private const string LocalLoginProvider = "Local";

        #endregion

        #region Constructors

        public AuthController()
        {
        }

        public AuthController(ApplicationUserManager userManager, ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        #endregion

        #region Properties

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationTokenRemoveProvider TokenRemoveProvider
        {
            get
            {
                return Startup.OAuthOptions.RefreshTokenProvider as IAuthenticationTokenRemoveProvider;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        private IAuthenticationManager AuthenticationManager
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IOwinRequest OwinRequest
        {
            get { return Request.GetOwinContext().Request; }
        }

        #endregion

        #region APIs

        [HttpPost]
        [Route("VerifyToken")]
        public async Task<IHttpActionResult> VerifyToken(VerifyTokenRequestModel request)
        {
            try
            {
                var authentication_result = await OwinRequest.Context.Authentication.AuthenticateAsync(Startup.OAuthOptions.AuthenticationType);

                var access_token = ActionContext.Request.Headers.Authorization.Parameter;
                var app_identifier = authentication_result.Properties.Dictionary.TryGetValue("app_identifier");

                ValidateAppSecretProof(request?.AppSecretProof, access_token, app_identifier);

                VerifyTokenResponseModel response = new VerifyTokenResponseModel
                {
                    UserName = authentication_result.Identity.GetUserName(),
                    GroupToken = authentication_result.Properties.Dictionary.TryGetValue("group_token"),
                    Email = authentication_result.Properties.Dictionary.TryGetValue("email"),
                    FullName = authentication_result.Properties.Dictionary.TryGetValue("fullName"),
                    FirstName = authentication_result.Properties.Dictionary.TryGetValue("firstName"),
                    LastName = authentication_result.Properties.Dictionary.TryGetValue("lastName"),
                    IssuedAt = authentication_result.Properties.IssuedUtc.Value.DateTime,
                    ExpiresIn = (int)(authentication_result.Properties.ExpiresUtc.Value - DateTime.UtcNow).TotalSeconds,
                    ExpiresAt = authentication_result.Properties.ExpiresUtc.Value.DateTime,
                    ExternalUserID = authentication_result.Properties.Dictionary.TryGetValue("externalUserID"),
                    ProfilePicture = authentication_result.Properties.Dictionary.TryGetValue("profilePicture"),
                    FirstTimeLogin = Convert.ToBoolean(authentication_result.Properties.Dictionary.TryGetValue("firstTimeLogin")),
                };

                if (!String.IsNullOrEmpty(authentication_result.Properties.Dictionary.TryGetValue("friends")))
                {
                    response.Friends = JsonSerializerHelper.Deserialize<List<String>>(authentication_result.Properties.Dictionary["friends"]);
                }

                if (authentication_result.Identity.Claims.Count(c => c.Type == ClaimTypes.Role) != 0)
                {
                    var roles = authentication_result.Identity.Claims.Where(c => c.Type == ClaimTypes.Role);
                    response.Roles = roles.Select(x => x.Value.Split(':')[1]).ToList();
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ClientEncryptionToken")]
        [AllowAnonymous]
        public IHttpActionResult GenerateClientEncryptionToken(GenerateClientEncryptionTokenRequest request)
        {
            try
            {
                if (Startup.TokenProvider == null)
                {
                    throw new NotImplementedException();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var client = AuthServiceFactory.Instance.GetService<IClientService>().GetByClientUID(request.ClientID);

                if (client.HasValue && client.Status < 200)
                {
                    GenerateClientEncryptionTokenResponse response = new GenerateClientEncryptionTokenResponse
                    {
                        Token = Startup.TokenProvider.Generate(TotpTokenPurpose.Encryption, client.SecurityStamp, client.ClientID.ToString())
                    };

                    return Ok(response);
                }
                else
                {
                    throw new ApiCustomException("invalid_clientId");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ActivateClient")]
        [AllowAnonymous]
        public IHttpActionResult ActivateClient(ActivateClientRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var client = AuthServiceFactory.Instance.GetService<IClientService>().GetByClientUIDAndSecret(request.TemporaryClientID, request.TemporaryClientSecret ?? "");

                if (client.HasValue)
                {
                    if (client.Status == 255)
                    {
                        ActivateClientResponse response = new ActivateClientResponse
                        {
                            ClientID = Guid.NewGuid().ToString().ToLower().Replace("-", ""),
                            ClientSecret = HashHelper.GetRandomAlphanumericString(32)
                        };

                        client.ClientUID = response.ClientID;
                        client.ClientSecret = response.ClientSecret;
                        client.SecurityStamp = Guid.NewGuid().ToString().ToUpper();
                        client.Status = 0;

                        string deviceIdentifier = request.DeviceIdentifier;

                        if (!String.IsNullOrWhiteSpace(deviceIdentifier))
                        {
                            var device = AuthServiceFactory.Instance.GetService<IDeviceService>().GetByKeySimple(deviceIdentifier);

                            if (device.HasValue)
                            {
                                var clientDevice = AuthServiceFactory.Instance.GetService<IClientDeviceService>().GetByKeySimple(device.DeviceID, client.ClientID);

                                if (clientDevice.HasValue)
                                {
                                    if (clientDevice.Client.ClientID != client.ClientID)
                                    {
                                        throw new ApiCustomException("Device is already activated.");
                                    }
                                }
                                else
                                {
                                    AuthServiceFactory.Instance.GetService<IClientDeviceService>().Add(new ClientDevice
                                    {
                                        Client = client,
                                        Device = device
                                    });
                                }
                            }
                            else
                            {
                                AuthServiceFactory.Instance.GetService<IClientDeviceService>().AddWithDetail(new ClientDevice
                                {
                                    Client = client,
                                    Device = new Device
                                    {
                                        DeviceUID = Guid.NewGuid(),
                                        DeviceIdentifier = deviceIdentifier,
                                        State = ObjectState.Added
                                    },
                                    State = ObjectState.Added
                                });
                            }
                        }
                        else
                        {
                            throw new ApiCustomException("Device Identifier is not provided.");
                        }

                        AuthServiceFactory.Instance.GetService<IClientService>().Edit(client);

                        return Ok(response);
                    }
                    else
                    {
                        throw new ApiCustomException("Client is already activated.");
                    }
                }
                else
                {
                    throw new ApiCustomException("invalid_clientId");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("VerifyClientToken")]
        public async Task<IHttpActionResult> VerifyClientToken()
        {
            try
            {
                var authentication_result = await OwinRequest.Context.Authentication.AuthenticateAsync(Startup.OAuthOptions.AuthenticationType);

                VerifyClientTokenResponseModel response = new VerifyClientTokenResponseModel
                {
                    ClientIdentifier = authentication_result.Properties.Dictionary.TryGetValue("client_identifier"),
                    ClientName = authentication_result.Properties.Dictionary.TryGetValue("client_name"),
                    IssuedAt = authentication_result.Properties.IssuedUtc.Value.DateTime,
                    ExpiresIn = (int)(authentication_result.Properties.ExpiresUtc.Value - DateTime.UtcNow).TotalSeconds,
                    ExpiresAt = authentication_result.Properties.ExpiresUtc.Value.DateTime,
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        [Route("Logout")]
        public async Task<IHttpActionResult> Logout()
        {
            try
            {
                if (TokenRemoveProvider != null)
                {
                    var authentication_result = await OwinRequest.Context.Authentication.AuthenticateAsync(Startup.OAuthOptions.AuthenticationType);
                    await TokenRemoveProvider.RemoveAsync(User.Identity.GetUserId<Int32>(), authentication_result.Properties.Dictionary.TryGetValue("group_token"));
                }

                AuthenticationManager.SignOut(CookieAuthenticationDefaults.AuthenticationType);

                return Ok<Boolean>(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("ForceLogout")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ForceLogout(ForceLogoutRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                if (TokenRemoveProvider != null)
                {
                    await TokenRemoveProvider.RemoveAsync(request.GroupToken);
                }

                return Ok<Boolean>(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId<Int32>(), model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId<Int32>(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        [Route("UpdateUserInfo")]
        public async Task<IHttpActionResult> UpdateUserInfo(UpdateUserInfoRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userID = User.Identity.GetUserId<Int32>();
            var user = await UserManager.FindByIdAsync(userID);

            bool emailIsChanged = false;

            if (!String.IsNullOrEmpty(request.Email) && user.Email.ToLower() != request.Email.ToLower())
            {
                if (user.Email.ToLower() == user.UserName.ToLower())
                {
                    return BadRequest("Email cannot be updated for this user.");
                }
                else
                {
                    emailIsChanged = true;
                }
            }

            var claims = await UserManager.GetClaimsAsync(userID);

            user.FullName = $"{request.FirstName} {request.LastName}";

            if (emailIsChanged)
            {
                user.Email = request.Email;
            }

            if (!String.IsNullOrEmpty(request.PhoneNumber))
            {
                user.PhoneNumber = request.PhoneNumber;
            }

            IdentityResult result = await UserManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            else
            {
                await ReAddClaim(claims, user.UserID, ClaimTypes.GivenName, request.FirstName);
                await ReAddClaim(claims, user.UserID, ClaimTypes.Surname, request.LastName);
                await ReAddClaim(claims, user.UserID, CustomClaimTypes.DisplayName, user.FullName);

                if (emailIsChanged)
                {
                    await ReAddClaim(claims, user.UserID, ClaimTypes.Email, request.Email);
                }
            }

            return Ok();
        }

        private async Task ReAddClaim(IList<Claim> claims, int userId, string claimType, string claimValue)
        {
            var claim = claims.FirstOrDefault(c => c.Type == claimType);

            if (claim != null)
            {
                await UserManager.RemoveClaimAsync(userId, claim);
            }

            await UserManager.AddClaimAsync(userId, new Claim(claimType, claimValue));
        }

        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await AuthenticationManager.GetExternalLoginInfoAsync();

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null && ticket.Properties.ExpiresUtc.HasValue && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId<Int32>(), new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId<Int32>());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId<Int32>(), new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public IHttpActionResult GetExternalLogin(string client_id, string provider, string redirect_uri, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            string externalAccessToken = AuthenticationManager.User.Claims.FirstOrDefault(c => c.Type == "ExternalAccessToken").Value;

            string redirectUri = String.Format("{0}#external_access_token={1}&provider={2}", redirect_uri,
                                                                                             externalAccessToken,
                                                                                             provider);

            return Redirect(redirectUri);
        }

        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = AuthenticationManager.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };

                logins.Add(login);
            }

            return logins;
        }

        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string userName = model.UserName ?? model.Email;

            var user = new User() { UserName = userName, Email = model.Email, FullName = model.FullName, PhoneNumber = model.PhoneNumber };

            user.AccountPolicy = AuthServiceFactory.Instance.GetService<IAccountPolicyService>().GetByKeySimple("Default Policy");

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddClaimAsync(user.UserID, new Claim(ClaimTypes.NameIdentifier, user.UserName));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddClaimAsync(user.UserID, new Claim(ClaimTypes.Name, model.UserName));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddClaimAsync(user.UserID, new Claim(ClaimTypes.Email, model.Email));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddClaimAsync(user.UserID, new Claim(ClaimTypes.GivenName, model.FirstName));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddClaimAsync(user.UserID, new Claim(ClaimTypes.Surname, model.LastName));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddClaimAsync(user.UserID, new Claim(CustomClaimTypes.DisplayName, model.FullName));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            if (!String.IsNullOrEmpty(model.InitialRole))
            {
                result = await UserManager.AddToRoleAsync(user.UserID, model.InitialRole);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
            }

            return Ok();
        }

        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var verifiedAccessToken = await ExternalAuthenticationManager.Instance.GetHandler(model.ApplicationCode, model.Provider, Request.GetOwinContext()).VerifyToken(model.ExternalAccessToken);

            User user = await UserManager.FindAsync(new UserLoginInfo(model.Provider, verifiedAccessToken.ProviderKey));

            if (user == null || !user.HasValue)
            {
                user = new User() { UserName = verifiedAccessToken.UserID, Email = model.Email, FullName = model.FullName };

                IdentityResult result = await UserManager.CreateAsync(user);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                var info = new ExternalLoginInfo()
                {
                    DefaultUserName = verifiedAccessToken.UserID,
                    Login = new UserLoginInfo(model.Provider, verifiedAccessToken.UserID)
                };

                result = await UserManager.AddLoginAsync(user.Id, info.Login);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
            }

            return Ok();
        }

        [HttpPost]
        [Route("GetAuthorizedRoles")]
        public IHttpActionResult GetAuthorizedRoles(GetAuthorizedRolesBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                AuthorizedRolesResponseModel result = UserManager.GetAuthorizedRoles(model);
                return Ok<AuthorizedRolesResponseModel>(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetAuthorizationStatus")]
        public IHttpActionResult GetAuthorizationStatus(GetAuthorizedRolesBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                bool isAuthorized = false;
                AuthorizedRolesResponseModel authorizedRoles = UserManager.GetAuthorizedRoles(model);

                if (authorizedRoles.HasError)
                {
                    isAuthorized = false;
                }
                else if (!authorizedRoles.NeedAuthorization)
                {
                    isAuthorized = true;
                }
                else
                {
                    var userRoles = this.AuthenticationManager.User.Claims.Where(c => c.Type == System.Security.Claims.ClaimTypes.Role);

                    if (authorizedRoles.Roles != null && authorizedRoles.Roles.Count != 0 && userRoles.Count() != 0)
                    {
                        isAuthorized = authorizedRoles.Roles.Any(r => userRoles.Select(ur => ur.Value).Contains(r));

                        if (isAuthorized)
                        {
                            //Task.Run(() =>
                            //{
                            //    try
                            //    {
                            //        //var userSession = UserManager.GetLatestSession(User.Identity.GetUserId<Int32>(), model.ApplicationCode);

                            //        switch (model.ObjectType)
                            //        {
                            //            case AuthObjectType.View:
                            //                AuthServiceFactory.Instance.GetService<IViewVisitService>().Add(new ViewVisit
                            //                {
                            //                    User = new User { UserID = User.Identity.GetUserId<Int32>() },
                            //                    ApplicationHierarchy = AuthServiceFactory.Instance.GetService<IApplicationHierarchyService>().GetByActionName(model.ApplicationCode, model.ControllerName, model.ActionName),
                            //                    //SessionID = userSession.UserSessionID
                            //                });
                            //                break;
                            //            case AuthObjectType.Action:
                            //                break;
                            //            case AuthObjectType.Service:
                            //                break;
                            //            default:
                            //                break;
                            //        }
                            //    }
                            //    catch (ApiCustomException)
                            //    {

                            //    }
                            //    catch (Exception)
                            //    {

                            //    }
                            //});

                            switch (model.ObjectType)
                            {
                                case AuthObjectType.View:
                                    AuthServiceFactory.Instance.GetService<IViewVisitService>().Add(new ViewVisit
                                    {
                                        User = new User { UserID = User.Identity.GetUserId<Int32>() },
                                        ApplicationHierarchy = AuthServiceFactory.Instance.GetService<IApplicationHierarchyService>().GetByActionName(model.ApplicationCode, model.ControllerName, model.ActionName),
                                        //SessionID = userSession.UserSessionID
                                    });
                                    break;
                                case AuthObjectType.Action:
                                    break;
                                case AuthObjectType.Service:
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }

                return Ok<Boolean>(isAuthorized);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("CreateSession")]
        public IHttpActionResult CreateSession(CreateSessionBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var applications = AuthServiceFactory.Instance.GetService<ISubApplicationService>().GetByFilter(a => a.Code == model.ApplicationCode);

                if (applications.Count == 0)
                {
                    throw new ApiCustomException(String.Format("Application '{0}' is not registered.", model.ApplicationCode));
                }

                UserSession userSession = new UserSession
                {
                    IsApplicationSession = model.IsApplicationSession,
                    SubApplication = new SubApplication { SubApplicationID = applications.First().SubApplicationID },
                    IP = model.IP,
                    StartTime = DateTime.Now,
                    BrowserDetail = String.IsNullOrEmpty(model.BrowserDetail) ? null : model.BrowserDetail,
                    OperatingSystemDetail = String.IsNullOrEmpty(model.OperatingSystemDetail) ? null : model.OperatingSystemDetail,
                    UrlReferrer = String.IsNullOrEmpty(model.UrlReferrer) ? null : model.UrlReferrer
                };

                var result = AuthServiceFactory.Instance.GetService<IUserSessionService>().Add(userSession);
                return Ok(result.ReplyModel.UserSessionID);
            }
            catch (Exception ex)
            {
                ApiExceptionHandler.Instance.HandleException(ref ex, this.ControllerContext, this.ActionContext);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetApplicationHierarchies")]
        public IHttpActionResult GetApplicationHierarchies(GetApplicationHierarchiesBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                //var applications = AuthServiceFactory.Instance.GetService<ISubApplicationService>().GetByFilter(a => a.Code == model.ClientID);

                //if (applications.Count == 0)
                //{
                //    throw new ApiCustomException(String.Format("Application '{0}' is not registered.", model.ClientID));
                //}

                var response = new List<ApplicationHierarchiesResponseModel>();

                //AuthServiceFactory.Instance.GetService<IApplicationHierarchyService>().GetByApplication(applications.First().Application.ApplicationID).ForEach(ah =>
                //{
                //    response.Add(new ApplicationHierarchiesResponseModel
                //    {
                //        HIDString = ah.HIDString,
                //        Code = ah.Code,
                //        Name = ah.Name,
                //        Description = ah.Description,
                //        Type = ah.Type,
                //        Path = ah.Path,
                //        ControllerName = ah.ControllerName,
                //        ActionName = ah.ActionName,
                //        RouteValues = ah.RouteValues,
                //        IconType = ah.IconType,
                //        Icon = ah.Icon,
                //        NeedAuthorization = ah.NeedAuthorization
                //    });
                //});

                return Ok<List<ApplicationHierarchiesResponseModel>>(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetRecentApplicationHierarchies")]
        public IHttpActionResult GetRecentApplicationHierarchies(GetApplicationHierarchiesBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                //var applications = AuthServiceFactory.Instance.GetService<ISubApplicationService>().GetByFilter(a => a.Code == model.ClientID);

                //if (applications.Count == 0)
                //{
                //    throw new ApiCustomException(String.Format("Application '{0}' is not registered.", model.ClientID));
                //}

                //var response = new List<ApplicationHierarchiesResponseModel>();

                //var query = new DynamicQuery
                //{
                //    Filter = FilterBase.CreateFilter<ViewVisit>(v => v.ApplicationHierarchy.Application.ApplicationID == applications.First().Application.ApplicationID &&
                //                                                     v.User.UserID == User.Identity.GetUserId<Int32>() &&
                //                                                     (v.ApplicationHierarchy.ControllerName != "Home" ||
                //                                                     v.ApplicationHierarchy.ActionName != "Index")),
                //    OrderBy = new OrderByList().Add<ViewVisit>(v => v.ViewVisitID, OrderByType.Descending)
                //};

                //var viewVisits = AuthServiceFactory.Instance.GetService<IViewVisitService>().DynamicGet(query, 0, 20);

                //if (viewVisits.Count != 0)
                //{
                //    viewVisits.Select(v => v.ApplicationHierarchy).DistinctBy(a => a.ApplicationHierarchyID).Take(6).ForEach(ah =>
                //    {
                //        response.Add(new ApplicationHierarchiesResponseModel
                //        {
                //            HIDString = ah.HIDString,
                //            Code = ah.Code,
                //            Name = ah.Name,
                //            Description = ah.Description,
                //            Type = ah.Type,
                //            Path = ah.Path,
                //            ControllerName = ah.ControllerName,
                //            ActionName = ah.ActionName,
                //            RouteValues = ah.RouteValues,
                //            IconType = ah.IconType,
                //            Icon = ah.Icon,
                //            NeedAuthorization = ah.NeedAuthorization
                //        });
                //    });
                //}

                //return Ok<List<ApplicationHierarchiesResponseModel>>(response);

                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ForgetPassword")]
        public async Task<IHttpActionResult> ForgetPassword(ForgetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = UserManager.FindByName(model.UserName);

                if (user == null || !user.HasValue)
                {
                    throw new Exception("There is no user registered with this username.");
                }

                var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var resetUrl = String.Format("{0}?token={1}", model.RedirectUrl, HttpUtility.UrlEncode(token));

                var keyValuePairs = JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(model.TemplateKeyValues);

                await UserManager.SendForgetPasswordEmailAsync(user, resetUrl, keyValuePairs);

                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = UserManager.FindByName(model.UserName);

                if (user == null || !user.HasValue)
                {
                    throw new Exception("There is no user registered with this email.");
                }

                var result = await UserManager.ResetPasswordAsync(user.Id, model.Token, model.NewPassword);

                if (user.EmailConfirmed == false)
                {
                    UserManager.RemoveValueFromContext(OwinRequest.Context, "CurrentUser");

                    user = UserManager.FindById(user.Id);
                    await UserManager.SetEmailConfirmedAsync(user, true);
                }

                if (result.Succeeded)
                {
                    return Ok(true);
                }
                else
                {
                    return GetErrorResult(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Activate")]
        public async Task<IHttpActionResult> Activate(ActivateBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = await UserManager.FindByNameAsync(model.UserName);

                if (user == null || !user.HasValue)
                {
                    user = await UserManager.FindByEmailAsync(model.Email ?? model.UserName);
                }

                if (user == null || !user.HasValue)
                {
                    if (AppSettingsManager.Instance.Get<Boolean>("RegisterOnActivationIfNotExists"))
                    {
                        user = new User()
                        {
                            UserName = model.UserName,
                            Email = String.IsNullOrWhiteSpace(model.Email) ? model.UserName : model.Email,
                            FullName = String.IsNullOrWhiteSpace(model.FullName) ? model.UserName : model.FullName,
                        };

                        user.AccountPolicy = AuthServiceFactory.Instance.GetService<IAccountPolicyService>().GetByKeySimple("Default Policy");

                        string password = System.Web.Security.Membership.GeneratePassword(20, 5);
                        IdentityResult createResult = await UserManager.CreateAsync(user, password);

                        if (createResult.Succeeded)
                        {
                            user = await UserManager.FindByNameAsync(model.UserName);
                        }
                        else
                        {
                            return GetErrorResult(createResult);
                        }
                    }
                    else
                    {
                        throw new Exception("User not found.");
                    }
                }

                var token = await UserManager.GenerateTwoFactorTokenAsync(user.Id, PasswordlessLoginEmailTokenProvider.Purpose);
                await UserManager.NotifyTwoFactorTokenAsync(user.UserID, PasswordlessLoginEmailTokenProvider.Purpose, token);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ConfirmEmail")]
        public async Task<IHttpActionResult> ConfirmEmail(ConfirmEmailBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = UserManager.FindByName(model.Email);

                if (user == null || !user.HasValue)
                {
                    throw new Exception("There is no user registered with this email.");
                }

                if (user.EmailConfirmed)
                {
                    throw new Exception("This email is already confirmed.");
                }

                string token = await UserManager.GenerateEmailConfirmationTokenAsync(user.UserID);

                string confirmationUrl;

                if (String.IsNullOrWhiteSpace(model.ConfirmEmailUrl))
                {
                    confirmationUrl =
                        String.Format("{0}/Auth/ConfirmEmail?email={1}&token={2}&redirectUrl={3}",
                            Request.RequestUri.GetLeftPart(UriPartial.Authority),
                            HttpUtility.UrlEncode(model.Email),
                            HttpUtility.UrlEncode(token),
                            HttpUtility.UrlEncode(model.RedirectUrl));
                }
                else
                {
                    confirmationUrl =
                        model.ConfirmEmailUrl
                            .Replace("[email]", HttpUtility.UrlEncode(model.Email))
                            .Replace("[token]", HttpUtility.UrlEncode(token))
                            .Replace("[redirectUrl]", HttpUtility.UrlEncode(model.RedirectUrl));
                }

                await UserManager.SendEmailConfirmationAsync(user, confirmationUrl, model.TemplateDictionary);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("ConfirmEmailCallback")]
        public async Task<IHttpActionResult> ConfirmEmailCallback([FromUri] ConfirmEmailCallbackBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = UserManager.FindByName(model.Email);

                if (user == null || !user.HasValue)
                {
                    throw new Exception("There is no user registered with this email.");
                }

                IdentityResult result = await UserManager.ConfirmEmailAsync(user.UserID, model.Token);

                if (result.Succeeded)
                {
                    return Redirect(model.RedirectUrl);
                }
                else
                {
                    return GetErrorResult(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ConfirmEmailCallback")]
        public async Task<IHttpActionResult> PostConfirmEmailCallback(ConfirmEmailCallbackBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = UserManager.FindByName(model.Email);

                if (user == null || !user.HasValue)
                {
                    throw new Exception("There is no user registered with this email.");
                }

                IdentityResult result = await UserManager.ConfirmEmailAsync(user.UserID, model.Token);

                if (result.Succeeded)
                {
                    return Ok(new RedirectContext(model.RedirectUrl));
                }
                else
                {
                    return GetErrorResult(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region Overrided Methods

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #endregion

        #region Helpers

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        var errorParts = error.Split(new string[] { ". " }, StringSplitOptions.None);

                        if (errorParts.Length == 1)
                        {
                            ModelState.AddModelError("", error);
                        }
                        else
                        {
                            foreach (var errorPart in errorParts)
                            {
                                ModelState.AddModelError("", errorPart.Trim('.') + ".");
                            }
                        }
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private void ValidateAppSecretProof(string appSecretProof, string access_token, string app_identifier)
        {
            if (!String.IsNullOrEmpty(app_identifier))
            {
                if (String.IsNullOrEmpty(appSecretProof))
                {
                    throw new ApiCustomException("invalid_grant");
                }

                var application = AuthServiceFactory.Instance.GetService<IApplicationService>().GetByKeySimple(app_identifier);

                if (!application.HasValue)
                {
                    throw new ApiCustomException("invalid_grant");
                }

                var appSecretProof_local = EncryptUtility.GetHash(access_token, application.AppSecret);

                if (appSecretProof_local != appSecretProof)
                {
                    throw new ApiCustomException("invalid_grant");
                }
            }
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer) || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}