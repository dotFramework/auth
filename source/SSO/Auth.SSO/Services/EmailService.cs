using DotFramework.Core.Configuration;
using Microsoft.AspNet.Identity;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace DotFramework.Auth.SSO.Services
{
    public class EmailService : IIdentityMessageService
    {
        private readonly string _senderName = AppSettingsManager.Instance.Get("EmailSenderName");
        private readonly string _senderEmail = AppSettingsManager.Instance.Get("EmailSenderAddress");

        private readonly string _smtpHost = AppSettingsManager.Instance.Get("SMTPHost");
        private readonly int _smtpPort = AppSettingsManager.Instance.Get<Int32>("SMTPPort");
        private readonly bool _smtpEnableSsl = AppSettingsManager.Instance.Get<Boolean>("SMTPSSLEnabled");
        private readonly string _smtpTargetName = AppSettingsManager.Instance.Get("SMTPTargetName");
        private readonly NetworkCredential _credentials = new NetworkCredential(AppSettingsManager.Instance.Get("SMTPCredentialUserName"), AppSettingsManager.Instance.Get("SMTPCredentialPassword"));

        public async Task SendAsync(IdentityMessage message)
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(_senderEmail, _senderName);
            mail.Sender = new MailAddress(_senderEmail, _senderName);

            mail.To.Add(new MailAddress(message.Destination));
            mail.Subject = message.Subject;

            mail.Body = message.Body;
            mail.IsBodyHtml = true;

            using (SmtpClient client = new SmtpClient(_smtpHost, _smtpPort))
            {
                client.EnableSsl = _smtpEnableSsl;

                if (!String.IsNullOrWhiteSpace(_smtpTargetName))
                {
                    client.TargetName = _smtpTargetName;
                }

                if (_credentials != null)
                {
                    client.Credentials = _credentials;
                }

                await client.SendMailAsync(mail);
            }
        }
    }
}