﻿using DotFramework.Core;
using DotFramework.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Auth.SSO.Services
{
    internal class VerifiedClinetsService : SingletonProvider<VerifiedClinetsService>
    {
        private readonly string[] _verifiedClients;

        private VerifiedClinetsService()
        {
            _verifiedClients = GetVerifiedClient();
        }
        private string[] GetVerifiedClient()
        {
            var verifiedClientIds =
                AppSettingsManager.Instance.Get<string>("VerifiedClientIDs",
                AppSettingsManager.Instance.Get<string>("ExternalClientIDs"));

            if (!string.IsNullOrEmpty(verifiedClientIds))
            {
                return verifiedClientIds.Split(',');
            }

            return null;
        }

        public bool IsVerifiedClient(string clientID)
        {
            return _verifiedClients != null && _verifiedClients.Contains(clientID);
        }

    }
}
