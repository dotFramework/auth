﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DotFramework.Auth.SSO
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{action}/{id}",
                defaults: new { controller = "Index", action = "KeepAlive", id = UrlParameter.Optional }
            );
        }
    }
}
