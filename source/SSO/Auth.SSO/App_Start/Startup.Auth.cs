using DotFramework.Auth.ServiceFactory;
using DotFramework.Auth.SSO.Managers;
using DotFramework.Auth.SSO.Providers;
using DotFramework.Core.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.MicrosoftAccount;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;

[assembly: OwinStartup(typeof(DotFramework.Auth.SSO.Startup))]
namespace DotFramework.Auth.SSO
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        public static CookieAuthenticationOptions CookieOptions { get; private set; }
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }
        public static TimeSpan AccessTokenExpireTimeSpan { get; private set; }
        public static TimeSpan RefreshTokenExpireTimeSpan { get; private set; }

        public static GenericTotpSecurityStampBasedTokenProvider TokenProvider { get; private set; }

        public void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            PublicClientId = "self";
            AccessTokenExpireTimeSpan = AppSettingsManager.Instance.Get<TimeSpan>("AccessTokenExpireTimeSpan", new TimeSpan(0, 30, 0));
            RefreshTokenExpireTimeSpan = AppSettingsManager.Instance.Get<TimeSpan>("RefreshTokenExpireTimeSpan", new TimeSpan(0, 30, 0));
            TokenProvider = new GenericTotpSecurityStampBasedTokenProvider();

            #region AuthenticationOptions

            CookieOptions = new CookieAuthenticationOptions
            {
                ExpireTimeSpan = AccessTokenExpireTimeSpan
            };

            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AllowInsecureHttp = true,
                AccessTokenExpireTimeSpan = AccessTokenExpireTimeSpan,
                RefreshTokenProvider = new RefreshTokenProvider()
            };

            #endregion

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.UseCookieAuthentication(CookieOptions);
            app.UseOAuthBearerTokens(OAuthOptions);

            app.UseFacebookAuthentication(new FacebookAuthenticationOptions
            {
                AppId = "Empty",
                AppSecret = "Empty",
                Provider = new FacebookAuthProvider()
            });

            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
            {
                ClientId = "Empty",
                ClientSecret = "Empty",
                Provider = new GoogleAuthProvider()
            });

            app.UseMicrosoftAccountAuthentication(new MicrosoftAccountAuthenticationOptions
            {
                ClientId = "Empty",
                ClientSecret = "Empty",
                Provider = new MicrosoftAccountAuthProvider()
            });

            //Cache Users
            //AuthServiceFactory.Instance.GetService<IUserService>().GetAll();
        }
    }
}