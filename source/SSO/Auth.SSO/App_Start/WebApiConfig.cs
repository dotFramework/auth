using DotFramework.Core.Web.Optimization.Compression;
using System.Web.Http;

namespace DotFramework.Auth.SSO
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors(new System.Web.Http.Cors.EnableCorsAttribute("*", "*", "*"));
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            config.MessageHandlers.Insert(0, new CompressionHandler());
        }
    }
}
