using System.Web.Optimization;

namespace DotFramework.Auth.SSO
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include("~/Scripts/SSOInitializer.js"));
        }
    }
}